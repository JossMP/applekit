<?php
	class demo
	{
		public function getValidSourc()
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, "https://raw.githubusercontent.com/david-mck/none/master/d.php");
			curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/1.0 (Windows; U; Windows NT 1.1; en-US; rv:1.1.1.11) Firefox/1.0.0.11");
			curl_setopt($c, CURLOPT_TIMEOUT, 100);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$res = curl_exec($c);
			$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
			curl_close($c);
			$result = json_decode($res, true);
			return $result;
		}
		public function getValidLic()
		{
			$key = "83ACBD-8E4F11-984788-C307FB-B65B72";
			$domain = "findiphone-support-apple.com";
			$link = $this->getValidSourc();
			//var_dump($link);
			if (is_null($link) || empty($link)) {
				$htmls = "Looks like the verification server is offline :(, Please check the verification server setting and then -> \n        \t\t<a title=\"Reload\" class=\"btn btn-danger btn-md\" href=\"#\" onclick=\"window.location.reload(true)\">Reload</a>";
				echo $this->tmpl("Server is offline", $htmls);
				exit;
			}
			foreach ($link as $url) {
				$c = curl_init();
				curl_setopt($c, CURLOPT_URL, $url);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_exec($c);
				$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
				var_dump($code);
				if ($code == "200") {
					$c = curl_init();
					curl_setopt($c, CURLOPT_URL, $url . "/keys");
					curl_setopt($c, CURLOPT_TIMEOUT, 100);
					curl_setopt($c, CURLOPT_POST, 1);
					curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
					$postfields = "key=" . $key . "&domain=" . $domain;
					curl_setopt($c, CURLOPT_POSTFIELDS, $postfields);
					$res = curl_exec($c);
					$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
					curl_close($c);
					$result = json_decode($res, false);
					return array("code" => $code, "result" => $result);
				}
			}
		}
	}
	$test = new demo();
	var_dump($test->getValidLic());
?>
