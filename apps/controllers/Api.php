<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class Api extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function Credit($id = NULL)
    {
        $key = $this->global_data["master_api_key"];
        $params = array("api_key" => $key);
        $cl = curl_init("http://nemoz.net/API/credit");
        curl_setopt_array($cl, array(CURLOPT_TIMEOUT => 180, CURLOPT_CONNECTTIMEOUT => 180, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => false, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $params));
        $req = curl_exec($cl);
        if ($id == NULL) {
            AJAX::error(json_decode($req));
        } else {
            $msg = json_decode($req);
            $data = array("credit" => $msg->msg->credit);
            $this->session->set_userdata($data);
            AJAX::success($msg, 200);
        }
    }
}

?>