<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Smtp extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Smtp_m", "smtp");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Extra Smtp Server", "smtp" => $this->smtp->getAll(), "docs" => "http://nemoze.net");
        $this->blade->view("backend.setting.smtp", $data);
    }
    public function edit($id = NULL)
    {
        is_null($id);
        is_null($id) ? redirect(site_url("admin/smtp"), "auto") : false;
        $data = array("title" => "Extra Smtp Server", "smtp" => $this->smtp->getById($id));
        $this->blade->view("backend.setting.smtpedit", $data);
    }
    public function addNew()
    {
        $this->form_validation->set_rules("name", "Short Name", "trim|required|min_length[2]|max_length[10]");
        $this->form_validation->set_rules("hosts", "Smtp Server Host", "trim|required");
        $this->form_validation->set_rules("username", "Smtp Server Username", "trim|required");
        $this->form_validation->set_rules("password", "Smtp Server Password", "trim|required");
        $this->form_validation->set_rules("port", "Smtp Server Port", "trim|required|is_natural_no_zero");
        $this->form_validation->set_rules("type", "Smtp Server Encrypted", "trim|required");
        if ($this->form_validation->run() == true) {
            $sdata = array("name" => $this->input->post("name", true), "host" => $this->input->post("hosts", true), "username" => $this->input->post("username", true), "password" => $this->input->post("password", true), "port" => $this->input->post("port", true), "type" => $this->input->post("type", true));
            $addsmtp = $this->smtp->insert($sdata);
            if ($addsmtp) {
                $this->session->set_flashdata("success", "Done adding new server <strong>" . $sdata["name"] . "</strong> Successfully to your database.");
                redirect("admin/smtp/");
            } else {
                $this->session->set_flashdata("error", "Something goes wrong please try again later.");
                redirect("admin/smtp/");
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("", "<br/>"));
            redirect("admin/smtp/");
        }
    }
    public function update()
    {
        $this->form_validation->set_rules("name", "Short Name", "trim|required|min_length[2]|max_length[10]");
        $this->form_validation->set_rules("hosts", "Smtp Server Host", "trim|required");
        $this->form_validation->set_rules("username", "Smtp Server Username", "trim|required");
        $this->form_validation->set_rules("password", "Smtp Server Password", "trim|required");
        $this->form_validation->set_rules("port", "Smtp Server Port", "trim|required|is_natural_no_zero");
        $this->form_validation->set_rules("type", "Smtp Server Encrypted", "trim|required");
        if ($this->form_validation->run() == true) {
            $sdata = array("id" => $this->input->post("id", true), "name" => $this->input->post("name", true), "host" => $this->input->post("hosts", true), "username" => $this->input->post("username", true), "password" => $this->input->post("password", true), "port" => $this->input->post("port", true), "type" => $this->input->post("type", true));
            $addsmtp = $this->smtp->update($sdata["id"], $sdata);
            if ($addsmtp) {
                $this->session->set_flashdata("success", "Done Editing server <strong>" . $sdata["name"] . "</strong> Successfully to your database.");
                redirect("admin/smtp/");
            } else {
                $this->session->set_flashdata("error", "Something goes wrong please try again later.");
                redirect("admin/smtp/");
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("", "<br/>"));
            redirect("admin/smtp/");
        }
    }
    public function delete($id = NULL)
    {
        is_null($id);
        is_null($id) ? redirect(site_url("admin/smtp"), "auto") : false;
        $smtpde = $this->smtp->delete($id);
        if ($smtpde) {
            $this->session->set_flashdata("success", "Smtp Server have been deleted successfully.");
            redirect("admin/smtp");
        } else {
            $this->session->set_flashdata("error", "Something goes wrong please try again later.");
            redirect("admin/smtp");
        }
    }
}

?>
