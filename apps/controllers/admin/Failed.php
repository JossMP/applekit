<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Failed extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Wrong_m", "wrong");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Failed login logs", "fail" => $this->wrong->getAll());
        $this->blade->view("backend.fail.all", $data);
    }
    public function clear()
    {
        if ($this->wrong->cut() == true) {
            redirect(site_url("admin/failed/?success"), "auto");
        } else {
            redirect(site_url("admin/failed/?error"), "auto");
        }
    }
}

?>
