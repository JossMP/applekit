<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Login extends Admin
{
    private $username = false;
    private $password = false;
    private $checkAuth = false;
    public function __construct()
    {
        parent::__construct();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => $this->lang->line("Dashdoard_message"));
        $this->form_validation->set_rules("userName", "username", "required|trim");
        $this->form_validation->set_rules("userPwd", "password", "required|trim");
        $this->username = $this->input->post("userName", true);
        $this->password = $this->input->post("userPwd", true);
        if ($this->form_validation->run() == true) {
            $this->load->model("Admin_m", "admin_m");
            $this->checkAuth = $this->admin_m->auth();
            if ($this->checkAuth == 1) {
                $adminInfo = $this->admin_m->getByUser2($this->username);
                $array = array("logged_admin" => true, "adminName" => $this->username, "adminID" => $adminInfo["id"], "adminEmail" => $adminInfo["userEmail"], "adminFname" => $adminInfo["fName"], "adminReg" => $adminInfo["time"]);
                $this->session->set_userdata($array);
                if ($this->session->userdata("admin_session")) {
                    redirect($this->session->userdata("admin_session"), "auto");
                } else {
                    redirect("admin/dashboard", "auto");
                }
            } else {
                $data["error"] = $this->checkAuth;
                $this->load->view("backend/login", $data);
            }
        } else {
            $this->load->view("backend/login", $data);
        }
    }
}
$l = 56;

?>
