<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Logout extends Admin
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $this->session->unset_userdata("logged_admin");
        redirect("admin", "auto");
    }
}

?>
