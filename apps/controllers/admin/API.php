<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class API extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("ajax", false);
        $this->load->model("Options_m", "opt");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "API for SenderKit", "api" => (object) $this->global_data);
        $this->blade->view("backend.api.index", $data);
    }
    public function save()
    {
        $this->form_validation->set_rules("apikey", "apikey", "required|trim");
        $apikey = $this->input->post("apikey", true);
        if ($this->form_validation->run() == true) {
            $array = array("senderKitAPI" => $apikey);
            $this->opt->modify($array);
            $this->session->set_flashdata("success", "New key have been add :)");
            redirect(ADMINPATH . "api", "auto");
        } else {
            $this->session->set_flashdata("error", "Huston Something goes wrong!");
            redirect(ADMINPATH . "api", "auto");
        }
    }
    public function removalTest()
    {
        $config_key = config_item("removalApi");
        $store_key = $this->global_data["removalApi"];
        if (empty($config_key)) {
            $key = $store_key;
        } else {
            $key = $config_key;
        }
        $this->load->helper("string");
        $randu = random_string("alpha", 10);
        $randp = random_string("unique", 5);
        $para = array("key" => $key, "appleid" => $randu . "@icloud.com", "password" => $randp, "type" => 2);
        $this->load->library("handler", $para);
        if ($this->handler->msg["status"] == true) {
            Ajax::success("success", 200);
        } else {
            Ajax::error($this->handler->msg["msg"]["error"], 200);
        }
    }
    public function fmionoff($imei)
    {
        $imei == NULL ? Ajax::error("You must set an imei to check on", 200) : NULL;
        $this->load->library("handler", false);
        $config_key = config_item("removalApi");
        $store_key = $this->global_data["removalApi"];
        if (empty($config_key)) {
            $key = $store_key;
        } else {
            if (empty($store_key)) {
                Ajax::error("You have to set an ifreeicloud API Key before using this feature!", 200);
            }
            $key = $config_key;
        }
        $para = array("id" => "125", "imei" => $imei, "key" => $key);
        $q = $this->handler->serviec($para);
        if ($q["status"] == true) {
            Ajax::success($q["msg"], 200);
        } else {
            Ajax::error($q["msg"], 200);
        }
    }
    public function devicedetails($imei = NULL)
    {
        $imei == NULL ? Ajax::error("You must set an imei to check on", 200) : NULL;
        $this->load->library("handler", false);
        $config_key = config_item("removalApi");
        $store_key = $this->global_data["removalApi"];
        if (empty($config_key)) {
            $key = $store_key;
        } else {
            if (empty($store_key)) {
                Ajax::error("You have to set an ifreeicloud API Key before using this feature!", 200);
            }
            $key = $config_key;
        }
        $para = array("id" => "0", "imei" => $imei, "key" => $key);
        $q = $this->handler->serviec($para);
        if ($q["status"] == true) {
            $d = explode("Model: ", $q["msg"]);
            Ajax::success($d[1], 200);
        } else {
            Ajax::error($q["msg"], 200);
        }
    }
    public function mail()
    {
    }
}

?>