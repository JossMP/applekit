<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class Checkup extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $this->getRequest("358571073061119", 1);
    }
    private function checkResult($data)
    {
        if (!strpos($data, "GB")) {
            return array("res" => false, "type" => "Service is Down Try again later on", "color" => "Service is Down Try again later on", "size" => "Service is Down Try again later on", "fmi" => "Service is Down Try again later on", "status" => "Service is Down Try again later on", "serial" => "Service is Down Try again later on", "fulldata" => "Service is Down Try again later on");
        }
    }
    private function getCurl($imei, $service = 2)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(CURLOPT_RETURNTRANSFER => true, CURLOPT_FOLLOWLOCATION => true, CURLOPT_ENCODING => "UTF-8", CURLOPT_URL => "http://infocheckup.net/getdata/?user=Osmani778866&service=" . $service . "&imeis=" . $imei));
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }
    private function setInfoFMI($imei)
    {
        $data = $this->getCurl($imei, 2);
        $this->checkResult($data);
        $res = preg_split("/(\\s*<(\\/?p|br)\\s*\\/?>\\s*)+/u", $data, NULL, PREG_SPLIT_NO_EMPTY);
        $model = str_replace("Model: ", "", $res[0]);
        $model = preg_split("/\\s+/", $model);
        $serial = str_replace("Serial Number: ", "", $res[2]);
        $FMI = isset($res[3]) ? str_replace("Find My iPhone : ", "", $res[3]) : NULL;
        $Status = isset($res[4]) ? str_replace("iCloud Status : ", "", $res[4]) : NULL;
        $type = $model[2] == "Plus" ? $model[0] . " " . $model[1] . " " . $model[2] : $model[0] . " " . $model[1];
        if ($model[2] == "Plus") {
            if (!strpos($model[4], "GB")) {
                $color = $model[3] . " " . $model[4];
            } else {
                $color = $model[3];
            }
        } else {
            if (!strpos($model[3], "GB")) {
                $color = $model[2] . " " . $model[3];
            } else {
                $color = $model[2];
            }
        }
        if (!strpos($model[4], "GB")) {
            if (isset($model[5]) && strpos($model[5], "GB")) {
                $size = $model[5];
            } else {
                $size = $model[3];
            }
        } else {
            $size = $model[4];
        }
        $data = array("res" => true, "type" => $type, "color" => $color, "size" => $size, "serial" => $serial, "fulldata" => strip_tags($data));
        $data["fmi"] = !is_null($FMI) ? strip_tags($FMI) : "Unknown";
        $data["status"] = !is_null($Status) ? strip_tags($Status) : "Unknown";
        return $data;
    }
    private function setCheckFMI($imei)
    {
        $data = $this->getCurl($imei, 1);
        $this->checkResult($data);
        return $data;
    }
    public function getRequest($imei, $service = 2)
    {
        switch ($service) {
            case "1":
                $data = $this->setCheckFMI($imei, 1);
                $data ? Ajax::success($data, 200) : Ajax::error("error", 200);
                break;
            default:
                $data = $this->setInfoFMI($imei, 2);
                $data ? Ajax::success($data, 200) : Ajax::error("error", 200);
        }
    }
}

?>