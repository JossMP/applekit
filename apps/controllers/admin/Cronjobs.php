<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class Cronjobs extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Victim_m", "victim");
        $this->load->model("Options_m", "options");
        $this->load->library("ajax", false);
        $this->load->model("Visit_m", "visit");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Cron Jobs", "get" => (object) $this->global_data);
        $this->blade->view("backend.cronjobs.index", $data);
    }
    public function setting()
    {
        $data = array("title" => "Cron Jobs", "get" => (object) $this->global_data);
        $this->blade->view("backend.cronjobs.setting", $data);
    }
    public function Status()
    {
        $this->form_validation->set_rules("isCrons", "Cron Status", "trim");
        if ($this->form_validation->run() == true) {
            $input = $this->input->post("isCrons", true) == 1 ? 1 : 0;
            $datas = array("cronJobs" => $input);
            $modify = $this->options->modify($datas);
            $modify ? Ajax::success($input ? "CronJobs has been enabled!" : "CronJobs has been disabled!", 200) : Ajax::error("Something wrong happend please refresh the page!", 200);
        } else {
            Ajax::error(validation_errors("", ""), 200);
        }
    }
}

?>