<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Admins extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Admin_m", "admin");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => $this->lang->line("Admins_message"), "admins" => $this->admin->getAll());
        $this->blade->view("backend.admins.all", $data);
    }
    public function add()
    {
        $data = array("title" => $this->lang->line("AddAdmin_message"));
        $this->form_validation->set_rules("userName", "Username", "required|min_length[4]|is_unique[admins.userName]");
        $this->form_validation->set_rules("passWord", "Password", "required|min_length[8]");
        $this->form_validation->set_rules("passWord2", "Re-Password", "required|matches[passWord]|min_length[8]");
        $this->form_validation->set_rules("fName", "Full Name", "required");
        $this->form_validation->set_rules("userEmail", "Email", "required|valid_email|is_unique[admins.userEmail]");
        if ($this->form_validation->run() == false) {
            $this->blade->view("backend.admins.add", $data);
        } else {
            $new = $this->admin->insert();
            if ($new == true) {
                $this->session->set_flashdata("success", "Admin have been added successfully.");
                redirect("admin/admins", "auto");
            } else {
                $this->session->set_flashdata("error", "Admin have been added unsuccessfully.");
                redirect("admin/admins", "auto");
            }
        }
    }
    public function edit($id = "")
    {
        $ids = $id;
        $data = array("title" => $this->lang->line("EditAdmin_message"), "admin" => $this->admin->getByID($ids));
        $this->form_validation->set_rules("fName", "Full Name", "required");
        if ($this->form_validation->run() == false) {
            $this->blade->view("backend.admins.edit", $data);
        } else {
            $new = $this->admin->edit($ids);
            if ($new == true) {
                $this->session->set_flashdata("success", "Admin have been edited successfully.");
                redirect("admin/admins", "auto");
            } else {
                $this->session->set_flashdata("error", "Admin have been edited unsuccessfully.");
                redirect("admin/admins", "auto");
            }
        }
    }
    public function updatePass($id = "")
    {
        $ids = $id;
        $data = array("title" => $this->lang->line("ChangePassword_message"), "admin" => $this->admin->getByID($ids));
        $this->form_validation->set_rules("OldpassWord", "Old Password", "required");
        $this->form_validation->set_rules("passWord", "Password", "required|min_length[8]");
        $this->form_validation->set_rules("passWord2", "Re-Password", "required|matches[passWord]|min_length[8]");
        if ($this->form_validation->run() == false) {
            $this->blade->view("backend.admins.pass", $data);
        } else {
            $new = $this->admin->updatePass($ids);
            if ($new == true) {
                $this->session->set_flashdata("success", "Admin Password have been change successfully.");
                redirect("admin/admins", "auto");
            } else {
                $this->session->set_flashdata("error", "Admin Password have been change unsuccessfully.");
                redirect("admin/admins", "auto");
            }
        }
    }
    public function updateEmail($id = "")
    {
        $ids = $id;
        $data = array("title" => $this->lang->line("UpdateEmail_message"), "admin" => $this->admin->getByID($ids));
        $this->form_validation->set_rules("userEmail", "Email", "required|valid_email|is_unique[admins.userEmail]");
        $this->form_validation->set_rules("userEmail2", "Re-Email", "required|valid_email|matches[userEmail]");
        if ($this->form_validation->run() == false) {
            $this->blade->view("backend.admins.mail", $data);
        } else {
            $new = $this->admin->updateEmail($ids);
            if ($new == true) {
                $this->session->set_flashdata("success", "Admin Email have been change successfully.");
                redirect("admin/admins", "auto");
            } else {
                $this->session->set_flashdata("error", "Admin Email have been change unsuccessfully.");
                redirect("admin/admins", "auto");
            }
        }
    }
    public function del($id)
    {
        $ids = $id;
        $this->load->library("Ajax", false);
        if ($this->admin->del($ids) == true) {
            Ajax::success("success");
        } else {
            Ajax::error("error", 200);
        }
    }
}

?>
