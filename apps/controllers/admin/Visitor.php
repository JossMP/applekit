<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Visitor extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Visit_m", "visit");
        $this->load->library("Ajax", false);
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $data = array("title" => "Visitor Log", "visits" => $this->visit->getAll());
        $this->blade->view("backend.visitor.all", $data);
    }
    public function clear()
    {
        if ($this->visit->cut() === true) {
            redirect(site_url("admin/visitor/?success"), "auto");
        } else {
            redirect(site_url("admin/visitor/?error"), "auto");
        }
    }
}

?>
