<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class FindMyiPhone extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->visitors();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $this->load->model("Device_m", "device");
        $this->load->model("Applekit_m", "kit");
        $data = array("userID" => $this->session->userdata("userID"), "allDevice" => $this->device->getByUser($this->session->userdata("userID")), "countOn" => $this->device->getByStatus($this->session->userdata("userID")), "countOn2" => $this->device->getByStatus2($this->session->userdata("userID")), "countOn3" => $this->device->getByStatus3($this->session->userdata("userID")));
        $this->load->view("assets/headfind", $data);
        $this->load->view("Find/index", $data);
        $this->load->view("assets/footfind", $data);
    }
}

?>
