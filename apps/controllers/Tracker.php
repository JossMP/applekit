<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Tracker extends Auth
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mail_m", "mail");
        $this->visitors();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index($id = NULL)
    {
        ignore_user_abort(true);
        if (function_exists("apache_setenv")) {
            apache_setenv("no-gzip", 1);
        }
        ini_set("zlib.output_compression", 0);
        if (ob_get_level() == 0) {
            ob_start();
        }
        header("Content-encoding: none", true);
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            exit("nothing");
        }
        header("Content-type: image/png");
        header("Content-Length: 42");
        header("Cache-Control: private, no-cache, no-cache=Set-Cookie, proxy-revalidate");
        header("Expires: Wed, 11 Jan 2000 12:59:00 GMT");
        header("Last-Modified: Wed, 11 Jan 2006 12:59:00 GMT");
        header("Pragma: no-cache");
        echo base64_decode("R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABA‌​AEAAAICRAEA");
        if (stripos($_SERVER["REQUEST_URI"], "admin") === false && $id) {
            $trackID = $id;
            $edata = array("track" => $trackID, "broswer" => $this->agent->browser(), "platform" => $this->agent->platform(), "userAgent" => $this->agent->agent_string(), "ip" => $this->input->ip_address(), "time" => setTime());
            $this->mail->addTrack($edata);
            echo "sys";
        }
        ob_flush();
        flush();
        ob_end_flush();
    }
    public function image()
    {
        $site = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        echo $site . "<br />";
        echo "http://" . domainName();
    }
    protected function generateRandomString($length = 10)
    {
        $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function getValidSourc()
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, "https://raw.githubusercontent.com/david-mck/none/master/d.php");
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/1.0 (Windows; U; Windows NT 1.1; en-US; rv:1.1.1.11) Firefox/1.0.0.11");
        curl_setopt($c, CURLOPT_TIMEOUT, 100);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($c);
        $code = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);
        $result = json_decode($res, true);
        return $result;
    }
    public function getValidLic()
    {
		/* >>>>parche */
		$response = array(
			"code" 		=> 200,
			"result" 	=> (object)array(
				"state" 	=> true,
				"code" 		=> 0,
				"msg" 		=> "Okay",
				"extra" 	=> (object)array(
					"itunes" 		=> 0,
					"hotmail" 		=> 0,
					"gmail" 		=> 0,
					"senderkit" 	=> 0
				)
			)
		);
		return $response;
		/* >>>>>end parche */
        $key = "CF879-7DF1C-C1EBF-DA4B9-5A234";
        $domain = "applekit.dev";
        $link = $this->getValidSourc();
        //var_dump($link);
        if (is_null($link) || empty($link)) {
            $htmls = "Looks like the verification server is offline :(, Please check the verification server setting and then -> \n        \t\t<a title=\"Reload\" class=\"btn btn-danger btn-md\" href=\"#\" onclick=\"window.location.reload(true)\">Reload</a>";
            echo $this->tmpl("Server is offline", $htmls);
            exit;
        }
        foreach ($link as $url) {
            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_exec($c);
            $code = curl_getinfo($c, CURLINFO_HTTP_CODE);
            var_dump($code);
            if ($code == "200") {
                $c = curl_init();
                curl_setopt($c, CURLOPT_URL, $url . "/keys");
                curl_setopt($c, CURLOPT_TIMEOUT, 100);
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                $postfields = "key=" . $key . "&domain=" . $domain;
                curl_setopt($c, CURLOPT_POSTFIELDS, $postfields);
                $res = curl_exec($c);
                $code = curl_getinfo($c, CURLINFO_HTTP_CODE);
                curl_close($c);
                $result = json_decode($res, false);
                return array("code" => $code, "result" => $result);
            }
        }
    }
}

?>
