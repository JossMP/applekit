<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Errcode extends indexes
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function error_404()
    {
        $opt = $this->Opts->get();
        $data = array("login" => !empty($opt[0]->loginPage) ? $opt[0]->loginPage : "login");
        $this->load->view("errors/error_404", $data);
    }
}

?>
