<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class Logout extends Auth
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($this->goptss)) {
            exit;
        }
    }
    public function index()
    {
        $this->unsetS();
        if ($this->session->userdata("template")) {
            if ($this->session->userdata("track")) {
                redirect(site_url($this->session->userdata("template") . "/" . $this->session->userdata("track")), "auto");
            } else {
                redirect(site_url($this->session->userdata("template")), "auto");
            }
        } else {
            redirect(site_url(), "auto");
        }
    }
    public function unsetS()
    {
        $data = array("email", "logged_in", "userID", "pass", "firstName");
        $this->session->unset_userdata($data);
    }
}

?>
