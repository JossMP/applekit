<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
echo '<!DOCTYPE html>' . "\n" . '<html lang="en">' . "\n" . '<head>' . "\n" . '<meta charset="utf-8">' . "\n" . '<title>Error</title>' . "\n" . '<style type="text/css">' . "\n\n" . '::selection { background-color: #E13300; color: white; }' . "\n" . '::-moz-selection { background-color: #E13300; color: white; }' . "\n\n" . 'body {' . "\n\t" . 'background-color: #fff;' . "\n\t" . 'margin: 40px;' . "\n\t" . 'font: 13px/20px normal Helvetica, Arial, sans-serif;' . "\n\t" . 'color: #4F5155;' . "\n" . '}' . "\n\n" . 'a {' . "\n\t" . 'color: #003399;' . "\n\t" . 'background-color: transparent;' . "\n\t" . 'font-weight: normal;' . "\n" . '}' . "\n\n" . 'h1 {' . "\n\t" . 'color: #444;' . "\n\t" . 'background-color: transparent;' . "\n\t" . 'border-bottom: 1px solid #D0D0D0;' . "\n\t" . 'font-size: 19px;' . "\n\t" . 'font-weight: normal;' . "\n\t" . 'margin: 0 0 14px 0;' . "\n\t" . 'padding: 14px 15px 10px 15px;' . "\n" . '}' . "\n\n" . 'code {' . "\n\t" . 'font-family: Consolas, Monaco, Courier New, Courier, monospace;' . "\n\t" . 'font-size: 12px;' . "\n\t" . 'background-color: #f9f9f9;' . "\n\t" . 'border: 1px solid #D0D0D0;' . "\n\t" . 'color: #002166;' . "\n\t" . 'display: block;' . "\n\t" . 'margin: 14px 0 14px 0;' . "\n\t" . 'padding: 12px 10px 12px 10px;' . "\n" . '}' . "\n\n" . '#container {' . "\n\t" . 'margin: 10px;' . "\n\t" . 'border: 1px solid #D0D0D0;' . "\n\t" . 'box-shadow: 0 0 8px #D0D0D0;' . "\n" . '}' . "\n\n" . 'p {' . "\n\t" . 'margin: 12px 15px 12px 15px;' . "\n" . '}' . "\n" . '</style>' . "\n" . '</head>' . "\n" . '<body>' . "\n\t" . '<div id="container">' . "\n\t\t" . '<h1>';
echo $heading;
echo '</h1>' . "\n\t\t";
echo $message;
echo "\t" . '</div>' . "\n" . '</body>' . "\n" . '</html>';

?>
