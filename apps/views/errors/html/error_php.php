<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
echo "\n" . '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">' . "\n\n" . '<h4>A PHP Error was encountered</h4>' . "\n\n" . '<p>Severity: ';
echo $severity;
echo '</p>' . "\n" . '<p>Message:  ';
echo $message;
echo '</p>' . "\n" . '<p>Filename: ';
echo $filepath;
echo '</p>' . "\n" . '<p>Line Number: ';
echo $line;
echo '</p>' . "\n\n";
if (defined('SHOW_DEBUG_BACKTRACE') && (SHOW_DEBUG_BACKTRACE === true)) {
	echo "\n\t" . '<p>Backtrace:</p>' . "\n\t";

	foreach (debug_backtrace() as $error) {
		echo "\n\t\t";

		if (isset($error['file']) && (strpos($error['file'], realpath(BASEPATH)) !== 0)) {
			echo "\n\t\t\t" . '<p style="margin-left:10px">' . "\n\t\t\t" . 'File: ';
			echo $error['file'];
			echo '<br />' . "\n\t\t\t" . 'Line: ';
			echo $error['line'];
			echo '<br />' . "\n\t\t\t" . 'Function: ';
			echo $error['function'];
			echo "\t\t\t" . '</p>' . "\n\n\t\t";
		}

		echo "\n\t";
	}

	echo "\n";
}

echo "\n" . '</div>';

?>
