<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
echo "\n" . 'An uncaught Exception was encountered' . "\n\n" . 'Type:        ';
echo get_class($exception);
echo "\n";
echo 'Message:     ';
echo $message;
echo "\n";
echo 'Filename:    ';
echo $exception->getFile();
echo "\n";
echo 'Line Number: ';
echo $exception->getLine();
echo "\n";
if (defined('SHOW_DEBUG_BACKTRACE') && (SHOW_DEBUG_BACKTRACE === true)) {
	echo "\n" . 'Backtrace:' . "\n";

	foreach ($exception->getTrace() as $error) {
		if (isset($error['file']) && (strpos($error['file'], realpath(BASEPATH)) !== 0)) {
			echo "\t" . 'File: ';
			echo $error['file'];
			echo "\n";
			echo "\t" . 'Line: ';
			echo $error['line'];
			echo "\n";
			echo "\t" . 'Function: ';
			echo $error['function'];
			echo "\n\n";
		}
	}

	echo "\n";
}

?>
