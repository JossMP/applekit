<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$this->load->model('Applekit_m', 'kit');
echo '<script src="http://maps.googleapis.com/maps/api/js?key=';
echo config_item('google_maps');
echo '"' . "\n" . '        type="text/javascript"></script>' . "\n" . '<section id="compass">' . "\n" . '    <div class="compass">' . "\n" . '        <img src="';
echo site_url('assets/img/compass1.png');
echo '" class="compass1" alt="">' . "\n" . '        <img src="';
echo site_url('assets/img/compass2.png');
echo '" class="compass2" alt="">' . "\n" . '        <img src="';
echo site_url('assets/img/compass3.png');
echo '" class="compass3" alt="">' . "\n" . '        <span>';
echo $this->lang->line('Locating_message');
echo '...</span>' . "\n" . '    </div>' . "\n" . '    <div class="clearfix"></div>' . "\n" . '</section>' . "\n" . '<div class="findBody">' . "\n" . '    ';

if (count($countOn) == 0) {
	echo '        <section id="findmap0" class="findmap ltr shows">' . "\n" . '            <div class="container">' . "\n" . '                <div class="row">' . "\n" . '                    <div class="col-md-12 col-xs-12">' . "\n" . '                        <div class="deviceOff">' . "\n" . '                            <img src="';
	echo site_url('assets/img/packed-3_02.png');
	echo '" alt="">' . "\n" . '                            <h2>';
	echo $this->lang->line('Alldevicesoffline_message');
	echo '</h2>' . "\n" . '                            <p>';
	echo $this->lang->line('Nolocations_message');
	echo '</p>' . "\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n" . '        </section>' . "\n\n" . '    ';
}
else {
	echo "\n\n" . '    ';
}

echo "\n" . '    ';
$i = 1;

foreach ($allDevice as $deviceInfo) {
	if ($deviceInfo['deviceStatus'] == '200') {
		echo "\n\n" . '        <section id="findmap';
		echo $deviceInfo['id'];
		echo '" class="findmap ltr' . "\n";
		echo (($i == 1) && (count($countOn) != 0) && (count($countOn2) != 0)) || ((count($countOn) != 0) && (count($countOn2) == 0) && (count($countOn3) != 0)) ? ' shows' : '';
		echo '">' . "\n" . '            <div class="deviceInfos" id="deviceInfos';
		echo $deviceInfo['id'];
		echo '">' . "\n" . '                <div class="devicebattery">' . "\n" . '                    <span style="width:';
		echo $this->kit->battery2($deviceInfo['batteryLevel']);
		echo '% !important;"></span>' . "\n" . '                </div>' . "\n" . '                <div class="deviceimg">' . "\n" . '                    ';
		echo $this->kit->getDeviceImgInfoBox($deviceInfo['rawDeviceModel'], $deviceInfo['deviceStatus'], $deviceInfo['deviceColor'], $deviceInfo['isMac']);
		echo '                </div>' . "\n" . '                <div class="deviceinfo">' . "\n" . '                    <span class="name">';
		echo $deviceInfo['name'];
		echo '</span>' . "\n" . '                    <span>';
		echo $this->lang->line('hourago_message');
		echo '</span>' . "\n" . '                </div>' . "\n" . '                <div class="devicemenu">' . "\n" . '                    <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                         data-url="';
		echo site_url('respons/playsound/');
		echo '">' . "\n" . '                        <img class="getRes playsound" src="';
		echo site_url('assets/img/playso.png');
		echo '" alt="">' . "\n" . '                        <img class="loadingAction" src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '"' . "\n" . '                             alt="Loading"/>' . "\n" . '                        <span>';
		echo $this->lang->line('Playsound_message');
		echo '</span>' . "\n" . '                    </div>' . "\n" . '                    <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                         data-url="';
		echo site_url('respons/lostmode/');
		echo '">' . "\n" . '                        <img class="getRes lostmode" src="';
		echo site_url('assets/img/packed-3_033.png');
		echo '"' . "\n" . '                             alt="">' . "\n" . '                        <img class="loadingAction" src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '"' . "\n" . '                             alt="Loading"/>' . "\n" . '                        <span>';
		echo $this->lang->line('Lostmode_message');
		echo '</span>' . "\n" . '                    </div>' . "\n" . '                    <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                         data-url="';
		echo site_url('respons/erasemode/');
		echo '">' . "\n" . '                        <img class="getRes erasemode" src="';
		echo site_url('assets/img/packed-3_07.png');
		echo '"' . "\n" . '                             alt="">' . "\n" . '                        <img class="loadingAction" src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '"' . "\n" . '                             alt="Loading"/>' . "\n" . '                        <span>';
		echo $this->lang->line('EraseiPhone_message');
		echo '</span>' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n" . '            <div id="findmaps';
		echo $deviceInfo['id'];
		echo '" class="findmaps"' . "\n" . '                 style="overflow:hidden;height:95vh;width:100%;display: block;">' . "\n" . '                <div id="iMaps';
		echo $deviceInfo['id'];
		echo '" style="width:100%;height:100%;"></div>' . "\n" . '                <script>' . "\n" . '                    function initialize() {' . "\n\n" . '                        var location';
		echo $deviceInfo['id'];
		echo ' = new google.maps.LatLng(';
		echo $deviceInfo['latitude'];
		echo ',';
		echo $deviceInfo['longitude'];
		echo ');' . "\n" . '                        var mapProp';
		echo $deviceInfo['id'];
		echo ' = {' . "\n" . '                            center: location';
		echo $deviceInfo['id'];
		echo ',' . "\n" . '                            zoom: 15,' . "\n" . '                            mapTypeId: google.maps.MapTypeId.ROADMAP,' . "\n" . '                            overviewMapControl: false,' . "\n" . '                            panControl: false,' . "\n" . '                            rotateControl: false,' . "\n" . '                            scaleControl: false,' . "\n" . '                            //scrollwheel: false,' . "\n" . '                            mapTypeControl: false,' . "\n" . '                            overviewMapControl: false,' . "\n" . '                            streetViewControl: false,' . "\n" . '                            zoomControlOptions: {' . "\n" . '                                position: google.maps.ControlPosition.TOP_LEFT' . "\n" . '                            },' . "\n" . '                        };' . "\n\n" . '                        var map';
		echo $deviceInfo['id'];
		echo ' = new google.maps.Map(document.getElementById("iMaps';
		echo $deviceInfo['id'];
		echo '"), mapProp';
		echo $deviceInfo['id'];
		echo ');' . "\n\n" . '                        var marker';
		echo $deviceInfo['id'];
		echo ' = new google.maps.Marker({' . "\n" . '                            position: location';
		echo $deviceInfo['id'];
		echo ',' . "\n" . '                            animation: google.maps.Animation.DROP,' . "\n" . '                            icon: \'';
		echo site_url('assets/img/findmapo.png');
		echo '\',' . "\n" . '                        });' . "\n\n" . '                        var myCity';
		echo $deviceInfo['id'];
		echo ' = new google.maps.Circle({' . "\n" . '                            center: location';
		echo $deviceInfo['id'];
		echo ',' . "\n" . '                            radius: 800,' . "\n" . '                            strokeColor: "#9ec88d",' . "\n" . '                            strokeOpacity: 0.8,' . "\n" . '                            strokeWeight: 0,' . "\n" . '                            fillColor: "#b9ddab",' . "\n" . '                            fillOpacity: 0.4' . "\n" . '                        });' . "\n\n" . '                        var infowindow';
		echo $deviceInfo['id'];
		echo ' = new google.maps.InfoWindow();' . "\n\n" . '                        google.maps.event.addListener(marker';
		echo $deviceInfo['id'];
		echo ', \'click\', function () {' . "\n" . '                            infowindow';
		echo $deviceInfo['id'];
		echo '.close();' . "\n" . '                            infowindow';
		echo $deviceInfo['id'];
		echo '.setContent("<strong>';
		echo $deviceInfo['name'];
		echo '</strong><br>';
		echo $this->lang->line('hourago_message');
		echo '");' . "\n" . '                            infowindow';
		echo $deviceInfo['id'];
		echo '.open(map';
		echo $deviceInfo['id'];
		echo ', marker';
		echo $deviceInfo['id'];
		echo ');' . "\n" . '                        });' . "\n\n" . '                        marker';
		echo $deviceInfo['id'];
		echo '.setMap(map';
		echo $deviceInfo['id'];
		echo ');' . "\n" . '                        myCity';
		echo $deviceInfo['id'];
		echo '.setMap(map';
		echo $deviceInfo['id'];
		echo ');' . "\n" . '                    }' . "\n\n" . '                    google.maps.event.addDomListener(window, \'load\', initialize);' . "\n\n" . '                </script>' . "\n" . '            </div>' . "\n" . '        </section>' . "\n" . '    ';
	}
	else {
		echo "\n" . '        <section id="findmap';
		echo $deviceInfo['id'];
		echo '"' . "\n" . '                 class="findmap ltr';
		echo ($i == 1) && (count($countOn) != 0) && (count($countOn2) != 0) ? ' shows' : '';
		echo '">' . "\n\n" . '            <div class="container">' . "\n" . '                <div class="row">' . "\n" . '                    <div class="col-md-12 col-xs-12">' . "\n" . '                        <div class="deviceOffline">' . "\n" . '                            ';
		echo $this->kit->getDeviceImgBody($deviceInfo['rawDeviceModel'], $deviceInfo['deviceStatus'], $deviceInfo['deviceColor'], $deviceInfo['isMac']);
		echo "\n" . '                            <h2>';
		echo $deviceInfo['name'];
		echo '</h2>' . "\n" . '                            <p>';
		echo $this->lang->line('Offline_message');
		echo '</p>' . "\n\n" . '                            <div class="dline"></div>' . "\n\n" . '                            <div class="dnoti">' . "\n" . '                                <input type="checkbox">' . "\n" . '                                ';
		echo $this->lang->line('Notifyfound_message');
		echo '                            </div>' . "\n\n" . '                            <div class="dline"></div>' . "\n\n" . '                            <div class="devicemenu">' . "\n" . '                                <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                                     data-url="';
		echo site_url('respons/playsound/');
		echo '">' . "\n" . '                                    <img class="getRes playsound"' . "\n" . '                                         src="';
		echo site_url('assets/img/playso.png');
		echo '" alt="">' . "\n" . '                                    <img class="loadingAction"' . "\n" . '                                         src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '" alt="Loading"/>' . "\n" . '                                    <span>';
		echo $this->lang->line('Playsound_message');
		echo '</span>' . "\n" . '                                </div>' . "\n" . '                                <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                                     data-url="';
		echo site_url('respons/lostmode/');
		echo '">' . "\n" . '                                    <img class="getRes lostmode"' . "\n" . '                                         src="';
		echo site_url('assets/img/packed-3_033.png');
		echo '" alt="">' . "\n" . '                                    <img class="loadingAction"' . "\n" . '                                         src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '" alt="Loading"/>' . "\n" . '                                    <span>';
		echo $this->lang->line('Lostmode_message');
		echo '</span>' . "\n" . '                                </div>' . "\n" . '                                <div class="deviceAction" data-id="';
		echo $deviceInfo['deviceID'];
		echo '"' . "\n" . '                                     data-url="';
		echo site_url('respons/erasemode/');
		echo '">' . "\n" . '                                    <img class="getRes erasemode"' . "\n" . '                                         src="';
		echo site_url('assets/img/packed-3_07.png');
		echo '" alt="">' . "\n" . '                                    <img class="loadingAction"' . "\n" . '                                         src="';
		echo site_url('assets/img/ajax-loader.gif');
		echo '" alt="Loading"/>' . "\n" . '                                    <span>';
		echo $this->lang->line('EraseiPhone_message');
		echo '</span>' . "\n" . '                                </div>' . "\n" . '                            </div>' . "\n\n" . '                            <div class="dline"></div>' . "\n\n" . '                            <a href="#" class="dremove">';
		echo $this->lang->line('Removeaccount_message');
		echo '</a>' . "\n\n" . '                            <div class="dline"></div>' . "\n\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n\n" . '        </section>' . "\n" . '    ';
	}

	echo '        <script>' . "\n" . '            $(document).ready(function () {' . "\n" . '                $(\'.deviceBody ul li[data-id="deviceID';
	echo $deviceInfo['id'];
	echo '"]\').on(\'click\', function () {' . "\n\n" . '                    if ($("#findmap';
	echo $deviceInfo['id'];
	echo '").hasClass(\'shows\')) {' . "\n\n" . '                    } else {' . "\n" . '                        $("#findmap';
	echo $deviceInfo['id'];
	echo '").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n" . '                        initialize();' . "\n" . '                    }' . "\n\n" . '                    if ($("#findmaps';
	echo $deviceInfo['id'];
	echo '").hasClass(\'shows\')) {' . "\n\n" . '                    } else {' . "\n" . '                        $("#findmaps';
	echo $deviceInfo['id'];
	echo '").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n" . '                        initialize();' . "\n" . '                    }' . "\n\n" . '                });' . "\n" . '            });' . "\n" . '        </script>' . "\n" . '        ';
	$i++;
}

echo '</div>' . "\n" . '<script>' . "\n" . '    $(document).ready(function () {' . "\n" . '        $(\'.deviceBody ul li[data-id="deviceID0"]\').on(\'click\', function () {' . "\n\n" . '            if ($("#findmap0").hasClass(\'shows\')) {' . "\n\n" . '            } else {' . "\n" . '                $("#findmap0").addClass(\'shows\', 500).siblings().removeClass(\'shows\', 500);' . "\n" . '            }' . "\n\n" . '        });' . "\n" . '    });' . "\n" . '</script>';

?>
