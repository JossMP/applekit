<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<!DOCTYPE html>' . "\n" . '<html>' . "\n\n" . '<head>' . "\n\n" . '    <meta charset="utf-8">' . "\n" . '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' . "\n\n" . '    <title>AppleKit | Login</title>' . "\n\n" . '    <link href="';
echo site_url('assets/backend/css/bootstrap.min.css');
echo '" rel="stylesheet">' . "\n" . '    <link href="';
echo site_url('assets/layout/fontawesome-all.css');
echo '" rel="stylesheet">' . "\n\n" . '    <link href="';
echo site_url('assets/backend/css/animate.css');
echo '" rel="stylesheet">' . "\n" . '    <link href="';
echo site_url('assets/backend/css/style.css');
echo '" rel="stylesheet">' . "\n" . '    <style type="text/css">' . "\n" . '        body {' . "\n" . '            background-image: url("';
echo site_url('assets/img/adbk.jpg');
echo '");' . "\n" . '            color: #fff !important;' . "\n" . '            background-position: center;' . "\n" . '            background-size: cover;' . "\n" . '            background-repeat: no-repeat;' . "\n" . '        }' . "\n" . '        .logo-name small {' . "\n" . '            color: #fff !important;' . "\n" . '        }' . "\n" . '    </style>' . "\n" . '</head>' . "\n\n" . '<body class="gray-bg">' . "\n\n" . '<div class="middle-box text-center loginscreen animated fadeInDown">' . "\n" . '    <div>' . "\n" . '        <div>' . "\n\n" . '            <h1 class="logo-name">' . "\n" . '                <small><i class="fab fa-apple"></i> kit</small>' . "\n" . '            </h1>' . "\n\n" . '        </div>' . "\n" . '        <h3>Welcome to <i class="fab fa-apple"></i> Applekit</h3>' . "\n" . '        <p>Simple but powerful' . "\n" . '            <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->' . "\n" . '        </p>' . "\n" . '        <p>Login in. To see it in action.</p>' . "\n" . '        ';
echo form_open(site_url('admin/login'), array('role' => 'form', 'class' => 'm-t'));
echo '        ';
if (isset($_GET['import']) && ($_GET['import'] == 'success')) {
	echo '            <div class="alert alert-success">Database have been restored successfully</div>' . "\n" . '            <!-- /.alert alert-success -->' . "\n" . '        ';
}

echo '        ';
echo validation_errors('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
echo '        ';

if (isset($error)) {
	echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
	echo $error;
	echo '</div>';
}

echo '        <div class="form-group">' . "\n" . '            <input style="color: #000" type="text" id="userName" name="userName" class="form-control" placeholder="Username" required="" value="';
echo set_value('userName');
echo '">' . "\n" . '        </div>' . "\n" . '        <div class="form-group">' . "\n" . '            <input style="color: #000" type="password" id="userPwd" name="userPwd" class="form-control" placeholder="Password" required="">' . "\n" . '        </div>' . "\n" . '        <button type="submit" id="doLogin" name="doLogin" class="btn btn-primary block full-width m-b">Login</button>' . "\n\n" . '        <a href="#">' . "\n" . '            <small>Forgot password?</small>' . "\n" . '        </a>' . "\n" . '        </form>' . "\n" . '        <p class="m-t">' . "\n" . '            <small>All copy rights [to] NemoZe.com &copy; ';
echo date('Y');
echo '</small>' . "\n" . '        </p>' . "\n" . '    </div>' . "\n" . '</div>' . "\n\n" . '<!-- Mainly scripts -->' . "\n" . '<script src="';
echo site_url('assets/backend/js/jquery-2.1.1.js');
echo '"></script>' . "\n" . '<script src="';
echo site_url('assets/backend/js/bootstrap.min.js');
echo '"></script>' . "\n\n" . '</body>' . "\n\n" . '</html>';

?>
