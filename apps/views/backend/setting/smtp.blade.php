<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@push(\'extrajs\')' . "\n" . '<!-- FooTable -->' . "\n" . '<script src="{{site_url(\'assets/js/ajax-form.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/footable/footable.all.min.js\')}}"></script>' . "\n" . '<!-- Sweet alert -->' . "\n\n" . '<!-- Ladda -->' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/spin.min.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/ladda.min.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/ladda.jquery.min.js\')}}"></script>' . "\n" . '<script>' . "\n" . '    $(document).ready(function () {' . "\n" . '        $(\'.footable\').footable();' . "\n" . '    });' . "\n" . '</script>' . "\n" . '@endpush' . "\n\n" . '@push(\'extracss\')' . "\n\n" . '<!-- FooTable -->' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/footable/footable.core.css\')}}" rel="stylesheet">' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/sweetalert/sweetalert.css\')}}" rel="stylesheet">' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/ladda/ladda-themeless.min.css\')}}" rel="stylesheet">' . "\n" . '@endpush' . "\n\n" . '@section(\'content\')' . "\n" . '<div class="row">' . "\n\n" . '    <div class="col-xs-12">' . "\n" . '        <div class="ibox">' . "\n" . '            <div class="ibox-header">' . "\n" . '                <h4 class="ibox-title">Add new smtp server</h4>' . "\n" . '            </div>' . "\n" . '            <!-- /.box-header -->' . "\n" . '            <div class="ibox-content">' . "\n" . '                ';
echo form_open(site_url('admin/smtp/addNew/'));
echo "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="name">Short Name</label>' . "\n" . '                        <input type="text" value="';
echo set_value('name');
echo '" class="form-control" name="name" id="name" placeholder="Short Name Descripe the smtp server exp. gmail,yahoo,hotmail etc">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="hosts">Smtp Host</label>' . "\n" . '                        <input type="text" value="';
echo set_value('hosts');
echo '" class="form-control" name="hosts" id="hosts" placeholder="Smtp Host exp mail.gmail.com, smtp.live.com etc.">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="username">Smtp Username</label>' . "\n" . '                        <input type="text" value="';
echo set_value('username');
echo '" class="form-control" name="username" id="username" placeholder="Smtp Username">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="password">Smtp Passwprd</label>' . "\n" . '                        <input type="text" value="';
echo set_value('password');
echo '" class="form-control" name="password" id="password" placeholder="Smtp Passwprd">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="port">Smtp Port</label>' . "\n" . '                        <input type="text" value="';
echo set_value('port');
echo '" class="form-control" name="port" id="port" placeholder="Smtp Port">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="type">Smtp Encrypted Type</label>' . "\n" . '                        <select name="type" id="type" class="select2 form-control show-tick show-menu-arrow" data-width="100%">' . "\n" . '                            <option value="0" ';
echo set_select('type', 0, true);
echo '>None</option>' . "\n" . '                            <option value="ssl" data-subtext="Recommended" ';
echo set_select('type', 'ssl');
echo '>SSL</option>' . "\n" . '                            <option value="tls" ';
echo set_select('type', 'tls');
echo '>TLS</option>' . "\n" . '                        </select>' . "\n" . '                        <!-- /#type -->' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-xs-12 text-center">' . "\n" . '                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add New Server </button>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-xs-12 -->' . "\n" . '                ';
echo form_close();
echo '                <div class="clearfix"></div>' . "\n" . '                <!-- /.clearfix -->' . "\n" . '            </div>' . "\n" . '            <!-- /.box-body -->' . "\n" . '        </div>' . "\n" . '        <!-- /.box -->' . "\n" . '    </div>' . "\n" . '    <!-- /.col-xs-12 -->' . "\n\n\n" . '    ';

if ($ci->session->flashdata('success')) {
	echo '        <div class="col-xs-12">' . "\n" . '            <div class="alert alert-success alert-dismissable">' . "\n" . '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . "\n" . '                ';
	echo $ci->session->flashdata('success');
	echo '            </div>' . "\n" . '        </div>' . "\n" . '    ';
}

echo "\n" . '    ';

if ($ci->session->flashdata('error')) {
	echo '        <div class="col-xs-12">' . "\n" . '            <div class="alert alert-danger alert-dismissable">' . "\n" . '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . "\n" . '                ';
	echo $ci->session->flashdata('error');
	echo '            </div>' . "\n" . '        </div>' . "\n" . '    ';
}

echo "\n" . '    <div class="col-xs-12">' . "\n\n" . '        <div class="ibox">' . "\n" . '            <div class="ibox-content">' . "\n" . '                <table class="example1 table table-bordered table-striped">' . "\n" . '                    <thead>' . "\n" . '                    <tr>' . "\n" . '                        <th width="5%">#</th>' . "\n" . '                        <th>Short Name</th>' . "\n" . '                        <th>Host</th>' . "\n" . '                        <th>Username</th>' . "\n" . '                        <th>Password</th>' . "\n" . '                        <th>Port</th>' . "\n" . '                        <th>Encrypted</th>' . "\n" . '                        <th width="10%">Remove</th>' . "\n" . '                    </tr>' . "\n" . '                    </thead>' . "\n" . '                    <tbody>' . "\n" . '                    ';

foreach ($smtp as $smtps) {
	echo '<tr><td>' . $smtps->id . '</td>';
	echo '<td>' . $smtps->name . '</td>';
	echo '<td>' . $smtps->host . '</td>';
	echo '<td>' . $smtps->username . '</td>';
	echo '<td>' . $smtps->password . '</td>';
	echo '<td>' . $smtps->port . '</td>';
	$smtps->type == '0' ? $type = 'None' : $type = $smtps->type;
	echo '<td><span class="label label-warning">' . strtoupper($type) . '</span></td>';
	echo '<td><a onclick="return confirm(\'Are you sure deleting this ip!\');" class="btn-danger btn btn-sm" href="' . site_url('admin/smtp/delete/' . $smtps->id) . '" data-toggle="tooltip" data-placement="top" title="Delete Smtp Server"><i class="fa fa-times"></i></a> <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit Server" href="' . site_url('admin/smtp/edit/' . $smtps->id) . '"><i class="fa fa-edit"></i></a></td>';
	echo '</tr>';
}

echo '                    </tbody>' . "\n" . '                    <tfoot>' . "\n" . '                    <tr>' . "\n" . '                        <th width="5%">#</th>' . "\n" . '                        <th>Short Name</th>' . "\n" . '                        <th>Host</th>' . "\n" . '                        <th>Username</th>' . "\n" . '                        <th>Password</th>' . "\n" . '                        <th>Port</th>' . "\n" . '                        <th>Encrypted</th>' . "\n" . '                        <th width="10%">Remove</th>' . "\n" . '                    </tr>' . "\n" . '                    </tfoot>' . "\n" . '                </table>' . "\n" . '            </div><!-- /.box-body -->' . "\n" . '        </div><!-- /.box -->' . "\n" . '    </div>' . "\n" . '</div>' . "\n" . '@endsection';

?>
