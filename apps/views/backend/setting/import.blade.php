<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@push(\'extrajs\')' . "\n" . '<!-- FooTable -->' . "\n" . '<script src="{{site_url(\'assets/js/ajax-form.js\')}}"></script>' . "\n" . '@endpush' . "\n\n\n" . '@section(\'content\')' . "\n" . '    <div class="row">' . "\n" . '        ';

if ($ci->session->flashdata('error')) {
	echo '        <div class="col-xs-12">' . "\n" . '            <div class="alert alert-danger">' . "\n" . '                ';
	echo $ci->session->flashdata('error');
	echo '            </div>' . "\n" . '            <!-- /.alert alert-danger -->' . "\n" . '        </div>' . "\n" . '        <!-- /.col-xs-12 -->' . "\n" . '        ';
}

echo "\n" . '        <div class="col-xs-12">' . "\n" . '            <div class="tabs-container">' . "\n" . '                <ul class="nav nav-tabs">' . "\n" . '                    <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="false"> Export/Backup' . "\n" . '                            Database</a></li>' . "\n" . '                    <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="true">Import/Restore Database</a>' . "\n" . '                    </li>' . "\n" . '                </ul>' . "\n" . '                <div class="tab-content">' . "\n" . '                    <div id="tab-1" class="tab-pane active">' . "\n" . '                        <div class="panel-body">' . "\n" . '                            <h3 class="ibox-title"' . "\n" . '                                style="border-bottom: 1px #e0e0e0 solid; padding-bottom: 10px;margin-bottom: 10px;"><i' . "\n" . '                                        class="fa fa-cloud-download"></i> Export/Backup Database</h3>' . "\n" . '                            <div class="text-left">' . "\n" . '                                <p><strong>Database Driver:</strong> ';
echo $ci->db->platform();
echo '</p>' . "\n" . '                                <p><strong>Database Version:</strong> ';
echo $ci->db->version();
echo '</p>' . "\n" . '                                <p><strong>Table Status:</strong>' . "\n" . '                                    <br/> ';

foreach ($ci->db->list_tables() as $tables) {
	echo $tables . ' - <strong class="text-success">Good!</strong>' . '<br />';
}

echo '</p>' . "\n" . '                            </div>' . "\n" . '                            <!-- /.text-left -->' . "\n" . '                            <div class="text-left" style="margin-top: 20px;">' . "\n" . '                                <a href="';
echo site_url('admin/options/exportdb');
echo '"' . "\n" . '                                   class="btn btn-success btn-lg">Download' . "\n" . '                                    Database</a>' . "\n" . '                            </div>' . "\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                    <div id="tab-2" class="tab-pane">' . "\n" . '                        <div class="panel-body">' . "\n" . '                            <h3 class="ibox-title"' . "\n" . '                                style="border-bottom: 1px #e0e0e0 solid; padding-bottom: 10px;margin-bottom: 10px;"><i' . "\n" . '                                        class="fa fa-cloud-upload"></i> Import/Restore Database</h3>' . "\n\n" . '                            ';
echo form_open_multipart(site_url('admin/options/importdatabase'));
echo '                            <div class="form-group">' . "\n" . '                                <label for=""></label>' . "\n" . '                                <input type="file" class="form-control" name="userfile" id="userfile"' . "\n" . '                                       placeholder="Input...">' . "\n" . '                            </div>' . "\n" . '                            <button type="submit" class="btn btn-primary">Restore</button>' . "\n" . '                            </form>' . "\n\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n\n\n" . '            </div>' . "\n" . '        </div>' . "\n" . '        <!-- /.col-xs-12 -->' . "\n\n" . '    </div>' . "\n" . '    <!-- /.row -->' . "\n" . '@endsection' . "\n";

?>
