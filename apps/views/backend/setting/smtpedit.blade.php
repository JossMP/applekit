<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@push(\'extrajs\')' . "\n" . '<!-- FooTable -->' . "\n" . '<script src="{{site_url(\'assets/js/ajax-form.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/footable/footable.all.min.js\')}}"></script>' . "\n" . '<!-- Sweet alert -->' . "\n\n" . '<!-- Ladda -->' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/spin.min.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/ladda.min.js\')}}"></script>' . "\n" . '<script src="{{site_url(\'assets/backend/js/plugins/ladda/ladda.jquery.min.js\')}}"></script>' . "\n" . '<script>' . "\n" . '    $(document).ready(function () {' . "\n" . '        $(\'.footable\').footable();' . "\n" . '    });' . "\n" . '</script>' . "\n" . '@endpush' . "\n\n" . '@push(\'extracss\')' . "\n\n" . '<!-- FooTable -->' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/footable/footable.core.css\')}}" rel="stylesheet">' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/sweetalert/sweetalert.css\')}}" rel="stylesheet">' . "\n" . '<link href="{{site_url(\'assets/backend/css/plugins/ladda/ladda-themeless.min.css\')}}" rel="stylesheet">' . "\n" . '@endpush' . "\n\n" . '@section(\'content\')' . "\n" . '<div class="row">' . "\n\n" . '    <div class="col-xs-12">' . "\n" . '        <div class="ibox">' . "\n" . '            <div class="ibox-header">' . "\n" . '                <h4 class="ibox-title">Edit smtp server <strong>"';
echo $smtp->name;
echo ' -> ';
echo $smtp->host;
echo '"</strong></h4>' . "\n" . '            </div>' . "\n" . '            <!-- /.box-header -->' . "\n" . '            <div class="ibox-content">' . "\n" . '                ';
echo form_open(site_url('admin/smtp/update/'));
echo "\n" . '                <input type="hidden" id="id" name="id" value="';
echo $smtp->id;
echo '" />' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="name">Short Name</label>' . "\n" . '                        <input type="text" value="';
echo set_value('name', $smtp->name);
echo '" class="form-control" name="name" id="name" placeholder="Short Name Descripe the smtp server exp. gmail,yahoo,hotmail etc">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="hosts">Smtp Host</label>' . "\n" . '                        <input type="text" value="';
echo set_value('hosts', $smtp->host);
echo '" class="form-control" name="hosts" id="hosts" placeholder="Smtp Host exp mail.gmail.com, smtp.live.com etc.">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="username">Smtp Username</label>' . "\n" . '                        <input type="text" value="';
echo set_value('username', $smtp->username);
echo '" class="form-control" name="username" id="username" placeholder="Smtp Username">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="password">Smtp Passwprd</label>' . "\n" . '                        <input type="text" value="';
echo set_value('password', $smtp->password);
echo '" class="form-control" name="password" id="password" placeholder="Smtp Passwprd">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="port">Smtp Port</label>' . "\n" . '                        <input type="text" value="';
echo set_value('port', $smtp->port);
echo '" class="form-control" name="port" id="port" placeholder="Smtp Port">' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n" . '                <div class="col-md-6 col-xs-12">' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="type">Smtp Encrypted Type</label>' . "\n" . '                        <select name="type" id="type" class="select2 form-control show-tick show-menu-arrow" data-width="100%">' . "\n" . '                            <option value="0" ';
echo set_select('type', 0);
echo $smtp->type == 0 ? 'selected' : false;
echo '>None</option>' . "\n" . '                            <option value="ssl" data-subtext="Recommended" ';
echo set_select('type', 'ssl');
echo $smtp->type == 'ssl' ? 'selected' : false;
echo '>SSL</option>' . "\n" . '                            <option value="tls" ';
echo set_select('type', 'tls');
echo $smtp->type == 'tls' ? 'selected' : false;
echo '>TLS</option>' . "\n" . '                        </select>' . "\n" . '                        <!-- /#type -->' . "\n" . '                    </div>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-md-6 col-xs-12 -->' . "\n\n\n" . '                <div class="col-xs-12 text-center">' . "\n" . '                    <a href="';
echo site_url('admin/smtp');
echo '" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back to Smtp Server </a>' . "\n" . '                    <button type="submit" class="btn btn-primary">Update Server <i class="fa fa-arrow-right"></i></button>' . "\n" . '                </div>' . "\n" . '                <!-- /.col-xs-12 -->' . "\n" . '                ';
echo form_close();
echo '                <div class="clearfix"></div>' . "\n" . '                <!-- /.clearfix -->' . "\n" . '            </div>' . "\n" . '            <!-- /.box-body -->' . "\n" . '        </div>' . "\n" . '        <!-- /.box -->' . "\n" . '    </div>' . "\n" . '    <!-- /.col-xs-12 -->' . "\n\n" . '</div>' . "\n" . '@endsection';

?>
