<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/froala_editor.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/froala_style.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/code_view.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/colors.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/emoticons.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/image_manager.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/image.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/line_breaker.min.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/fullscreen.min.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/char_counter.min.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/table.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/char_counter.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/plugins/video.css\')}}">' . "\n" . '<link rel="stylesheet" href="{{site_url(\'assets/backend/editor/css/codemirror.min.css\')}}">';

?>
