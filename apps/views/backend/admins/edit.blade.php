<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@section(\'content\')' . "\n\t" . '<div class="row">' . "\n\t\t" . '<div class="col-xs-12">' . "\n\t\t\t" . '<div class="ibox ibox-info">' . "\n" . '        <div class="ibox-header with-border">' . "\n" . '          <h3 class="ibox-title">';
echo $ci->lang->line('EditAdmin_message');
echo ' "';
echo $admin['userName'];
echo '"</h3>' . "\n" . '        </div><!-- /.box-header -->' . "\n" . '        <!-- form start -->' . "\n" . '        ';
echo form_open('', array('class' => 'form-horizontal'));
echo '          <div class="ibox-content">' . "\n" . '          ';
echo validation_errors('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
echo '            <div class="form-group">' . "\n" . '              <label for="fName" class="col-sm-2 control-label">';
echo $ci->lang->line('FullName_message');
echo '</label>' . "\n" . '              <div class="col-sm-10">' . "\n" . '                <input type="text" class="form-control" id="fName" name="fName" value="';
echo $admin['fName'];
echo '" placeholder="';
echo $ci->lang->line('Typethefullname_message');
echo '">' . "\n" . '              </div>' . "\n" . '            </div>' . "\n" . '          </div><!-- /.box-body -->' . "\n" . '          <div class="ibox-footer">' . "\n" . '            <a href="{{site_url(ADMINPATH.\'admins\')}}" class="btn btn-default">';
echo $ci->lang->line('Cancel_message');
echo '</a>' . "\n" . '            <button type="submit" class="btn btn-info pull-right">';
echo $ci->lang->line('EditAdmin_message');
echo '</button>' . "\n" . '              <div class="clearfix"></div>' . "\n" . '          </div><!-- /.box-footer -->' . "\n" . '        </form>' . "\n\n" . '                <!-- /.clearfix -->' . "\n" . '      </div>' . "\n\t\t" . '</div>' . "\n\t" . '</div>' . "\n" . '@endsection';

?>
