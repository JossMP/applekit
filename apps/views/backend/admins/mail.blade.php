<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@section(\'content\')' . "\n" . '    <div class="row">' . "\n" . '        <div class="col-xs-12">' . "\n" . '            <div class="ibox ibox-info">' . "\n" . '                <div class="ibox-header with-border">' . "\n" . '                    <h3 class="ibox-title">';
echo $ci->lang->line('UpdateAdminEmail_message');
echo '                        "';
echo $admin['userName'];
echo '"</h3>' . "\n" . '                </div><!-- /.box-header -->' . "\n" . '                <!-- form start -->' . "\n" . '                ';
echo form_open('', array('class' => 'form-horizontal'));
echo '                <div class="ibox-content">' . "\n" . '                    ';
echo validation_errors('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
echo '                    <div class="form-group">' . "\n" . '                        <label for="userEmail"' . "\n" . '                               class="col-sm-2 control-label">';
echo $ci->lang->line('Email_message');
echo '</label>' . "\n" . '                        <div class="col-sm-10">' . "\n" . '                            <input type="email" class="form-control" id="userEmail" name="userEmail"' . "\n" . '                                   value="';
echo $admin['userEmail'];
echo '"' . "\n" . '                                   placeholder="';
echo $ci->lang->line('Typetheemailaddress_message');
echo '">' . "\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                    <div class="form-group">' . "\n" . '                        <label for="userEmail2"' . "\n" . '                               class="col-sm-2 control-label">';
echo $ci->lang->line('ReEmail_message');
echo '</label>' . "\n" . '                        <div class="col-sm-10">' . "\n" . '                            <input type="email" class="form-control" id="userEmail2" name="userEmail2"' . "\n" . '                                   placeholder="';
echo $ci->lang->line('Typetheemailaddress_message');
echo '">' . "\n" . '                        </div>' . "\n" . '                    </div>' . "\n" . '                </div><!-- /.box-body -->' . "\n" . '                <div class="ibox-footer">' . "\n" . '                    <a href="{{site_url(ADMINPATH.\'admins\')}}"' . "\n" . '                            class="btn btn-default">';
echo $ci->lang->line('Cancel_message');
echo '</a>' . "\n" . '                    <button type="submit"' . "\n" . '                            class="btn btn-info pull-right">';
echo $ci->lang->line('UpdateMail_message');
echo '</button>' . "\n" . '                </div><!-- /.box-footer -->' . "\n" . '                </form>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '@endsection';

?>
