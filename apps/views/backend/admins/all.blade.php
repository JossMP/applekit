<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '@extends(\'assets.backend.default\')' . "\n" . '{{--*/ $ci =& get_instance(); /*--}}' . "\n\n" . '@push(\'extrajs\')' . "\n" . '    <!-- FooTable -->' . "\n" . '    <script src="{{site_url(\'assets/js/ajax-form.js\')}}"></script>' . "\n" . '    <script src="{{site_url(\'assets/backend/js/plugins/footable/footable.all.min.js\')}}"></script>' . "\n" . '    <!-- Page-Level Scripts -->' . "\n" . '    <script>' . "\n" . '        $(document).ready(function () {' . "\n" . '            $(\'.footable\').footable();' . "\n" . '        });' . "\n" . '    </script>' . "\n" . '    <!-- Sweet alert -->' . "\n\n" . '@endpush' . "\n\n" . '@push(\'extracss\')' . "\n\n" . '    <!-- FooTable -->' . "\n" . '    <link href="{{site_url(\'assets/backend/css/plugins/footable/footable.core.css\')}}" rel="stylesheet">' . "\n" . '    <link href="{{site_url(\'assets/backend/css/plugins/sweetalert/sweetalert.css\')}}" rel="stylesheet">' . "\n" . '@endpush' . "\n\n" . '@section(\'content\')' . "\n\n" . '    <div class="row">' . "\n" . '        <div class="col-xs-12">' . "\n" . '            ';

if ($ci->session->flashdata('success')) {
	echo '            <div class="alert alert-success">' . "\n" . '                ';
	echo $ci->session->flashdata('success');
	echo '            </div>' . "\n" . '            ';
}

echo "\n" . '            ';

if ($ci->session->flashdata('error')) {
	echo '            <div class="alert alert-danger">' . "\n" . '                ';
	echo $ci->session->flashdata('error');
	echo '            </div>' . "\n" . '            ';
}

echo '        </div>' . "\n\n" . '        <div class="col-xs-12">' . "\n" . '            <div class="ibox">' . "\n" . '                <div class="ibox-header">' . "\n" . '                    <h3 class="ibox-title">';
echo $ci->lang->line('fulladminlist_message');
echo '</h3>' . "\n" . '                </div><!-- /.box-header -->' . "\n" . '                <div class="ibox-content">' . "\n" . '                    <table class="footable table table-stripped toggle-plus" data-page-size="20" data-filter=#filte>' . "\n" . '                        <thead>' . "\n" . '                        <tr>' . "\n" . '                            <th data-toggle="true" data-hide="phone">#</th>' . "\n" . '                            <th>Username</th>' . "\n" . '                            <th>Full Name</th>' . "\n" . '                            <th>Email</th>' . "\n" . '                            <th>Admin Since</th>' . "\n" . '                            <th style="width: 5%; text-align: center">Action</th>' . "\n" . '                        </tr>' . "\n" . '                        </thead>' . "\n" . '                        <tbody>' . "\n" . '                        ';

foreach ($admins as $admin) {
	echo '<tr><td>' . $admin['id'] . '</td>';
	echo '<td>' . $admin['userName'] . '</td>';
	echo '<td>' . $admin['fName'] . '</td>';
	echo '<td>' . $admin['userEmail'] . '</td>';
	echo '<td>' . niceTime($admin['time']) . '</td>';
	echo "\n" . '                        <td style="width: 20%; text-align: center">' . "\n" . '                        <a data-toggle="tooltip" data-placement="top" ' . "\n" . '                        ';
	if (($ci->session->userdata('adminName') == $admin['userName']) || ($ci->session->userdata('adminID') == 1)) {
		echo 'title="' . $ci->lang->line('EditAdmin_message') . '" ' . 'href="' . site_url('admin/admins/edit/' . $admin['id']) . '"';
	}
	else {
		echo 'title="' . $ci->lang->line('NotAuthority_message') . '" disabled ';
	}

	echo ' class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a> ';
	echo '<a data-toggle="tooltip" data-placement="top"';
	if (($ci->session->userdata('adminName') == $admin['userName']) || ($ci->session->userdata('adminID') == 1)) {
		echo 'title="' . $ci->lang->line('UpdateMail_message') . '" ' . 'href="' . site_url('admin/admins/updateEmail/' . $admin['id']) . '"';
	}
	else {
		echo 'title="' . $ci->lang->line('NotAuthority_message') . '" disabled ';
	}

	echo ' class="btn btn-default btn-sm"><i class="fa fa-envelope"></i></a>' . "\n" . '                        <a data-toggle="tooltip" data-placement="top" ';
	if (($ci->session->userdata('adminName') == $admin['userName']) || ($ci->session->userdata('adminID') == 1)) {
		echo 'title="' . $ci->lang->line('ChangePassword_message') . '" ' . 'href="' . site_url('admin/admins/updatePass/' . $admin['id']) . '"';
	}
	else {
		echo 'title="' . $ci->lang->line('NotAuthority_message') . '" disabled ';
	}

	echo ' class="btn btn-default btn-sm"><i class="fa fa-lock"></i></a>';

	if ($admin['id'] != 1) {
		echo '<a data-toggle="tooltip" data-placement="top" ';

		if ($ci->session->userdata('adminID') == 1) {
			echo 'title="' . $ci->lang->line('Deleteadmin_message') . '" ';
		}
		else {
			echo 'title="' . $ci->lang->line('NotAuthority_message') . '" disabled ';
		}

		if ($ci->session->userdata('adminID') == 1) {
			echo ' data-id="' . $admin['id'] . '" data-url="' . site_url('admin/admins/del') . '" class="removeVic btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
		}
		else {
			echo ' disabled readonly class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
		}
	}

	echo '</td>';
	echo '</tr>';
}

echo '                        </tbody>' . "\n" . '                        <tfoot>' . "\n" . '                        <tr>' . "\n" . '                            <td colspan="5">' . "\n" . '                                <ul class="pagination pull-right"></ul>' . "\n" . '                            </td>' . "\n" . '                        </tr>' . "\n" . '                        </tfoot>' . "\n" . '                    </table>' . "\n" . '                </div><!-- /.box-body -->' . "\n" . '            </div><!-- /.box -->' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '@endsection';

?>
