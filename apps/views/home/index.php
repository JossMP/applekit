<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<!DOCTYPE html>' . "\n" . '<html lang="en">' . "\n" . '<head>' . "\n" . '    <meta charset="UTF-8">' . "\n" . '    <meta http-equiv="X-UA-Compatible" content="IE=edge">' . "\n" . '    <meta content="width=device-width, initial-scale=1" name="viewport"/>' . "\n" . '    <meta name="robots" content="noindex, nofollow"/>' . "\n" . '    <title>';
echo $text;
echo '</title>' . "\n\n" . '    <style>' . "\n" . '        @font-face {' . "\n" . '            font-family: \'sf-pro-light\';' . "\n" . '            src: url(\'../font/sf-pro-display-light.eot\'); /* IE9 Compat Modes */' . "\n" . '            src: url(\'../font/sf-pro-display-light.eot?#iefix\') format(\'embedded-opentype\'), /* IE6-IE8 */ url(\'../font/sf-pro-display-light.woff\') format(\'woff\'), /* Modern Browsers */ url(\'../font/sf-pro-display-light.ttf\') format(\'truetype\'), /* Safari, Android, iOS */ url(\'../font/sf-pro-display-light.svg#80d8021909d835b751af094d043ad177\') format(\'svg\'); /* Legacy iOS */' . "\n" . '        }' . "\n\n" . '        body {' . "\n" . '            position: relative;' . "\n" . '            background: #e0e0e0;' . "\n" . '            margin: 0;' . "\n" . '            padding: 0;' . "\n" . '        }' . "\n\n" . '        .coming {' . "\n" . '            position: absolute;' . "\n" . '            font-family: \'sf-pro-light\', Tahoma;' . "\n" . '            font-weight: 100 !important;' . "\n" . '            font-size: 5em;' . "\n" . '            text-align: center;' . "\n" . '            width: 100%;' . "\n" . '            margin-top: 300px;' . "\n" . '            top: 50%;' . "\n" . '        }' . "\n\n" . '        @media only screen and (min-width: 1900px) and (max-width: 2000px) {' . "\n" . '            .coming {' . "\n" . '                margin-top: 400px;' . "\n" . '                top: 50%;' . "\n" . '            }' . "\n" . '        }' . "\n" . '    </style>' . "\n" . '</head>' . "\n" . '<body>' . "\n\n" . '<div class="coming">' . "\n" . '    ';
echo $text;
echo '</div>' . "\n" . '</body>' . "\n" . '</html>';

?>
