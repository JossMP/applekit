<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
if (!function_exists("setTime")) {
    function setTime()
    {
        $ci =& get_instance();
        $time = now($ci->global_data["timeZone"]);
        return date("Y-m-d H:i:s", $time);
    }
}
if (!function_exists("wLimit")) {
    function wLimit($input = "", $maxWords = "", $maxChars = "")
    {
        $words = preg_split("/\\s+/", $input);
        $words = array_slice($words, 0, $maxWords);
        $words = array_reverse($words);
        $chars = 0;
        $truncated = array();
        while (0 < count($words)) {
            $fragment = trim(array_pop($words));
            $chars += strlen($fragment);
            if ($maxChars < $chars) {
                break;
            }
            $truncated[] = $fragment;
        }
        $result = implode($truncated, " ");
        return $result . ($input == $result ? "" : "Hover to see full ...");
    }
}
if (!function_exists("checkVer")) {
    function checkVer($version, $echo = NULL)
    {
        $cver = $version;
        $url = "http://nemoze.net/applver.php";
        $check = json_decode(getCurl($url));
        if ($check == false) {
            return "<small class=\"label label-danger\">There's error happen while checking version !</small>";
        }
        $get = explode(" ", $check[0]);
        $ver = explode(" ", $cver);
        if (!is_null($echo)) {
            if ($ver[0] == $get[0]) {
                if (strval(preg_replace("/\\s+/", "", $get[1])) == strval(preg_replace("/\\s+/", "", $ver[1]))) {
                    return "<div class=\"btn btn-success\">Your up to date " . $check[0] . "</div>";
                }
                return "<div class=\"btn btn-danger\">There's a new version released " . $check[0] . "</div>";
            }
            return "<div class=\"btn btn-danger\">There's a new version released " . $check[0] . "</div>";
        }
        if ($ver[0] == $get[0]) {
            if (strval(preg_replace("/\\s+/", "", $get[1])) == strval(preg_replace("/\\s+/", "", $ver[1]))) {
                return "<small class=\"label label-success\">Your up to date " . $check[0] . "</small>";
            }
            return "<small class=\"label label-danger\">There's a new version released " . $check[0] . "</small>";
        }
        return "<small class=\"label label-danger\">There's a new version released " . $check[0] . "</small>";
    }
}
if (!function_exists("checkVer2")) {
    function checkVer2($version)
    {
        $cver = $version;
        $url = "http://nemoze.com/applver.php";
        $check = json_decode(getCurl($url));
        if ($check == false) {
            return false;
        }
        $get = explode(" ", $check[0]);
        $ver = explode(" ", $cver);
        if ($ver[0] == $get[0]) {
            if (strval(preg_replace("/\\s+/", "", $get[1])) == strval(preg_replace("/\\s+/", "", $ver[1]))) {
                return true;
            }
            return false;
        }
        return false;
    }
}
if (!function_exists("getCurl")) {
    function getCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
if (!function_exists("phpver")) {
    function phpver($ver, $start = "", $end = "", $error = "")
    {
        if ("5.3" <= $ver) {
            return $start . "v" . $ver . $end;
        }
        return $error . "that's a bad choise of php version please upgrade " . $ver . $end;
    }
}
if (!function_exists("getIPimg")) {
    function getIPimg($ip)
    {
        $ip == "::1" ? $ips = "122.212.212.222" : ($ips = $ip);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=" . $ips);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $data = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($data);
        if ($res) {
            return $res->geoplugin_countryCode;
        }
    }
}
if (!function_exists("Jredirect")) {
    function Jredirect($param = "")
    {
        echo "<script>\n            window.location = \"" . site_url($param) . "\";\n            </script>";
    }
}
if (!function_exists("directory_copy")) {
    function directory_copy($srcdir, $dstdir)
    {
        $srcdir = rtrim($srcdir, "/");
        $dstdir = rtrim($dstdir, "/");
        if (!is_dir($dstdir)) {
            mkdir($dstdir, 511, true);
        }
        $dir_map = directory_map($srcdir);
        foreach ($dir_map as $object_key => $object_value) {
            if (is_numeric($object_key)) {
                copy($srcdir . "/" . $object_value, $dstdir . "/" . $object_value);
            } else {
                directory_copy($srcdir . "/" . $object_key, $dstdir . "/" . $object_key);
            }
        }
    }
    function copy2($source, $target)
    {
        if (!is_dir($source)) {
            copy($source, $target);
            return true;
        }
        @mkdir($target);
        $d = dir($source);
        $navFolders = array(".", "..");
        while (false !== ($fileEntry = $d->read())) {
            if (in_array($fileEntry, $navFolders)) {
                continue;
            }
            $s = (string) $source . "/" . $fileEntry;
            $t = (string) $target . "/" . $fileEntry;
            $copys = copy2($s, $t);
        }
        $d->close();
        return true;
    }
}
if (!function_exists("domainName")) {
    function domainName()
    {
        $url = "http://" . $_SERVER["HTTP_HOST"];
        $urlobj = parse_url($url);
        $domain = $urlobj["host"];
        if (preg_match("/(?<domain>[a-z0-9][a-z0-9\\-]{1,63}\\.[a-z\\.]{2,6})\$/i", $domain, $regs)) {
            return $regs["domain"];
        }
        return false;
    }
}
if (!function_exists("ServIp")) {
    function ServIp()
    {
        $host = gethostname();
        return gethostbyname($host);
    }
}
if (!function_exists("checkExpire")) {
    function checkExpire($create, $expire)
    {
        $getexpire = date("Y-m-d H:i:s", strtotime("+" . $expire . " hours", strtotime($create)));
        $checkExpire = strtotime($getexpire);
        $now = now();
        return $now <= $checkExpire ? true : false;
    }
}
if (!function_exists("niceTime")) {
    function niceTime($date)
    {
        return date("D, j M Y - h:i a", strtotime($date));
    }
}
if (!function_exists("testPorts")) {
    function testPorts()
    {
        $host = domainName();
        $ports = array(25, 80, 443, 465, 587, 2525, 8025, 8465);
        foreach ($ports as $port) {
            $connection = @fsockopen($host, $port);
            if (is_resource($connection)) {
                echo "<tr><td class=\"success\"><i class=\"fas fa-check-square fa-lg\" style=\"color: seagreen;\"></i> " . $host . "<strong>:" . $port . " " . "</strong>(" . getservbyport($port, "tcp") . ") is Open.</td></tr>";
                fclose($connection);
            } else {
                echo "<tr><td class=\"danger\"><i class=\"fa fa-times-circle fa-lg\" style=\"color: darkred;\"></i> " . $host . ":<strong>" . $port . "</strong> is Blocked.</td></tr>";
            }
        }
    }
}
if (!function_exists("timezones")) {
    function timezones($timez = NULL)
    {
        $regions = array("Africa" => DateTimeZone::AFRICA, "America" => DateTimeZone::AMERICA, "Antarctica" => DateTimeZone::ANTARCTICA, "Aisa" => DateTimeZone::ASIA, "Atlantic" => DateTimeZone::ATLANTIC, "Europe" => DateTimeZone::EUROPE, "Indian" => DateTimeZone::INDIAN, "Pacific" => DateTimeZone::PACIFIC);
        $timezones = array();
        foreach ($regions as $name => $mask) {
            $zones = DateTimeZone::listIdentifiers($mask);
            foreach ($zones as $timezone) {
                $time = new DateTime(NULL, new DateTimeZone($timezone));
                $ampm = 12 < $time->format("H") ? " (" . $time->format("g:i a") . ")" : "";
                $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . " - " . $time->format("H:i") . $ampm;
            }
        }
        echo "<select id=\"timeZone\" name=\"timeZone\" class=\"select2 form-control show-tick show-menu-arrow\" data-width=\"100%\" data-live-search=\"true\">";
        foreach ($timezones as $region => $list) {
            echo "<optgroup label=\"" . $region . "\">" . "\n";
            foreach ($list as $timezone => $name) {
                echo "<option value=\"" . $timezone . "\"";
                if ($timez == $timezone) {
                    echo "selected ";
                }
                echo ">" . $name . "</option>" . "\n";
            }
            echo "<optgroup>" . "\n";
        }
        echo "</select>";
    }
}
if (!function_exists("getTemplateList")) {
    function getTemplateList($files = false, $type = NULL)
    {
        $directories = glob(APPPATH . "mailTemplate" . "/*", GLOB_ONLYDIR);
        foreach ($directories as $dirs) {
            if ($type == NULL) {
                if ($files == false) {
                    $str = explode("mailTemplate/", $dirs);
                    $str2 = explode("apps/", $dirs);
                    foreach (new DirectoryIterator($dirs . "/idevice") as $file) {
                        if ($file->isFile() && $file != ".DS_Store" && $file != "." && $file != ".." && !is_dir($file) && $file->getFilename() != "index.html") {
                            echo "<option value=\"" . $str2[1] . "/" . replacedash(str_replace(".html", "", $file->getFilename())) . "\">" . replacedash(str_replace(".html", "", $file->getFilename())) . "</option>";
                        }
                    }
                } else {
                    $str = explode("mailTemplate/", $dirs);
                    $str2 = explode("apps/", $dirs);
                    echo "<optgroup label=\"Language => " . strtoupper($str[1]) . "\">";
                    foreach (new DirectoryIterator($dirs) as $file) {
                        if ($file->isFile() && $file != ".DS_Store" && $file != "." && $file != ".." && !is_dir($file) && $file->getFilename() != "index.html") {
                            echo "<option value=\"" . $str2[1] . "/" . $file->getFilename() . "\">" . strtoupper($str[1]) . " => " . replacedash(str_replace(".html", "", $file->getFilename())) . "</option>";
                        }
                    }
                    echo "<optgroup label=\"iDevice => " . strtoupper($str[1]) . "\">";
                    foreach (new DirectoryIterator($dirs . "/idevice") as $file) {
                        if ($file->isFile() && $file != ".DS_Store" && $file != "." && $file != ".." && !is_dir($file) && $file->getFilename() != "index.html") {
                            echo "<option value=\"" . $str2[1] . "/idevice/" . $file->getFilename() . "\">" . strtoupper($str[1]) . " => " . replacedash(str_replace(".html", "", $file->getFilename())) . "</option>";
                        }
                    }
                    echo "</optgroup></optgroup>";
                }
            } else {
                foreach (new DirectoryIterator($dirs . "other") as $file) {
                    if ($file->isFile() && $file != ".DS_Store" && $file != "." && $file != ".." && !is_dir($file) && $file->getFilename() != "index.html") {
                        echo "<option value=\"other/" . $file->getFilename() . "\">" . replacedash(str_replace(".html", "", $file->getFilename())) . "</option>";
                    }
                }
            }
        }
    }
}
if (!function_exists("readFileName")) {
    function readFileName($Path, $type, $ext)
    {
        foreach (new DirectoryIterator(APPPATH . $Path) as $fileInfo) {
            if ($fileInfo->isFile() && $fileInfo != ".DS_Store" && $fileInfo != "." && $fileInfo != ".." && !is_dir($fileInfo) && $fileInfo->getFilename() != "index.html") {
                $file = replacedash(str_replace("." . $ext, "", $fileInfo->getFilename()));
                switch ($type) {
                    case "option":
                        echo "<option value=\"" . $file . "\">" . $file . "</option>";
                        break;
                    default:
                        echo $file . "<br/>";
                        break;
                }
            }
        }
    }
}
if (!function_exists("replacedash")) {
    function replacedash($item)
    {
        $item = str_replace("-", " ", $item);
        return $item;
    }
}
if (!function_exists("replacespace")) {
    function replacespace($item)
    {
        $item = str_replace(" ", "-", $item);
        return $item;
    }
}
if (!function_exists("getTime")) {
    function getTime($time, $format)
    {
        $date = date_create($time);
        return date_format($date, $format);
    }
}
function validBase64($string)
{
    $decoded = base64_decode($string, true);
    if (!preg_match("/^[a-zA-Z0-9\\/\\r\\n+]*={0,2}\$/", $string)) {
        return false;
    }
    if (!base64_decode($string, true)) {
        return false;
    }
    if (base64_encode($decoded) != $string) {
        return false;
    }
    return true;
}
function time_ago($datetime, $full = false)
{
    $today = now();
    $createdday = strtotime($datetime);
    $datediff = abs($today - $createdday);
    $difftext = "";
    $years = floor($datediff / (365 * 60 * 60 * 24));
    $months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $hours = floor($datediff / 3600);
    $minutes = floor($datediff / 60);
    $seconds = floor($datediff);
    if ($difftext == "") {
        if (1 < $years) {
            $difftext = $years . " years ago";
        } else {
            if ($years == 1) {
                $difftext = $years . " year ago";
            }
        }
    }
    if ($difftext == "") {
        if (1 < $months) {
            $difftext = $months . " months ago";
        } else {
            if ($months == 1) {
                $difftext = $months . " month ago";
            }
        }
    }
    if ($difftext == "") {
        if (1 < $days) {
            $difftext = $days . " days ago";
        } else {
            if ($days == 1) {
                $difftext = $days . " day ago";
            }
        }
    }
    if ($difftext == "") {
        if (1 < $hours) {
            $difftext = $hours . " hours ago";
        } else {
            if ($hours == 1) {
                $difftext = $hours . " hour ago";
            }
        }
    }
    if ($difftext == "") {
        if (1 < $minutes) {
            $difftext = $minutes . " minutes ago";
        } else {
            if ($minutes == 1) {
                $difftext = $minutes . " minute ago";
            }
        }
    }
    if ($difftext == "") {
        if (1 < $seconds) {
            $difftext = $seconds . " seconds ago";
        } else {
            if ($seconds == 1) {
                $difftext = $seconds . " second ago";
            }
        }
    }
    return $difftext;
}

?>
