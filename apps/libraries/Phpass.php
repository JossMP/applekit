<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Phpass
{
	protected $PasswordHash;
	protected $iteration_count_log2 = 8;
	protected $portable_hashes = false;

	public function __construct($config = array())
	{
		if (!file_exists($path = dirname(__FILE__) . '/../vendor/PasswordHash.php')) {
			show_error('The phpass class file was not found.');
		}

		include $path;

		if (!empty($config)) {
			$this->initialize($config);
		}

		$this->PasswordHash = new PasswordHash($this->iteration_count_log2, $this->portable_hashes);
	}

	public function initialize($config = array())
	{
		foreach ($config as $key => $val) {
			if ($key != 'PasswordHash') {
				$this->{$key} = $val;
			}
		}
	}

	public function hash($password)
	{
		return $this->PasswordHash->HashPassword($password);
	}

	public function check($password, $stored_hash)
	{
		return $this->PasswordHash->CheckPassword($password, $stored_hash);
	}

	public function __call($name, $arguments)
	{
		return call_user_func_array(array($this->PasswordHash, $name), $arguments);
	}

	public function __get($name)
	{
		return $this->PasswordHash->{$name};
	}
}

defined('BASEPATH') || exit('No direct script access allowed');

?>
