<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Extra
{
	private $username;
	private $password;
	public $devices = array();
	public $infos;
	private $ci;
	private $loggedIn;
	private $email_updates = true;
	private $host = 'fmipmobile.icloud.com';
	private $scope;
	private $client_context = array('appName' => 'FindMyiPhone', 'appVersion' => '4.0', 'buildVersion' => '472.1', 'clientTimestamp' => 0, 'deviceUDID' => NULL, 'inactiveTime' => 1, 'osVersion' => '11.0.1', 'productType' => 'iPhone8,1');
	private $server_context = array('callbackIntervalInMS' => 10000, 'classicUser' => false, 'clientId' => NULL, 'cloudUser' => true, 'deviceLoadStatus' => '203', 'enableMapStats' => true, 'isHSA' => true, 'lastSessionExtensionTime' => NULL, 'maxCallbackIntervalInMS' => 60000, 'minTrackLocThresholdInMts' => 100, 'macCount' => 0, 'maxDeviceLoadTime' => 60000, 'maxLocatingTime' => 999999, 'preferredLanguage' => 'en-us', 'useAuthWidget' => true, 'prefsUpdateTime' => 0, 'sessionLifespan' => 900000, 'timezone' => NULL, 'trackInfoCacheDurationInSecs' => 86400, 'validRegion' => true);

	public function __construct($param)
	{
		if (!extension_loaded('curl')) {
			throw new ExtraException('PHP extension cURL is not loaded.');
		}

		$this->ci = &get_instance();
		$this->username = $param['username'];
		$this->password = $param['password'];
		$this->init_client();
		$logged = $this->login();

		if (!$logged) {
			return $this->loggedIn = false;
		}

		return $this->loggedIn = (object) $logged;
	}

	private function login()
	{
		$url = 'https://setup.icloud.com/setup/ws/1/login';
		$data = array('apple_id' => $this->username, 'password' => $this->password, 'extended_login' => false);
		$response = $this->request($url, json_encode($data));
		$result = $response[0];
		$result_with_headers = $response[1];
		$res = json_decode($result, true);

		if (isset($res['error'])) {
			return false;
		}

		$this->host = $res['webservices']['findme']['url'];
		$this->cookies = 'Cookie: ' . $this->parseCookies($result_with_headers);
		$this->refresh_client();
		return $res;
	}

	public function parseCookies($headers)
	{
		preg_match_all('/^Set-Cookie:\\s*([^;]*)/mi', $headers, $matches);
		$cookies = '';

		foreach ($matches[0] as $value) {
			$value = str_replace('Set-Cookie: ', '', $value);
			$cookies .= $value . '; ';
		}

		return $cookies;
	}

	public function loggedIn()
	{
		return $this->loggedIn;
	}

	public function set_email_updates($email_updates)
	{
		$this->email_updates = (bool) $email_updates;
	}

	private function init_client()
	{
		$post_data = json_encode(array('clientContext' => $this->client_context));
		$headers = $this->parse_curl_headers($this->make_request('initClient', $post_data, true));

		if (strpos($headers['http_code'], '200') === false) {
			throw new ExtraException('Wrong Password');
		}

		$this->host = isset($headers['X-Apple-MMe-Host']) ? $headers['X-Apple-MMe-Host'] : $this->host;
		$this->scope = isset($headers['X-Apple-MMe-Scope']) ? $headers['X-Apple-MMe-Scope'] : '';
		$this->refresh_client();
	}

	public function refresh_client()
	{
		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context));

		foreach (json_decode($this->make_request('refreshClient', $post_data))->content as $id => $device) {
			$this->devices[$id] = $device;
		}
	}

	public function play_sound($device_id, $subject = 'Find My iPhone Alert')
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($subject)) {
			throw new ExtraException('Expected $subject to be a string');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'subject' => $subject));
		return json_decode($this->make_request('playSound', $post_data))->content[0]->snd;
	}

	public function remove_device($device_id)
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		$i = 0;
		5 < $i;

		while ($i++) {
			sleep(1);
			$this->refresh_client();
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id));
		return json_decode($this->make_request('remove', $post_data));
	}

	public function send_message($device_id, $text, $sound = false, $subject = 'Important Message')
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($text)) {
			throw new ExtraException('Expected $text to be a string');
		}

		if (!is_bool($sound)) {
			throw new ExtraException('Expected $sound to be a bool');
		}

		if (!is_string($subject)) {
			throw new ExtraException('Expected $subject to be a string');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'emailUpdates' => $this->email_updates, 'sound' => $sound, 'subject' => $subject, 'text' => $text, 'userText' => true));
		return json_decode($this->make_request('sendMessage', $post_data))->content[0]->msg;
	}

	public function lost_device($device_id, $passcode, $owner_phone_number = '911', $sound = true, $text = 'This iPhone has been lost. Please call me.')
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($passcode)) {
			throw new ExtraException('Expected $passcode to be a string');
		}

		if (strlen($passcode) !== 4) {
			throw new ExtraException('Expected $passcode to be 4 characters long');
		}

		if (!is_string($owner_phone_number)) {
			throw new ExtraException('Expected $owner_phone_number to be a string');
		}

		if (!is_bool($sound)) {
			throw new ExtraException('Expected $sound to be a bool');
		}

		if (!is_string($text)) {
			throw new ExtraException('Expected $text to be a string');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'emailUpdates' => $this->email_updates, 'lostModeEnabled' => true, 'ownerNbr' => $owner_phone_number, 'passcode' => $passcode, 'sound' => $sound, 'text' => $text, 'trackingEnabled' => true, 'userText' => true));
		return json_decode($this->make_request('lostDevice', $post_data))->content[0]->lostDevice;
	}

	public function notify_when_found($device_id, $notify = true)
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($notify)) {
			throw new ExtraException('Expected $notify to be a boolean');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'lostModeEnabled' => $notify));
		return json_decode($this->make_request('saveLocFoundPref', $post_data))->content[0]->locFoundEnabled;
	}

	public function lock_and_message($device_id, $passcode, $text, $sound = true, $title = 'Find My iPhone Alert')
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($passcode)) {
			throw new ExtraException('Expected $passcode to be a string');
		}

		if (strlen($passcode) !== 4) {
			throw new ExtraException('Expected $passcode to be 4 characters long');
		}

		if (!is_string($text)) {
			throw new ExtraException('Expected $text to be a string');
		}

		if (!is_bool($sound)) {
			throw new ExtraException('Expected $sound to be a bool');
		}

		if (!is_string($title)) {
			throw new ExtraException('Expected $title to be a string');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'emailUpdates' => $this->email_updates, 'passcode' => $passcode, 'sound' => $sound, 'text' => $text, 'title' => $title, 'userText' => true));
		return json_decode($this->make_request('lockAndMessage', $post_data))->content[0]->remoteLock;
	}

	public function remote_lock($device_id, $passcode)
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($passcode)) {
			throw new ExtraException('Expected $passcode to be a string');
		}

		if (strlen($passcode) !== 4) {
			throw new ExtraException('Expected $passcode to be 4 characters long');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'emailUpdates' => $this->email_updates, 'passcode' => $passcode));
		return json_decode($this->make_request('remoteLock', $post_data))->content[0]->remoteLock;
	}

	public function remote_wipe($device_id, $passcode, $text)
	{
		if (!is_string($device_id)) {
			throw new ExtraException('Expected $device_id to be a string');
		}

		if (!is_string($passcode)) {
			throw new ExtraException('Expected $passcode to be a string');
		}

		if (strlen($passcode) !== 4) {
			throw new ExtraException('Expected $passcode to be 4 characters long');
		}

		if (!is_string($text)) {
			throw new ExtraException('Expected $text to be a string');
		}

		$post_data = json_encode(array('clientContext' => $this->client_context, 'serverContext' => $this->server_context, 'device' => $device_id, 'passcode' => $passcode, 'text' => $text, 'emailUpdates' => $this->email_updates));
		return json_decode($this->make_request('remoteWipe', $post_data))->content[0]->remoteWipe;
	}

	public function locate_device($device, $timeout = 120)
	{
		if (!is_integer($device)) {
			throw new ExtraException('Expected $device to be an integer');
		}

		if (!isset($this->devices[$device])) {
			$this->refresh_client();
		}

		$start = time();

		while (!$this->devices[$device]->location->locationFinished) {
			if (intval($timeout) < (time() - $start)) {
				throw new ExtraException('Failed to locate device! Request timed out.');
			}

			sleep(5);
			$this->refresh_client();
		}

		return $this->devices[$device]->location;
	}

	private function make_request($method, $post_data, $return_headers = false, $headers = array())
	{
		if (!is_string($method)) {
			throw new ExtraException('Expected $method to be a string');
		}

		if (!$this->is_json($post_data)) {
			throw new ExtraException('Expected $post_data to be json');
		}

		if (!is_array($headers)) {
			throw new ExtraException('Expected $headers to be an array');
		}

		if (!is_bool($return_headers)) {
			throw new ExtraException('Expected $return_headers to be a bool');
		}

		if (!isset($this->scope)) {
			$this->scope = $this->username;
		}

		array_push($headers, 'Accept-Language: en-us');
		array_push($headers, 'Content-Type: application/json; charset=utf-8');
		array_push($headers, 'X-Apple-Realm-Support: 1.0');
		array_push($headers, 'X-Apple-Find-Api-Ver: 3.0');
		array_push($headers, 'X-Apple-Authscheme: UserIdGuest');
		$curl = curl_init();
		curl_setopt_array($curl, array(CURLOPT_TIMEOUT => 120, CURLOPT_CONNECTTIMEOUT => 120, CURLOPT_SSL_VERIFYPEER => true, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => false, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $post_data, CURLOPT_HTTPHEADER => $headers, CURLOPT_HEADER => $return_headers, CURLOPT_URL => sprintf('https://%s/fmipservice/device/%s/%s', $this->host, $this->scope, $method), CURLOPT_USERPWD => $this->username . ':' . $this->password, CURLOPT_USERAGENT => 'FindMyiPhone/472.1 CFNetwork/711.1.12 Darwin/14.0.0'));
		$http_result = curl_exec($curl);
		$this->infos = json_decode($http_result, true);
		curl_close($curl);
		return $http_result;
	}

	private function parse_curl_headers($response)
	{
		$headers = array();

		foreach (explode("\r\n", substr($response, 0, strpos($response, "\r\n\r\n"))) as $i => $line) {
			if ($i === 0) {
				$headers['http_code'] = $line;
			}
			else {
				list($key, $value) = explode(': ', $line);
				$headers[$key] = $value;
			}
		}

		return $headers;
	}

	public function request($url, $data)
	{
		$this->ci = &get_instance();
		$headers = array('Origin: https://www.icloud.com ', 'User-Agent: ' . $this->ci->agent->agent_string() . ' ', 'Content-Type: text/plain ', 'Accept: */*', 'Referer: https://www.icloud.com/ ', 'Accept-Encoding: gzip, deflate ', 'Accept-Language: en-US,en;q=0.8 ');
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		$result = curl_exec($ch);
		$res = json_decode($result, true);

		if (!isset($res['error'])) {
			curl_setopt($ch, CURLOPT_HEADER, true);
			$result_with_headers = curl_exec($ch);
			return array($result, $result_with_headers);
		}

		return array($result, NULL);
	}

	private function is_json($var)
	{
		json_decode($var);
		return json_last_error() == JSON_ERROR_NONE;
	}

	private function rand_array($type = false)
	{
		if ($type == false) {
			$ios = array('11.1.1', '11.1', '11.0.1', '11.0.2', '11.0.3', '10.3.3', '10.3.2', '10.3.1', '10.3');
			return array_rand($ios, 1);
		}

		$idevice = array('iPhone10,4', 'iPhone10,5', 'iPhone10,6', 'iPhone8,2', 'iPhone8,1', 'iPhone8,4', 'iPhone9,1', 'iPhone9,2', 'iPhone7,2', 'iPhone7,1');
		return array_rand($idevice, 1);
	}
}

class ExtraException
{}

defined('BASEPATH') || exit('No direct script access allowed');

?>
