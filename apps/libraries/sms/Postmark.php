<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Postmark
{
	public function send($apikey, $from, $to, $subject, $body)
	{
		try {
			$client = new Postmark\PostmarkClient($apikey);
			$send = $client->sendEmail($from, $to, $subject, $body);

			if ($send['errorcode'] == 0) {
				$data = array('status' => true, 'msg' => 'Email sent');
			}
			else {
				$data = array('status' => false, 'msg' => $send['message']);
			}

			return $data;
		}
		catch (Postmark\Models\PostmarkException $ex) {
			$data = array('status' => false, 'msg' => $ex->message);
			return $data;
		}
		catch (Exception $generalException) {
		}
	}
}


?>
