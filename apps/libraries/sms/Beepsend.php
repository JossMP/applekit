<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Beepsend
{
	public function send($token, $to, $from, $msg)
	{
		$data = array('to' => $to, 'from' => $from, 'body' => $msg, 'encoding' => 'UTF-8', 'receive_dlr' => 0);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.beepsend.com/2/send/');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array();
		$headers[] = 'Authorization: Token ' . $token;
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}

		curl_close($ch);
		$result = json_decode($result, true);

		if ($result['errors']) {
			$res = array('status' => false, 'msg' => $result['errors'][0]);
			return $res;
		}

		$res = array('status' => true, 'msg' => $result[0]['id'][0]);
		return $res;
	}
}


?>
