<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Viasms
{
	public function sendsms($Username, $Password, $MsgSender, $DestinationAddress, $Message)
	{
		$url = 'http://smsc.vianett.no/ActiveServer/MT/?username=%s&password=%s&destinationaddr=%s&message=%s&refno=1';
		$url = sprintf($url, urlencode($Username), urlencode($Password), urlencode($DestinationAddress), urlencode($Message));

		if (is_numeric($MsgSender)) {
			$url .= '&sourceAddr=' . $MsgSender;
		}
		else {
			$url .= '&fromAlpha=' . $MsgSender;
		}

		$XMLResponse = $this->GetResponseAsXML($url);
		$Result = $this->ParseXMLResponse($XMLResponse);
		return $Result;
	}

	private function GetResponseAsXML($url)
	{
		try {
			return simplexml_load_file($url);
		}
		catch (Exception $e) {
			throw new Exception('Error occured while connecting to server: ' . $e->getMessage());
		}
	}

	private function ParseXMLResponse($XMLResponse)
	{
		$Result = new Result();
		$Result->ErrorCode = $XMLResponse[0]['errorcode'];
		$Result->ErrorMessage = $XMLResponse[0];
		$Result->Success = $XMLResponse[0]['errorcode'] == 0;
		return $Result;
	}
}

class Result
{
	public $Success;
	public $ErrorCode;
	public $ErrorMessage;
}


?>
