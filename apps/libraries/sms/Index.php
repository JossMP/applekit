<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

include_once 'ViaNettSMS.php';
$Username = 'YourUsername';
$Password = 'YourPassword';
$MsgSender = 'YourSender, example: a phone number like 4790000000';
$DestinationAddress = 'Receiver - A phone number';
$Message = 'Hello World!';
$ViaNettSMS = new ViaNettSMS($Username, $Password);

try {
	$Result = $ViaNettSMS->SendSMS($MsgSender, $DestinationAddress, $Message);

	if ($Result->Success == true) {
		$Message = 'Message successfully sent!';
	}
	else {
		$Message = 'Error occured while sending SMS<br />Errorcode: ' . $Result->ErrorCode . '<br />Errormessage: ' . $Result->ErrorMessage;
	}
}
catch (Exception $e) {
	$Message = $e->getMessage();
}

echo "\t\t\t" . '<p><strong>SMS Result</strong><br />Status: ' . $Message . '</p>';
echo "\t" . '</body>' . "\r\n" . '</html>';

?>
