<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Plivo
{
	public function __construct()
	{
	}

	public function send($auth_id, $auth_token, $data)
	{
		$p = new Plivo\RestAPI($auth_id, $auth_token);
		$params = array('src' => $data['src'], 'dst' => $data['dst'], 'text' => $data['text']);
		$response = $p->send_message($params);
		return $response['response'];
	}
}


?>
