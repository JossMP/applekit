<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Budget
{
	public function send($data)
	{
		$url = 'https://api.budgetsms.net/sendsms/?';
		$request = $this->reqest($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			return curl_error($ch);
		}

		curl_close($ch);
		return $result;
	}

	public function reqest($data)
	{
		$req = '';

		foreach ($data as $k => $v) {
			$req .= urlencode($k) . '=' . urlencode($v) . '&';
		}

		$req = rtrim($req, '&');
		return $req;
	}
}


?>
