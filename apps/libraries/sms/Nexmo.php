<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Nexmo
{
	public function send($key, $secret, $to, $from, $msg)
	{
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query(array('api_key' => $key, 'api_secret' => $secret, 'to' => $to, 'from' => $from, 'text' => $msg));
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$decoded_response = json_decode($response, true);
		return $decoded_response;
	}
}


?>
