<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Sinch
{
	public function send($key, $secret, $to, $from, $msg)
	{
		$key = $key;
		$secret = $secret;
		$phone_number = $to;
		$user = 'application\\' . $key . ':' . $secret;
		$message = array('message' => $msg, 'from' => $from);
		$data = json_encode($message);
		$ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_USERPWD, $user);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			return 'Curl error: ' . curl_error($ch);
		}

		return json_decode($result, true);
	}
}


?>
