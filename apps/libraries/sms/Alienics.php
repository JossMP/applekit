<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Alienics
{
	public function send($username, $password, $to, $from, $msg)
	{
		$post_fields = array('username' => $username, 'password' => $password, 'from' => $from, 'to' => $to, 'text' => $msg, 'type' => 'unicode');
		$param = $this->make_post_body($post_fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://192.99.47.166/sms/xml?' . $param);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}

		curl_close($ch);
		$xml = simplexml_load_string($result);
		$json = json_encode($xml);
		$arr = json_decode($json, true);

		if ($arr['messages']['message']['status'] == 0) {
			$data = array('status' => true, 'msg' => 'SMS Sent');
		}
		else {
			$data = array('status' => false, 'msg' => $arr['messages']['message']['error-text']);
		}

		return $data;
	}

	public function make_post_body($post_fields)
	{
		$stop_dup_id = $this->make_stop_dup_id();

		if (0 < $stop_dup_id) {
			$post_fields['stop_dup_id'] = $this->make_stop_dup_id();
		}

		$post_body = '';

		foreach ($post_fields as $key => $value) {
			$post_body .= urlencode($key) . '=' . urlencode($value) . '&';
		}

		$post_body = rtrim($post_body, '&');
		return $post_body;
	}

	public function make_stop_dup_id()
	{
		return 0;
	}
}


?>
