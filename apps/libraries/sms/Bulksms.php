<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Bulksms
{
	public function formatted_server_response($result)
	{
		$this_result = '';

		if ($result['success']) {
			$this_result .= 'Success: batch ID ' . $result['api_batch_id'] . 'API message: ' . $result['api_message'] . "\n" . 'Full details ' . $result['details'];
		}
		else {
			$this_result .= 'Fatal error: HTTP status ' . $result['http_status_code'] . ', API status ' . $result['api_status_code'] . ' API message ' . $result['api_message'] . ' full details ' . $result['details'];

			if ($result['transient_error']) {
				$this_result .= 'This is a transient error - you should retry it in a production environment';
			}
		}

		return $this_result;
	}

	public function send_message($data)
	{
		$url = 'https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0';
		$post_body = $this->unicode_sms($data['username'], $data['password'], $data['msg'], $data['msisdn'], $data['sender']);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		$response_string = curl_exec($ch);
		$curl_info = curl_getinfo($ch);
		$sms_result = array();
		$sms_result['success'] = 0;
		$sms_result['details'] = '';
		$sms_result['transient_error'] = 0;
		$sms_result['http_status_code'] = $curl_info['http_code'];
		$sms_result['api_status_code'] = '';
		$sms_result['api_message'] = '';
		$sms_result['api_batch_id'] = '';

		if ($response_string == false) {
			$sms_result .= 'details';
		}
		else if ($curl_info['http_code'] != 200) {
			$sms_result['transient_error'] = 1;
			$sms_result .= 'details';
		}
		else {
			$sms_result .= 'details';
			$api_result = explode('|', $response_string);
			$status_code = $api_result[0];
			$sms_result['api_status_code'] = $status_code;
			$sms_result['api_message'] = $api_result[1];

			if (count($api_result) != 3) {
				$sms_result .= 'details';
			}
			else if ($status_code == '0') {
				$sms_result['success'] = 1;
				$sms_result['api_batch_id'] = $api_result[2];
				$sms_result .= 'details';
			}
			else if ($status_code == '1') {
				$sms_result['success'] = 1;
				$sms_result['api_batch_id'] = $api_result[2];
			}
			else {
				$sms_result .= 'details';
			}
		}

		curl_close($ch);
		return $sms_result;
	}

	public function seven_bit_sms($username, $password, $message, $msisdn)
	{
		$post_fields = array('username' => $username, 'password' => $password, 'message' => $this->character_resolve($message), 'msisdn' => $msisdn, 'allow_concat_text_sms' => 0, 'concat_text_sms_max_parts' => 2);
		return $this->make_post_body($post_fields);
	}

	public function unicode_sms($username, $password, $message, $msisdn, $sender)
	{
		$post_fields = array('username' => $username, 'password' => $password, 'message' => $this->string_to_utf16_hex($message), 'msisdn' => $msisdn, 'sender' => $sender, 'dca' => '16bit');
		return $this->make_post_body($post_fields);
	}

	public function eight_bit_sms($username, $password, $message, $msisdn)
	{
		$post_fields = array('username' => $username, 'password' => $password, 'message' => $message, 'msisdn' => $msisdn, 'dca' => '8bit');
		return $this->make_post_body($post_fields);
	}

	public function make_post_body($post_fields)
	{
		$stop_dup_id = $this->make_stop_dup_id();

		if (0 < $stop_dup_id) {
			$post_fields['stop_dup_id'] = $this->make_stop_dup_id();
		}

		$post_body = '';

		foreach ($post_fields as $key => $value) {
			$post_body .= urlencode($key) . '=' . urlencode($value) . '&';
		}

		$post_body = rtrim($post_body, '&');
		return $post_body;
	}

	public function character_resolve($body)
	{
		$special_chrs = array('Δ' => '0xD0', 'Φ' => '0xDE', 'Γ' => '0xAC', 'Λ' => '0xC2', 'Ω' => '0xDB', 'Π' => '0xBA', 'Ψ' => '0xDD', 'Σ' => '0xCA', 'Θ' => '0xD4', 'Ξ' => '0xB1', '¡' => '0xA1', '£' => '0xA3', '¤' => '0xA4', '¥' => '0xA5', '§' => '0xA7', '¿' => '0xBF', 'Ä' => '0xC4', 'Å' => '0xC5', 'Æ' => '0xC6', 'Ç' => '0xC7', 'É' => '0xC9', 'Ñ' => '0xD1', 'Ö' => '0xD6', 'Ø' => '0xD8', 'Ü' => '0xDC', 'ß' => '0xDF', 'à' => '0xE0', 'ä' => '0xE4', 'å' => '0xE5', 'æ' => '0xE6', 'è' => '0xE8', 'é' => '0xE9', 'ì' => '0xEC', 'ñ' => '0xF1', 'ò' => '0xF2', 'ö' => '0xF6', 'ø' => '0xF8', 'ù' => '0xF9', 'ü' => '0xFC');
		$ret_msg = '';

		if (mb_detect_encoding($body, 'UTF-8') != 'UTF-8') {
			$body = utf8_encode($body);
		}

		$i = 0;

		while ($i < mb_strlen($body, 'UTF-8')) {
			$c = mb_substr($body, $i, 1, 'UTF-8');

			if (isset($special_chrs[$c])) {
				$ret_msg .= chr($special_chrs[$c]);
			}
			else {
				$ret_msg .= $c;
			}

			++$i;
		}

		return $ret_msg;
	}

	public function make_stop_dup_id()
	{
		return 0;
	}

	public function string_to_utf16_hex($string)
	{
		return bin2hex(mb_convert_encoding($string, 'UTF-16', 'UTF-8'));
	}

	public function xml_to_wbxml($msg_body)
	{
		$wbxmlfile = 'xml2wbxml_' . md5(uniqid(time())) . '.wbxml';
		$xmlfile = 'xml2wbxml_' . md5(uniqid(time())) . '.xml';
		$fp = fopen($xmlfile, 'w+');
		fwrite($fp, $msg_body);
		fclose($fp);
		exec(xml2wbxml . ' -v 1.2 -o ' . $wbxmlfile . ' ' . $xmlfile . ' 2>/dev/null');

		if (!file_exists($wbxmlfile)) {
			print_ln('Fatal error: xml2wbxml conversion failed');
			return false;
		}

		$wbxml = trim(file_get_contents($wbxmlfile));
		unlink($xmlfile);
		unlink($wbxmlfile);
		return $wbxml;
	}
}


?>
