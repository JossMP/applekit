<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Nimbow
{
	public function send($key, $to, $from, $msg)
	{
		$sms_parameter = array('to' => $to, 'text' => $msg, 'from' => $from, 'test' => 0, 'getmessageid' => 1, 'GetMessageParts' => 1, 'GetFrom' => 1, 'GetTo' => 1, 'GetCost' => 1, 'GetDeliveryReport' => 1);
		$curl = curl_init();
		curl_setopt_array($curl, array(CURLOPT_URL => 'https://api.nimbow.com/sms?key=' . $key . '&format=json', CURLOPT_RETURNTRANSFER => 1, CURLOPT_TIMEOUT => 100, CURLINFO_HTTP_CODE => true, CURLOPT_POSTFIELDS => $sms_parameter));
		$result = json_decode(curl_exec($curl));
		curl_close($curl);
		return $result;
	}

	public function errorMessage($StatusCode)
	{
		if ($StatusCode == 0) {
			return 'success';
		}

		if ($StatusCode == 1) {
			return 'An error has occurred in the platform while processing this message';
		}

		if ($StatusCode == 2) {
			return 'An undocumented error occurred';
		}

		if ($StatusCode == 3) {
			return 'Your account does not have sufficient fund to process this message';
		}

		if ($StatusCode == 4) {
			return 'A valid URL to call the API must be used';
		}

		if ($StatusCode == 5) {
			return 'The provided API key could be wrong or the IP Addressed used is not configured to be allowed to use the API.';
		}

		if ($StatusCode == 101) {
			return 'The request contains no receiver information';
		}

		if ($StatusCode == 102) {
			return 'The request contains no message content';
		}

		if ($StatusCode == 200) {
			return 'The numeric sender is allowed to contain max. 16 digits.';
		}

		if ($StatusCode == 203) {
			return 'The message text must contain only GSM characters. If non GSM character shall be sent, please use ‘type=unicode’.';
		}

		if ($StatusCode == 204) {
			return 'The alpha numeric sender is allowed to have 1 to 11 characters.';
		}

		if ($StatusCode == 205) {
			return 'The receiver must have a length between 5 and 16 digits.';
		}

		if ($StatusCode == 209) {
			return 'Wrong type given. See documentation of API Call ‘sms’.';
		}

		if ($StatusCode == 211) {
			return 'Using type=Unicode requires the message text to be hex encoded';
		}

		return 'error';
	}
}


?>
