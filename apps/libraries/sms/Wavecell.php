<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Wavecell
{
	public function __construct($data = array())
	{
		$this->id = $data['id'];
		$this->sub = $data['sub'];
		$this->password = $data['pass'];
		$this->dest = $data['dest'];
		$this->source = $data['source'];
		$this->body = $data['body'];
	}

	public function send()
	{
		$request = array('AccountId' => $this->id, 'SubAccountId' => $this->sub, 'Password' => $this->password, 'Destination' => $this->dest, 'Source' => $this->source, 'Body' => $this->body, 'UMID' => '', 'Encoding' => 'utf-8', 'ScheduledDateTime' => '');
		$body = $this->make_post_body($request);
		$curl = curl_init();
		curl_setopt_array($curl, array(CURLOPT_URL => 'https://wms1.wavecell.com/Send.asmx/SendMT?' . $body, CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => '', CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => 'GET'));
		$response = curl_exec($curl);
		$err = curl_getinfo($curl);
		curl_close($curl);

		if ($err['http_code'] != '200') {
			$res = array('status' => false, 'msg' => $response);
			return $res;
		}

		$res = array('status' => true, 'msg' => $response);
		return $res;
	}

	public function make_post_body($post_fields)
	{
		$stop_dup_id = $this->make_stop_dup_id();

		if (0 < $stop_dup_id) {
			$post_fields['stop_dup_id'] = $this->make_stop_dup_id();
		}

		$post_body = '';

		foreach ($post_fields as $key => $value) {
			$post_body .= urlencode($key) . '=' . urlencode($value) . '&';
		}

		$post_body = rtrim($post_body, '&');
		return $post_body;
	}

	public function make_stop_dup_id()
	{
		return 0;
	}
}


?>
