<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Cmsms
{
	static public function buildMessageXml($key, $recipient, $message, $to)
	{
		$xml = new SimpleXMLElement('<MESSAGES/>');
		$authentication = $xml->addChild('AUTHENTICATION');
		$authentication->addChild('PRODUCTTOKEN', $key);
		$msg = $xml->addChild('MSG');
		$msg->addChild('FROM', $to);
		$msg->addChild('TO', $recipient);
		$msg->addChild('BODY', $message);
		return $xml->asXML();
	}

	static public function sendMessage($key, $recipient, $message, $to)
	{
		$xml = self::buildMessageXml($key, $recipient, $message, $to);
		$ch = curl_init();
		curl_setopt_array($ch, array(
	CURLOPT_URL            => 'https://sgw01.cm.nl/gateway.ashx',
	CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
	CURLOPT_POST           => true,
	CURLOPT_POSTFIELDS     => $xml,
	CURLOPT_RETURNTRANSFER => true
	));
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public function send($key, $recipient, $message, $to)
	{
		$url = 'https://gw.cmtelecom.com/v1.0/message';
		$data = array(
			'messages' => array(
				'authentication' => array('producttoken' => $key),
				'msg'            => array(
					array(
						'from' => $to,
						'to'   => array(
							array('number' => $recipient)
							),
						'body' => array('content' => $message)
						)
					)
				)
			);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			return curl_error($ch);
		}

		curl_close($ch);
		return json_decode($result, true);
	}
}


?>
