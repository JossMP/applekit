<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Blade
{
	public function __construct()
	{
		$path = array(APPPATH . 'views/');
		$cachePath = APPPATH . 'cache/views';
		$compiler = new Xiaoler\Blade\Compilers\BladeCompiler($cachePath);
		$engine = new Xiaoler\Blade\Engines\CompilerEngine($compiler);
		$finder = new Xiaoler\Blade\FileViewFinder($path);
		$finder->addExtension('php');
		$this->factory = new Xiaoler\Blade\Factory($engine, $finder);
	}

	public function view($path, $vars = array())
	{
		echo $this->factory->make($path, $vars);
	}

	public function exists($path)
	{
		return $this->factory->exists($path);
	}

	public function share($key, $value)
	{
		return $this->factory->share($key, $value);
	}

	public function render($path, $vars = array())
	{
		return $this->factory->make($path, $vars)->render();
	}
}

defined('BASEPATH') || exit('No direct script access allowed');

?>
