<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Aes
{
	static private $sBox = array(99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22);
	static private $rCon = array(
		array(0, 0, 0, 0),
		array(1, 0, 0, 0),
		array(2, 0, 0, 0),
		array(4, 0, 0, 0),
		array(8, 0, 0, 0),
		array(16, 0, 0, 0),
		array(32, 0, 0, 0),
		array(64, 0, 0, 0),
		array(128, 0, 0, 0),
		array(27, 0, 0, 0),
		array(54, 0, 0, 0)
		);

	static public function cipher($input, $w)
	{
		$Nb = 4;
		$Nr = (count($w) / $Nb) - 1;
		$state = array();
		$i = 0;

		while ($i < (4 * $Nb)) {
			$state[$i % 4][floor($i / 4)] = $input[$i];
			++$i;
		}

		$state = self::addRoundKey($state, $w, 0, $Nb);
		$round = 1;

		while ($round < $Nr) {
			$state = self::subBytes($state, $Nb);
			$state = self::shiftRows($state, $Nb);
			$state = self::mixColumns($state, $Nb);
			$state = self::addRoundKey($state, $w, $round, $Nb);
			++$round;
		}

		$state = self::subBytes($state, $Nb);
		$state = self::shiftRows($state, $Nb);
		$state = self::addRoundKey($state, $w, $Nr, $Nb);
		$output = array(4 * $Nb);
		$i = 0;

		while ($i < (4 * $Nb)) {
			$output[$i] = $state[$i % 4][floor($i / 4)];
			++$i;
		}

		return $output;
	}

	static private function addRoundKey($state, $w, $rnd, $Nb)
	{
		$r = 0;

		while ($r < 4) {
			$c = 0;

			while ($c < $Nb) {
				$state[$r] ^= $c;
				++$c;
			}

			++$r;
		}

		return $state;
	}

	static private function subBytes($s, $Nb)
	{
		$r = 0;

		while ($r < 4) {
			$c = 0;

			while ($c < $Nb) {
				$s[$r][$c] = self::$sBox[$s[$r][$c]];
				++$c;
			}

			++$r;
		}

		return $s;
	}

	static private function shiftRows($s, $Nb)
	{
		$t = array(4);
		$r = 1;

		while ($r < 4) {
			$c = 0;

			while ($c < 4) {
				$t[$c] = $s[$r][($c + $r) % $Nb];
				++$c;
			}

			$c = 0;

			while ($c < 4) {
				$s[$r][$c] = $t[$c];
				++$c;
			}

			++$r;
		}

		return $s;
	}

	static private function mixColumns($s, $Nb)
	{
		$c = 0;

		while ($c < 4) {
			$a = array(4);
			$b = array(4);
			$i = 0;

			while ($i < 4) {
				$a[$i] = $s[$i][$c];
				$b[$i] = $s[$i][$c] & 128 ? ($s[$i][$c] << 1) ^ 283 : $s[$i][$c] << 1;
				++$i;
			}

			$s[0][$c] = $b[0] ^ $a[1] ^ $b[1] ^ $a[2] ^ $a[3];
			$s[1][$c] = $a[0] ^ $b[1] ^ $a[2] ^ $b[2] ^ $a[3];
			$s[2][$c] = $a[0] ^ $a[1] ^ $b[2] ^ $a[3] ^ $b[3];
			$s[3][$c] = $a[0] ^ $b[0] ^ $a[1] ^ $a[2] ^ $b[3];
			++$c;
		}

		return $s;
	}

	static public function keyExpansion($key)
	{
		$Nb = 4;
		$Nk = count($key) / 4;
		$Nr = $Nk + 6;
		$w = array();
		$temp = array();
		$i = 0;

		while ($i < $Nk) {
			$r = array($key[4 * $i], $key[(4 * $i) + 1], $key[(4 * $i) + 2], $key[(4 * $i) + 3]);
			$w[$i] = $r;
			++$i;
		}

		$i = $Nk;

		while ($i < ($Nb * ($Nr + 1))) {
			$w[$i] = array();
			$t = 0;

			while ($t < 4) {
				$temp[$t] = $w[$i - 1][$t];
				++$t;
			}

			if (($i % $Nk) == 0) {
				$temp = self::subWord(self::rotWord($temp));
				$t = 0;

				while ($t < 4) {
					$temp ^= $t;
					++$t;
				}
			}
			else if ((6 < $Nk) && (($i % $Nk) == 4)) {
				$temp = self::subWord($temp);
			}

			$t = 0;

			while ($t < 4) {
				$w[$i][$t] = $w[$i - $Nk][$t] ^ $temp[$t];
				++$t;
			}

			++$i;
		}

		return $w;
	}

	static private function subWord($w)
	{
		$i = 0;

		while ($i < 4) {
			$w[$i] = self::$sBox[$w[$i]];
			++$i;
		}

		return $w;
	}

	static private function rotWord($w)
	{
		$tmp = $w[0];
		$i = 0;

		while ($i < 3) {
			$w[$i] = $w[$i + 1];
			++$i;
		}

		$w[3] = $tmp;
		return $w;
	}
}

class AesCtr extends Aes
{
	static $sBox = array(99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22);
	static $rCon = array(
		array(0, 0, 0, 0),
		array(1, 0, 0, 0),
		array(2, 0, 0, 0),
		array(4, 0, 0, 0),
		array(8, 0, 0, 0),
		array(16, 0, 0, 0),
		array(32, 0, 0, 0),
		array(64, 0, 0, 0),
		array(128, 0, 0, 0),
		array(27, 0, 0, 0),
		array(54, 0, 0, 0)
		);

	static public function encrypt($plaintext, $password, $nBits)
	{
		$blockSize = 16;
		if (!(($nBits == 128) || ($nBits == 192) || ($nBits == 256))) {
			return '';
		}

		$nBytes = $nBits / 8;
		$pwBytes = array();
		$i = 0;

		while ($i < $nBytes) {
			$pwBytes[$i] = ord(substr($password, $i, 1)) & 255;
			++$i;
		}

		$key = Aes::cipher($pwBytes, Aes::keyExpansion($pwBytes));
		$key = array_merge($key, array_slice($key, 0, $nBytes - 16));
		$counterBlock = array();
		$nonce = floor(microtime(true) * 1000);
		$nonceMs = $nonce % 1000;
		$nonceSec = floor($nonce / 1000);
		$nonceRnd = floor(rand(0, 65535));
		$i = 0;

		while ($i < 2) {
			$counterBlock[$i] = self::urs($nonceMs, $i * 8) & 255;
			++$i;
		}

		$i = 0;

		while ($i < 2) {
			$counterBlock[$i + 2] = self::urs($nonceRnd, $i * 8) & 255;
			++$i;
		}

		$i = 0;

		while ($i < 4) {
			$counterBlock[$i + 4] = self::urs($nonceSec, $i * 8) & 255;
			++$i;
		}

		$ctrTxt = '';
		$i = 0;

		while ($i < 8) {
			$ctrTxt .= chr($counterBlock[$i]);
			++$i;
		}

		$keySchedule = Aes::keyExpansion($key);
		$blockCount = ceil(strlen($plaintext) / $blockSize);
		$ciphertxt = array();
		$b = 0;

		while ($b < $blockCount) {
			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c] = self::urs($b, $c * 8) & 255;
				++$c;
			}

			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c - 4] = self::urs($b / 4294967296, $c * 8);
				++$c;
			}

			$cipherCntr = Aes::cipher($counterBlock, $keySchedule);
			$blockLength = ($b < ($blockCount - 1) ? $blockSize : ((strlen($plaintext) - 1) % $blockSize) + 1);
			$cipherByte = array();
			$i = 0;

			while ($i < $blockLength) {
				$cipherByte[$i] = $cipherCntr[$i] ^ ord(substr($plaintext, ($b * $blockSize) + $i, 1));
				$cipherByte[$i] = chr($cipherByte[$i]);
				++$i;
			}

			$ciphertxt[$b] = implode('', $cipherByte);
			++$b;
		}

		$ciphertext = $ctrTxt . implode('', $ciphertxt);
		$ciphertext = base64_encode($ciphertext);
		return $ciphertext;
	}

	static public function decrypt($ciphertext, $password, $nBits)
	{
		$blockSize = 16;
		if (!(($nBits == 128) || ($nBits == 192) || ($nBits == 256))) {
			return '';
		}

		$ciphertext = base64_decode($ciphertext);
		$nBytes = $nBits / 8;
		$pwBytes = array();
		$i = 0;

		while ($i < $nBytes) {
			$pwBytes[$i] = ord(substr($password, $i, 1)) & 255;
			++$i;
		}

		$key = Aes::cipher($pwBytes, Aes::keyExpansion($pwBytes));
		$key = array_merge($key, array_slice($key, 0, $nBytes - 16));
		$counterBlock = array();
		$ctrTxt = substr($ciphertext, 0, 8);
		$i = 0;

		while ($i < 8) {
			$counterBlock[$i] = ord(substr($ctrTxt, $i, 1));
			++$i;
		}

		$keySchedule = Aes::keyExpansion($key);
		$nBlocks = ceil((strlen($ciphertext) - 8) / $blockSize);
		$ct = array();
		$b = 0;

		while ($b < $nBlocks) {
			$ct[$b] = substr($ciphertext, 8 + ($b * $blockSize), 16);
			++$b;
		}

		$ciphertext = $ct;
		$plaintxt = array();
		$b = 0;

		while ($b < $nBlocks) {
			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c] = self::urs($b, $c * 8) & 255;
				++$c;
			}

			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c - 4] = self::urs((($b + 1) / 4294967296) - 1, $c * 8) & 255;
				++$c;
			}

			$cipherCntr = Aes::cipher($counterBlock, $keySchedule);
			$plaintxtByte = array();
			$i = 0;

			while ($i < strlen($ciphertext[$b])) {
				$plaintxtByte[$i] = $cipherCntr[$i] ^ ord(substr($ciphertext[$b], $i, 1));
				$plaintxtByte[$i] = chr($plaintxtByte[$i]);
				++$i;
			}

			$plaintxt[$b] = implode('', $plaintxtByte);
			++$b;
		}

		$plaintext = implode('', $plaintxt);
		return $plaintext;
	}

	static private function urs($a, $b)
	{
		$a &= 4294967295;
		$b &= 31;

		if (($a & 2147483648) && (0 < $b)) {
			$a = ($a >> 1) & 2147483647;
			$a = $a >> ($b - 1);
		}
		else {
			$a = $a >> $b;
		}

		return $a;
	}

	static public function cipher($input, $w)
	{
	}

	static private function addRoundKey($state, $w, $rnd, $Nb)
	{
	}

	static private function subBytes($s, $Nb)
	{
	}

	static private function shiftRows($s, $Nb)
	{
	}

	static private function mixColumns($s, $Nb)
	{
	}

	static public function keyExpansion($key)
	{
	}

	static private function subWord($w)
	{
	}

	static private function rotWord($w)
	{
	}
}


?>
