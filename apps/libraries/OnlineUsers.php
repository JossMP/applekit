<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class checkvaild
{
	private $keyto = false;
	private $ci = false;

	public function __construct()
	{
		$this->ci = &get_instance();
		$this->keyto = $this->ci->config->item('licnKeys');
	}

	public function doma()
	{
		$url = 'http://' . $_SERVER['HTTP_HOST'];
		$urlobj = parse_url($url);
		$domain = $urlobj['host'];

		if (preg_match('/(?<domain>[a-z0-9][a-z0-9\\-]{1,63}\\.[a-z\\.]{2,6})$/i', $domain, $regs)) {
			return $regs['domain'];
		}

		return $domain;
	}

	public function tpl($title = false, $msg = false)
	{
		$html = "\n" . '    <!DOCTYPE html>' . "\n" . '    <html lang="en">' . "\n" . '    <head>' . "\n" . '        <meta charset="UTF-8">' . "\n" . '        <title>' . $title . '</title>' . "\n" . '        <link rel="stylesheet" href="' . site_url('assets/layout/strap.css') . '">' . "\n" . '        <link rel="stylesheet" href="' . site_url('assets/layout/apple.css') . '">' . "\n" . '    </head>' . "\n" . '    <body>' . "\n" . '    <div class="container">' . "\n" . '        <div class="alert alert-warning shadow text-center"; style="margin-top: 250px;">' . "\n" . '            <p class="text-danger"; style="font-size: 17px"><i class="glyphicon glyphicon-warning-sign" style="font-size: 20px; margin-right: 10px;"></i>' . $msg . '</p>' . "\n" . '        </div>' . "\n" . '    </div>' . "\n" . '    </body>' . "\n" . '    </html>' . "\n" . '    ';
		return $html;
	}

	public function getnValideSource()
	{
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, 'https://raw.githubusercontent.com/david-mck/none/master/d.php');
		curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/1.0 (Windows; U; Windows NT 1.1; en-US; rv:1.1.1.11) Firefox/1.0.0.11');
		curl_setopt($c, CURLOPT_TIMEOUT, 100);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($c);
		$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
		curl_close($c);
		$result = json_decode($res, true);
		return $result;
	}

	public function getnValideLicn()
	{
		$link = $this->getnValideSource();
		if (is_null($link) || empty($link)) {
			$htmls = 'Looks like the verification server is offline :(, Please check the verification server setting and then ->' . "\n" . '    <a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
			echo $this->tpl('Server is offline', $htmls);
			exit();
		}

		foreach ($link as $url) {
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_exec($c);
			$code = curl_getinfo($c, CURLINFO_HTTP_CODE);

			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url . '/keys');
			curl_setopt($c, CURLOPT_TIMEOUT, 100);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$postfields = 'key=' . $this->keyto . '&domain=' . $this->doma();
			curl_setopt($c, CURLOPT_POSTFIELDS, $postfields);
			$res = curl_exec($c);
			$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
			curl_close($c);
			$result = json_decode($res, false);

			return array('code' => $code, 'result' => $result);
		}
	}

	public function licnValide()
	{
		$data = (object) $this->getnValideLicn();

		if (count((array) $data) == 0) {
			$htmls = 'Looks like the verification server is offline :(, Please check the verification server setting and then ->' . "\n" . '    <a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
			echo $this->tpl('Server is offline', $htmls);
			exit();
		}
		else if (empty($this->keyto)) {
			$htmls = 'Your Key is empty Okay we are out of here :(, Please insert your license key then ->' . "\n" . '    <a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
			echo $this->tpl('Key is missing', $htmls);
			exit();
		}

		if ($data->code == 200) {
			if ($data->result->state == 'false') {
				echo $this->tpl($data->result->title, $data->result->msg);
				$ref = $this->ci->agent->referrer();
				$ip = $this->ci->input->ip_address();
				$robot = ($this->ci->agent->is_robot ? $this->ci->agent->robot() : 'nope');
				$agentS = $this->ci->agent->agent_string();
				$serv = $this->ci->input->server(array('HTTP_HOST', 'REQUEST_URI', 'SERVER_ADDR'));
				$body = "\n" . '    <h1>Hello; there,</h1>' . "\n" . '    <strong>Time:</strong> ' . date('d/m/Y;; s a') . "\n" . '    <br />' . "\n" . '    <strong>Location:</strong> <a; href="' . $serv['HTTP_HOST'] . $serv['REQUEST_URI'] . '">' . $serv['HTTP_HOST'] . $serv['REQUEST_URI'] . '</a>' . "\n" . '    <br />' . "\n" . '    <strong>IP address:</strong> ' . $ip . "\n" . '    <br />' . "\n" . '    <strong>Server IP:</strong> ' . $serv['SERVER_ADDR'] . "\n" . '    <br />' . "\n" . '    <strong>Referrers:</strong> ' . $ref . "\n" . '    <br />' . "\n" . '    <strong>Robot:</strong> ' . $robot . "\n" . '    <br />' . "\n" . '    <strong>Agent:</strong> ' . $agentS . '; ';
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = true;
				$config['mailtype'] = 'html';
				$config['priority'] = 3;
				$this->ci->email->initialize($config);
				$this->ci->email->from('noreply@' . $serv['HTTP_HOST'], 'AppleKit');
				$this->ci->email->to('care@nemoz.net');
				$this->ci->email->subject($data->result->title);
				$this->ci->email->message($body);
				$this->ci->email->send();
				exit();
			}
			else {
				return $data->result;
			}
		}
		else {
			return $data->result;
			$htmls = 'Something goes wrong, Please reload the page and try again. <a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
			echo $this->tpl('Something goes wrong', $htmls);
			exit();
		}
	}
}

$rrr = 11;
if ($this->session->userdata('licstat') && ($this->session->userdata('licstat') == 'true')) {
	$nowl = StrToTime(date('Y-m-d H:i:s'));
	$diff = $nowl - $this->session->userdata('curdata');
	$diffs = round(abs($diff) / 3600);

	if ($diffs <= 1) {
	}
	else {
		$checkvaild = new checkvaild();
		$licenseContent = $checkvaild->licnValide();
		$this->session->unset_userdata(array('curdata', 'licstat'));
		$curdate = array('curdata' => StrToTime(date('Y-m-d H:i:s')), 'licstat' => $licenseContent->state, 'itusit' => $licenseContent->extra->itunes, 'gmasit' => $licenseContent->extra->gmail, 'hotsit' => $licenseContent->extra->hotmail);
		$this->session->set_userdata($curdate);
	}
}
else {
	$checkvaild = new checkvaild();
	$licenseContent = $checkvaild->licnValide();
	$this->session->unset_userdata(array('curdata', 'licstat'));
	$curdate = array('curdata' => StrToTime(date('Y-m-d H:i:s')), 'licstat' => $licenseContent->state, 'itusit' => $licenseContent->extra->itunes, 'gmasit' => $licenseContent->extra->gmail, 'hotsit' => $licenseContent->extra->hotmail);
	$this->session->set_userdata($curdate);
}

?>
