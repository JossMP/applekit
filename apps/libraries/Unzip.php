<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Unzip
{
	private $compressed_list = array();
	private $central_dir_list = array();
	private $end_of_central = array();
	private $info = array();
	private $error = array();
	private $_zip_file = '';
	private $_target_dir = false;
	private $apply_chmod = 511;
	private $fh;
	private $zip_signature = 'PK' . "\x3\x4";
	private $dir_signature = 'PK' . "\x1\x2";
	private $central_signature_end = 'PK' . "\x5\x6";
	private $_skip_dirs = array('__MACOSX');
	private $_allow_extensions;

	public function __construct()
	{
		log_message('debug', 'Unzip Class Initialized');
	}

	private function _reinit()
	{
		$this->compressed_list = array();
		$this->central_dir_list = array();
		$this->end_of_central = array();
		$this->info = array();
		$this->error = array();
	}

	public function extract($zip_file, $target_dir = NULL, $preserve_filepath = true)
	{
		$this->_reinit();
		$this->_zip_file = $zip_file;
		$this->_target_dir = $target_dir ? $target_dir : dirname($this->_zip_file);

		if (!($files = $this->_list_files())) {
			$this->set_error('ZIP folder was empty.');
			return false;
		}

		$file_locations = array();

		foreach ($files as $file => $trash) {
			$dirname = pathinfo($file, PATHINFO_DIRNAME);
			$extension = pathinfo($file, PATHINFO_EXTENSION);
			$folders = explode('/', $dirname);
			$out_dn = $this->_target_dir . '/' . $dirname;

			if (in_array(current($folders), $this->_skip_dirs)) {
				continue;
			}

			if (is_array($this->_allow_extensions) && $extension && !in_array($extension, $this->_allow_extensions)) {
				continue;
			}

			if (!is_dir($out_dn) && $preserve_filepath) {
				$str = '';

				foreach ($folders as $folder) {
					$str = ($str ? $str . '/' . $folder : $folder);

					if (!is_dir($this->_target_dir . '/' . $str)) {
						$this->set_debug('Creating folder: ' . $this->_target_dir . '/' . $str);

						if (!@mkdir($this->_target_dir . '/' . $str)) {
							$this->set_error('Desitnation path is not writable.');

							return false;
						}

						$this->apply_chmod && chmod($this->_target_dir . '/' . $str, $this->apply_chmod);
					}
				}
			}

			if (substr($file, -1, 1) == '/') {
				continue;
			}

			$file_locations[] = $file_location = $this->_target_dir . '/' . ($preserve_filepath ? $file : basename($file));
			$this->_extract_file($file, $file_location);
		}

		$this->compressed_list = array();
		return $file_locations;
	}

	public function allow($ext = NULL)
	{
		$this->_allow_extensions = $ext;
	}

	public function error_string($open = '<p>', $close = '</p>')
	{
		return $open . implode($close . $open, $this->error) . $close;
	}

	public function debug_string($open = '<p>', $close = '</p>')
	{
		return $open . implode($close . $open, $this->info) . $close;
	}

	public function set_error($string)
	{
		$this->error[] = $string;
	}

	public function set_debug($string)
	{
		$this->info[] = $string;
	}

	private function _list_files($stop_on_file = false)
	{
		if (sizeof($this->compressed_list)) {
			$this->set_debug('Returning already loaded file list.');
			return $this->compressed_list;
		}

		$fh = fopen($this->_zip_file, 'r');
		$this->fh = &$fh;

		if (!$fh) {
			$this->set_error('Failed to load file: ' . $this->_zip_file);
			return false;
		}

		$this->set_debug('Loading list from "End of Central Dir" index list...');

		if (!$this->_load_file_list_by_eof($fh, $stop_on_file)) {
			$this->set_debug('Failed! Trying to load list looking for signatures...');

			if (!$this->_load_files_by_signatures($fh, $stop_on_file)) {
				$this->set_debug('Failed! Could not find any valid header.');
				$this->set_error('ZIP File is corrupted or empty');
				return false;
			}
		}

		return $this->compressed_list;
	}

	private function _extract_file($compressed_file_name, $target_file_name = false)
	{
		if (!sizeof($this->compressed_list)) {
			$this->set_debug('Trying to unzip before loading file list... Loading it!');
			$this->_list_files(false, $compressed_file_name);
		}

		$fdetails = &$this->compressed_list[$compressed_file_name];

		if (!isset($this->compressed_list[$compressed_file_name])) {
			$this->set_error('File "<strong>$compressed_file_name</strong>" is not compressed in the zip.');
			return false;
		}

		if (substr($compressed_file_name, -1) == '/') {
			$this->set_error('Trying to unzip a folder name "<strong>$compressed_file_name</strong>".');
			return false;
		}

		if (!$fdetails['uncompressed_size']) {
			$this->set_debug('File "<strong>$compressed_file_name</strong>" is empty.');
			return $target_file_name ? file_put_contents($target_file_name, '') : '';
		}

		fseek($this->fh, $fdetails['contents_start_offset']);
		$ret = $this->_uncompress(fread($this->fh, $fdetails['compressed_size']), $fdetails['compression_method'], $fdetails['uncompressed_size'], $target_file_name);
		if ($this->apply_chmod && $target_file_name) {
			chmod($target_file_name, FILE_READ_MODE);
		}

		return $ret;
	}

	public function close()
	{
		if ($this->fh) {
			fclose($this->fh);
		}
	}

	public function __destroy()
	{
		$this->close();
	}

	private function _uncompress($content, $mode, $uncompressed_size, $target_file_name = false)
	{
		switch ($mode) {
		case 0:
			return $target_file_name ? file_put_contents($target_file_name, $content) : $content;
		case 1:
			$this->set_error('Shrunk mode is not supported... yet?');
			return false;
		case 2:
		case 3:
		case 4:
		case 5:
			$this->set_error('Compression factor ' . ($mode - 1) . ' is not supported... yet?');
			return false;
		case 6:
			$this->set_error('Implode is not supported... yet?');
			return false;
		case 7:
			$this->set_error('Tokenizing compression algorithm is not supported... yet?');
			return false;
		case 8:
			return $target_file_name ? file_put_contents($target_file_name, gzinflate($content, $uncompressed_size)) : gzinflate($content, $uncompressed_size);
		case 9:
			$this->set_error('Enhanced Deflating is not supported... yet?');
			return false;
		case 10:
			$this->set_error('PKWARE Date Compression Library Impoloding is not supported... yet?');
			return false;
		case 12:
			return $target_file_name ? file_put_contents($target_file_name, bzdecompress($content)) : bzdecompress($content);
		case 18:
			$this->set_error('IBM TERSE is not supported... yet?');
			return false;
		}

		();
		return false;
	}

	private function _load_file_list_by_eof(&$fh, $stop_on_file = false)
	{
		$x = 0;

		while ($x < 1024) {
			fseek($fh, -22 - $x, SEEK_END);
			$signature = fread($fh, 4);

			if ($signature == $this->central_signature_end) {
				$eodir['disk_number_this'] = unpack('v', fread($fh, 2));
				$eodir['disk_number'] = unpack('v', fread($fh, 2));
				$eodir['total_entries_this'] = unpack('v', fread($fh, 2));
				$eodir['total_entries'] = unpack('v', fread($fh, 2));
				$eodir['size_of_cd'] = unpack('V', fread($fh, 4));
				$eodir['offset_start_cd'] = unpack('V', fread($fh, 4));
				$zip_comment_lenght = unpack('v', fread($fh, 2));
				$eodir['zipfile_comment'] = $zip_comment_lenght[1] ? fread($fh, $zip_comment_lenght[1]) : '';
				$this->end_of_central = array('disk_number_this' => $eodir['disk_number_this'][1], 'disk_number' => $eodir['disk_number'][1], 'total_entries_this' => $eodir['total_entries_this'][1], 'total_entries' => $eodir['total_entries'][1], 'size_of_cd' => $eodir['size_of_cd'][1], 'offset_start_cd' => $eodir['offset_start_cd'][1], 'zipfile_comment' => $eodir['zipfile_comment']);
				fseek($fh, $this->end_of_central['offset_start_cd']);
				$signature = fread($fh, 4);

				while ($signature == $this->dir_signature) {
					$dir['version_madeby'] = unpack('v', fread($fh, 2));
					$dir['version_needed'] = unpack('v', fread($fh, 2));
					$dir['general_bit_flag'] = unpack('v', fread($fh, 2));
					$dir['compression_method'] = unpack('v', fread($fh, 2));
					$dir['lastmod_time'] = unpack('v', fread($fh, 2));
					$dir['lastmod_date'] = unpack('v', fread($fh, 2));
					$dir['crc-32'] = fread($fh, 4);
					$dir['compressed_size'] = unpack('V', fread($fh, 4));
					$dir['uncompressed_size'] = unpack('V', fread($fh, 4));
					$zip_file_length = unpack('v', fread($fh, 2));
					$extra_field_length = unpack('v', fread($fh, 2));
					$fileCommentLength = unpack('v', fread($fh, 2));
					$dir['disk_number_start'] = unpack('v', fread($fh, 2));
					$dir['internal_attributes'] = unpack('v', fread($fh, 2));
					$dir['external_attributes1'] = unpack('v', fread($fh, 2));
					$dir['external_attributes2'] = unpack('v', fread($fh, 2));
					$dir['relative_offset'] = unpack('V', fread($fh, 4));
					$dir['file_name'] = fread($fh, $zip_file_length[1]);
					$dir['extra_field'] = $extra_field_length[1] ? fread($fh, $extra_field_length[1]) : '';
					$dir['file_comment'] = $fileCommentLength[1] ? fread($fh, $fileCommentLength[1]) : '';
					$binary_mod_date = str_pad(decbin($dir['lastmod_date'][1]), 16, '0', STR_PAD_LEFT);
					$binary_mod_time = str_pad(decbin($dir['lastmod_time'][1]), 16, '0', STR_PAD_LEFT);
					$last_mod_year = bindec(substr($binary_mod_date, 0, 7)) + 1980;
					$last_mod_month = bindec(substr($binary_mod_date, 7, 4));
					$last_mod_day = bindec(substr($binary_mod_date, 11, 5));
					$last_mod_hour = bindec(substr($binary_mod_time, 0, 5));
					$last_mod_minute = bindec(substr($binary_mod_time, 5, 6));
					$last_mod_second = bindec(substr($binary_mod_time, 11, 5));
					$this->central_dir_list[$dir['file_name']] = array('version_madeby' => $dir['version_madeby'][1], 'version_needed' => $dir['version_needed'][1], 'general_bit_flag' => str_pad(decbin($dir['general_bit_flag'][1]), 8, '0', STR_PAD_LEFT), 'compression_method' => $dir['compression_method'][1], 'lastmod_datetime' => mktime($last_mod_hour, $last_mod_minute, $last_mod_second, $last_mod_month, $last_mod_day, $last_mod_year), 'crc-32' => str_pad(dechex(ord($dir['crc-32'][3])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($dir['crc-32'][2])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($dir['crc-32'][1])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($dir['crc-32'][0])), 2, '0', STR_PAD_LEFT), 'compressed_size' => $dir['compressed_size'][1], 'uncompressed_size' => $dir['uncompressed_size'][1], 'disk_number_start' => $dir['disk_number_start'][1], 'internal_attributes' => $dir['internal_attributes'][1], 'external_attributes1' => $dir['external_attributes1'][1], 'external_attributes2' => $dir['external_attributes2'][1], 'relative_offset' => $dir['relative_offset'][1], 'file_name' => $dir['file_name'], 'extra_field' => $dir['extra_field'], 'file_comment' => $dir['file_comment']);
					$signature = fread($fh, 4);
				}

				if ($this->central_dir_list) {
					foreach ($this->central_dir_list as $filename => $details) {
						$i = $this->_get_file_header($fh, $details['relative_offset']);
						$this->compressed_list[$filename]['file_name'] = $filename;
						$this->compressed_list[$filename]['compression_method'] = $details['compression_method'];
						$this->compressed_list[$filename]['version_needed'] = $details['version_needed'];
						$this->compressed_list[$filename]['lastmod_datetime'] = $details['lastmod_datetime'];
						$this->compressed_list[$filename]['crc-32'] = $details['crc-32'];
						$this->compressed_list[$filename]['compressed_size'] = $details['compressed_size'];
						$this->compressed_list[$filename]['uncompressed_size'] = $details['uncompressed_size'];
						$this->compressed_list[$filename]['lastmod_datetime'] = $details['lastmod_datetime'];
						$this->compressed_list[$filename]['extra_field'] = $i['extra_field'];
						$this->compressed_list[$filename]['contents_start_offset'] = $i['contents_start_offset'];

						break;
					}
				}

				return true;
			}

			++$x;
		}

		return false;
	}

	private function _load_files_by_signatures(&$fh, $stop_on_file = false)
	{
		fseek($fh, 0);
		$return = false;

		while (true) {
			$details = $this->_get_file_header($fh);

			if (!$details) {
				$this->set_debug('Invalid signature. Trying to verify if is old style Data Descriptor...');
				fseek($fh, 12 - 4, SEEK_CUR);
				$details = $this->_get_file_header($fh);
			}

			if (!$details) {
				$this->set_debug('Still invalid signature. Probably reached the end of the file.');
				break;
			}

			$filename = $details['file_name'];
			$this->compressed_list[$filename] = $details;
			$return = true;

			if (strtolower($stop_on_file) == strtolower($filename)) {
				break;
			}
		}

		return $return;
	}

	private function _get_file_header(&$fh, $start_offset = false)
	{
		if ($start_offset !== false) {
			fseek($fh, $start_offset);
		}

		$signature = fread($fh, 4);

		if ($signature == $this->zip_signature) {
			$file['version_needed'] = unpack('v', fread($fh, 2));
			$file['general_bit_flag'] = unpack('v', fread($fh, 2));
			$file['compression_method'] = unpack('v', fread($fh, 2));
			$file['lastmod_time'] = unpack('v', fread($fh, 2));
			$file['lastmod_date'] = unpack('v', fread($fh, 2));
			$file['crc-32'] = fread($fh, 4);
			$file['compressed_size'] = unpack('V', fread($fh, 4));
			$file['uncompressed_size'] = unpack('V', fread($fh, 4));
			$zip_file_length = unpack('v', fread($fh, 2));
			$extra_field_length = unpack('v', fread($fh, 2));
			$file['file_name'] = fread($fh, $zip_file_length[1]);
			$file['extra_field'] = $extra_field_length[1] ? fread($fh, $extra_field_length[1]) : '';
			$file['contents_start_offset'] = ftell($fh);
			fseek($fh, $file['compressed_size'][1], SEEK_CUR);
			$binary_mod_date = str_pad(decbin($file['lastmod_date'][1]), 16, '0', STR_PAD_LEFT);
			$binary_mod_time = str_pad(decbin($file['lastmod_time'][1]), 16, '0', STR_PAD_LEFT);
			$last_mod_year = bindec(substr($binary_mod_date, 0, 7)) + 1980;
			$last_mod_month = bindec(substr($binary_mod_date, 7, 4));
			$last_mod_day = bindec(substr($binary_mod_date, 11, 5));
			$last_mod_hour = bindec(substr($binary_mod_time, 0, 5));
			$last_mod_minute = bindec(substr($binary_mod_time, 5, 6));
			$last_mod_second = bindec(substr($binary_mod_time, 11, 5));
			$i = array('file_name' => $file['file_name'], 'compression_method' => $file['compression_method'][1], 'version_needed' => $file['version_needed'][1], 'lastmod_datetime' => mktime($last_mod_hour, $last_mod_minute, $last_mod_second, $last_mod_month, $last_mod_day, $last_mod_year), 'crc-32' => str_pad(dechex(ord($file['crc-32'][3])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($file['crc-32'][2])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($file['crc-32'][1])), 2, '0', STR_PAD_LEFT) . str_pad(dechex(ord($file['crc-32'][0])), 2, '0', STR_PAD_LEFT), 'compressed_size' => $file['compressed_size'][1], 'uncompressed_size' => $file['uncompressed_size'][1], 'extra_field' => $file['extra_field'], 'general_bit_flag' => str_pad(decbin($file['general_bit_flag'][1]), 8, '0', STR_PAD_LEFT), 'contents_start_offset' => $file['contents_start_offset']);
			return $i;
		}

		return false;
	}
}

defined('BASEPATH') || exit('No direct script access allowed');

?>
