<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Shortkit
{
	static private $api = false;
	static private $url = false;

	public function __construct($api)
	{
		self::$api = $api;
	}

	static public function exe($method, $post = array())
	{
		if (!is_array($post)) {
			throw new ShortKitException('Expected $post to be an array');
		}

		return self::handelRequest(self::Request($method, $post));
	}

	static private function handelRequest($request)
	{
		return json_decode($request, false);
	}

	static private function getUrl($api)
	{
		$url = explode('@', $api);
		return base64_decode($url[0]);
	}

	static private function Request($method, $post)
	{
		$post['api'] = self::$api;
		$post['method'] = $method;
		self::$url = self::getUrl(self::$api);
		$curl = curl_init();
		curl_setopt_array($curl, array(CURLOPT_TIMEOUT => 180, CURLOPT_CONNECTTIMEOUT => 180, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => true, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $post, CURLOPT_URL => self::$url . '/API/V1'));
		$http_result = curl_exec($curl);
		$http = http_response_code();
		curl_close($curl);
		return $http_result;
	}
}

class ShortKitException
{}


?>
