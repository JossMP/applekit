<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Responding
{
	private $ci;

	public function __construct()
	{
		$this->ci = &get_instance();
	}

	public function Request($msg = NULL)
	{
		if ($msg == NULL) {
			return false;
		}

		$chat_id = $this->gettingChatID();
		$params = array('tele_user' => $chat_id, 'tele_msg' => $msg, 'key' => $this->ci->config->item('licnKeys'), 'domain' => $this->gettingChatMsg());
		$curl = curl_init('http://nemoz.net/API/tteellee');
		curl_setopt_array($curl, array(CURLOPT_TIMEOUT => 180, CURLOPT_CONNECTTIMEOUT => 180, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => false, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $params));
		$result = curl_exec($curl);
		$res = json_decode($result, false);
		curl_close($curl);
	}

	private function gettingChatID()
	{
		$this->ci = &get_instance();
		$this->ci->load->model('options_m', 'options_m');
		$q = $this->ci->options_m->getOpt();
		return $q->telegram;
	}

	public function gettingChatMsg()
	{
		$url = 'http://' . $_SERVER['HTTP_HOST'];
		$urlobj = parse_url($url);
		$domain = $urlobj['host'];

		if (preg_match('/(?<domain>[a-z0-9][a-z0-9\\-]{1,63}\\.[a-z\\.]{2,6})$/i', $domain, $regs)) {
			return $regs['domain'];
		}

		return $domain;
	}
}


?>
