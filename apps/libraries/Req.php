<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Req
{
	static public $proxy = false;
	static public $cookie = 'kook.txt';
	static public $user_agent = 'Mozilla/5.0 (Windows NT 5.1; rv:35.0) Gecko/20100101 Firefox/14.0';
	static public $last_url;
	static public $ch;
	static public $last_http_code;
	static public $verbose = false;

	static public function SetCookieFile($cookie_f)
	{
		if ($cookie_f) {
			if (!file_exists($cookie_f)) {
				$ft = fopen($cookie_f, 'w+');
				fclose($ft);
			}

			self::$cookie = realpath($cookie_f);
		}
		else {
			self::$cookie = false;
		}
	}

	static public function SetUserAgent($user_agent_1)
	{
		self::$user_agent = $user_agent_1;
	}

	static public function Start()
	{
		self::$ch = curl_init();
	}

	public function NewSession()
	{
		self::$cookie = rand() . '.txt';
		self::$ch = curl_init();
	}

	static public function UseProxy($proxy_obj)
	{
		self::$proxy = $proxy_obj;
	}

	static public function OpenPage($url, $headers = array(), $cookies = false, $location = true, $header = false)
	{
		if (self::$proxy) {
			curl_setopt(self::$ch, CURLOPT_PROXY, self::$proxy['host']);

			if (!empty(self::$proxy['auth'])) {
				curl_setopt(self::$ch, CURLOPT_PROXYUSERPWD, self::$proxy['auth']);
			}
		}

		curl_setopt(self::$ch, CURLOPT_URL, $url);
		curl_setopt(self::$ch, CURLOPT_VERBOSE, self::$verbose);
		curl_setopt(self::$ch, CURLOPT_ENCODING, '');
		curl_setopt(self::$ch, CURLOPT_POST, 0);
		curl_setopt(self::$ch, CURLOPT_USERAGENT, self::$user_agent);
		curl_setopt(self::$ch, CURLOPT_COOKIEFILE, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_COOKIEJAR, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt(self::$ch, CURLOPT_FAILONERROR, 0);
		curl_setopt(self::$ch, CURLOPT_FOLLOWLOCATION, $location);
		curl_setopt(self::$ch, CURLOPT_HEADER, $header);
		curl_setopt(self::$ch, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_TIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYHOST, 0);

		if ($cookies) {
			curl_setopt(self::$ch, CURLOPT_COOKIE, $cookies);
		}

		curl_setopt(self::$ch, CURLOPT_HTTPHEADER, $headers);
		$rez = curl_exec(self::$ch);
		self::$last_http_code = curl_getinfo(self::$ch, CURLINFO_HTTP_CODE);
		self::$last_url = curl_getinfo(self::$ch, CURLINFO_EFFECTIVE_URL);

		if (empty($rez)) {
			return false;
		}

		return $rez;
	}

	static public function OpenPageWithPost($url, $post, $headers = array(), $cookies = false, $location = true, $header = false)
	{
		if (self::$proxy) {
			curl_setopt(self::$ch, CURLOPT_PROXY, self::$proxy['host']);

			if (!empty(self::$proxy['auth'])) {
				curl_setopt(self::$ch, CURLOPT_PROXYUSERPWD, self::$proxy['auth']);
			}
		}

		curl_setopt(self::$ch, CURLOPT_URL, $url);
		curl_setopt(self::$ch, CURLOPT_VERBOSE, self::$verbose);
		curl_setopt(self::$ch, CURLOPT_ENCODING, '');
		curl_setopt(self::$ch, CURLOPT_POST, 1);
		curl_setopt(self::$ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt(self::$ch, CURLOPT_USERAGENT, self::$user_agent);
		curl_setopt(self::$ch, CURLOPT_COOKIEFILE, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_COOKIEJAR, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt(self::$ch, CURLOPT_FAILONERROR, 0);
		curl_setopt(self::$ch, CURLOPT_HEADER, $header);
		curl_setopt(self::$ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt(self::$ch, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_TIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(self::$ch, CURLOPT_FOLLOWLOCATION, $location);

		if ($cookies) {
			curl_setopt(self::$ch, CURLOPT_COOKIE, $cookies);
		}

		$rez = curl_exec(self::$ch);
		self::$last_http_code = curl_getinfo(self::$ch, CURLINFO_HTTP_CODE);
		self::$last_url = curl_getinfo(self::$ch, CURLINFO_EFFECTIVE_URL);

		if (empty($rez)) {
			return false;
		}

		return $rez;
	}

	static public function PostQuery($url, $data, $headers = array(), $location = true, $header = false)
	{
		if (self::$proxy) {
			curl_setopt(self::$ch, CURLOPT_PROXY, self::$proxy['host']);

			if (!empty(self::$proxy['auth'])) {
				curl_setopt(self::$ch, CURLOPT_PROXYUSERPWD, self::$proxy['auth']);
			}
		}

		curl_setopt(self::$ch, CURLOPT_URL, $url);
		curl_setopt(self::$ch, CURLOPT_HEADER, $header);
		curl_setopt(self::$ch, CURLOPT_POST, 1);
		curl_setopt(self::$ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt(self::$ch, CURLOPT_FOLLOWLOCATION, $location);
		curl_setopt(self::$ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt(self::$ch, CURLOPT_CONNECTTIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_TIMEOUT, 300);
		curl_setopt(self::$ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(self::$ch, CURLOPT_USERAGENT, self::$user_agent);
		curl_setopt(self::$ch, CURLOPT_COOKIEFILE, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_COOKIEJAR, self::$cookie);
		curl_setopt(self::$ch, CURLOPT_HTTPHEADER, $headers);
		$res = curl_exec(self::$ch);
		self::$last_http_code = curl_getinfo(self::$ch, CURLINFO_HTTP_CODE);
		return $res;
	}

	public function OpenPages($urls)
	{
		$mh = curl_multi_init();
		unset($conn);

		foreach ($urls as $i => $url) {
			$conn[$i] = curl_init(trim($url));
			curl_setopt($conn[$i], CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($conn[$i], CURLOPT_TIMEOUT, 30);
			curl_setopt($conn[$i], CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
			curl_setopt($conn[$i], CURLOPT_COOKIEFILE, self::$cookie);
			curl_setopt($conn[$i], CURLOPT_COOKIEJAR, self::$cookie);
			curl_setopt($conn[$i], CURLOPT_HEADER, 0);
			curl_setopt($conn[$i], CURLOPT_FOLLOWLOCATION, 1);
			curl_multi_add_handle($mh, $conn[$i]);
		}

		do {
			$n = curl_multi_exec($mh, $active);
			usleep(100);
		} while ($active);

		foreach ($urls as $i => $url) {
			$result[] = curl_multi_getcontent($conn[$i]);
			curl_close($conn[$i]);
		}

		curl_multi_close($mh);
		return $result;
	}

	static public function Close()
	{
		curl_close(self::$ch);
	}
}

defined('BASEPATH') || exit('No direct script access allowed');

?>
