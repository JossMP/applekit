<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class AesCtr
{
	static public function encrypt($plaintext, $password, $nBits)
	{
		$blockSize = 16;
		if (!(($nBits == 128) || ($nBits == 192) || ($nBits == 256))) {
			return '';
		}

		$nBytes = $nBits / 8;
		$pwBytes = array();
		$i = 0;

		while ($i < $nBytes) {
			$pwBytes[$i] = ord(substr($password, $i, 1)) & 255;
			++$i;
		}

		$key = Aes::cipher($pwBytes, Aes::keyExpansion($pwBytes));
		$key = array_merge($key, array_slice($key, 0, $nBytes - 16));
		$counterBlock = array();
		$nonce = floor(microtime(true) * 1000);
		$nonceMs = $nonce % 1000;
		$nonceSec = floor($nonce / 1000);
		$nonceRnd = floor(rand(0, 65535));
		$i = 0;

		while ($i < 2) {
			$counterBlock[$i] = self::urs($nonceMs, $i * 8) & 255;
			++$i;
		}

		$i = 0;

		while ($i < 2) {
			$counterBlock[$i + 2] = self::urs($nonceRnd, $i * 8) & 255;
			++$i;
		}

		$i = 0;

		while ($i < 4) {
			$counterBlock[$i + 4] = self::urs($nonceSec, $i * 8) & 255;
			++$i;
		}

		$ctrTxt = '';
		$i = 0;

		while ($i < 8) {
			$ctrTxt .= chr($counterBlock[$i]);
			++$i;
		}

		$keySchedule = Aes::keyExpansion($key);
		$blockCount = ceil(strlen($plaintext) / $blockSize);
		$ciphertxt = array();
		$b = 0;

		while ($b < $blockCount) {
			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c] = self::urs($b, $c * 8) & 255;
				++$c;
			}

			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c - 4] = self::urs($b / 4294967296, $c * 8);
				++$c;
			}

			$cipherCntr = Aes::cipher($counterBlock, $keySchedule);
			$blockLength = ($b < ($blockCount - 1) ? $blockSize : ((strlen($plaintext) - 1) % $blockSize) + 1);
			$cipherByte = array();
			$i = 0;

			while ($i < $blockLength) {
				$cipherByte[$i] = $cipherCntr[$i] ^ ord(substr($plaintext, ($b * $blockSize) + $i, 1));
				$cipherByte[$i] = chr($cipherByte[$i]);
				++$i;
			}

			$ciphertxt[$b] = implode('', $cipherByte);
			++$b;
		}

		$ciphertext = $ctrTxt . implode('', $ciphertxt);
		$ciphertext = base64_encode($ciphertext);
		return $ciphertext;
	}

	static public function decrypt($ciphertext, $password, $nBits)
	{
		$blockSize = 16;
		if (!(($nBits == 128) || ($nBits == 192) || ($nBits == 256))) {
			return '';
		}

		$ciphertext = base64_decode($ciphertext);
		$nBytes = $nBits / 8;
		$pwBytes = array();
		$i = 0;

		while ($i < $nBytes) {
			$pwBytes[$i] = ord(substr($password, $i, 1)) & 255;
			++$i;
		}

		$key = Aes::cipher($pwBytes, Aes::keyExpansion($pwBytes));
		$key = array_merge($key, array_slice($key, 0, $nBytes - 16));
		$counterBlock = array();
		$ctrTxt = substr($ciphertext, 0, 8);
		$i = 0;

		while ($i < 8) {
			$counterBlock[$i] = ord(substr($ctrTxt, $i, 1));
			++$i;
		}

		$keySchedule = Aes::keyExpansion($key);
		$nBlocks = ceil((strlen($ciphertext) - 8) / $blockSize);
		$ct = array();
		$b = 0;

		while ($b < $nBlocks) {
			$ct[$b] = substr($ciphertext, 8 + ($b * $blockSize), 16);
			++$b;
		}

		$ciphertext = $ct;
		$plaintxt = array();
		$b = 0;

		while ($b < $nBlocks) {
			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c] = self::urs($b, $c * 8) & 255;
				++$c;
			}

			$c = 0;

			while ($c < 4) {
				$counterBlock[15 - $c - 4] = self::urs((($b + 1) / 4294967296) - 1, $c * 8) & 255;
				++$c;
			}

			$cipherCntr = Aes::cipher($counterBlock, $keySchedule);
			$plaintxtByte = array();
			$i = 0;

			while ($i < strlen($ciphertext[$b])) {
				$plaintxtByte[$i] = $cipherCntr[$i] ^ ord(substr($ciphertext[$b], $i, 1));
				$plaintxtByte[$i] = chr($plaintxtByte[$i]);
				++$i;
			}

			$plaintxt[$b] = implode('', $plaintxtByte);
			++$b;
		}

		$plaintext = implode('', $plaintxt);
		return $plaintext;
	}

	static private function urs($a, $b)
	{
		$a &= 4294967295;
		$b &= 31;

		if (($a & 2147483648) && (0 < $b)) {
			$a = ($a >> 1) & 2147483647;
			$a = $a >> ($b - 1);
		}
		else {
			$a = $a >> $b;
		}

		return $a;
	}
}


?>
