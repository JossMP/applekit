<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class MY_Email
{
	public function clear_debugger_messages()
	{
		$this->_debug_msg = array();
	}

	public function get_debugger_messages()
	{
		return $this->_debug_msg;
	}
}


?>
