<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Mailin
{
	public $api_key;
	public $base_url;
	public $timeout;
	public $curl_opts = array();

	public function __construct($base_url, $api_key, $timeout = '')
	{
		if (!function_exists('curl_init')) {
			throw new RuntimeException('Mailin requires cURL module');
		}

		$this->base_url = $base_url;
		$this->api_key = $api_key;
		$this->timeout = $timeout;
	}

	private function do_request($resource, $method, $input)
	{
		$called_url = $this->base_url . '/' . $resource;
		$ch = curl_init($called_url);
		$auth_header = 'api-key:' . $this->api_key;
		$content_header = 'Content-Type:application/json';
		$timeout = ($this->timeout != '' ? $this->timeout : 30000);

		if (($timeout != '') && (($timeout <= 0) || (60000 < $timeout))) {
			throw new Exception('value not allowed for timeout');
		}

		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, array($auth_header, $content_header));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			throw new RuntimeException('cURL error: ' . curl_error($ch));
		}

		if (!is_string($data) || !strlen($data)) {
			throw new RuntimeException('Request Failed');
		}

		curl_close($ch);
		return json_decode($data, true);
	}

	public function get($resource, $input)
	{
		return $this->do_request($resource, 'GET', $input);
	}

	public function put($resource, $input)
	{
		return $this->do_request($resource, 'PUT', $input);
	}

	public function post($resource, $input)
	{
		return $this->do_request($resource, 'POST', $input);
	}

	public function delete($resource, $input)
	{
		return $this->do_request($resource, 'DELETE', $input);
	}

	public function get_account()
	{
		return $this->get('account', '');
	}

	public function get_smtp_details()
	{
		return $this->get('account/smtpdetail', '');
	}

	public function create_child_account($data)
	{
		return $this->post('account', json_encode($data));
	}

	public function update_child_account($data)
	{
		return $this->put('account', json_encode($data));
	}

	public function delete_child_account($data)
	{
		return $this->delete('account/' . $data['auth_key'], '');
	}

	public function get_reseller_child($data)
	{
		return $this->post('account/getchildv2', json_encode($data));
	}

	public function add_remove_child_credits($data)
	{
		return $this->post('account/addrmvcredit', json_encode($data));
	}

	public function get_campaign_v2($data)
	{
		return $this->get('campaign/' . $data['id'] . '/detailsv2', '');
	}

	public function get_campaigns_v2($data)
	{
		return $this->get('campaign/detailsv2', json_encode($data));
	}

	public function create_campaign($data)
	{
		return $this->post('campaign', json_encode($data));
	}

	public function update_campaign($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('campaign/' . $id, json_encode($data));
	}

	public function delete_campaign($data)
	{
		return $this->delete('campaign/' . $data['id'], '');
	}

	public function campaign_report_email($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->post('campaign/' . $id . '/report', json_encode($data));
	}

	public function campaign_recipients_export($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->post('campaign/' . $id . '/recipients', json_encode($data));
	}

	public function share_campaign($data)
	{
		return $this->post('campaign/sharelinkv2', json_encode($data));
	}

	public function send_bat_email($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->post('campaign/' . $id . '/test', json_encode($data));
	}

	public function update_campaign_status($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('campaign/' . $id . '/updatecampstatus', json_encode($data));
	}

	public function create_trigger_campaign($data)
	{
		return $this->post('campaign', json_encode($data));
	}

	public function update_trigger_campaign($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('campaign/' . $id, json_encode($data));
	}

	public function get_folders($data)
	{
		return $this->get('folder', json_encode($data));
	}

	public function get_folder($data)
	{
		return $this->get('folder/' . $data['id'], '');
	}

	public function create_folder($data)
	{
		return $this->post('folder', json_encode($data));
	}

	public function delete_folder($data)
	{
		return $this->delete('folder/' . $data['id'], '');
	}

	public function update_folder($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('folder/' . $id, json_encode($data));
	}

	public function get_lists($data)
	{
		return $this->get('list', json_encode($data));
	}

	public function get_list($data)
	{
		return $this->get('list/' . $data['id'], '');
	}

	public function create_list($data)
	{
		return $this->post('list', json_encode($data));
	}

	public function update_list($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('list/' . $id, json_encode($data));
	}

	public function delete_list($data)
	{
		return $this->delete('list/' . $data['id'], '');
	}

	public function display_list_users($data)
	{
		return $this->post('list/display', json_encode($data));
	}

	public function add_users_list($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->post('list/' . $id . '/users', json_encode($data));
	}

	public function delete_users_list($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->delete('list/' . $id . '/delusers', json_encode($data));
	}

	public function get_attributes()
	{
		return $this->get('attribute', '');
	}

	public function get_attribute($data)
	{
		return $this->get('attribute/' . $data['type'], '');
	}

	public function create_attribute($data)
	{
		return $this->post('attribute/', json_encode($data));
	}

	public function delete_attribute($data)
	{
		$type = $data['type'];
		unset($data['type']);
		return $this->post('attribute/' . $type, json_encode($data));
	}

	public function create_update_user($data)
	{
		return $this->post('user/createdituser', json_encode($data));
	}

	public function get_user($data)
	{
		return $this->get('user/' . $data['email'], '');
	}

	public function delete_user($data)
	{
		return $this->delete('user/' . $data['email'], '');
	}

	public function import_users($data)
	{
		return $this->post('user/import', json_encode($data));
	}

	public function export_users($data)
	{
		return $this->post('user/export', json_encode($data));
	}

	public function get_processes($data)
	{
		return $this->get('process', json_encode($data));
	}

	public function get_process($data)
	{
		return $this->get('process/' . $data['id'], '');
	}

	public function get_webhooks($data)
	{
		return $this->get('webhook', json_encode($data));
	}

	public function get_webhook($data)
	{
		return $this->get('webhook/' . $data['id'], '');
	}

	public function create_webhook($data)
	{
		return $this->post('webhook', json_encode($data));
	}

	public function delete_webhook($data)
	{
		return $this->delete('webhook/' . $data['id'], '');
	}

	public function update_webhook($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('webhook/' . $id, json_encode($data));
	}

	public function get_senders($data)
	{
		return $this->get('advanced', json_encode($data));
	}

	public function create_sender($data)
	{
		return $this->post('advanced', json_encode($data));
	}

	public function update_sender($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('advanced/' . $id, json_encode($data));
	}

	public function delete_sender($data)
	{
		return $this->delete('advanced/' . $data['id'], '');
	}

	public function send_email($data)
	{
		return $this->post('email', json_encode($data));
	}

	public function get_statistics($data)
	{
		return $this->post('statistics', json_encode($data));
	}

	public function get_report($data)
	{
		return $this->post('report', json_encode($data));
	}

	public function delete_bounces($data)
	{
		return $this->post('bounces', json_encode($data));
	}

	public function send_transactional_template($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('template/' . $id, json_encode($data));
	}

	public function create_template($data)
	{
		return $this->post('template', json_encode($data));
	}

	public function update_template($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('template/' . $id, json_encode($data));
	}

	public function send_sms($data)
	{
		return $this->post('sms', json_encode($data));
	}

	public function create_sms_campaign($data)
	{
		return $this->post('sms', json_encode($data));
	}

	public function update_sms_campaign($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->put('sms/' . $id, json_encode($data));
	}

	public function send_bat_sms($data)
	{
		$id = $data['id'];
		unset($data['id']);
		return $this->get('sms/' . $id, json_encode($data));
	}
}


?>
