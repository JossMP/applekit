<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Ajax
{
	static private $responseContentType = 'application/x-json';
	static private $timers = array();
	static private $callback;
	static private $path;
	static private $request;
	static private $lazy;
	static private $initialized;
	static private $requestType;

	static public function __callStatic($method, $arguments)
	{
		if (method_exists(get_class(), 'x_' . $method)) {
			if (!self::$initialized) {
				self::init();
			}

			return call_user_func_array(array(get_class(), 'x_' . $method), $arguments);
		}
	}

	static private function init($request = false, $path = array())
	{
		self::$initialized = true;

		if ($request === false) {
			$request = $_REQUEST;
		}

		self::timerStart('Request');
		self::$callback = (isset($request['callback']) ? $request['callback'] : false);
		self::$callback = (isset($request['callbackName']) ? $request['callbackName'] : self::$callback);
		self::$request = $request;
		self::$path = $path;
		self::$requestType = $_SERVER['REQUEST_METHOD'];
		self::$lazy = 0;

		if (isset(self::$request['lazy'])) {
			self::$lazy = self::$request['lazy'];
		}

		set_error_handler(array(get_class(), 'errorHandler'));
	}

	static protected function timerStart($title)
	{
		self::$timers[$title] = microtime(true);
	}

	static protected function timerEnd($title)
	{
		$end = microtime(true);
		return sprintf('%01.4f', $end - self::$timers[$title]);
	}

	static public function x_get($key)
	{
		if (!isset(self::$request[$key])) {
			return NULL;
		}

		$val = self::$request[$key];

		if (!is_array($val)) {
			if (strtolower($val) == 'true') {
				$val = true;
			}

			if (strtolower($val) == 'false') {
				$val = false;
			}
		}

		return $val;
	}

	static private function errorHandler($errno, $message, $filename, $line)
	{
		if (error_reporting() == 0) {
			return NULL;
		}

		if ($errno & (32767 ^ 8)) {
			$types = array(1 => 'error', 2 => 'warning', 4 => 'parse error', 8 => 'notice', 16 => 'core error', 32 => 'core warning', 64 => 'compile error', 128 => 'compile warning', 256 => 'user error', 512 => 'user warning', 1024 => 'user notice', 2048 => 'strict warning');
			$entry = '<div style=\'text-align:left;\'><span><b>' . @$types[$errno] . '</b></span>: ' . $message . ' <br><br>' . "\n" . '                <span> <b>in</b> </span>: ' . $filename . ' <br>' . "\n" . '                <span> <b>on line</b> </span>: ' . $line . ' </div>';
			error_log('Request Server Error:' . $message . "\n" . 'File:' . $filename . "\n" . 'On Line: ' . $line);
			self::x_error($entry, 500);
		}
	}

	static public function x_error($message, $status = 400)
	{
		$addHash['error'] = $message;
		$addHash['success'] = false;
		self::response($addHash, $status);
	}

	static public function x_success($message, $status = 200)
	{
		$addHash['message'] = $message;
		$addHash['success'] = true;
		self::response($addHash, $status);
	}

	static public function x_getType()
	{
		return self::$requestType;
	}

	static private function response($addHash, $status)
	{
		$addHash['duration'] = self::timerEnd('Request');
		@header('Cache-Control: no-cache, must-revalidate', true);
		@header('Expires: Sat, 26 Jul 1997 05:00:00 GMT', true);
		@header('Content-Type: ' . self::$responseContentType . '; charset=utf-8', true, $status);

		if (self::$callback) {
			$response = self::$callback . '(' . json_encode($addHash) . ');';
		}
		else {
			$response = json_encode($addHash);
		}

		echo $response;
		exit();
	}
}

defined('BASEPATH') || exit('No direct script access allowed');

?>
