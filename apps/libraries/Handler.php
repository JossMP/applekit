<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Handler
{
	private $request = false;
	private $key = false;
	private $type = 1;
	private $appid = false;
	private $pass = false;
	public $msg = false;

	public function __construct($para = NULL)
	{
		if ($para) {
			$this->key = $para['key'];
			$this->appid = $para['appleid'];
			$this->pass = $para['password'];
			$this->type = empty($para['type']) ? $this->type : $para['type'];
			return $this->Request();
		}
	}

	private function Request()
	{
		$this->request = array('appleid' => $this->appid, 'password' => $this->pass, 'key' => $this->key, 'subscription' => 1, 'format' => 'JSON');
		$curl = curl_init('https://api.ifreeicloud.co.uk');
		curl_setopt_array($curl, array(CURLOPT_TIMEOUT => 180, CURLOPT_CONNECTTIMEOUT => 180, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => false, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $this->request, CURLOPT_USERAGENT => 'Applekit/Software'));
		$Res = curl_exec($curl);
		$Http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		if ($this->type == 1) {
			return $this->Result($Http, $Res);
		}

		return $this->Result2($Http, $Res);
	}

	public function serviec($para)
	{
		$this->request = array('service' => $para['id'], 'imei' => $para['imei'], 'key' => $para['key'], 'format' => 'JSON');
		$curl = curl_init('https://api.ifreeicloud.co.uk');
		curl_setopt_array($curl, array(CURLOPT_TIMEOUT => 180, CURLOPT_CONNECTTIMEOUT => 180, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_FOLLOWLOCATION => true, CURLOPT_RETURNTRANSFER => true, CURLOPT_AUTOREFERER => true, CURLOPT_VERBOSE => false, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $this->request, CURLOPT_USERAGENT => 'Applekit/Software'));
		$Res = json_decode(curl_exec($curl));
		$Http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		if ($Http != 200) {
			return array('status' => false, 'msg' => 'Error: HTTP Code' . $Http);
		}

		if ($Res->success !== true) {
			return array('status' => false, 'msg' => 'Error: ' . $Res->error);
		}

		return array('status' => true, 'msg' => $Res->response);
	}

	private function Result($Http, $Res)
	{
		if (($Http == 200) && (stripos($Res, 'Invalid Apple ID\\/Password combination.') == false) && (stripos($Res, 'This domain is not authorised to use SilentRemove API') == false) && (stripos($Res, 'This Apple ID is locked') == false)) {
			$this->msg = array('status' => true, 'msg' => $Res);
			return $this->msg;
		}

		$this->msg = array('status' => false, 'msg' => $Res);
		return $this->msg;
	}

	private function Result2($Http, $Res)
	{
		$ress = json_decode($Res, true);

		if (($Http == 200) && (stripos($Res, 'This domain is not authorised to use SilentRemove API') == false) && (stripos($Res, 'Invalid PHP API Key') == false)) {
			$this->msg = array('status' => true, 'msg' => $ress);
			return $this->msg;
		}

		$this->msg = array('status' => false, 'msg' => $ress);
		return $this->msg;
	}
}


?>
