<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Gravatar
{
	protected $base_url = 'http://www.gravatar.com/';
	protected $secure_base_url = 'https://secure.gravatar.com/';
	protected $image_extension = '.png';
	protected $image_size = 80;
	protected $default_image = '';
	protected $force_default_image = false;
	protected $rating = '';
	protected $useragent = 'PHP Gravatar Library';
	protected $last_error = GRAVATAR_NO_ERROR;
	protected $is_https;
	protected $curl_exists;
	protected $allow_url_fopen;

	public function __construct($config = array())
	{
		if (!is_array($config)) {
			$config = array();
		}

		if (isset($config['gravatar_base_url'])) {
			$this->base_url = (string) $config['gravatar_base_url'];
		}

		if (isset($config['gravatar_secure_base_url'])) {
			$this->secure_base_url = (string) $config['gravatar_secure_base_url'];
		}

		if (isset($config['gravatar_image_extension'])) {
			$this->image_extension = (string) $config['gravatar_image_extension'];
		}

		if (isset($config['gravatar_image_size'])) {
			$image_size = (int) $config['gravatar_image_size'];

			if (0 < $image_size) {
				$this->image_size = $image_size;
			}
		}

		if (isset($config['gravatar_default_image'])) {
			$this->default_image = (string) $config['gravatar_default_image'];
		}

		if (isset($config['gravatar_force_default_image'])) {
			$this->force_default_image = !empty($config['gravatar_force_default_image']);
		}

		if (isset($config['gravatar_rating'])) {
			$this->rating = (string) $config['gravatar_rating'];
		}

		if (isset($config['gravatar_useragent'])) {
			$this->useragent = (string) $config['gravatar_useragent'];
		}

		$this->is_https = $this->is_https();
		$this->curl_exists = function_exists('curl_init');
		$allow_url_fopen = @ini_get('allow_url_fopen');
		$allow_url_fopen = ($allow_url_fopen === false) || in_array(strtolower($allow_url_fopen), array('on', 'true', '1'));
		$this->allow_url_fopen = $allow_url_fopen;
	}

	public function get($email, $size = NULL, $default_image = NULL, $force_default_image = NULL, $rating = NULL)
	{
		$url = ($this->is_https ? $this->secure_base_url : $this->base_url) . 'avatar/' . $this->create_hash($email) . $this->image_extension;
		$query = array();
		$size = (int) $size;

		if ($size <= 0) {
			$size = $this->image_size;
		}

		if (0 < $size) {
			$query['s'] = $size;
		}

		if (isset($default_image)) {
			$default_image = (string) $default_image;
		}
		else {
			$default_image = $this->default_image;
		}

		if ($default_image != '') {
			$query['d'] = $default_image;
		}

		if (isset($force_default_image)) {
			$force_default_image = !empty($force_default_image);
		}
		else {
			$force_default_image = $this->force_default_image;
		}

		if ($force_default_image) {
			$query['f'] = 'y';
		}

		if (isset($rating)) {
			$rating = (string) $rating;
		}
		else {
			$rating = $this->rating;
		}

		if ($rating != '') {
			$query['r'] = $rating;
		}

		if (!empty($query)) {
			$url = $url . '?' . http_build_query($query);
		}

		return $url;
	}

	public function get_profile_data($email)
	{
		$result = $this->execute_profile_request($email, 'php');

		if ($this->last_error != GRAVATAR_NO_ERROR) {
			return NULL;
		}

		$result = @unserialize($result);

		if ($result === false) {
			$this->last_error = GRAVATAR_INCORRECT_FORMAT;
			return NULL;
		}

		if (!is_array($result)) {
			$this->last_error = GRAVATAR_PROFILE_DOES_NOT_EXIST;
			return NULL;
		}

		if (!isset($result['entry']) || !isset($result['entry'][0])) {
			$this->last_error = GRAVATAR_INCORRECT_FORMAT;
			return NULL;
		}

		return $result['entry'][0];
	}

	public function execute_profile_request($email, $format = NULL)
	{
		$this->last_error = GRAVATAR_NO_ERROR;

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->last_error = GRAVATAR_INVALID_EMAIL;
			return NULL;
		}

		$format = trim($format);

		if ($format != '') {
			$format = '.' . ltrim($format, '.');
		}

		$result = NULL;

		if ($this->curl_exists) {
			$url = $this->secure_base_url . $this->create_hash($email) . $format;
			$ch = curl_init();
			$options = array(
				0                      => CURLOPT_USERAGENT,
				1                      => $this->useragent,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST           => true,
				CURLOPT_POSTFIELDS     => array(),
				CURLOPT_URL            => $url,
				CURLOPT_TIMEOUT        => 3
				);

			if (!ini_get('safe_mode') && !ini_get('open_basedir')) {
				$options[CURLOPT_FOLLOWLOCATION] = true;
			}

			curl_setopt_array($ch, $options);
			$result = curl_exec($ch);
			$code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
			@curl_close($ch);

			if ($code != 200) {
				$this->last_error = GRAVATAR_CANT_CONNECT;
				return NULL;

				if ($this->allow_url_fopen) {
					$url = $this->base_url . $this->create_hash($email) . $format;
					$options = array(
						'http' => array('method' => 'GET', 'useragent' => $this->useragent)
						);
					$context = stream_context_create($options);
					$result = @file_get_contents($url, false, $context);
				}
				else {
					$this->last_error = GRAVATAR_CANT_CONNECT;
					return NULL;
				}
			}
		}
		else {
			return NULL;
			$url = $this->base_url . $this->create_hash($email) . $format;
			$options = array(
				'http' => array('method' => 'GET', 'useragent' => $this->useragent)
				);
			$context = stream_context_create($options);
			$result = @file_get_contents($url, false, $context);
			$this->last_error = GRAVATAR_CANT_CONNECT;
			return NULL;
		}

		if ($result === false) {
			$this->last_error = GRAVATAR_CANT_CONNECT;
			return NULL;
		}

		return $result;
	}

	public function last_error()
	{
		return $this->last_error;
	}

	public function create_hash($email)
	{
		return md5(strtolower(trim($email)));
	}

	protected function is_https()
	{
		if (function_exists('is_https')) {
			return is_https();
		}

		if (!empty($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) !== 'off')) {
			return true;
		}

		if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https')) {
			return true;
		}

		if (!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && (strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')) {
			return true;
		}

		return false;
	}

	public function set_email($email)
	{
		$email = trim(strtolower($email));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return md5($email);
		}
	}

	public function get_gravatar($email, $rating = NULL, $size = NULL, $default_image = NULL, $secure = NULL)
	{
		$hash = $this->set_email($email);

		if ($hash === NULL) {
			$hash = 'invalid_email';
		}

		$query_string = NULL;
		$options = array();

		if ($rating !== NULL) {
			$options['r'] = $rating;
		}

		if ($size !== NULL) {
			$options['s'] = $size;
		}

		if ($default_image !== NULL) {
			$options['d'] = urlencode($default_image);
		}

		if (0 < count($options)) {
			$query_string = '?' . http_build_query($options);
		}

		if ($secure !== NULL) {
			$base = $this->secure_base_url;
		}
		else {
			$base = $this->base_url;
		}

		return $base . 'avatar/' . $hash . $query_string;
	}

	public function get_profile($email, $fetch_method = 'file')
	{
		$hash = $this->set_email($email);

		if ($hash === NULL) {
			return NULL;
		}

		libxml_use_internal_errors(true);

		if ($fetch_method === 'file') {
			if (ini_get('allow_url_fopen') == false) {
				return NULL;
			}

			$str = file_get_contents($this->base_url . $hash . '.xml');
		}

		if ($fetch_method === 'curl') {
			if (!function_exists('curl_init')) {
				return NULL;
			}

			$ch = curl_init();
			$options = array(CURLOPT_RETURNTRANSFER => true, CURLOPT_POST => true, CURLOPT_URL => $this->secure_base_url . $hash . '.xml', CURLOPT_TIMEOUT => 3);
			curl_setopt_array($ch, $options);
			$str = curl_exec($ch);
		}

		$xml = simplexml_load_string($str);

		if ($xml === false) {
			$errors = array();

			foreach (libxml_get_errors() as $error) {
				$errors[] = $error->message . '\\n';
			}

			$error_string = implode('\\n', $errors);
			throw new Exception('Failed loading XML\\n' . $error_string);
		}
		else {
			return $xml->entry;
		}
	}
}

defined('BASEPATH') || exit('No direct script access allowed');
defined('GRAVATAR_NO_ERROR') || define('GRAVATAR_NO_ERROR', 0);
defined('GRAVATAR_CANT_CONNECT') || define('GRAVATAR_CANT_CONNECT', 1);
defined('GRAVATAR_INVALID_EMAIL') || define('GRAVATAR_INVALID_EMAIL', 2);
defined('GRAVATAR_PROFILE_DOES_NOT_EXIST') || define('GRAVATAR_PROFILE_DOES_NOT_EXIST', 3);
defined('GRAVATAR_INCORRECT_FORMAT') || define('GRAVATAR_INCORRECT_FORMAT', 4);

?>
