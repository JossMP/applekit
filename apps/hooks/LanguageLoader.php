<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

class LanguageLoader
{
    public function initialize()
    {
        $ci =& get_instance();
        $ci->load->helper("language");
        if (!isset($_SERVER["HTTP_USER_AGENT"]) && !preg_match("/^(curl|wget)/i", $_SERVER["HTTP_USER_AGENT"])) {
            $ci->lang->load("main", "en");
        } else {
            $array = $ci->uri->segment_array();
            $linklang = false;
            foreach ($array as $arr) {
                if (strlen($arr) == 2) {
                    $linklang = $this->searchForLang($arr) ? $arr : false;
                }
            }
            $browlang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
            $brlang = $this->searchForLang($browlang);
            if ($linklang) {
                $ci->lang->load("main", $linklang);
            } else {
                if ($brlang) {
                    $ci->lang->load("main", $browlang);
                } else {
                    $ci->lang->load("main", $ci->global_data["Olang"]);
                }
            }
        }
    }
    private function searchForLang($langs)
    {
        $dir = scandir(APPPATH . "/language/", 1);
        if (in_array($langs, $dir)) {
            return true;
        }
        return false;
    }
}

?>