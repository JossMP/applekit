<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Template_m
{
	private $tplName = 'template';

	public function getEmail()
	{
		$this->db->where('type', 1);
		return $this->db->get($this->tplName)->result();
	}

	public function getEmailBy($id)
	{
		$this->db->where('type', $id);
		return $this->db->get($this->tplName)->result();
	}

	public function getEmailFiles()
	{
		$data = array();
		$num = 0;
		$di = new RecursiveDirectoryIterator(APPPATH . 'mailTemplate');

		foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
			if (!is_dir($filename)) {
				if ($file->getFilename() != '.DS_Store') {
					$data[$num]['title'] = $file->getFilename();
					$data[$num]['path'] = $file->getPathname();
				}
			}

			++$num;
		}

		return $data;
	}

	public function getSms()
	{
		$this->db->where('type', 2);
		return $this->db->get($this->tplName)->result();
	}

	public function getEmailbyID($id)
	{
		$this->db->where('id', $id);
		$q = $this->db->get($this->tplName)->result();
		return $q[0];
	}

	public function getSmsbyID($id)
	{
		$this->db->where('type', 2);
		$this->db->where('id', $id);
		$q = $this->db->get($this->tplName)->result();
		return $q[0];
	}

	public function getOpt()
	{
		$q = $this->db->get($this->tplName)->result();
		return $q[0];
	}

	public function insert($data)
	{
		$this->db->insert($this->tplName, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id, true);
		$this->db->set($data);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}
}

defined('BASEPATH') || true;

?>
