<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Mail_m
{
	private $tplName = 'mail';
	private $tplTname = 'mailLogs';

	public function __construct()
	{
		parent::__construct();
	}

	public function getVar()
	{
		return nemos();
	}

	public function countSent()
	{
		$this->db->where('res', 1);
		$query = $this->db->get($this->tplName)->result_array();
		return count($query);
	}

	public function getInbox()
	{
		$this->db->where('res', 1);
		$this->db->where('del', 0);
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result_array();
	}

	public function getSent()
	{
		$this->db->where('res', 0);
		$this->db->where('del', 0);
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result_array();
	}

	public function getTrash()
	{
		$this->db->where('del', 1);
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result_array();
	}

	public function getNew()
	{
		$this->db->where('res', 1);
		$this->db->where('read', 1);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tplName)->result_array();
		return count($query);
	}

	public function getByID($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->tplName)->result_array();
		return $query[0];
	}

	public function read($id)
	{
		$this->db->set('read', 0);
		$this->db->where('id', $id);
		$this->db->update($this->tplName);
	}

	public function insert($from)
	{
		$data = array('to' => $this->input->post('to', true), 'from' => $from, 'subject' => $this->input->post('subject', true), 'body' => $this->input->post('body', false), 'res' => 0, 'note' => $this->input->post('note', true), 'type' => $this->input->post('mailserver', true), 'track' => $this->input->post('random', true), 'time' => setTime());
		$this->db->insert($this->tplName, $data);
	}

	public function newID($to, $from, $subject, $body)
	{
		$data = array('to' => $to, 'from' => $from, 'subject' => $subject, 'body' => $body, 'res' => 1, 'time' => setTime());
		$this->db->insert($this->tplName, $data);
	}

	public function trash($id)
	{
		$this->db->set('del', 1);
		$this->db->where('id', $id);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function getByTrack($track)
	{
		$this->db->where('track', $track);
		$q = $this->db->get($this->tplName)->result();
		return $q[0];
	}

	public function findTrack($track)
	{
		$this->db->where('track', $track);
		return $this->db->get($this->tplTname)->result();
	}

	public function countTrack($track)
	{
		$this->db->select('*')->from($this->tplTname)->where('track', $track);
		$q = $this->db->get();
		return $q->num_rows();
	}

	public function getLastTrack($track)
	{
		return $this->db->select('time')->order_by('id', 'desc')->where('track', $track)->limit(1)->get($this->tplTname)->row('time');
	}

	public function addTrack($data)
	{
		$this->db->insert($this->tplTname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function lastMailSent()
	{
		$this->db->where('res', 0);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(5);
		$q = $this->db->get($this->tplName)->result();
		return $q ? $q : array();
	}
}


?>
