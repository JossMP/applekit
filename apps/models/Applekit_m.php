<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Applekit_m
{
	public $tplName = 'options';

	public function __construct()
	{
		parent::__construct();
	}

	public function getDeviceStatus($code)
	{
		if ($code == '200') {
			return '<img src="/assets/img/status.png" class="icon device-online">';
		}

		return '<img src="/assets/img/statusoff.png" class="icon device-online">';
	}

	public function getDeviceType($type, $code, $color, $mac = 0)
	{
		if ($code == '200') {
			if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
				return '<img src="/assets/img/devices/' . $type . 'nw.png" class="icon device-online">';
			}

			if (($color == '3b3b3c-99989b') || is_null($color)) {
				if ($mac == 0) {
					return '<img src="/assets/img/devices/' . $type . 'nb.png" class="icon device-online">';
				}

				return '<img src="/assets/img/devices/' . $type . 'nw.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/' . $type . 'nw.png" class="icon device-online">';
		}

		if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
			return '<img src="/assets/img/devices/' . $type . 'ow.png" class="icon device-online">';
		}

		if (($color == '3b3b3c-99989b') || is_null($color)) {
			if ($mac == 0) {
				return '<img src="/assets/img/devices/' . $type . 'ob.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/' . $type . 'ow.png" class="icon device-online">';
		}

		return '<img src="/assets/img/devices/' . $type . 'ow.png" class="icon device-online">';
	}

	public function getDeviceImgInfoBox($type, $code, $color, $mac = 0)
	{
		if ($code == '200') {
			if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
				return '<img src="/assets/img/devices/info/' . $type . 'nw.png" class="icon device-online">';
			}

			if (($color == '3b3b3c-99989b') || is_null($color)) {
				if ($mac == 0) {
					return '<img src="/assets/img/devices/info/' . $type . 'nb.png" class="icon device-online">';
				}

				return '<img src="/assets/img/devices/info/' . $type . 'nw.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/info/' . $type . 'nw.png" class="icon device-online">';
		}

		if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
			return '<img src="/assets/img/devices/info/' . $type . 'ow.png" class="icon device-online">';
		}

		if (($color == '3b3b3c-99989b') || is_null($color)) {
			if ($mac == 0) {
				return '<img src="/assets/img/devices/info/' . $type . 'ob.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/info/' . $type . 'ow.png" class="icon device-online">';
		}

		return '<img src="/assets/img/devices/info/' . $type . 'ow.png" class="icon device-online">';
	}

	public function getDeviceImgBody($type, $code, $color, $mac)
	{
		if ($code == '200') {
			if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
				return '<img src="/assets/img/devices/body/' . $type . 'nw.png" class="icon device-online">';
			}

			if (($color == '3b3b3c-99989b') || is_null($color)) {
				if ($mac == 0) {
					return '<img src="/assets/img/devices/body/' . $type . 'nb.png" class="icon device-online">';
				}

				return '<img src="/assets/img/devices/body/' . $type . 'nw.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/body/' . $type . 'nw.png" class="icon device-online">';
		}

		if (($color == 'e1e4e3-d7d9d8') || ($color == 'white')) {
			return '<img src="/assets/img/devices/body/' . $type . 'ow.png" class="icon device-online">';
		}

		if (($color == '3b3b3c-99989b') || is_null($color)) {
			if ($mac == 0) {
				return '<img src="/assets/img/devices/body/' . $type . 'ob.png" class="icon device-online">';
			}

			return '<img src="/assets/img/devices/body/' . $type . 'ow.png" class="icon device-online">';
		}

		return '<img src="/assets/img/devices/body/' . $type . 'ow.png" class="icon device-online">';
	}

	public function battery($bat)
	{
		if ($bat == 0) {
			return $bat . '% Dead';
		}

		$num = $bat;
		$nums = explode('.', $num);
		$batterys = str_split($nums[1], 2);
		$batt = $batterys[0];

		if ($batt == 1) {
			return '100 % Full Charged';
		}

		return $batt . ' %';
	}

	public function battery2($bat)
	{
		if ($bat == 0) {
			return $bat . '% Dead';
		}

		$num = $bat;
		$nums = explode('.', $num);
		$batterys = str_split($nums[1], 2);
		$batt = $batterys[0];

		if ($batt == 1) {
			return '100';
		}

		return $batt;
	}

	public function SendEmail2($smtp = true, $host = NULL, $user = NULL, $pwd = NULL, $prot = NULL, $port = NULL, $from = NULL, $fromN = NULL, $to = NULL, $subject = NULL, $body = NULL, $sec = true)
	{
		require APPPATH . 'libraries/extra/PHPMailerAutoload.php';
		$mail = new PHPMailer(true);

		try {
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';

			if ($smtp == true) {
				$mail->isSMTP();
				$mail->Host = $host;
				$mail->SMTPAuth = true;
				$mail->Username = $user;
				$mail->Password = $pwd;

				if ($sec == true) {
					$mail->SMTPSecure = $prot;
				}

				$mail->Port = $port;
			}

			$mail->setFrom($from, $fromN);
			$mail->addReplyTo($from, $fromN);
			$mail->addAddress($to);
			$mail->isHTML(true);
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->send();
			$res = array('status' => true, 'msg' => 'Message has been sent');
			return $res;
		}
		catch (phpmailerException $e) {
			$res = array('status' => false, 'msg' => $e->errorMessage());
			return $res;
		}
		catch (Exception $e) {
			$res = array('status' => false, 'msg' => $e->getMessage());
			return $res;
		}
	}
}


?>
