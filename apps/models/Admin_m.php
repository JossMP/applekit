<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Admin_m
{
	public $tplName = 'admins';
	private $username = false;
	private $password = false;
	private $userid = false;

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$this->db->select(array('id', 'userName', 'fName', 'userEmail', 'time'));
		$query = $this->db->get($this->tplName)->result_array();
		return $query;
	}

	public function getByID($id)
	{
		$this->db->select(array('id', 'userName', 'fName', 'userEmail', 'time'));
		$this->db->where('id', $id);
		$query = $this->db->get($this->tplName)->result_array();
		return $query[0];
	}

	public function getByUser2($username)
	{
		$this->db->select(array('id', 'userName', 'fName', 'userEmail', 'time'));
		$this->db->where('userName', $username);
		$query = $this->db->get($this->tplName)->result_array();
		return $query[0];
	}

	public function getByUser($userName)
	{
		$this->db->select(array('id', 'userName', 'time'));
		$this->db->where('userName', $userName);
		$query = $this->db->get($this->tplName)->result_array();
		return $query;
	}

	public function getByPass($userName)
	{
		$this->db->select(array('id', 'userName', 'passWord'));
		$this->db->where('userName', $userName);
		$query = $this->db->get($this->tplName)->result_array();
		return $query;
	}

	public function getByPass2($id)
	{
		$this->db->select(array('passWord'));
		$this->db->where('id', $id);
		$query = $this->db->get($this->tplName)->result_array();
		return $query[0];
	}

	public function auth()
	{
		$this->username = $this->input->post('userName', true);
		$this->password = $this->input->post('userPwd', true);

		if ($this->getByUser($this->username) == true) {
			$hashed = $this->getByPass($this->username);

			if ($this->phpass->check($this->password, $hashed[0]['passWord'])) {
				return true;
			}

			return 'Please check your Password is correct !';
		}

		return 'Please check your username is correct !';
	}

	public function insert()
	{
		$hashed = $this->phpass->hash($this->input->post('passWord', true));
		$data = array('userName' => $this->input->post('userName', true), 'passWord' => $hashed, 'fName' => $this->input->post('fName', true), 'userEmail' => $this->input->post('userEmail', true), 'time' => setTime());
		$this->db->set($data, true);
		$this->db->insert($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function edit($id)
	{
		$this->db->where('id', $id, true);
		$data = array('fName' => $this->input->post('fName', true));
		$this->db->set($data);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function updatePass($id)
	{
		$old = $this->input->post('OldpassWord');
		$hashed = $this->getByPass2($id);

		if ($this->phpass->check($old, $hashed['passWord'])) {
		}
		else {
			return false;
		}

		$hashed = $this->phpass->hash($this->input->post('passWord', true));
		$this->db->where('id', $id, true);
		$data = array('passWord' => $hashed);
		$this->db->set($data);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function updateEmail($id)
	{
		$this->db->where('id', $id, true);
		$data = array('userEmail' => $this->input->post('userEmail', true));
		$this->db->set($data);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}
}


?>
