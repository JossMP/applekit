<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Sms_m
{
	public $tplName = 'sms';
	public $tplApi = 'smsApi';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$this->db->insert($this->tplName, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result();
	}

	public function getBysuccess()
	{
		$this->db->where('scode', 0);
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result_array();
	}

	public function getByfaild()
	{
		$this->db->where('scode !=', 0);
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplName)->result_array();
	}

	public function del($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function getAllApi()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->tplApi)->result();
	}

	public function getApiBy($filed, $id, $type = false)
	{
		$this->db->where($filed, $id);
		$this->db->order_by('id', 'DESC');
		$q = $this->db->get($this->tplApi)->result();
		return $type == true ? $q[0] : $q;
	}

	public function addApi($data)
	{
		$this->db->insert($this->tplApi, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function updateApi($id, $data)
	{
		$this->db->where('id', $id, true);
		$this->db->update($this->tplApi, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function delApi($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tplApi);
		return 0 < $this->db->affected_rows() ? true : false;
	}
}


?>
