<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Device_m
{
	private $_tplName = 'device';

	public function __construct()
	{
		parent::__construct();
	}

	public function countAll()
	{
		return count($this->db->get($this->_tplName)->result_array());
	}

	public function countByIOS()
	{
		$this->db->where('isMac', 0);
		return count($this->db->get($this->_tplName)->result_array());
	}

	public function countByOSX()
	{
		$this->db->where('isMac', 1);
		return count($this->db->get($this->_tplName)->result_array());
	}

	public function getByUser($user)
	{
		$this->db->where('userID', $user);
		$this->db->where('deep', 0);
		$this->db->order_by('isMac ASC, deviceStatus ASC');
		$query = $this->db->get($this->_tplName)->result_array();
		return $query;
	}

	public function getByUserDeep($user)
	{
		$this->db->where('userID', $user);
		$this->db->where('deep', 1);
		$this->db->order_by('isMac ASC, deviceStatus ASC');
		$query = $this->db->get($this->_tplName)->result_array();
		return $query;
	}

	public function getByStatus($user)
	{
		$this->db->where('userID', $user);
		$this->db->where('deviceStatus', '200');
		$this->db->order_by('isMac ASC, deviceStatus ASC');
		$query = $this->db->get($this->_tplName)->result_array();
		return $query;
	}

	public function getByStatus2($user)
	{
		$this->db->where('userID', $user);
		$this->db->where('deviceStatus', '200');
		$this->db->where('isMac', 0);
		$this->db->order_by('isMac ASC, deviceStatus ASC');
		$query = $this->db->get($this->_tplName)->result_array();
		return $query;
	}

	public function getByStatus3($user)
	{
		$this->db->where('userID', $user);
		$this->db->where('deviceStatus', '200');
		$this->db->where('isMac', 1);
		$this->db->order_by('isMac ASC, deviceStatus ASC');
		$query = $this->db->get($this->_tplName)->result_array();
		return $query;
	}

	public function insert($data)
	{
		$this->db->insert($this->_tplName, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function del($data)
	{
		$this->db->where('id', $data);
		$this->db->delete($this->_tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}
}


?>
