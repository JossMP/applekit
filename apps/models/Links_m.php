<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Links_m
{
	public $tplname = 'links';
	public $tplTracer = 'linksLogs';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplname)->result();
	}

	public function getById($id)
	{
		$this->db->where('id', $id, true);
		$q = $this->db->get($this->tplname)->result();
		return $q[0];
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id, true);
		$this->db->update($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function insert($data)
	{
		$this->db->insert($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function delete($id)
	{
		$this->db->delete($this->tplname, 'id=' . $id);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function addView($data)
	{
		$this->db->insert($this->tplTracer, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function getByUniqL($uniq)
	{
		$this->db->where('uniq', $uniq);
		$q = $this->db->get($this->tplname)->result();
		return $q ? $q[0] : false;
	}

	public function getByUniqT($uniq)
	{
		$this->db->where('uniq', $uniq);
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplTracer)->result();
	}

	public function countView($uniq)
	{
		$this->db->select('*')->from($this->tplTracer)->where('uniq', $uniq);
		$q = $this->db->get();
		return $q->num_rows();
	}

	public function getLastView($uniq)
	{
		return $this->db->select('time')->order_by('id', 'desc')->where('uniq', $uniq)->limit(1)->get($this->tplTracer)->row('time');
	}
}


?>
