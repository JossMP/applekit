<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Wrong_m
{
	public $tplname = 'pssLog';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplname)->result_array();
	}

	public function insert($data)
	{
		$this->db->insert($this->tplname, $data);
		return $this->db->insert_id();
	}

	public function cut()
	{
		return $this->db->truncate($this->tplname);
	}
}


?>
