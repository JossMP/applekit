<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Cron_m
{
	public $tplname = 'cronjobs';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$this->db->order_by('id', 'ASC');
		return $this->db->get($this->tplname)->result();
	}

	public function getById($id)
	{
		$this->db->where('id', $id, true);
		$q = $this->db->get($this->tplname)->result();
		return $q[0];
	}

	public function getBy($id, $data)
	{
		$this->db->where($id, $data, true);
		$q = $this->db->get($this->tplname)->result();
		return $q ? $q : false;
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id, true);
		$this->db->update($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function insert($data)
	{
		$this->db->insert($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function delete($id)
	{
		$this->db->delete($this->tplname, 'id=' . $id);
		return 0 < $this->db->affected_rows() ? true : false;
	}
}


?>
