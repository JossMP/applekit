<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
class Visit_m
{
	public $tplname = 'vistors';
	private $tplNameb = 'blocked';

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll()
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplname)->result();
	}

	public function getNoti()
	{
		$this->db->where('noti', 1);
		return $this->db->get($this->tplname)->result();
	}

	public function getLast()
	{
		$now = now() - 600;
		$this->db->where('update >= ' . $now);
		return $this->db->get($this->tplname)->result();
	}

	public function vfindIp($ip)
	{
		$this->db->order_by('id', 'desc');
		$this->db->where('ip', $ip, true);
		return $this->db->get($this->tplname)->result();
	}

	public function updatev($ip, $data)
	{
		$this->db->where('ip', $ip, true);
		$this->db->update($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function updatemulti($ip, $data)
	{
		$this->db->update($this->tplname, $data, 'ip');
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function insert($data)
	{
		if ($this->vfindIp($data['ip'])) {
			$data['update'] = now();
			$data['noti'] = 1;
			return $this->updatev($data['ip'], $data);
		}

		$this->db->insert($this->tplname, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function cut()
	{
		return $this->db->truncate($this->tplname);
	}

	public function getAllip()
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tplNameb)->result_array();
	}

	public function findIp($ip)
	{
		$this->db->where('ip', $ip, true);
		return $this->db->get($this->tplNameb)->result();
	}

	public function block($data)
	{
		if ($this->findIp($data)) {
			return 'Exist';
		}

		$this->db->insert($this->tplNameb, array('ip' => $data, 'date' => setTime()));
		return 0 < $this->db->affected_rows() ? 'True' : 'False';
	}

	public function blockr($id)
	{
		$this->db->delete($this->tplNameb, 'id=' . $id);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function cutp()
	{
		return $this->db->truncate($this->tplNameb);
	}

	public function insertp($data)
	{
		$this->db->insert($this->tplNameb, $data);
		return 0 < $this->db->affected_rows() ? true : false;
	}

	public function insertp2($data)
	{
		$this->db->insert($this->tplNameb, $data);
	}
}


?>
