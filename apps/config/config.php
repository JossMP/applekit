<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
$config["base_url"] = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https" : "http";
$config["base_url"] .= "://" . $_SERVER["HTTP_HOST"];
$config["base_url"] .= str_replace(basename($_SERVER["SCRIPT_NAME"]), "", $_SERVER["SCRIPT_NAME"]);
$config["index_page"] = "";
$config["uri_protocol"] = "REQUEST_URI";
$config["url_suffix"] = "";
$config["language"] = "en";
$config["charset"] = "UTF-8";
$config["enable_hooks"] = true;
$config["subclass_prefix"] = "MY_";
$config["composer_autoload"] = "./vendor/autoload.php";
$config["permitted_uri_chars"] = "a-z 0-9~%.:_\\- @ =";
$config["allow_get_array"] = true;
$config["enable_query_strings"] = false;
$config["controller_trigger"] = "c";
$config["function_trigger"] = "m";
$config["directory_trigger"] = "d";
$config["log_threshold"] = 0;
$config["log_path"] = "";
$config["log_file_extension"] = "log";
$config["log_file_permissions"] = 420;
$config["log_date_format"] = "Y-m-d H:i:s";
$config["error_views_path"] = "";
$config["cache_path"] = "";
$config["cache_query_string"] = false;
$config["encryption_key"] = "";
$config["sess_driver"] = "database";
$config["sess_cookie_name"] = "ci_sessions";
$config["sess_expiration"] = 7200;
$config["sess_save_path"] = "ci_sessions";
$config["sess_match_ip"] = false;
$config["sess_time_to_update"] = 300;
$config["sess_regenerate_destroy"] = false;
$config["cookie_prefix"] = "";
$config["cookie_domain"] = "";
$config["cookie_path"] = "/";
$config["cookie_secure"] = false;
$config["cookie_httponly"] = false;
$config["standardize_newlines"] = false;
$config["global_xss_filtering"] = true;
$config["csrf_protection"] = false;
$config["csrf_token_name"] = "csrf_test_name";
$config["csrf_cookie_name"] = "csrf_cookie_name";
$config["csrf_expire"] = 7200;
$config["csrf_regenerate"] = false;
$config["csrf_exclude_uris"] = array();
$config["compress_output"] = false;
$config["time_reference"] = "local";
$config["rewrite_short_tags"] = false;
$config["proxy_ips"] = "";
$file = fopen(APPPATH . "config/m.txt", "r");
$config["google_maps"] = fgets($file);
fclose($file);

?>
