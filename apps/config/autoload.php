<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
$autoload["packages"] = array();
$autoload["libraries"] = array("database", "session", "email", "form_validation", "phpass", "user_agent", "Blade");
$autoload["drivers"] = array();
$autoload["helper"] = array("url", "form", "ago", "noti", "date", "language");
$autoload["config"] = array("global");
$autoload["language"] = array("main");
$autoload["model"] = array("options_m" => "Opts");

?>
