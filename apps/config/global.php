<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
$file = fopen(APPPATH . "config/g.txt", "r");
$config["licnKeys"] = fgets($file);
fclose($file);
$config["version"] = "4.2 Stable";
$config["index_text"] = array("Coming Soon", "Under Construction", "Temporary Maintenance", "Technical error", "We will be back soon", "Reload Again", "Closed at the moment");

?>
