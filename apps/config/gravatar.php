<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
$config["gravatar_base_url"] = "http://www.gravatar.com/";
$config["gravatar_secure_base_url"] = "https://secure.gravatar.com/";
$config["gravatar_image_extension"] = ".png";
$config["gravatar_image_size"] = 80;
$config["gravatar_default_image"] = "";
$config["gravatar_force_default_image"] = false;
$config["gravatar_rating"] = "";
$config["gravatar_useragent"] = "PHP Gravatar Library";

?>
