<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
$hook["post_controller_constructor"] = array("class" => "LanguageLoader", "function" => "initialize", "filename" => "LanguageLoader.php", "filepath" => "hooks");

?>
