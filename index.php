<?php
/*
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.1
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 */

date_default_timezone_set("America/Toronto");
if (!file_exists("apps/config/database.php")) {
    $link = (isset($_SERVER["HTTPS"]) ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["PHP_SELF"]);
    header("Location: " . $link . "/install");
    exit;
}
define("ENVIRONMENT", isset($_SERVER["CI_ENV"]) ? $_SERVER["CI_ENV"] : "development");
$file  = file_exists("debug.txt") ? fopen("debug.txt", "r") : 0;
$debug = file_exists("debug.txt") ? fgets($file) : 0;
/*
switch (ENVIRONMENT) {
    case "development":
        error_reporting(-1);
        ini_set("display_errors", $debug);
        break;
    case "testing":
    case "production":
        ini_set("display_errors", $debug);
        if (version_compare(PHP_VERSION, "5.3", ">=")) {
            error_reporting(32767 & ~8 & ~8192 & ~2048 & ~1024 & ~16384);
        } else {
            error_reporting(32767 & ~8 & ~2048 & ~1024);
        }
        break;
    default:
        header("HTTP/1.1 503 Service Unavailable.", true, 503);
        echo "The application environment is not set correctly.";
        exit(1);
}
*/
file_exists("debug.txt");
file_exists("debug.txt") ? fclose($file) : NULL;
$system_path        = "apps/sys";
$application_folder = "apps";
$view_folder        = "";
if (defined("STDIN")) {
    chdir(dirname(__FILE__));
}
if (($_temp = realpath($system_path)) !== false) {
    $system_path = $_temp . DIRECTORY_SEPARATOR;
} else {
    $system_path = strtr(rtrim($system_path, "/\\"), "/\\", DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
}
if (!is_dir($system_path)) {
    header("HTTP/1.1 503 Service Unavailable.", true, 503);
    echo "Your system folder path does not appear to be set correctly. Please open the following file and correct this: " . pathinfo(__FILE__, PATHINFO_BASENAME);
    exit(3);
}
define("SELF", pathinfo(__FILE__, PATHINFO_BASENAME));
define("BASEPATH", $system_path);
define("FCPATH", dirname(__FILE__) . DIRECTORY_SEPARATOR);
define("SYSDIR", basename(BASEPATH));
if (is_dir($application_folder)) {
    if (($_temp = realpath($application_folder)) !== false) {
        $application_folder = $_temp;
    } else {
        $application_folder = strtr(rtrim($application_folder, "/\\"), "/\\", DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
    }
} else {
    if (is_dir(BASEPATH . $application_folder . DIRECTORY_SEPARATOR)) {
        $application_folder = BASEPATH . strtr(trim($application_folder, "/\\"), "/\\", DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
    } else {
        header("HTTP/1.1 503 Service Unavailable.", true, 503);
        echo "Your application folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF;
        exit(3);
    }
}
define("APPPATH", $application_folder . DIRECTORY_SEPARATOR);
if (!isset($view_folder[0]) && is_dir(APPPATH . "views" . DIRECTORY_SEPARATOR)) {
    $view_folder = APPPATH . "views";
} else {
    if (is_dir($view_folder)) {
        if (($_temp = realpath($view_folder)) !== false) {
            $view_folder = $_temp;
        } else {
            $view_folder = strtr(rtrim($view_folder, "/\\"), "/\\", DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
        }
    } else {
        if (is_dir(APPPATH . $view_folder . DIRECTORY_SEPARATOR)) {
            $view_folder = APPPATH . strtr(trim($view_folder, "/\\"), "/\\", DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
        } else {
            header("HTTP/1.1 503 Service Unavailable.", true, 503);
            echo "Your view folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF;
            exit(3);
        }
    }
}
define("VIEWPATH", $view_folder . DIRECTORY_SEPARATOR);
require_once BASEPATH . "core/CodeIgniter.php";
time_get_set();
echo "EXIT";exit();
function time_get_set()
{
    require_once BASEPATH . "database/DB.php";
    $db =& DB();
    $result = $db->get("options")->result();
    if (!$result) {
        date_default_timezone_set("America/Toronto");
    } else {
        if (empty($result[0]->timeZone) || is_null($result[0]->timeZone)) {
            date_default_timezone_set("America/Toronto");
        } else {
            date_default_timezone_set($result[0]->timeZone);
        }
    }
}

?>
