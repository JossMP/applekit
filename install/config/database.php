<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

defined('BASEPATH') || exit('No direct script access allowed');
$active_group = '%TYPE%';
$query_builder = true;
$db['sqlite'] = array(
	'dsn'                       => '',
	'hostname'                  => '',
	'username'                  => '',
	'password'                  => '',
	'database'                  => APPPATH . SYSDIR . '/database/sys.sqlite',
	'dbdriver'                  => 'sqlite3',
	'dbprefix'                  => '',
	'pconnect'                  => false,
	'db_debug'                  => false,
	'cache_on'                  => false,
	'cachedir'                  => '',
	'char_set'                  => 'utf8',
	'dbcollat'                  => 'utf8_unicode_ci',
	'swap_pre'                  => '',
	'encrypt'                   => false,
	'compress'                  => false,
	'stricton'                  => false,
	'failover'                  => array(),
	'save_queries'              => true,
	'date_default_timezone_set' => 'Asia/Kolkata'
	);
$db['mysql'] = array(
	'dsn'                       => '',
	'hostname'                  => '%HOSTNAME%',
	'username'                  => '%USERNAME%',
	'password'                  => '%PASSWORD%',
	'database'                  => '%DATABASE%',
	'dbdriver'                  => 'mysqli',
	'dbprefix'                  => '',
	'pconnect'                  => false,
	'db_debug'                  => false,
	'cache_on'                  => false,
	'cachedir'                  => '',
	'char_set'                  => 'utf8',
	'dbcollat'                  => 'utf8_unicode_ci',
	'swap_pre'                  => '',
	'encrypt'                   => false,
	'compress'                  => false,
	'stricton'                  => false,
	'failover'                  => array(),
	'save_queries'              => true,
	'date_default_timezone_set' => 'Asia/Kolkata'
	);

?>
