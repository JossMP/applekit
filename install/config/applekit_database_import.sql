# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.38)
# Database: applekit
# Generation Time: 2018-06-19 21:31:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `passWord` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fName` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `userEmail` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table api
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api`;

CREATE TABLE `api` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `deviceName` varchar(222) DEFAULT NULL,
  `deviceOS` varchar(222) DEFAULT NULL,
  `deviceIP` varchar(222) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table blocked
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocked`;

CREATE TABLE `blocked` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `blocked` WRITE;
/*!40000 ALTER TABLE `blocked` DISABLE KEYS */;

INSERT INTO `blocked` (`id`, `ip`, `date`)
VALUES
	(1,'17.168.82.92','2016-04-30 20:26:25'),
	(2,'64.233.160.0','2016-04-30 20:26:46'),
	(3,'66.102.0.0','2016-04-30 20:26:53'),
	(4,'66.249.64.0','2016-04-30 20:27:04'),
	(5,'72.14.192.0','2016-04-30 20:27:12'),
	(6,'74.125.0.0','2016-04-30 20:27:20'),
	(7,'209.85.128.0','2016-04-30 20:27:26'),
	(8,'216.239.32.0','2016-04-30 20:27:33'),
	(9,'64.4.0.0','2016-04-30 20:27:38'),
	(10,'65.52.0.0','2016-04-30 20:27:45'),
	(11,'131.253.21.0','2016-04-30 20:27:50'),
	(12,'157.54.0.0','2016-04-30 20:27:54'),
	(13,'207.46.0.0','2016-04-30 20:27:59'),
	(14,'207.68.128.0','2016-04-30 20:28:03'),
	(15,'8.12.144.0','2016-04-30 20:28:09'),
	(16,'66.196.64.0','2016-04-30 20:28:14'),
	(17,'66.228.160.0','2016-04-30 20:28:18'),
	(18,'67.195.0.0','2016-04-30 20:28:22'),
	(19,'66.249.64.0','2016-04-30 20:28:27'),
	(20,'72.14.192.0','2016-04-30 20:28:32'),
	(21,'74.125.0.0','2016-04-30 20:28:36'),
	(22,'209.85.128.0','2016-04-30 20:28:41'),
	(23,'216.239.32.0','2016-04-30 20:28:45'),
	(24,'64.4.0.0','2016-04-30 20:28:51'),
	(25,'65.52.0.0','2016-04-30 20:28:55'),
	(26,'131.253.21.0','2016-04-30 20:28:59'),
	(27,'157.54.0.0','2016-04-30 20:29:03'),
	(28,'207.46.0.0','2016-04-30 20:29:08'),
	(29,'207.68.128.0','2016-04-30 20:29:11'),
	(30,'8.12.144.0','2016-04-30 20:29:16'),
	(31,'66.196.64.0','2016-04-30 20:29:21'),
	(32,'66.228.160.0','2016-04-30 20:29:25'),
	(33,'67.195.0.0','2016-04-30 20:29:32'),
	(34,'68.142.192.0','2016-04-30 20:29:38'),
	(35,'72.30.0.0','2016-04-30 20:29:42'),
	(36,'74.6.0.0','2016-04-30 20:29:46'),
	(37,'98.136.0.0','2016-04-30 20:29:51'),
	(38,'202.160.176.0','2016-04-30 20:29:55'),
	(39,'209.191.64.0','2016-04-30 20:29:56'),
	(40,'185.93.185.252','2016-05-03 14:42:22'),
	(41,'198.143.180.166','2016-05-03 14:43:37'),
	(42,'213.128.81.66','2016-05-03 14:43:53'),
	(43,'213.61.218.52','2016-05-03 14:44:10'),
	(44,'169.229.3.91','2016-05-03 14:44:43'),
	(45,'74.115.0.65','2016-05-03 14:49:04'),
	(46,'54.80.30.202','2016-05-03 14:49:21'),
	(47,'81.213.206.174','2016-05-03 14:49:42'),
	(48,'81.218.175.83','2016-05-03 14:50:18'),
	(49,'76.74.97.4','2016-05-03 14:51:14'),
	(50,'66.102.8.214','2016-05-03 14:53:39'),
	(51,'69.46.27.154','2016-05-03 14:53:59'),
	(52,'54.190.85.125','2016-05-03 14:57:40'),
	(53,'209.112.251.101','2016-05-03 14:57:58'),
	(54,'115.178.196.76','2016-05-03 14:58:09'),
	(55,'104.239.173.43','2016-05-03 14:58:22'),
	(56,'208.42.251.123','2016-05-03 14:58:31'),
	(57,'64.246.166.64','2016-05-03 14:58:40'),
	(58,'74.211.6.163','2016-05-03 14:58:49'),
	(59,'95.175.97.229','2016-05-03 14:58:59'),
	(60,'204.101.161.159','2016-05-03 14:59:12'),
	(61,'184.173.226.131','2016-05-03 14:59:22'),
	(62,'50.180.123.151','2016-05-03 14:59:32'),
	(63,'113.210.51.108','2016-05-03 14:59:41'),
	(64,'114.124.7.179','2016-05-03 14:59:50'),
	(65,'64.20.227.134','2016-05-03 15:04:42'),
	(66,'212.47.247.88','2016-05-03 19:15:24'),
	(67,'66.102.8.230','2016-05-04 07:40:22'),
	(68,'118.7.148.101','2016-05-04 07:40:34'),
	(69,'217.23.12.207','2016-05-04 07:41:15'),
	(70,'180.97.106.24','2016-05-04 07:41:40'),
	(71,'91.148.219.173','2016-05-04 07:41:53'),
	(72,'54.146.156.140','2016-05-04 07:42:11'),
	(73,'54.161.245.30','2016-05-04 07:42:21'),
	(74,'5.255.82.199','2016-05-04 07:42:42'),
	(75,'86.122.244.157','2016-05-04 07:44:19'),
	(76,'208.184.112.74','2016-05-04 07:44:43'),
	(77,'199.203.92.234','2016-05-04 07:44:55'),
	(78,'104.42.198.99','2016-05-04 07:45:05'),
	(79,'95.108.149.136','2016-05-04 07:45:19'),
	(80,'124.66.185.83','2016-05-04 07:45:29'),
	(81,'150.70.173.50','2016-05-04 07:45:37'),
	(82,'108.59.10.141','2016-05-04 07:46:26'),
	(83,'193.128.109.52','2016-05-04 07:46:45'),
	(84,'95.45.254.122','2016-05-04 07:47:29'),
	(85,'148.251.45.130','2016-05-04 07:47:38'),
	(86,'46.4.27.145','2016-05-04 07:47:52'),
	(87,'213.240.238.135','2016-05-04 07:49:07'),
	(88,'192.99.150.120','2016-05-04 07:49:17'),
	(89,'118.219.252.37','2016-05-04 07:50:15'),
	(90,'162.216.46.74','2016-05-04 07:50:26'),
	(91,'31.4.180.40','2016-05-04 07:50:38'),
	(92,'195.214.79.20','2016-05-04 07:51:06'),
	(93,'46.101.250.220','2016-05-04 07:51:14'),
	(95,'91.199.104.51','2016-05-04 07:51:55'),
	(96,'213.81.140.54','2016-05-04 07:52:36'),
	(97,'150.70.173.5','2016-05-04 07:53:08'),
	(98,'51.254.170.134','2016-05-04 07:53:37'),
	(99,'103.16.26.150','2016-05-04 07:53:48'),
	(100,'178.24.23.28','2016-05-04 07:54:19'),
	(101,'138.128.180.54','2016-05-04 07:54:40'),
	(102,'206.183.1.74','2016-05-04 07:55:07'),
	(103,'150.70.173.51','2016-05-04 07:55:24'),
	(104,'93.173.239.194','2016-05-04 07:55:33'),
	(105,'69.156.45.216','2016-05-04 07:55:52'),
	(106,'104.238.194.164','2016-05-04 07:58:41'),
	(107,'140.224.95.179','2016-05-04 07:59:05'),
	(108,'138.201.16.75','2016-05-04 16:10:36'),
	(109,'199.255.210.165','2016-05-04 16:11:06'),
	(110,'81.161.59.17','2016-06-19 22:45:09'),
	(111,'66.249.93.191','2016-06-19 22:45:27'),
	(112,'199.30.231.2','2016-06-19 22:45:36'),
	(113,'64.246.165.180','2016-06-19 22:45:46'),
	(117,'64.246.165.181','2016-08-29 21:42:04');

/*!40000 ALTER TABLE `blocked` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table cronjobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cronjobs`;

CREATE TABLE `cronjobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execute_at` timestamp NULL DEFAULT NULL,
  `email_to` varchar(222) NOT NULL DEFAULT '',
  `name_to` varchar(222) DEFAULT NULL,
  `send_by` varchar(222) DEFAULT 'default',
  `status` int(11) NOT NULL DEFAULT '0',
  `msg` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table device
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(11) NOT NULL,
  `email` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `deviceStatus` bigint(11) DEFAULT NULL,
  `deviceDisplayName` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `rawDeviceModel` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `name` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `deviceColor` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `batteryLevel` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `isMac` int(11) DEFAULT NULL,
  `latitude` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deviceID` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `serial` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imei` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `udid` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deep` int(11) NOT NULL DEFAULT '0',
  `img` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniq` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `victim` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `via` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'none',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;

INSERT INTO `links` (`id`, `uniq`, `victim`, `time`, `via`)
VALUES
	(1,'dDeEmMoO','demo@demo.com','2017-02-09 06:54:30',NULL);

/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table linksLogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `linksLogs`;

CREATE TABLE `linksLogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniq` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userAgent` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `broswer` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table mail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mail`;

CREATE TABLE `mail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `body` longtext COLLATE utf8_unicode_ci,
  `fav` int(1) DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read` int(1) DEFAULT '1',
  `res` int(1) DEFAULT '1',
  `del` int(1) DEFAULT '0',
  `note` longtext COLLATE utf8_unicode_ci,
  `type` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `track` varchar(222) COLLATE utf8_unicode_ci DEFAULT '0',
  `view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table mailLogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mailLogs`;

CREATE TABLE `mailLogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `track` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `broswer` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userAgent` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `close` int(1) DEFAULT '0',
  `Name` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `sentEmail` int(1) DEFAULT '0',
  `email` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `isDelete` int(1) DEFAULT '0',
  `sender` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `senderName` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(4) DEFAULT '2525',
  `protcol` varchar(11) COLLATE utf8_unicode_ci DEFAULT 'ssl',
  `smtp2go` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apiEmailPassword` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `langs` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'en',
  `template` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'icloud',
  `closeMsg` text COLLATE utf8_unicode_ci,
  `detect` int(1) DEFAULT '0',
  `mobile` int(11) DEFAULT '0',
  `emailvaild` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `smsvalidapi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loginPage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmiPage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmi2Page` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fmi2018Page` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disablePage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passPage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mapPage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itunesPage` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nicloudPage` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gmailPage` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotPage` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendPulse` longtext COLLATE utf8_unicode_ci,
  `sendPluseEmail` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elastice` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elasticeEmail` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `providerEmail` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `providerPwd` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `providerPort` int(20) DEFAULT NULL,
  `wavecellApi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `block` int(11) DEFAULT '0',
  `blockredirect` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` int(11) DEFAULT '0',
  `redirecturl` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'https://icloud.com/',
  `redirecturl2` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'https://appleid.apple.com/',
  `redirectMap` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'https://mapsconnect.apple.com',
  `notiSMTP` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiSMTPemail` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiSMTPpwd` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiSMTPport` int(11) DEFAULT NULL,
  `expireTime` int(11) DEFAULT '48',
  `isTrack` int(1) DEFAULT NULL,
  `tracklenght` int(1) DEFAULT '10',
  `linkVisit` int(1) DEFAULT '0',
  `blankCount` int(11) DEFAULT NULL,
  `disableCount` int(11) DEFAULT NULL,
  `disableLink` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expireLink` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeZone` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'America/Toronto',
  `smtpServ` int(11) DEFAULT '0',
  `senderID` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nimbowAPI` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmSMSapi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plivoAuthID` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plivoAuthToken` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viaUser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viaPass` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bulkUser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bulkPass` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budgetUser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budgetID` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budgetHandle` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waveid` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wavesub` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wavepwd` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nexmokey` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nexmosecert` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sinchkey` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sinchsecret` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `beepsendtoken` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alienicsuser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alienicspwd` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postmarktoken` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postmarksender` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendgridapi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senderKitAPI` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removalApi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shortKitApi` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cronJobs` int(1) DEFAULT '1',
  `smsserver` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailserver` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telegram` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;

INSERT INTO `options` (`id`, `close`, `Name`, `sentEmail`, `email`, `isDelete`, `sender`, `senderName`, `port`, `protcol`, `smtp2go`, `apiEmailPassword`, `langs`, `template`, `closeMsg`, `detect`, `mobile`, `emailvaild`, `smsvalidapi`, `loginPage`, `fmiPage`, `fmi2Page`, `fmi2018Page`, `disablePage`, `passPage`, `mapPage`, `itunesPage`, `nicloudPage`, `gmailPage`, `hotPage`, `sendPulse`, `sendPluseEmail`, `elastice`, `elasticeEmail`, `providerEmail`, `providerPwd`, `providerPort`, `wavecellApi`, `block`, `blockredirect`, `redirect`, `redirecturl`, `redirecturl2`, `redirectMap`, `notiSMTP`, `notiSMTPemail`, `notiSMTPpwd`, `notiSMTPport`, `expireTime`, `isTrack`, `tracklenght`, `linkVisit`, `blankCount`, `disableCount`, `disableLink`, `expireLink`, `timeZone`, `smtpServ`, `senderID`, `nimbowAPI`, `cmSMSapi`, `plivoAuthID`, `plivoAuthToken`, `viaUser`, `viaPass`, `bulkUser`, `bulkPass`, `budgetUser`, `budgetID`, `budgetHandle`, `waveid`, `wavesub`, `wavepwd`, `nexmokey`, `nexmosecert`, `sinchkey`, `sinchsecret`, `beepsendtoken`, `alienicsuser`, `alienicspwd`, `postmarktoken`, `postmarksender`, `sendgridapi`, `senderKitAPI`, `removalApi`, `shortKitApi`, `cronJobs`, `smsserver`, `mailserver`, `telegram`)
VALUES
	(1,0,'iCloud',1,'admin@admin.com',0,'sender-name@domain-name.com','iCloud',NULL,'tls','','','en',NULL,'We Are Closing For a Moment',0,0,'181a40abf9707ef0d7dc18d8f07972a35e2e3a7d458f322ab1cc957e9009','','','','','','','','','','','','','','','','','','',587,NULL,0,'http://icloud.com/',0,'https://icloud.com/','https://appleid.apple.com/','https://mapsconnect.apple.com','','','',587,72,1,5,0,1,1,'http://nemoze.com','http://nemoz.net','Europe/Istanbul',1,'iCloud','','','','','','','','','','','','','','','','','','','','','','','','','','','',1,'','csmtp-1','');

/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pssLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pssLog`;

CREATE TABLE `pssLog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `pass` varchar(222) COLLATE utf8_unicode_ci DEFAULT '',
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userAgent` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'iCloud',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table sms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms`;

CREATE TABLE `sms` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `scode` bigint(11) DEFAULT NULL,
  `msgParts` int(11) DEFAULT NULL,
  `from` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` longtext COLLATE utf8_unicode_ci,
  `via` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sMsg` text COLLATE utf8_unicode_ci,
  `login` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tmpl` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `track` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table smtp
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smtp`;

CREATE TABLE `smtp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `type` varchar(222) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `template`;

CREATE TABLE `template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '1',
  `title` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `lang` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'en',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;

INSERT INTO `template` (`id`, `type`, `title`, `subject`, `content`, `lang`, `time`)
VALUES
	(1,1,'Reset Password',NULL,'<!DOCTYPE html><html><head><meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"><base target=\"_blank\"></head><body style=\"margin: 0px; padding: 0px; min-height: 500px;\"><div class=\"mailwrapper\" style=\"font-size: 14px; color: #333; font-smooth: always; -webkit-font-smoothing: antialiased;\"><table style=\"table-layout: fixed;width: 100%;\"><tbody><tr><td><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"mainTable\" style=\"margin: 0 auto;font-size: inherit; line-height: inherit; text-align: center; border-spacing: 0; border-collapse: collapse; -premailer-cellpadding: 0; -premailer-cellspacing: 0; padding: 0; border: 0;\"><tbody><tr><td class=\"topPadding\" height=\"16\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; height: 16px; -premailer-height: 16;\"><br></td></tr><tr><td class=\"centerColumn\" style=\"width: 685px; -premailer-width: 685;\"><table class=\"iPhone_font\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; font-size: inherit; line-height: 18px; padding: 0px; border: 0px;\"><tbody><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"heading_logo\" style=\"                                 text-align: right;\r\n                           width: 600px;\"><img src=\"https://statici.icloud.com/emailimages/v4/common/apple_logo_web@2x.png\" style=\"width: 22px; height: 26px;\" height=\"26\" width=\"22\" class=\"fr-fic fr-dii\"></td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"iPhone_font\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; line-height: 18px; padding-top: 44px;                                 text-align: left;\r\n                               font-size: 20px; color: #2b2b2b; font-smooth: always; -webkit-font-smoothing: antialiased;\"><br></td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"iPhone_font\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; line-height: 18px;                                  text-align: left;\r\n                             font-size: 15px; color: #333; padding: 17px 0 0 0; font-smooth: always; -webkit-font-smoothing: antialiased;\"><span><a href=\"\" style=\"color: #2b2b2b; text-decoration: none;\"><strong>Dear {{name}}</strong></a></span></td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"iPhone_font\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; line-height: 18px; font-smooth: always; -webkit-font-smoothing: antialiased;                                  text-align: left;\r\n                                   font-size: 15px; color: #333; padding: 18px 0 0 0;\">Your Apple ID ( {{email}} ) was used to sign in to iCloud on an iphone 6. Time: {{time}} GMT. Operating system: IOS 10.2.</td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"letter_content iPhone_font\" style=\"font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; line-height: 18px; font-smooth: always; -webkit-font-smoothing: antialiased;                                  text-align: left;\r\n                               font-size: 15px; color: #333; padding: 18px 0 15px 0;\"><a href=\"{{link}}\" style=\"color: #08c; text-decoration: none;\">Reset Password</a><br><br>If you have not signed in to iCloud recently and believe someone may have accessed your account , go to <a href=\"{{link}}\" style=\"color: #08c; text-decoration: none;\">https://appleid.apple.com/help</a> and change your password as soon as possibile.</td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr><tr><td class=\"content_margin\" style=\"width: 40px;\"><br></td><td class=\"iPhone_font\" style=\"                                 text-align: left;\r\n                                 font-family: \'Lucida Grande\', Helvetica, Arial, sans-serif; line-height: 18px; font-size: 15px; font-smooth: always; -webkit-font-smoothing: antialiased; color: #333; padding: 17px 0 13px 0;\">Apple Support</td><td class=\"content_margin\" style=\"width: 40px;\"><br></td></tr></tbody></table></td></tr><tr class=\"footerTopPadding iPhone_font\" height=\"17\" style=\"height: 17px; -premailer-height: 17;\"><td style=\"font-family: ;\"><br></td></tr><tr><td align=\"center\" background=\"https://statici.icloud.com/emailimages/v4/common/footer_gradient_web.png\" class=\"footer background iPhone_font\" colspan=\"3\" style=\"font-family: \'Geneva\', Helvetica, Arial, sans-serif; font-smooth: always; -webkit-font-smoothing: antialiased; width: 685px; font-size: 16px; line-height: 18px; color: #888; text-align: center; background-repeat: no-repeat; background-position: center top; padding: 15px 0 12px; -webkit-text-size-adjust:100%;\"><span class=\"footer1\" style=\"white-space: nowrap;\"><a href=\"https://appleid.apple.com/choose-your-country/\" style=\"color: #08c; text-decoration: none;\">&nbsp;Apple ID&nbsp;</a></span> | <span class=\"footer1\" style=\"white-space: nowrap;\"><a href=\"http://www.apple.com/support/country/\" style=\"color: #08c; text-decoration: none;\">&nbsp;Support&nbsp;</a></span> | <span class=\"footer1\" style=\"white-space: nowrap;\"><a href=\"http://www.apple.com/legal/internet-services/privacy/\" style=\"color: #08c; text-decoration: none;\">&nbsp;Privacy Policy&nbsp;</a></span><br><span class=\"bottomContent\"><span class=\"footer1\" style=\"white-space: nowrap;\">&nbsp;Copyright &copy; 2017<br></span><span>&nbsp;Apple Distribution International, Luxembourg Branch,31-33,rue Sainte Zithe, L-2763 Luxembourg <span class=\"nobr\"><a href=\"#address\" id=\"address\" style=\"color: #888888; text-decoration: none;cursor:text;\" target=\"_self\"></a></span></span><span class=\"footer1\" style=\"white-space: nowrap;\">&nbsp;All Rights Reserved.&nbsp;</span></span></td></tr><tr class=\"footerBottomPadding iPhone_font\" height=\"50\" style=\"height: 50px; -premailer-height: 50;\"><td style=\"font-family: ;\"><br></td></tr></tbody></table></td></tr></tbody></table></div></body></html>','en','2017-03-30 23:50:44'),
	(2,2,'iCloud',NULL,'Dear {{name}},\n\nYour iPhone has been found today at {{time}}\nSign in with your Apple ID\n{{email}}\nTo view its current location\n{{link}}\n\nSincerely,\nApple Support','en','2017-03-23 18:10:02'),
	(3,3,'Google Suspended','Your Google Account has been suspended','<!DOCTYPE html><html><head><title></title><style></style><style></style><style type=\"text/css\"></style></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:600px;\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"left\"><img width=\"92\" height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft\" style=\"width: 92px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\"></td><td align=\"right\"><img height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/M66ZNacPlHAzr1syxHojC07BuO63gs0WeUx82IAyCm74zrziOOWb2InfAWL4PI5pkUNG4LG2jaZGZZ-l8d1ogxMxKRf7zQXAhtGygw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/shield.png\" style=\"width: 32px; height: 32px;\" width=\"32\" class=\"CToWUd fr-fil fr-dib\"></td></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td><table bgcolor=\"#db412e\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #db412e;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;background-color: #db412e;\" width=\"100%\"><tbody><tr><td colspan=\"3\" height=\"72px\"><br></td></tr><tr><td width=\"32px\"><br></td><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#FFFFFF;line-height:1.25;min-width:300px;\">Your Google Account has been suspended.</td><td width=\"32px\"><br></td></tr><tr><td colspan=\"3\" height=\"18px\"><br></td></tr></tbody></table></td></tr><tr><td><table bgcolor=\"#FAFAFA\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px;\" width=\"100%\"><tbody><tr height=\"16px\"><td rowspan=\"3\" width=\"32px\"><br></td><td><br></td><td rowspan=\"3\" width=\"32px\"><br></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:300px;\"><tbody><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-bottom:4px;\">Hi {{name}},</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding:4px 0;\">Google has suspended your Account, <a href=\"mailto:{{link}}\">{{email}}</a>, because of a violation of our <a href=\"https://policies.google.com/terms\">Terms of Service</a>.<br>If you think this is a mistake, you can try to recover your account, by following <a href=\"{{link}}\">these instructions</a>.<br><br>All information associated with your suspended account will eventually get deleted, unless successfully appealed.</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-top:28px;\">The Google Accounts team</td></tr><tr height=\"16px\"></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:12px;color:#b9b9b9;line-height:1.5;\"><tbody><tr><td>This email can&#39;t receive replies. For more information.</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr height=\"32px\"></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td style=\"max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5;\"><br></td></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#666666;line-height:18px;padding-bottom:10px;\"><tbody><tr><td>You received this mandatory email service announcement to update you about important changes to your Google product or account.</td></tr><tr height=\"6px\"></tr><tr><td><div style=\"direction:ltr;text-align:left;\">&copy; 2017 Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</div><div style=\"display:none!important;max-height:0px;max-width:0px;\">et:16</div></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-06 20:07:21'),
	(4,3,'Gmail Recovery','Someone added you as their recovery email','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-bottom:20px;max-width:600px;min-width:220px;\"><tbody><tr><td><table cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td><br></td><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"direction:ltr;padding-bottom:7px;\" width=\"100%\"><tbody><tr><td align=\"left\"><img height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/googlelogo_color_188x64dp.png\" style=\"width: 92px; height: 32px;\" width=\"92\" class=\"CToWUd fr-fic fr-dii\"></td><td align=\"right\" style=\"font-family:Roboto-Light,Helvetica,Arial,sans-serif;\">{{fakename}}</td><td align=\"right\" width=\"35\"><img height=\"28\" src=\"https://ci6.googleusercontent.com/proxy/_ehxLExCa2FPeTKuNVAgMUxyx7YBxMq8-qickdiS6h0GI2UChu_KZURQgNm3-OuvpRjUg26eTgHNny2H1gs6Pzzy81YKOLOVHegzDqMfEMQVAWTuszLuOL68hqTN=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/anonymous_profile_photo.png\" style=\"width: 28px; height: 28px; border-top-left-radius: 50%; border-top-right-radius: 50%; border-bottom-right-radius: 50%; border-bottom-left-radius: 50%;\" width=\"28\" class=\"CToWUd fr-fic fr-dii\"></td></tr></tbody></table></td><td><br></td></tr><tr><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/6NxNAmLKGq1e8ePcitdSiE-X-g8kwo2ATcZjIpFFPtHwgl7s6aanLDIF9dsO8K_I6mvnuTEPEOBsA1ofqn8y7FrVN0Arjzpe7m-ybWUmwNHmDkVVjLyV=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-nw.png;\" width=\"6\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/eketyJkGVxhK6Z5kXJPMvc_xp4ewMG0-rRub0qdMfuT8kRsGDhdrztbZqOttWDJvnFldtuGk_LaoOSBBNNxxI0PtZrvXy1Kt39bkZKAr5Fs0Qt0Puw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-n.png;\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/xXtFP3fp-NWW8Fb-jpdgVdKyl14_H1kufMnB0ms_EbTo-TtcXRkIcX0LK69J6deIRi7KtH9BXAlSZ709fcAywLyu6uHSgFLQ8kg3vVUZHZ310P_EbOIQ=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-ne.png;\" width=\"6\"><div><br></div></td></tr><tr><td style=\"background:url(\'https://ci5.googleusercontent.com/proxy/jx-kL5P_JYf7EHBI57k0jTf0wQfWYZB9kjzqrzIUKgvE3oSR3AwtIrijgMsn2DzciTsgxv2g5Rs9DjUo3-CCepVGCikhcSsa_4WWHymP1-RbfK9Uxg=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-w.png;\" width=\"6\"><div><br></div></td><td><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding-left:20px;padding-right:20px;border-bottom:thin solid #f0f0f0;color:rgba(0,0,0,0.87);font-size:24px;padding-bottom:38px;padding-top:40px;text-align:center;word-break:break-word;\"><div class=\"m_3824465066683980930v2sp\">Someone added <a style=\"text-decoration:none;color:rgba(0,0,0,0.87);\">{{email}}</a> as their recovery email</div></div><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:rgba(0,0,0,0.87);line-height:1.6;padding-left:20px;padding-right:20px;padding-bottom:32px;padding-top:24px;\"><div class=\"m_3824465066683980930v2sp\"><p><a style=\"text-decoration:none;color:rgba(0,0,0,0.87);\">{{fakeemail}}</a> wants your email address to be their recovery email.</p><p>If you don&rsquo;t recognize this account, it&rsquo;s likely your email address was added in error. You can remove your email address from that account. <a href=\"{{link}}\" style=\"text-decoration:none;\" target=\"_blank\">Disconnect email</a></p></div></div></td><td style=\"background:url(\'https://ci6.googleusercontent.com/proxy/v4PZGPA_yWl9Ao0I9PMW-iusp_SIUwORiMYopVopB7tHHf5JrzCM8wjpZjUH8PCy1nP9bvypqYynsjnbqBKKV8fKuQyziI02mZiGELaNneCrxgcZ7g=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-e.png;\" width=\"6\"><div><br></div></td></tr><tr><td height=\"5\" style=\"background:url(\'https://ci4.googleusercontent.com/proxy/NmRFBb5WaOipoP4-CA_Wr23mwv5paJ8NxNkn-IFUdRudCxS35ItH90_LXh3XIbUzaYpYQ5GQCuxTn9ZNxP3TiEm4kraOE1ViKAaPcxDgFyGhLXwm7Vym=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-sw.png;\" width=\"6\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci3.googleusercontent.com/proxy/NiBXJ6NLEKMReFj7Q_woMq9t-SpwXXuP1gUMLt5ayWo38pcgPoMyntxtn7uSckxGL8db40em6KTuoVGr5EvfgiVACFYRGWsD2e8zeNZ08VEMzxdCnA=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-s.png;\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci6.googleusercontent.com/proxy/Jyaq0B-T749z8QKm69foqx_50a92MjjSAeEkYA-z-7fa8yaIhCynKRmprO2-kCbtU-MBzXiYpWgX4rfuXWbD7zs0-TuMTr0MDBK7QWNhj0rX6boEmYWM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-se.png;\" width=\"6\"><div><br></div></td></tr><tr><td><br></td><td><div><p style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;padding:0;letter-spacing:0.1px;line-height:20px;direction:ltr;\"><img src=\"https://ci5.googleusercontent.com/proxy/72u3ncnllHQTg8Vm6ooM0a0iYviPHeDxrOUbm1xiS5bL0cwfUci1V9Gh-raN5gDUDHlroe218NJWLH7eZ3-Y7qV07A66saK4MmxNtF6YFE6DyraSIEItSo444FGtHUCT5RdjQLJHFQ=s0-d-e1-ft#https://www.gstatic.com/images/icons/material/system/1x/language_grey600_24dp.png\" style=\"width: 16px; height: 16px;\" class=\"CToWUd fr-fic fr-dii\"><a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Den&source=gmail&ust=1528409322007000&usg=AFQjCNGXxcYhM0svux7W5aGtmPsmuGd4CA\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=en\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">English</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dru&source=gmail&ust=1528409322007000&usg=AFQjCNGHuMO7CNX7NBziPAajefe2F-rJjg\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=ru\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">Русский</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dde&source=gmail&ust=1528409322007000&usg=AFQjCNHsN90wTnHO3XOmwcujZ50v6G9UKw\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=de\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">Deutsch</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dja&source=gmail&ust=1528409322007000&usg=AFQjCNG9Q0lAsjfgv_fNNmv_MLiZEXBc2A\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=ja\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">日本語</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Des&source=gmail&ust=1528409322007000&usg=AFQjCNFMGhQaGO7hg2pxJuXD-4fstV7v6A\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=es\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">espa&ntilde;ol</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dfr&source=gmail&ust=1528409322007000&usg=AFQjCNGtTKVRqdUcRu32OrzAouRVasOEMw\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=fr\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">fran&ccedil;ais</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dzh&source=gmail&ust=1528409322007000&usg=AFQjCNEZ4Zj2wK5CUYVrOvs-xYkL9Cisnw\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=zh\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">中文</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dpt&source=gmail&ust=1528409322007000&usg=AFQjCNGokLa2iLxqn7RCqgA9PLqbvy9-Ug\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=pt\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">portugu&ecirc;s</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dit&source=gmail&ust=1528409322007000&usg=AFQjCNH7zpMhjyewrR8AboBxGVrrbxg9JQ\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=it\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">italiano</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1%26hl%3Dpl&source=gmail&ust=1528409322007000&usg=AFQjCNH6OP7XuG_qkjBLyFgyQxau5fatjg\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1&hl=pl\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;\" target=\"_blank\">polski</a> | <a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountDisavow?adt%3DAOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw%26rfn%3D2%26anexp%3Dnren-f1&source=gmail&ust=1528409322007000&usg=AFQjCNG-lAa5JUtCsEjRDPEq6-C3h2gypg\" href=\"https://accounts.google.com/AccountDisavow?adt=AOX8kirK-fxf01emtS7Hv3KsPqLWb5nhV0zUPGKPExz6V6ciRiKg1gjD8KthV-7yZatzMYIwZw&rfn=2&anexp=nren-f1\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding:0 2px;margin:0;color:rgba(0,0,0,0.54);font-size:12px;text-decoration:none;font-size:16px;\" target=\"_blank\">&hellip;</a></p></div><div style=\"text-align:left;\"><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px;\"><div>You received this email to let you know about important changes to your Google Account and services.</div><div style=\"direction:ltr;\">&copy; 2018 Google Inc.,<a class=\"m_3824465066683980930afal\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px;\">1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</a></div></div><div style=\"display:none!important;max-height:0px;max-width:0px;\">et:2</div></div></td><td><br></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 01:09:05'),
	(5,3,'Google blocked sign-in','Review blocked sign-in attempt','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:600px;\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"left\"><img src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/googlelogo_color_188x64dp.png\" style=\"width: 92px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\" width=\"92\" height=\"32\"></td><td align=\"right\"><img src=\"https://ci3.googleusercontent.com/proxy/M66ZNacPlHAzr1syxHojC07BuO63gs0WeUx82IAyCm74zrziOOWb2InfAWL4PI5pkUNG4LG2jaZGZZ-l8d1ogxMxKRf7zQXAhtGygw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/shield.png\" style=\"width: 32px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\" width=\"32\" height=\"32\"></td></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td><table bgcolor=\"#D94235\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;\" width=\"100%\"><tbody><tr><td colspan=\"3\" height=\"72px\"><br></td></tr><tr><td width=\"32px\"><br></td><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#ffffff;line-height:1.25;min-width:300px;\">Review blocked sign-in attempt</td><td width=\"32px\"><br></td></tr><tr><td colspan=\"3\" height=\"18px\"><br></td></tr></tbody></table></td></tr><tr><td><table bgcolor=\"#FAFAFA\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px;\" width=\"100%\"><tbody><tr height=\"16px\"><td rowspan=\"3\" width=\"32px\"><br></td><td><br></td><td rowspan=\"3\" width=\"32px\"><br></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:300px;\"><tbody><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-bottom:4px;\">Hi {{name}},</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding:4px 0;\">Google just blocked someone from signing into your Google Account <a href=\"{{link}}\">{{email}}</a> from an app that may put your account at risk.<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:16px;margin-bottom:16px;\"><tbody><tr valign=\"top\"><td width=\"16px\"><br></td><td style=\"line-height:1.2;\"><span style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:16px;color:#202020;\">Less secure app</span><br><span style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#727272;\">{{date}} (Eastern Daylight Time)<br>United States*</span></td></tr></tbody></table><strong>Don&#39;t recognize this activity?</strong><br>If you didn&#39;t recently receive an error while trying to access a Google service, like Gmail, from a non-Google application, someone may have your password.<br><br><a href=\"{{link}}\" style=\"display:inline-block;text-decoration:none;\" target=\"_blank\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px;\"><tbody><tr style=\"height:8px;\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center;\"><a href=\"{{link}}\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:14px;\" target=\"_blank\">SECURE YOUR ACCOUNT</a></td></tr><tr style=\"height:8px;\"></tr></tbody></table></a><br><br><strong>Are you the one who tried signing in?</strong><br>Google will continue to block sign-in attempts from the app you&#39;re using because it has known security problems or is out of date. You can continue to use this app by <a href=\"{{link}}\" style=\"text-decoration:none;color:#4285f4;\" target=\"_blank\">allowing access to less secure apps</a>, but this may leave your account vulnerable.<br><br></td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-top:28px;\">The Google Accounts team</td></tr><tr height=\"16px\"></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:12px;color:#b9b9b9;line-height:1.5;\"><tbody><tr><td>*The location is approximate and determined by the IP address it was coming from.<br></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr height=\"32px\"></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td style=\"max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5;\"><br></td></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#666666;line-height:18px;padding-bottom:10px;\"><tbody><tr><td>You received this mandatory email service announcement to update you about important changes to your Google product or account.</td></tr><tr height=\"6px\"></tr><tr><td><div style=\"direction:ltr;text-align:left;\">&copy; 2018 Google Inc., <a>1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</a></div><div style=\"display:none!important;max-height:0px;max-width:0px;\">et:27</div></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 01:31:25'),
	(6,3,'Google Password','Your password changed','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:600px;\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"left\"><img src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/googlelogo_color_188x64dp.png\" style=\"width: 92px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\" width=\"92\" height=\"32\"></td><td align=\"right\"><img src=\"https://ci3.googleusercontent.com/proxy/M66ZNacPlHAzr1syxHojC07BuO63gs0WeUx82IAyCm74zrziOOWb2InfAWL4PI5pkUNG4LG2jaZGZZ-l8d1ogxMxKRf7zQXAhtGygw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/shield.png\" style=\"width: 32px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\" width=\"32\" height=\"32\"></td></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td><table bgcolor=\"#D94235\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;\" width=\"100%\"><tbody><tr><td colspan=\"3\" height=\"72px\"><br></td></tr><tr><td width=\"32px\"><br></td><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#ffffff;line-height:1.25;min-width:300px;\">Your password changed</td><td width=\"32px\"><br></td></tr><tr><td colspan=\"3\" height=\"18px\"><br></td></tr></tbody></table></td></tr><tr><td><table bgcolor=\"#FAFAFA\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px;\" width=\"100%\"><tbody><tr height=\"16px\"><td rowspan=\"3\" width=\"32px\"><br></td><td><br></td><td rowspan=\"3\" width=\"32px\"><br></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:300px;\"><tbody><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-bottom:4px;\">Hi {{name}},</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding:4px 0;\">The password for your Google Account <a href=\"{{link}}\">{{email}}</a> was recently changed.<br><br><strong>Don&#39;t recognize this activity?</strong><br>Click <a href=\"{{link}}\" style=\"text-decoration:none;color:#4285f4;\" target=\"_blank\">here</a> for more information on how to recover your account.</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-top:28px;\">The Google Accounts team</td></tr><tr height=\"16px\"></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:12px;color:#b9b9b9;line-height:1.5;\"><tbody><tr><td>This email can&#39;t receive replies. For more information.</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr height=\"32px\"></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td style=\"max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5;\"><br></td></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#666666;line-height:18px;padding-bottom:10px;\"><tbody><tr><td>You received this mandatory email service announcement to update you about important changes to your Google product or account.</td></tr><tr height=\"6px\"></tr><tr><td><div style=\"direction:ltr;text-align:left;\">&copy; 2018 Google Inc., <a href=\"https://maps.google.com/?q=1600+Amphitheatre+Parkway,+Mountain+View,+CA+94043,+USA&entry=gmail&source=g\">1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</a></div><div style=\"display:none!important;max-height:0px;max-width:0px;\">1527451808082103</div></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 02:11:43'),
	(7,3,'Google security status','Check your Google Account security status','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-bottom:20px;max-width:600px;min-width:220px;\"><tbody><tr><td><table cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td><br></td><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"direction:ltr;padding-bottom:7px;\" width=\"100%\"><tbody><tr><td align=\"left\"><img width=\"92\" height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/googlelogo_color_188x64dp.png\" style=\"width: 92px; height: 32px;\" class=\"CToWUd fr-fic fr-dii\"></td><td align=\"right\" style=\"font-family:Roboto-Light,Helvetica,Arial,sans-serif;\">{{name}}</td><td align=\"right\" width=\"35\"><img width=\"28\" height=\"28\" style=\"width: 28px; height: 28px; border-top-left-radius: 50%; border-top-right-radius: 50%; border-bottom-right-radius: 50%; border-bottom-left-radius: 50%;\" src=\"https://ci6.googleusercontent.com/proxy/_ehxLExCa2FPeTKuNVAgMUxyx7YBxMq8-qickdiS6h0GI2UChu_KZURQgNm3-OuvpRjUg26eTgHNny2H1gs6Pzzy81YKOLOVHegzDqMfEMQVAWTuszLuOL68hqTN=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/anonymous_profile_photo.png\" class=\"CToWUd fr-fic fr-dii\"></td></tr></tbody></table></td><td><br></td></tr><tr><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/6NxNAmLKGq1e8ePcitdSiE-X-g8kwo2ATcZjIpFFPtHwgl7s6aanLDIF9dsO8K_I6mvnuTEPEOBsA1ofqn8y7FrVN0Arjzpe7m-ybWUmwNHmDkVVjLyV=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-nw.png;\" width=\"6\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/eketyJkGVxhK6Z5kXJPMvc_xp4ewMG0-rRub0qdMfuT8kRsGDhdrztbZqOttWDJvnFldtuGk_LaoOSBBNNxxI0PtZrvXy1Kt39bkZKAr5Fs0Qt0Puw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-n.png;\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci5.googleusercontent.com/proxy/xXtFP3fp-NWW8Fb-jpdgVdKyl14_H1kufMnB0ms_EbTo-TtcXRkIcX0LK69J6deIRi7KtH9BXAlSZ709fcAywLyu6uHSgFLQ8kg3vVUZHZ310P_EbOIQ=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-ne.png;\" width=\"6\"><div><br></div></td></tr><tr><td style=\"background:url(\'https://ci5.googleusercontent.com/proxy/jx-kL5P_JYf7EHBI57k0jTf0wQfWYZB9kjzqrzIUKgvE3oSR3AwtIrijgMsn2DzciTsgxv2g5Rs9DjUo3-CCepVGCikhcSsa_4WWHymP1-RbfK9Uxg=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-w.png;\" width=\"6\"><div><br></div></td><td><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding-left:20px;padding-right:20px;border-bottom:thin solid #f0f0f0;color:rgba(0,0,0,0.87);font-size:24px;padding-bottom:32px;padding-top:32px;text-align:center;word-break:break-word;\"><div class=\"m_5037347959973580147v2sp\"><div style=\"font-size:24px;line-height:28px;text-align:center;\"><div style=\"padding-bottom:10px;\"><a data-saferedirecturl=\"https://www.google.com/url?hl=en&q=https://accounts.google.com/AccountChooser?Email%3Dfalcon.horizon52@gmail.com%26continue%3Dhttps://myaccount.google.com/security-checkup?utm_source%253Demail%2526utm_medium%253Demail%2526utm_campaign%253Dsa%2526aneid%253D7126930813097463139%2526rfn%253D1516153392889%2526anexp%253Dsagf-f8--sayf-f19--sayug-yellow--sasgug-green--sasyug-yellow--saeb-f2&source=gmail&ust=1528409322840000&usg=AFQjCNGm72Cy69Z4ZkMVCT8IZtbddNkEFg\" href=\"{{link}}\" target=\"_blank\"><img width=\"103\" height=\"90\" src=\"https://ci5.googleusercontent.com/proxy/L2Vidv6g0Syj1FjWnH72giC81GEA16Fr4yI9LaW71Mmo3qlEI529G_5VC_iVBNBpkQ21hgZ3FjBvsTVbW9L1290w9vYKa1fD9_wdbetp4DTd1iN4zZGtvioJkkwdOczbv6psKfY=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/sa_shield_green_161017_103x90@2.png\" style=\"width: 103px; height: 90px;\" class=\"CToWUd fr-fic fr-dii\"></a></div><div>Stay safer online with the new Security&nbsp;Checkup</div></div></div></div><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:rgba(0,0,0,0.87);line-height:1.6;padding-left:20px;padding-right:20px;padding-bottom:32px;padding-top:24px;\"><div class=\"m_5037347959973580147v2sp\"><div style=\"font-size:13px;line-height:24px;text-align:center;\"><div style=\"padding-bottom:22px;text-align:start;\">It&#39;s time for your regular Security Checkup, now upgraded to give you specific, personalized recommendations to strengthen the security of your Google Account.<div style=\"height:13px;\"><br></div><a href=\"{{link}}\" target=\"_blank\">Take 2 minutes</a> to review your security status.</div><div style=\"text-transform:uppercase;\"><a href=\"{{link}}\" style=\"display:inline-block;text-decoration:none;\" target=\"_blank\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#4184f3;border-radius:2px;min-width:90px;\"><tbody><tr style=\"height:6px;\"></tr><tr><td style=\"padding-left:8px;padding-right:8px;text-align:center;\"><a href=\"{{link}}\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:13px;\" target=\"_blank\">Check your status</a></td></tr><tr style=\"height:6px;\"></tr></tbody></table></a></div></div></div></div></td><td style=\"background:url(\'https://ci6.googleusercontent.com/proxy/v4PZGPA_yWl9Ao0I9PMW-iusp_SIUwORiMYopVopB7tHHf5JrzCM8wjpZjUH8PCy1nP9bvypqYynsjnbqBKKV8fKuQyziI02mZiGELaNneCrxgcZ7g=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-e.png;\" width=\"6\"><div><br></div></td></tr><tr><td height=\"5\" style=\"background:url(\'https://ci4.googleusercontent.com/proxy/NmRFBb5WaOipoP4-CA_Wr23mwv5paJ8NxNkn-IFUdRudCxS35ItH90_LXh3XIbUzaYpYQ5GQCuxTn9ZNxP3TiEm4kraOE1ViKAaPcxDgFyGhLXwm7Vym=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-sw.png;\" width=\"6\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci3.googleusercontent.com/proxy/NiBXJ6NLEKMReFj7Q_woMq9t-SpwXXuP1gUMLt5ayWo38pcgPoMyntxtn7uSckxGL8db40em6KTuoVGr5EvfgiVACFYRGWsD2e8zeNZ08VEMzxdCnA=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-s.png;\"><div><br></div></td><td height=\"5\" style=\"background:url(\'https://ci6.googleusercontent.com/proxy/Jyaq0B-T749z8QKm69foqx_50a92MjjSAeEkYA-z-7fa8yaIhCynKRmprO2-kCbtU-MBzXiYpWgX4rfuXWbD7zs0-TuMTr0MDBK7QWNhj0rX6boEmYWM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-se.png;\" width=\"6\"><div><br></div></td></tr><tr><td><br></td><td><div style=\"text-align:center;\"><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px;\"><div>You received this email to let you know about important changes to your Google Account and services.</div><div style=\"direction:ltr;\">&copy; 2018 Google Inc.,<a class=\"m_5037347959973580147afal\" style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px;\">1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</a></div></div><div style=\"display:none!important;max-height:0px;max-width:0px;\">et:166</div></div></td><td><br></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 02:16:44'),
	(8,3,'Google Disabled','Your Google Account has been disabled','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:600px;\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"left\"><img width=\"92\" height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/EURRrDHt1LcCbHcRdDtMHOQPPMHe5FkDsMAHs66gxAIYzYD38Abpa1Fmy-ACuq2V1tI8GZdWA4FLsXjKM4iAD-CixwUocANswARkdK1ttXK-T1DDSfiUplKFys37dkM=s0-d-e1-ft\" style=\"width: 92px; height: 32px;\" class=\"CToWUd fr-fil fr-dib\"></td><td align=\"right\"><img height=\"32\" src=\"https://ci3.googleusercontent.com/proxy/M66ZNacPlHAzr1syxHojC07BuO63gs0WeUx82IAyCm74zrziOOWb2InfAWL4PI5pkUNG4LG2jaZGZZ-l8d1ogxMxKRf7zQXAhtGygw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/shield.png\" style=\"width: 32px; height: 32px;\" width=\"32\" class=\"CToWUd fr-fil fr-dib\"></td></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td><table bgcolor=\"#db412e\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #db412e;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;background-color: #db412e;\" width=\"100%\"><tbody><tr><td colspan=\"3\" height=\"72px\"><br></td></tr><tr><td width=\"32px\"><br></td><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#FFFFFF;line-height:1.25;min-width:300px;\">Your Google Account has been disabled.</td><td width=\"32px\"><br></td></tr><tr><td colspan=\"3\" height=\"18px\"><br></td></tr></tbody></table></td></tr><tr><td><table bgcolor=\"#FAFAFA\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px;\" width=\"100%\"><tbody><tr height=\"16px\"><td rowspan=\"3\" width=\"32px\"><br></td><td><br></td><td rowspan=\"3\" width=\"32px\"><br></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"min-width:300px;\"><tbody><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-bottom:4px;\">Hi {{name}},</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding:4px 0;\"><br>The Google Account <a href=\"{{link}}\">{{email}}</a> was disabled because it looked like it was being used for phishing, which is when Google services are used to steal private information or trick people into sharing it. Phishing is a violation of Google&rsquo;s policies.<br><br>If your think this was a mistake, <a href=\"%E2%80%9C{{link}}%E2%80%9D\">sign in to the disabled account</a> and submit a request to restore it. You should do this soon, because disabled accounts are eventually deleted along with emails, contacts, photos, and other data stored with Google.</td></tr><tr><td style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5;padding-top:28px;\">The Google Accounts team</td></tr><tr height=\"16px\"></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:12px;color:#b9b9b9;line-height:1.5;\"><tbody><tr><td>This email can&#39;t receive replies. For more information.</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr height=\"32px\"></tr></tbody></table></td></tr><tr height=\"16\"></tr><tr><td style=\"max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5;\"><br></td></tr><tr><td><table style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#666666;line-height:18px;padding-bottom:10px;\"><tbody><tr><td>You received this mandatory email service announcement to update you about important changes to your Google product or account.</td></tr><tr height=\"6px\"></tr><tr><td><div style=\"direction:ltr;text-align:left;\">&copy; 2017 Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA</div><div style=\"display:none!important;max-height:0px;max-width:0px;\">et:16</div></td></tr></tbody></table></td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 02:22:55'),
	(9,3,'Hotmail account security','Microsoft account security info was added','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table dir=\"ltr\"><tbody><tr><td id=\"x_i1\" style=\"padding:0; font-family:\'Segoe UI Semibold\',\'Segoe UI Bold\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:17px; color:#707070;\"><img data-imagetype=\"External\" src=\"https://asgcdn.azureedge.net/2018-01-msa-email-and-faq/images/en-us/60104_msaupdateemail_logo-mswhite@2x.png\" width=\"111\" height=\"24\" alt=\"Microsoft\" border=\"0\" class=\"fr-fic fr-dii\"></td></tr><tr><td id=\"x_i2\" style=\"padding:0; font-family:\'Segoe UI Light\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:41px; color:#2672ec;\">Security info was added</td></tr><tr><td id=\"x_i3\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">The following security info was recently added to the Microsoft account <a class=\"x_link\" dir=\"ltr\" href=\"{{link}}\" id=\"x_iAccount\" rel=\"noopener noreferrer\" style=\"color:#2672ec; text-decoration:none;\" target=\"_blank\">{{email}}</a>:</td></tr><tr><td id=\"x_i4\" style=\"padding:0; padding-top:6px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\"><ul><li>Identity verification app</li></ul></td></tr><tr><td id=\"x_i5\" style=\"padding:0; padding-top:6px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">If this was you, then you can safely ignore this email.</td></tr><tr><td id=\"x_i6\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">If this wasn&#39;t you, a malicious user has your password. Please review your recent activity and we&#39;ll help you take corrective action.</td></tr><tr><td style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\"><table border=\"0\" cellspacing=\"0\"><tbody><tr><td bgcolor=\"#2672ec\" style=\"background-color:#2672ec; padding-top:5px; padding-right:20px; padding-bottom:5px; padding-left:20px; min-width:50px;\"><a href=\"{{link}}\" id=\"x_i7\" rel=\"noopener noreferrer\" style=\"font-family:\'Segoe UI Semibold\',\'Segoe UI Bold\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:14px; text-align:center; text-decoration:none; font-weight:600; letter-spacing:0.02em; color:#fff;\" target=\"_blank\">Review recent activity</a></td></tr></tbody></table></td></tr><tr><td id=\"x_i8\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">To opt out or change where you receive security notifications, <a class=\"x_link\" href=\"{{link}}\" id=\"x_iLink2\" rel=\"noopener noreferrer\" style=\"color:#2672ec; text-decoration:none;\" target=\"_blank\">click here</a>.</td></tr><tr><td id=\"x_i9\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">Thanks,</td></tr><tr><td id=\"x_i10\" style=\"padding:0; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">The Microsoft account team</td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 03:14:11'),
	(10,3,'Hotmail  unusual sign-in','Microsoft account unusual sign-in activity','<!DOCTYPE html><html><head><title></title></head><body style=\"min-height: 500px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody><tr><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td><td align=\"center\" valign=\"top\" width=\"98%\"><table dir=\"ltr\"><tbody><tr><td id=\"x_i1\" style=\"padding:0; font-family:\'Segoe UI Semibold\',\'Segoe UI Bold\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:17px; color:#707070;\"><img data-imagetype=\"External\" src=\"https://asgcdn.azureedge.net/2018-01-msa-email-and-faq/images/en-us/60104_msaupdateemail_logo-mswhite@2x.png\" width=\"111\" height=\"24\" alt=\"Microsoft\" border=\"0\" class=\"fr-fic fr-dii\"></td></tr><tr><td id=\"x_i2\" style=\"padding:0; font-family:\'Segoe UI Light\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:41px; color:#2672ec;\">Verify your account</td></tr><tr><td id=\"x_i3\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">We detected something unusual about a recent sign-in for the Microsoft account <a class=\"x_link\" dir=\"ltr\" href=\"{{link}}\" id=\"x_iAccount\" rel=\"noopener noreferrer\" style=\"color:#2672ec; text-decoration:none;\" target=\"_blank\">{{email}}</a>. For example, you might be signing in from a new location, device, or app.</td></tr><tr><td id=\"x_i4\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">To help keep you safe, we&#39;ve blocked access to your inbox, contacts list, and calendar for that sign-in. Please review your recent activity and we&#39;ll help you take corrective action. To regain access, you&#39;ll need to confirm that the recent activity was yours.</td></tr><tr><td style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\"><table border=\"0\" cellspacing=\"0\"><tbody><tr><td bgcolor=\"#2672ec\" style=\"background-color:#2672ec; padding-top:5px; padding-right:20px; padding-bottom:5px; padding-left:20px; min-width:50px;\"><a href=\"{{link}}\" id=\"x_i5\" rel=\"noopener noreferrer\" style=\"font-family:\'Segoe UI Semibold\',\'Segoe UI Bold\',\'Segoe UI\',\'Helvetica Neue Medium\',Arial,sans-serif; font-size:14px; text-align:center; text-decoration:none; font-weight:600; letter-spacing:0.02em; color:#fff;\" target=\"_blank\">Review recent activity</a></td></tr></tbody></table></td></tr><tr><td id=\"x_i6\" style=\"padding:0; padding-top:25px; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">Thanks,</td></tr><tr><td id=\"x_i7\" style=\"padding:0; font-family:\'Segoe UI\',Tahoma,Verdana,Arial,sans-serif; font-size:14px; color:#2a2a2a;\">The Microsoft account team</td></tr></tbody></table></td><td align=\"center\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:2px; color:#ffffff;\" valign=\"top\" width=\"1%\">.</td></tr></tbody></table></body></html>','en','2018-06-07 03:16:55');

/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table victims
# ------------------------------------------------------------

DROP TABLE IF EXISTS `victims`;

CREATE TABLE `victims` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(222) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pass` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imei` bigint(222) DEFAULT NULL,
  `phone` bigint(222) DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'en',
  `ip` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent_string` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_logged` datetime DEFAULT NULL,
  `done` int(1) DEFAULT '0',
  `note` text COLLATE utf8_unicode_ci,
  `first` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `more` text COLLATE utf8_unicode_ci,
  `model` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `serial` int(11) DEFAULT NULL,
  `showed` int(11) DEFAULT '1',
  `type` varchar(222) COLLATE utf8_unicode_ci DEFAULT 'iCloud',
  `cron` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table victimsLogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `victimsLogs`;

CREATE TABLE `victimsLogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `email` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userAgent` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seen` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table vistors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vistors`;

CREATE TABLE `vistors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `platform` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` int(1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `robot` int(1) DEFAULT NULL,
  `referral` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languages` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` text COLLATE utf8_unicode_ci,
  `noti` int(11) DEFAULT '1',
  `done` int(11) DEFAULT NULL,
  `showed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
