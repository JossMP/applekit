<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Core
{
	public function forms()
	{
		if ($_POST) {
			if (isset($_POST['licenseCheck'])) {
				if (empty($_POST['license'])) {
					$res = array('state' => 'fails', 'title' => 'Empty License Key', 'msg' => 'License key can not be empty, Please insert the license key to move to the next step.');
					return $res;
				}

				$return = $this->checkLicense($_POST['license']);

				if ($return['state'] == 'fails') {
					$_SESSION['stage1'] = NULL;
					$_SESSION['stage1lic'] = NULL;
					return $return;
				}

				$_SESSION['stage1'] = 'done';
				$_SESSION['stage1lic'] = $_POST['license'];
			}

			if (isset($_POST['adminCreate'])) {
				if (empty($_POST['username'])) {
					$res = array('state' => 'fails', 'title' => 'Empty License Key', 'msg' => 'License key can not be empty, Please insert the license key to move to the next step.');
					return $res;
				}

				if (empty($_POST['password'])) {
					$res = array('state' => 'fails', 'title' => 'Empty License Key', 'msg' => 'License key can not be empty, Please insert the license key to move to the next step.');
					return $res;
				}

				require_once 'database_class.php';
				$db = new Database();
				require_once 'PasswordHash.php';
				$pwd = new PasswordHash(8, false);
				$passwd = $pwd->HashPassword($_POST['password']);
				$return = $db->create_admin($_SESSION['db'], $_POST['username'], $passwd, $_POST['email']);

				if ($return['state'] == 'fails') {
					$_SESSION['stage4'] = NULL;
					$_SESSION['adminu'] = NULL;
					return $return;
				}

				$_SESSION['stage5'] = 'done';
				$_SESSION['adminu'] = $_POST['username'];
			}

			if (isset($_POST['databaseCheck'])) {
				if ($_POST['db'] == 'sqlite') {
					$this->write_config('sqlite');
					$_SESSION['db'] = 'sqlite';
					$_SESSION['stage4'] = 'done';
				}
				else if (empty($_POST['localhost'])) {
					$res = array('state' => 'fails', 'title' => 'Empty Localhost', 'msg' => 'Localhost Can not be empty, Please insert it and try again.');
					$_SESSION['db'] = 'mysql';
					return $res;
				}
				else if (empty($_POST['dbname'])) {
					$res = array('state' => 'fails', 'title' => 'Empty Database Name', 'msg' => 'Database Name Can not be empty, Please insert it and try again.');
					$_SESSION['db'] = 'mysql';
					return $res;
				}
				else if (empty($_POST['dbuser'])) {
					$res = array('state' => 'fails', 'title' => 'Empty Database Username', 'msg' => 'Database UserName Can not be empty, Please insert it and try again.');
					$_SESSION['db'] = 'mysql';
					return $res;
				}
				else if (empty($_POST['dbpwd'])) {
					$res = array('state' => 'fails', 'title' => 'Empty Database Password', 'msg' => 'Database Password Can not be empty, Please insert it and try again.');
					$_SESSION['db'] = 'mysql';
					return $res;
				}
				else {
					//return $res;
					require_once 'database_class.php';
					$db = new Database();
					$datab = $db->create_tables($_POST['localhost'], $_POST['dbname'], $_POST['dbuser'], $_POST['dbpwd']);

					if ($datab['state'] != 'fails') {
						$wconfig = $this->write_config('mysql', $_POST['localhost'], $_POST['dbname'], $_POST['dbuser'], $_POST['dbpwd']);

						if ($wconfig) {
							$_SESSION['localhost'] = $_POST['localhost'];
							$_SESSION['dbname'] = $_POST['dbname'];
							$_SESSION['dbuser'] = $_POST['dbuser'];
							$_SESSION['dbpwd'] = $_POST['dbpwd'];
							$_SESSION['db'] = 'mysql';
							$_SESSION['stage4'] = 'done';
						}
					}
					else {
						$_SESSION['db'] = 'mysql';
						return $datab;
					}
				}
			}
		}
	}

	public function domain()
	{
		$url = 'http://' . $_SERVER['HTTP_HOST'];
		$urlobj = parse_url($url);
		$domain = $urlobj['host'];

		if (preg_match('/(?<domain>[a-z0-9][a-z0-9\\-]{1,63}\\.[a-z\\.]{2,6})$/i', $domain, $regs)) {
			return $regs['domain'];
		}

		return false;
	}

	public function checkLicense($keys)
	{
	    $license = array(
        	'AAAAA-DDDDD-MMMMM-IIIII-NNNNN',
        	'CF879-7DF1C-DEMO1-DA4B9-C1EBF',
        	'CF879-7DF1C-20008-DA4B9-4VVCW',
        	'CF879-7DA4B-C1EBF-DA4B9-ZASW2',
        	'CF879-7DF1C-C1EBF-DA4B9-CV56G',
        	'CF879-5TGFR-C1EBF-DA4B9-7OO9G',
        	'CF879-OPEYA-C13CD-DA4B9-00001',
        	'CF879-7ALIA-CCXSF-DA4B9-XZ3TD',
        	'CF879-MUN72-C1EBF-DA4B9-WWW4R',
        	'CF879-7D331-C1EBF-DA4B9-8JFSW',
        	'CF879-7VC1C-C1EBF-DA4B9-JHDJB',
        	'CF879-BBF1C-XZ334-DA4B9-2DV77',
        	'CF879-BVR1C-C1EBF-DA4B9-DGERD',
        	'CF879-7DF1C-C1EBF-DA4B9-85D4G',
        	'CF879-XZAWC-C1EBF-DA4B9-75RGH',
        	'CF879-7DCCV-BBBBB-DA4B9-3FGDW',
        	'CF879-7AZ1Q-VC34F-DA4B9-5A230',
        	'CF879-7DF1C-00000-DA4B9-5A230',
        	'CF879-7789G-63VVV-DA4B9-RFGFF',
        	'CF879-7ERDS-3E42W-DA4B9-TGGG6',
        	'CF879-7CVRE-20RE7-DA4B9-1GU4W'
        );
        if( in_array($keys,$license) )
        {
    	    $res = array('state' => 'success');
    		return $res;
        }
        else
        {
            $res = array('state' => 'fails', 'title' => 'Something Wrong', 'msg' => 'Something Goes Wrong');
    		return $res;
        }
        exit();

		
		
		$key = $keys;
		$domain = $this->domain();
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, 'http://nemoz.net/keys');
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		$postfields = 'key=' . $key . '&domain=' . $domain;
		curl_setopt($c, CURLOPT_POSTFIELDS, $postfields);
		$res = curl_exec($c);
		$code = curl_getinfo($c, CURLINFO_HTTP_CODE);
		curl_close($c);
		$result = json_decode($res, false);

		if ($code == 200) {
			if ($result->state == 'false') {
				$res = array('state' => 'fails', 'title' => $result->title, 'msg' => $result->msg);
				return $res;
			}

			$res = array('state' => 'success');
			return $res;
		}

		$res = array('state' => 'fails', 'title' => 'Something Wrong', 'msg' => 'Something Goes Wrong');
		return $res;
	}

	public function validate_post($data)
	{
		return !empty($data['hostname']) && !empty($data['username']) && !empty($data['database']);
	}

	public function show_message($type = NULL, $message)
	{
		return $message;
	}

	public function write_license($key)
	{
		$output_path = '../apps/config/g.txt';
		$new = $key;
		$handle = fopen($output_path, 'w+');
		@chmod($output_path, 420);

		if (is_writable($output_path)) {
			if (fwrite($handle, $new)) {
				return true;
			}

			return false;
		}

		return false;
	}

	public function write_config($type, $local = NULL, $dbname = NULL, $dbuser = NULL, $dbpwd = NULL)
	{
		$template_path = 'config/database.php';
		$output_path = '../apps/config/database.php';

		if ($type == 'mysql') {
			$database_file = file_get_contents($template_path);
			$new = str_replace('%TYPE%', 'mysql', $database_file);
			$new = str_replace('%HOSTNAME%', $local, $new);
			$new = str_replace('%USERNAME%', $dbuser, $new);
			$new = str_replace('%PASSWORD%', $dbpwd, $new);
			$new = str_replace('%DATABASE%', $dbname, $new);
			$handle = fopen($output_path, 'w+');
			@chmod($output_path, 420);

			if (is_writable($output_path)) {
				if (fwrite($handle, $new)) {
					return true;
				}

				return false;
			}

			return false;
		}

		$database_file = file_get_contents($template_path);
		$new = str_replace('%TYPE%', 'sqlite', $database_file);
		$new = str_replace('%HOSTNAME%', 'localhost', $new);
		$new = str_replace('%USERNAME%', 'empty', $new);
		$new = str_replace('%PASSWORD%', 'empty', $new);
		$new = str_replace('%DATABASE%', 'empty', $new);
		$handle = fopen($output_path, 'w+');
		@chmod($output_path, 420);

		if (is_writable($output_path)) {
			if (fwrite($handle, $new)) {
				return true;
			}

			return false;
		}

		return false;
	}
}

$rand = 11;

?>
