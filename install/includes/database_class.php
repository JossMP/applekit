<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class Database
{
	public function check_connection($local, $dbname, $dbuser, $dbpwd)
	{
		$dsn = 'mysql:host=' . $local . ';dbname=' . $dbname;
		$db = new PDO($dsn, $dbuser, $dbpwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
		return $db ? true : false;
	}

	public function create_admin($type, $user = NULL, $pwd = NULL, $email = NULL)
	{
		ini_set('memory_limit', '512M');
		set_time_limit(0);

		if ($type == 'sqlite') {
			try {
				$db = new PDO('sqlite:../apps/sys/database/sys.sqlite');
				$sql="INSERT INTO admins(userName,passWord,userEmail)VALUES ('".$user."', '".$pwd."', '".$email."');";
				$db->exec($sql);
				$res = array('state' => 'success', 'title' => 'Success', 'msg' => 'Success');
				return $res;
			}
			catch (PDOException $e) {
				$res = array('state' => 'fails', 'title' => 'Can not Create the admin user', 'msg' => $e->getMessage());
				return $res;
			}
		}
		else {
			$dsn = 'mysql:host=' . $_SESSION['localhost'] . ';dbname=' . $_SESSION['dbname'];

			try {
				$db = new PDO($dsn, $_SESSION['dbuser'], $_SESSION['dbpwd'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
				$sql="INSERT INTO admins(userName,passWord,userEmail)VALUES ('".$user."', '".$pwd."', '".$email."');";
				$db->exec($sql);
				$res = array('state' => 'success', 'title' => 'Success', 'msg' => 'Success');
				return $res;
			}
			catch (PDOException $e) {
				$res = array('state' => 'fails', 'title' => 'Can not Create the admin user', 'msg' => $e->getMessage());
				return $res;
			}
		}
	}

	public function create_database($data)
	{
		$mysqli = new mysqli($data['hostname'], $data['username'], $data['password'], '');

		if (mysqli_connect_errno()) {
			return false;
		}

		$mysqli->query('CREATE DATABASE IF NOT EXISTS ' . $data['database']);
		$mysqli->close();
		return true;
	}

	public function create_tables($local, $dbname, $dbuser, $dbpwd)
	{
		ini_set('memory_limit', '5120M');
		set_time_limit(0);
		$dsn = 'mysql:host=' . $local . ';dbname=' . $dbname;

		try {
			$db = new PDO($dsn, $dbuser, $dbpwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));

			if ($db) {
				$dbms_schema = 'config/applekit_database_import.sql';

				//exit('problem ');
				($sql_query = @fread(@fopen($dbms_schema, 'r'), @filesize($dbms_schema))) || true;
				$sql_query = $this->remove_remarks($sql_query);
				$sql_query = $this->remove_comments($sql_query);
				$sql_query = $this->split_sql_file($sql_query, ';');

				foreach ($sql_query as $sql)
				{
					if(!empty($sql))
						$db->exec($sql);
				}

				$res = array('state' => 'success', 'title' => 'Success', 'msg' => 'Success');
				return $res;
			}
		}
		catch (PDOException $e) {
			$res = array('state' => 'fails', 'title' => 'Can not connect to database', 'msg' => $e->getMessage());
			return $res;
		}
	}

	public function remove_comments(&$output)
	{
		$lines = explode("\n", $output);
		$output = '';
		$linecount = count($lines);
		$in_comment = false;
		$i = 0;

		while ($i < $linecount) {
			if (preg_match('/^\\/\\*/', preg_quote($lines[$i]))) {
				$in_comment = true;
			}

			if (!$in_comment) {
				$output .= $lines[$i] . "\n";
			}

			if (preg_match('/\\*\\/$/', preg_quote($lines[$i]))) {
				$in_comment = false;
			}

			++$i;
		}

		unset($lines);
		return $output;
	}

	public function remove_remarks($sql)
	{
		$lines = explode("\n", $sql);
		$sql = '';
		$linecount = count($lines);
		$output = '';
		$i = 0;

		while ($i < $linecount) {
			if (($i != $linecount - 1) || (0 < strlen($lines[$i]))) {
				if (isset($lines[$i][0]) && ($lines[$i][0] != '#')) {
					$output .= $lines[$i] . "\n";
				}
				else {
					$output .= "\n";
				}

				$lines[$i] = '';
			}

			++$i;
		}

		return $output;
	}

	public function split_sql_file($sql, $delimiter)
	{
		$tokens = explode($delimiter, $sql);
		$sql = '';
		$output = array();
		$matches = array();
		$token_count = count($tokens);
		$i = 0;

		while ($i < $token_count) {
			if (($i != $token_count - 1) || strlen(0 < $tokens[$i])) {
				$total_quotes = preg_match_all('/\'/', $tokens[$i], $matches);
				$escaped_quotes = preg_match_all('/(?<!\\\\)(\\\\\\\\)*\\\\\'/', $tokens[$i], $matches);
				$unescaped_quotes = $total_quotes - $escaped_quotes;

				if (($unescaped_quotes % 2) == 0) {
					$output[] = $tokens[$i];
					$tokens[$i] = '';
				}
				else {
					$temp = $tokens[$i] . $delimiter;
					$tokens[$i] = '';
					$complete_stmt = false;
					$j = $i + 1;

					$total_quotes = preg_match_all('/\'/', $tokens[$j], $matches);
					$escaped_quotes = preg_match_all('/(?<!\\\\)(\\\\\\\\)*\\\\\'/', $tokens[$j], $matches);
					$unescaped_quotes = $total_quotes - $escaped_quotes;

					if (($unescaped_quotes % 2) == 1) {
						$output[] = $temp . $tokens[$j];
						$tokens[$j] = '';
						$temp = '';
						$complete_stmt = true;
						$i = $j;
					}
					else {
						$temp .= $tokens[$j] . $delimiter;
						$tokens[$j] = '';
					}

					++$j;
				}
			}

			++$i;
		}

		return $output;
	}
}


?>
