<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<span class="logo-installer"></span>' . "\n\t\t\t\t\t" . '<span class="text-installer text-center">' . "\n\t\t\t\t\t\t" . '<h1 class="installer-h1">Applekit Installer</h1>' . "\n\t\t\t\t\t\t" . '<p>System check up befor installation proccess.</p>' . "\n\t\t\t\t\t" . '</span>' . "\n\n\t\t\t\t\t" . '<div class="form-installer">' . "\n\t\t\t\t\t\t" . '<div class="col-xs-12">' . "\n\t\t\t\t\t\t\t" . '<table class="table-installer text-center">' . "\n\t\t\t\t\t\t\t\t" . '<tr>' . "\n\t\t\t\t\t\t\t\t\t" . '<td>' . "\n\t\t\t\t\t\t\t\t\t\t";
$phpver = phpversion();

if (5.4000000000000004 <= $phpver) {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/right.png" alt=""> PHP Version Check: <strong>';
	echo $phpver;
	echo '</strong> just fine :)' . "\n\t\t\t\t\t\t\t\t\t\t";
}
else {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/wrong.png" alt=""> PHP Version Check: <strong>';
	echo $phpver;
	echo '</strong> Need Something Greater than 5.4' . "\n\t\t\t\t\t\t\t\t\t\t";
}

echo "\t\t\t\t\t\t\t\t\t" . '</td>' . "\n\t\t\t\t\t\t\t\t" . '</tr>' . "\n\t\t\t\t\t\t\t\t" . '<tr>' . "\n\t\t\t\t\t\t\t\t\t" . '<td>' . "\n\t\t\t\t\t\t\t\t\t\t";
/*
if (!extension_loaded('ionCube Loader')) {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/wrong.png" alt=""> PHP ioncube Check: <strong>ioncube loader</strong> isn\'t installed yet please contact support.' . "\n\t\t\t\t\t\t\t\t\t\t";
}
else {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/right.png" alt=""> PHP ioncube Check: <strong>ioncube loader</strong> Installed your ready to go.' . "\n\t\t\t\t\t\t\t\t\t\t";
}
*/
echo "\t\t\t\t\t\t\t\t\t" . '</td>' . "\n\t\t\t\t\t\t\t\t" . '</tr>' . "\n\t\t\t\t\t\t\t\t" . '<tr>' . "\n\t\t\t\t\t\t\t\t\t" . '<td>' . "\n\t\t\t\t\t\t\t\t\t\t";

if (extension_loaded('pdo_mysql')) {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/right.png" alt=""> PHP PDO check: <strong>PDO_mysql</strong> is installed your ready to go.' . "\n\t\t\t\t\t\t\t\t\t\t";
}
else {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/wrong.png" alt=""> PHP PDO check: <strong>PDO_mysql</strong> isn\'t installed please contact support.' . "\n\t\t\t\t\t\t\t\t\t\t";
}

echo "\t\t\t\t\t\t\t\t\t" . '</td>' . "\n\t\t\t\t\t\t\t\t" . '</tr>' . "\n\n\t\t\t\t\t\t\t\t" . '<tr>' . "\n\t\t\t\t\t\t\t\t\t" . '<td>' . "\n\t\t\t\t\t\t\t\t\t\t";
$db_config_path = '../.htaccess';
echo "\t\t\t\t\t\t\t\t\t\t";

if (file_exists($db_config_path)) {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/right.png" alt=""> Please make sure the <strong>"htaccess.txt"</strong> file have been renamed to <strong>".htaccess"</strong> ' . "\n\t\t\t\t\t\t\t\t\t\t";
}
else {
	echo "\t\t\t\t\t\t\t\t\t\t\t" . '<img src="assets/img/wrong.png" alt=""> Please make sure the <strong>"htaccess.txt"</strong> file have been renamed to <strong>".htaccess"</strong>' . "\n\t\t\t\t\t\t\t\t\t\t";
}

echo "\t\t\t\t\t\t\t\t\t" . '</td>' . "\n\t\t\t\t\t\t\t\t" . '</tr>' . "\n\n\n\t\t\t\t\t\t\t" . '</table>' . "\n\t\t\t\t\t\t" . '</div>' . "\n\t\t\t\t\t\t" . '<div class="clearfix"></div>' . "\n\t\t\t\t\t" . '</div>' . "\n\n\t\t\t\t\t" . '<div class="footer-installer">' . "\n\t\t\t\t\t\t" . '<a href="javascript:history.go(-1)" class="button-installer">' . "\n\t\t\t\t\t\t\t" . '<img src="assets/img/arrows-left.png" alt="">' . "\n\t\t\t\t\t\t\t" . '<span>Back</span>' . "\n\t\t\t\t\t\t" . '</a>' . "\n" . '                        ';

//if (extension_loaded('ionCube Loader'))
{
	echo '                            <a href="?step=stage3" disabled="disabled" readonly class="button-installer">' . "\n" . '                                <img src="assets/img/arrows-right.png" alt="">' . "\n" . '                                <span>Continue</span>' . "\n" . '                            </a>' . "\n" . '                        ';
}

echo "\t\t\t\t\t" . '</div>' . "\n\n\t\t\t\t" . '</div>' . "\n\t\t\t" . '</div>';

?>
