<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<span class="logo-installer"></span>' . "\n" . '<span class="text-installer text-center">' . "\n\t" . '<h1 class="installer-h1">Applekit Installer</h1>' . "\n\t" . '<p>Please Insert your license Key to verify.</p>' . "\n" . '</span>' . "\n\n" . '<div class="form-installer">' . "\n\n\t";

if (isset($_SESSION['stage1']) && ($_SESSION['stage1'] == 'done')) {
	echo "\t\n\t" . '<div class="col-xs-12">' . "\n\t\t" . '<table class="table-installer text-center">' . "\n\t\t\t" . '<tr>' . "\n\t\t\t\t" . '<td><img src="assets/img/right.png" alt=""> Vaild License ';
	echo $_SESSION['stage1lic'];
	echo '</td>' . "\n\t\t\t" . '</tr>' . "\n\t\t" . '</table>' . "\n\t" . '</div>' . "\n\n\t";
}
else {
	echo "\n\t" . '<form action="?step=stage1" class="stage1" method="POST">' . "\n\t\t" . '<input type="text" placeholder="Insert License Key" id="license" name="license">' . "\n\t\t" . '<button type="submit" id="licenseCheck" name="licenseCheck"></button>' . "\n\t" . '</form>' . "\n\n\t";
}

echo "\n\t";
$msgs = $core->forms();

if ($msgs['state'] == 'fails') {
	echo "\t\t\n\t" . '<div class="alert-installer">' . "\n\t\t" . '<div class="alert-error">' . "\n\t\t";
	$msg = $core->forms();
	echo "\t\t\t" . '<p>';
	echo $msg['title'];
	echo '</p>' . "\n\t\t\t" . '<p>';
	echo $msg['msg'];
	echo '</p>' . "\n\t\t" . '</div>' . "\n\t" . '</div>' . "\n\n\t";
}

echo '</div>' . "\n\n" . '<div class="footer-installer">' . "\n\t" . '<a href="javascript:history.go(-1)" class="button-installer">' . "\n\t\t" . '<img src="assets/img/arrows-left.png" alt="">' . "\n\t\t" . '<span>Back</span>' . "\n\t" . '</a>' . "\n\n\t";

if (isset($_SESSION['stage1']) && ($_SESSION['stage1'] == 'done')) {
	echo "\t" . '<a href="?step=stage2" class="button-installer">' . "\n\t\t" . '<img src="assets/img/arrows-right.png" alt="">' . "\n\t\t" . '<span>Continue</span>' . "\n\t" . '</a>' . "\n\t";
}
else {
	echo "\t" . '<a class="button-installer">' . "\n\t\t" . '<img src="assets/img/arrows-right2.png" alt="">' . "\n\t\t" . '<span>Continue</span>' . "\n\t" . '</a>' . "\n\t";
}

echo '</div>';

?>
