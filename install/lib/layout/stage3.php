<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<span class="logo-installer"></span>' . "\n\n" . '<!-- /.select-db -->' . "\n" . '<span class="text-installer text-center">' . "\n\t\t\t\t\t\t" . '<h1 class="installer-h1">Applekit Installer</h1>' . "\n\t\t\t\t\t\t" . '<p>Please Insert your database info\'s to compelete the installation proccess.</p>' . "\n\t\t\t\t\t\t" . '<p>If your noobie and dosn\'t know how to create database <a' . "\n" . '                                    href="https://www.youtube.com/watch?v=ltT0Kc5O-yo" target="_blank">Here\'s</a> an exp</p>' . "\n\t\t\t\t\t" . '</span>' . "\n\n" . '<div class="form-installer">' . "\n\n" . '    ';

if (isset($_SESSION['stage4']) && ($_SESSION['stage4'] == 'done')) {
	echo "\n" . '        <div class="col-xs-12">' . "\n" . '            <table class="table-installer text-center">' . "\n" . '                <tr>' . "\n" . '                    <td><img src="assets/img/right.png" alt=""> Database Created and imported successfully Please hit' . "\n" . '                        continue button.' . "\n" . '                    </td>' . "\n" . '                </tr>' . "\n" . '            </table>' . "\n" . '        </div>' . "\n\n" . '    ';
}
else {
	echo "\n" . '        <!-- /# -->' . "\n" . '        <form action="?step=stage3" class="stage3"' . "\n" . '              method="POST">' . "\n" . '            ';

	if (extension_loaded('sqlite3')) {
		echo '                <div class="select-db"' . "\n" . '                     style="position: relative; display: block; text-align: center; margin-bottom: 20px">' . "\n" . '                    <label for="">Choose Database type:</label>' . "\n" . '                    <select name="db" id="db" style="margin: 0 auto; margin-right: auto; margin-left:auto;">' . "\n" . '                        ';

		if (extension_loaded('sqlite3')) {
			echo '<option value="sqlite">Local File</option>';
		}

		echo '                        <option value="mysql" ';

		if (isset($_SESSION['db']) && ($_SESSION['db'] == 'mysql')) {
			echo ' selected ';
		}

		echo '>Mysql - server depend</option>' . "\n" . '                    </select>' . "\n" . '                </div>' . "\n" . '            ';
	}

	echo '            <div id="dbform"' . "\n" . '                ';
	if (extension_loaded('sqlite3') && isset($_SESSION['db'])) {
		echo '                    ';

		if (isset($_SESSION['db']) && ($_SESSION['db'] == 'sqlite')) {
			echo '                        style="display: none;"' . "\n" . '                    ';
		}
		else {
			echo '                        style="display: block;"' . "\n" . '                    ';
		}

		echo '                ';
	}
	else {
		echo '                    ';
		if (extension_loaded('sqlite3') && !isset($_SESSION['db'])) {
			echo '                        style="display: none;"' . "\n" . '                    ';
		}
		else {
			echo '                        style="display: block;"' . "\n" . '                    ';
		}

		echo '                ';
	}

	echo '>' . "\n" . '                <input type="text" placeholder="LocalHost" value="localhost" id="localhost" name="localhost">' . "\n" . '                <input type="text" placeholder="Database Name" id="dbname" name="dbname">' . "\n" . '                <input type="text" placeholder="Database Username" id="dbuser" name="dbuser">' . "\n" . '                <input type="text" placeholder="Database Password" id="dbpwd" name="dbpwd">' . "\n" . '            </div>' . "\n" . '            <!-- /#dbform -->' . "\n" . '            <button type="submit" id="databaseCheck" name="databaseCheck" title="Connect with Database!"></button>' . "\n" . '        </form>' . "\n" . '    ';
}

echo '    ';
$msgs = $core->forms();

if ($msgs['state'] == 'fails') {
	echo "\n" . '        <div class="alert-installer">' . "\n" . '            <div class="alert-error">' . "\n" . '                ';
	$msg = $core->forms();
	echo '                <p>';
	echo $msg['title'];
	echo '</p>' . "\n" . '                <p>';
	echo $msg['msg'];
	echo '</p>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    ';
}

echo '</div>' . "\n\n" . '<div class="footer-installer">' . "\n" . '    <a href="javascript:history.go(-1)" class="button-installer">' . "\n" . '        <img src="assets/img/arrows-left.png" alt="">' . "\n" . '        <span>Back</span>' . "\n" . '    </a>' . "\n\n" . '    ';

if (isset($_SESSION['stage4']) && ($_SESSION['stage4'] == 'done')) {
	echo '        <a href="?step=stage4" class="button-installer go">' . "\n" . '            <img src="assets/img/arrows-right.png" alt="">' . "\n" . '            <span>Continue</span>' . "\n" . '        </a>' . "\n" . '    ';
}
else {
	echo '        <a class="button-installer go">' . "\n" . '            <img src="assets/img/arrows-right.png" alt="">' . "\n" . '            <span>Continue</span>' . "\n" . '        </a>' . "\n" . '    ';
}

echo "\n" . '</div>' . "\n\n" . '<script type="text/javascript" src="../assets/js/jquery.js"></script>' . "\n" . '<script type="text/javascript">' . "\n" . '    $(\'#db\').on(\'change\', function () {' . "\n" . '        if ($(this).val() == \'mysql\') {' . "\n" . '            $(\'#dbform\').show();' . "\n" . '            $(\'.button-installer\').removeAttr(\'href\');' . "\n" . '            $(\'.button-installer.go img\').attr(\'src\', \'assets/img/arrows-right2.png\');' . "\n" . '        } else {' . "\n" . '            $(\'#dbform\').hide();' . "\n" . '        }' . "\n" . '    });' . "\n" . '</script>';

?>
