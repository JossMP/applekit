<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

echo '<span class="logo-installer"></span>' . "\n" . '<span class="text-installer text-center">' . "\n\t\t\t\t\t\t" . '<h1 class="installer-h1">Applekit Installer</h1>' . "\n\t\t\t\t\t\t" . '<p>Please Insert your admin info\'s to compelete the installation proccess.</p>' . "\n\t\t\t\t\t" . '</span>' . "\n\n" . '<div class="form-installer">' . "\n\n" . '    ';

if (isset($_SESSION['stage5']) && ($_SESSION['stage5'] == 'done')) {
	echo "\n" . '        <div class="col-xs-12">' . "\n" . '            <table class="table-installer text-center">' . "\n" . '                <tr>' . "\n" . '                    <td><img src="assets/img/right.png" alt=""> Admin user have been added successfully, Please hit' . "\n" . '                        continue button.' . "\n" . '                    </td>' . "\n" . '                </tr>' . "\n" . '            </table>' . "\n" . '        </div>' . "\n\n" . '    ';
}
else {
	echo '        <form action="?step=stage4" class="stage3" method="POST">' . "\n" . '            <input type="text" placeholder="Username exp. Admin" id="username" name="username">' . "\n" . '            <input type="text" placeholder="Password" id="password" name="password">' . "\n" . '            <input type="text" placeholder="Email Address" id="email" name="email">' . "\n" . '            <button type="submit" id="adminCreate" name="adminCreate"></button>' . "\n" . '        </form>' . "\n" . '    ';
}

echo '    ';
$msgs = $core->forms();

if ($msgs['state'] == 'fails') {
	echo "\n" . '        <div class="alert-installer">' . "\n" . '            <div class="alert-error">' . "\n" . '                ';
	$msg = $core->forms();
	echo '                <p>';
	echo $msg['title'];
	echo '</p>' . "\n" . '                <p>';
	echo $msg['msg'];
	echo '</p>' . "\n" . '            </div>' . "\n" . '        </div>' . "\n\n" . '    ';
}

echo '</div>' . "\n\n" . '<div class="footer-installer">' . "\n" . '    <a href="javascript:history.go(-1)" class="button-installer">' . "\n" . '        <img src="assets/img/arrows-left.png" alt="">' . "\n" . '        <span>Back</span>' . "\n" . '    </a>' . "\n\n" . '    ';

if (isset($_SESSION['stage4']) && ($_SESSION['stage4'] == 'done')) {
	echo '        <a href="?step=stage5" class="button-installer">' . "\n" . '            <img src="assets/img/arrows-right.png" alt="">' . "\n" . '            <span>Continue</span>' . "\n" . '        </a>' . "\n" . '    ';
}
else {
	echo '        <a class="button-installer">' . "\n" . '            <img src="assets/img/arrows-right2.png" alt="">' . "\n" . '            <span>Continue</span>' . "\n" . '        </a>' . "\n" . '    ';
}

echo "\n" . '</div>';

?>
