<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

ob_start();
session_start();
require_once 'includes/core_class.php';
require_once 'includes/database_class.php';
$link = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
$core = new Core();
$core->forms();
echo '<!DOCTYPE html>' . "\n" . '<html lang="en">' . "\n" . '<head>' . "\n\t" . '<meta charset="UTF-8">' . "\n\t" . '<title>Applekit Installer</title>' . "\n\t" . '<link rel="stylesheet" href="assets/css/install.css">' . "\n\t" . '<link rel="stylesheet" href="assets/css/strap.css">' . "\n" . '</head>' . "\n" . '<body>' . "\n" . '<div class="container">' . "\n\t" . '<div class="row">' . "\n\t\t" . '<div class="col-xs-12">' . "\n\t\t\t\n\t\t\t" . '<div class="container-installer">' . "\n\t\t\t\t" . '<div class="header-installer">' . "\n\t\t\t\t\t" . '<span class="head-installer"></span>' . "\n\t\t\t\t" . '</div>' . "\n\t\t\t\t" . '<div class="body-installer">' . "\n\t\t\t\t\n\t\t\t\t";

if (!$_GET) {
	echo "\t\t\t\t\t" . '<span class="logo-installer"></span>' . "\n\t\t\t\t\t" . '<span class="text-installer text-center">' . "\n\t\t\t\t\t\t" . '<h1 class="installer-h1">Applekit Installer</h1>' . "\n\t\t\t\t\t\t" . '<p>To set up the installation of Applekit, click Continue.</p>' . "\n\t\t\t\t\t" . '</span>' . "\n\t\t\t\t\t" . '<div class="clearfix"></div>' . "\n\t\t\t\t\t" . '<div class="footer-installer">' . "\n\t\t\t\t\t\t";

	if (isset($_SESSION['stage1'])) {
		echo "\t\t\t\t\t\t" . '<a href="?step=startover" class="button-installer">' . "\n\t\t\t\t\t\t\t" . '<img src="assets/img/arrows-right.png" alt="">' . "\n\t\t\t\t\t\t\t" . '<span>Start Over</span>' . "\n\t\t\t\t\t\t" . '</a>' . "\n\t\t\t\t\t\t";
	}
	else {
		echo "\t\t\t\t\t\t" . '<a href="?step=stage1" class="button-installer">' . "\n\t\t\t\t\t\t\t" . '<img src="assets/img/arrows-right.png" alt="">' . "\n\t\t\t\t\t\t\t" . '<span>Continue</span>' . "\n\t\t\t\t\t\t" . '</a>' . "\n\t\t\t\t\t\t";
	}

	echo "\t\t\t\t\t" . '</div>' . "\n\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'stage1')) {
	echo "\t\t\t\t";
	require_once 'lib/layout/stage1.php';
	echo "\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'stage2') && isset($_SESSION['stage1']) && ($_SESSION['stage1'] == 'done')) {
	echo "\t\t\t\t";
	$core->write_license(trim($_SESSION['stage1lic']));
	require_once 'lib/layout/stage2.php';
	echo "\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'stage3') && isset($_SESSION['stage1']) && ($_SESSION['stage1'] == 'done')) {
	echo "\t\t\t\t";
	require_once 'lib/layout/stage3.php';
	echo "\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'stage4') && isset($_SESSION['stage4']) && ($_SESSION['stage4'] == 'done')) {
	echo "\t\t\t\t";
	require_once 'lib/layout/stage4.php';
	echo "\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'stage5') && isset($_SESSION['stage5']) && ($_SESSION['stage5'] == 'done'))
{
	echo "\t\t\t\t";
	require_once 'lib/layout/success.php';
	echo "\t\t\t\t";
}

echo "\n\t\t\t\t";

if (isset($_GET['step']) && ($_GET['step'] == 'startover')) {
	echo "\t\t\t\t\t";
	session_destroy();
	echo "\t\t\t\t\t" . '<script>window.location = \'';
	echo $link;
	echo '/\';</script>' . "\n\t\t\t\t";
}

echo "\n\t\t\t\t" . '</div>' . "\n\t\t\t" . '</div>' . "\n\t\t\n\t\t" . '</div>' . "\n\t" . '</div>' . "\n" . '</div>' . "\n\t\n\n\n" . '</body>' . "\n" . '</html>' . "\n\n";
ob_end_flush();

?>
