////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Hebrew (he)
	translated by: David Cohen
	site: dav.co.il
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "תמונה {current} מתוך {total}",
	previous: "הקודם",
	next: "הבא",
	close: "סגור",
	xhrError: "שגיאה בטעינת התוכן.",
	imgError: "שגיאה בטעינת התמונה.",
	slideshowStart: "התחל מצגת",
	slideshowStop: "עצור מצגת"
});
