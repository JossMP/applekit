////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
	language: Bulgarian (bg)
	translated by: Marian M.Bida
	site: webmax.bg
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "изображение {current} от {total}",
	previous: "предишна",
	next: "следваща",
	close: "затвори",
	xhrError: "Неуспешно зареждане на съдържанието.",
	imgError: "Неуспешно зареждане на изображението.",
	slideshowStart: "пусни слайд-шоу",
	slideshowStop: "спри слайд-шоу"
});
