////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Russian (ru)
	translated by: Marfa
	site: themarfa.name
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "изображение {current} из {total}",
	previous: "назад",
	next: "вперёд",
	close: "закрыть",
	xhrError: "Не удалось загрузить содержимое.",
	imgError: "Не удалось загрузить изображение.",
	slideshowStart: "начать слайд-шоу",
	slideshowStop: "остановить слайд-шоу"
});