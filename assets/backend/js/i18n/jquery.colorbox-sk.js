////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Slovak (sk)
	translated by: Jaroslav Kostal
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{current}. obrázok z {total}",
	previous: "Predchádzajúci",
	next: "Následujúci",
	close: "Zatvoriť",
	xhrError: "Obsah sa nepodarilo načítať.",
	imgError: "Obrázok sa nepodarilo načítať.",
	slideshowStart: "Spustiť slideshow",
	slideshowStop: "zastaviť slideshow"
});