////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Norwegian (no)
	translated by: lars-erik
	site: markedspartner.no
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Bilde {current} av {total}",
	previous: "Forrige",
	next: "Neste",
	close: "Lukk",
	xhrError: "Feil ved lasting av innhold.",
	imgError: "Feil ved lasting av bilde.",
	slideshowStart: "Start lysbildefremvisning",
	slideshowStop: "Stopp lysbildefremvisning"
});
