////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Swedish (sv)
	translated by: Mattias Reichel
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Bild {current} av {total}",
	previous: "Föregående",
	next: "Nästa",
	close: "Stäng",
	xhrError: "Innehållet kunde inte laddas.",
	imgError: "Den här bilden kunde inte laddas.",
	slideshowStart: "Starta bildspel",
	slideshowStop: "Stoppa bildspel"
});