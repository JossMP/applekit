////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Catala (ca)
	translated by: eduard salla
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Imatge {current} de {total}",
	previous: "Anterior",
	next: "Següent",
	close: "Tancar",
	xhrError: "Error en la càrrega del contingut.",
	imgError: "Error en la càrrega de la imatge."
});
