////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Danish (da)
	translated by: danieljuhl
	site: danieljuhl.dk
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Billede {current} af {total}",
	previous: "Forrige",
	next: "Næste",
	close: "Luk",
	xhrError: "Indholdet fejlede i indlæsningen.",
	imgError: "Billedet fejlede i indlæsningen.",
	slideshowStart: "Start slideshow",
	slideshowStop: "Stop slideshow"
});
