////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
	language: Galician (gl)
	translated by: donatorouco
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Imaxe {current} de {total}",
	previous: "Anterior",
	next: "Seguinte",
	close: "Pechar",
	xhrError: "Erro na carga do contido.",
	imgError: "Erro na carga da imaxe."
});
