////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Dutch (nl)
	translated by: barryvdh
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Afbeelding {current} van {total}",
	previous: "Vorige",
	next: "Volgende",
	close: "Sluiten",
	xhrError: "Deze inhoud kan niet geladen worden.",
	imgError: "Deze afbeelding kan niet geladen worden.",
	slideshowStart: "Diashow starten",
	slideshowStop: "Diashow stoppen"
});