////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
  language: Romanian (ro)
  translated by: shurub3l
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "imagine {current} din {total}",
	previous: "precedenta",
	next: "următoarea",
	close: "închideți",
	xhrError: "Acest conținut nu poate fi încărcat.",
	imgError: "Această imagine nu poate fi încărcată",
	slideshowStart: "începeți prezentarea (slideshow)",
	slideshowStop: "opriți prezentarea (slideshow)"
});