////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Korean (kr)
	translated by: lunareffect
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "총 {total} 중 {current}",
	previous: "이전",
	next: "다음",
	close: "닫기",
	xhrError: "컨텐츠를 불러오는 데 실패했습니다.",
	imgError: "이미지를 불러오는 데 실패했습니다.",
	slideshowStart: "슬라이드쇼 시작",
	slideshowStop: "슬라이드쇼 중지"
});
