////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Czech (cs)
	translated by: Filip Novak
	site: mame.napilno.cz/filip-novak
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{current}. obrázek z {total}",
	previous: "Předchozí",
	next: "Následující",
	close: "Zavřít",
	xhrError: "Obsah se nepodařilo načíst.",
	imgError: "Obrázek se nepodařilo načíst.",
	slideshowStart: "Spustit slideshow",
	slideshowStop: "Zastavit slideshow"
});