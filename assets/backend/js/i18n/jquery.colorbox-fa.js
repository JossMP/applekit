////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Persian (Farsi)
	translated by: Mahdi Jaberzadeh Ansari (MJZSoft)
	site: www.mjzsoft.ir
	email: mahdijaberzadehansari (at) yahoo.co.uk
	Please note : Persian language is right to left like arabic.
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "تصویر {current} از {total}",
	previous: "قبلی",
	next: "بعدی",
	close: "بستن",
	xhrError: "متاسفانه محتویات مورد نظر قابل نمایش نیست.",
	imgError: "متاسفانه بارگذاری این عکس با مشکل مواجه شده است.",
	slideshowStart: "آغاز نمایش خودکار",
	slideshowStop: "توقف نمایش خودکار"
});
