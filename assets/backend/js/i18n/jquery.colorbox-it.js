////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Italian (it)
	translated by: maur8ino
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Immagine {current} di {total}",
	previous: "Precedente",
	next: "Successiva",
	close: "Chiudi",
	xhrError: "Errore nel caricamento del contenuto.",
	imgError: "Errore nel caricamento dell'immagine.",
	slideshowStart: "Inizia la presentazione",
	slideshowStop: "Termina la presentazione"
});
