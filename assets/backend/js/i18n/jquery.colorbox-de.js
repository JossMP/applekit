////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: German (de)
	translated by: wallenium
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Bild {current} von {total}",
	previous: "Zurück",
	next: "Vor",
	close: "Schließen",
	xhrError: "Dieser Inhalt konnte nicht geladen werden.",
	imgError: "Dieses Bild konnte nicht geladen werden.",
	slideshowStart: "Slideshow starten",
	slideshowStop: "Slideshow anhalten"
});