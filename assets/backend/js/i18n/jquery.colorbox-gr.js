////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Greek (gr)
	translated by: S.Demirtzoglou
	site: webiq.gr
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Εικόνα {current} από {total}",
	previous: "Προηγούμενη",
	next: "Επόμενη",
	close: "Απόκρυψη",
	xhrError: "Το περιεχόμενο δεν μπόρεσε να φορτωθεί.",
	imgError: "Απέτυχε η φόρτωση της εικόνας.",
	slideshowStart: "Έναρξη παρουσίασης",
	slideshowStop: "Παύση παρουσίασης"
});
