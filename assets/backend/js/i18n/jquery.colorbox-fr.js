////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
  language: French (fr)
  translated by: oaubert
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "image {current} sur {total}",
	previous: "pr&eacute;c&eacute;dente",
	next: "suivante",
	close: "fermer",
	xhrError: "Impossible de charger ce contenu.",
	imgError: "Impossible de charger cette image.",
	slideshowStart: "d&eacute;marrer la pr&eacute;sentation",
	slideshowStop: "arr&ecirc;ter la pr&eacute;sentation"
});
