////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Turkish (tr)
	translated by: Caner ÖNCEL
	site: egonomik.com
	
	edited by: Sinan Eldem
	www.sinaneldem.com.tr
*/
jQuery.extend(jQuery.colorbox.settings, {
  current: "Görsel {current} / {total}",
  previous: "Önceki",
  next: "Sonraki",
  close: "Kapat",
  xhrError: "İçerik yüklenirken hata meydana geldi.",
  imgError: "Resim yüklenirken hata meydana geldi.",
  slideshowStart: "Slaytı Başlat",
  slideshowStop: "Slaytı Durdur"
});
