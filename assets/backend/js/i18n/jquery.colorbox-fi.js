////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Finnish (fi)
	translated by: Mikko
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Kuva {current} / {total}",
	previous: "Edellinen",
	next: "Seuraava",
	close: "Sulje",
	xhrError: "Sisällön lataaminen epäonnistui.",
	imgError: "Kuvan lataaminen epäonnistui.",
	slideshowStart: "Aloita kuvaesitys.",
	slideshowStop: "Lopeta kuvaesitys."	
});
