////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
jQuery Colorbox language configuration
language: Bengali (bn)
translated by: Arkaprava Majumder
http://github.com/arkaindas
*/
jQuery.extend(jQuery.colorbox.settings, {
current: "ছবি {current} এর {total}",
previous: "আগে",
next: "পরে",
close: "বন্ধ",
xhrError: "এই কন্টেন্ট লোড করা যায়নি.",
imgError: "এই ছবিটি লোড করা যায়নি.",
slideshowStart: "স্লাইডশো শুরু",
slideshowStop: "স্লাইডশো বন্ধ"
});
