////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Slovenian (si)
	translated by: Boštjan Pišler (pisler.si)
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Slika {current} od {total}",
	previous: "Prejšnja",
	next: "Naslednja",
	close: "Zapri",
	xhrError: "Vsebine ni bilo mogoče naložiti.",
	imgError: "Slike ni bilo mogoče naložiti.",
	slideshowStart: "Zaženi prezentacijo",
	slideshowStop: "Zaustavi prezentacijo"
});