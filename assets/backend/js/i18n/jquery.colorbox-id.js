////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Indonesian (id)
	translated by: sarwasunda
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "ke {current} dari {total}",
	previous: "Sebelumnya",
	next: "Berikutnya",
	close: "Tutup",
	xhrError: "Konten ini tidak dapat dimuat.",
	imgError: "Gambar ini tidak dapat dimuat.",
	slideshowStart: "Putar",
	slideshowStop: "Berhenti"
});
