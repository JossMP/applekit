////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Chinese Traditional (zh-TW)
	translated by: Atans Chiu
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "圖片 {current} 總共 {total}",
	previous: "上一頁",
	next: "下一頁",
	close: "關閉",
	xhrError: "此內容載入失敗.",
	imgError: "此圖片加入失敗.",
	slideshowStart: "開始幻燈片",
	slideshowStop: "結束幻燈片"
});
