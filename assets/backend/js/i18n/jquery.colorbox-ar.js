////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
  language: Arabic (ar)
  translated by: A.Rhman Sayes
*/
jQuery.extend(jQuery.colorbox.settings, {
  current: "الصورة {current} من {total}",
	previous: "السابق",
	next: "التالي",
	close: "إغلاق",
	xhrError: "حدث خطأ أثناء تحميل المحتوى.",
	imgError: "حدث خطأ أثناء تحميل الصورة.",
	slideshowStart: "تشغيل العرض",
	slideshowStop: "إيقاف العرض"
});
