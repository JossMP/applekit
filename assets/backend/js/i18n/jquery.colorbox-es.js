////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
	language: Spanish (es)
	translated by: migolo
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Imagen {current} de {total}",
	previous: "Anterior",
	next: "Siguiente",
	close: "Cerrar",
	xhrError: "Error en la carga del contenido.",
	imgError: "Error en la carga de la imagen."
});
