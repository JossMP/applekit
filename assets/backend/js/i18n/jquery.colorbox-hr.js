////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Croatian (hr)
	translated by: Mladen Bicanic (base.hr)
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Slika {current} od {total}",
	previous: "Prethodna",
	next: "Sljedeća",
	close: "Zatvori",
	xhrError: "Neuspješno učitavanje sadržaja.",
	imgError: "Neuspješno učitavanje slike.",
	slideshowStart: "Pokreni slideshow",
	slideshowStop: "Zaustavi slideshow"
});