////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
  jQuery Colorbox language configuration
  language: Japanaese (ja)
  translated by: Hajime Fujimoto
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{total}枚中{current}枚目",
	previous: "前",
	next: "次",
	close: "閉じる",
	xhrError: "コンテンツの読み込みに失敗しました",
	imgError: "画像の読み込みに失敗しました",
	slideshowStart: "スライドショー開始",
	slideshowStop: "スライドショー終了"
});
