////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Polski (pl)
	translated by: Tomasz Wasiński
	site: 2bevisible.pl
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{current}. obrazek z {total}",
	previous: "Poprzedni",
	next: "Następny",
	close: "Zamknij",
	xhrError: "Nie udało się załadować treści.",
	imgError: "Nie udało się załadować obrazka.",
	slideshowStart: "rozpocznij pokaz slajdów",
	slideshowStop: "zatrzymaj pokaz slajdów"
});