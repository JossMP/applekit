////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
        jQuery Colorbox language configuration
        language: Serbian (sr)
        translated by: Sasa Stefanovic (baguje.com)
*/
jQuery.extend(jQuery.colorbox.settings, {
        current: "Slika {current} od {total}",
        previous: "Prethodna",
        next: "Sledeća",
        close: "Zatvori",
        xhrError: "Neuspešno učitavanje sadržaja.",
        imgError: "Neuspešno učitavanje slike.",
        slideshowStart: "Pokreni slideshow",
        slideshowStop: "Zaustavi slideshow"
});
