////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Brazilian Portuguese (pt-BR)
	translated by: ReinaldoMT
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "Imagem {current} de {total}",
	previous: "Anterior",
	next: "Próxima",
	close: "Fechar",
	slideshowStart: "iniciar apresentação de slides",
	slideshowStop: "parar apresentação de slides",
	xhrError: "Erro ao carregar o conteúdo.",
	imgError: "Erro ao carregar a imagem."
});