////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Estonian (et)
	translated by: keevitaja
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{current}/{total}",
	previous: "eelmine",
	next: "järgmine",
	close: "sulge",
	xhrError: "Sisu ei õnnestunud laadida.",
	imgError: "Pilti ei õnnestunud laadida.",
	slideshowStart: "Käivita slaidid",
	slideshowStop: "Peata slaidid"
});