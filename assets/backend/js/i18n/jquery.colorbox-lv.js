////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Latvian (lv)
	translated by: Matiss Roberts Treinis
    site: x0.lv
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "attēls {current} no {total}",
	previous: "iepriekšējais",
	next: "nākamais",
	close: "aizvērt",
	xhrError: "Neizdevās ielādēt saturu.",
	imgError: "Neizdevās ielādēt attēlu.",
        slideshowStart: "sākt slaidrādi",
        slideshowStop: "apturēt slaidrādi"
});
