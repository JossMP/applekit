////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
    jQuery ColorBox language configuration
    language: Ukrainian (uk)
    translated by: Andrew
    http://acisoftware.com.ua
*/
jQuery.extend(jQuery.colorbox.settings, {
    current: "зображення {current} з {total}",
    previous: "попереднє",
    next: "наступне",
    close: "закрити",
    xhrError: "Не вдалося завантажити вміст.",
    imgError: "Не вдалося завантажити зображення.",
    slideshowStart: "почати слайд-шоу",
    slideshowStop: "зупинити слайд-шоу"
});