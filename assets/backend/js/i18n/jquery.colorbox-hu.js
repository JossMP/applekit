////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	jQuery Colorbox language configuration
	language: Hungarian (hu)
	translated by: kovadani
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "{current}/{total} kép",
	previous: "Előző",
	next: "Következő",
	close: "Bezár",
	xhrError: "A tartalmat nem sikerült betölteni.",
	imgError: "A képet nem sikerült betölteni.",
	slideshowStart: "Diavetítés indítása",
	slideshowStop: "Diavetítés leállítása"
});