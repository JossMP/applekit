////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    function base64_encode(data) {
        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

        if (!data) {
            return data;
        }

        data = unescape(encodeURIComponent(data));

        do {
            // pack three octets into four hexets
            o1 = data.charCodeAt(i++);
            o2 = data.charCodeAt(i++);
            o3 = data.charCodeAt(i++);

            bits = o1 << 16 | o2 << 8 | o3;

            h1 = bits >> 18 & 0x3f;
            h2 = bits >> 12 & 0x3f;
            h3 = bits >> 6 & 0x3f;
            h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        } while (i < data.length);

        enc = tmp_arr.join('');

        var r = data.length % 3;

        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    }

    function getPath(templ, lang) {
        // return template path with the template file;
        if ( lang === '') {
            return '/apps/mailTemplate/' + templ;
        } else {
            return '/apps/mailTemplate/'+ lang + '/' + templ;
        }
    }

    function mailCreate(temp, data, lang) {
        $.get(getPath(temp, lang), function(template) {
            var rendered = Mustache.render(template, data);
            $('#maileditor').froalaEditor('html.set', rendered);
        });
    }


    $('#generatemaps').on('click', function () {
        // name
        var name, trans_lng;

        if ($('#langs').val() === '1') {
            trans_lng = $('#olang').val();
        } else {
            trans_lng = $('#langs').val();
        }

        if ($('#email').val() === '') {
            toastr.error('You have to test the email first befor sending it !', 'Error');
            $('.checkexs').addClass('has-error');
            $('.checktxt').html('<i class="fa fa-times"></i> Check email vaild first !');
            $('body,html').animate({
                scrollTop: $("body").offset().top
            }, 500);
        } else {
            var data = {
                link: $('#linkm').val(),
            };

            mailCreate('Maps-Connect.html', data, trans_lng);
            var emails = $('#email').val();
            $('#to').val(emails);

            $('#subject').val(maptmp[trans_lng]['title']);

            $('.themeBody').removeClass('has-error');
            $('.themetxt').html('');
            $('#msgType').val('map');
            $('body,html').animate({
                scrollTop: $("#maileditor").offset().top + 700
            }, 500);
            $(this).html('Re-generate Mail Template <i class="fa fa-refresh"></i>');
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-success');
            return false;
        }
    });
});