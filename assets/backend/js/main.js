////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function()
{


     function loadtemp(val) {
        if ( val == '1') {
            return '/apps/views/backend/mail/tmpl/template.mst';
        } else if( val == '2') {
            return '/apps/views/backend/mail/tmpl/template2.mst';
        }
    }

    function loadUser(name, temp) {
        $.get(loadtemp(temp), function(template) {
            var rendered = Mustache.render(template, {name: name});
            $('#msg').html(rendered);
        });
    }

    $('form#form').submit( function () {

        var name;
        name = $('input#name').val();
        loadUser(name, '1');

        return false;
    });


     if ($('input#close').prop("checked") === true ) {
          $('.closeMesg').show();
     }
     if ($('#redirect').prop("checked") === true ) {
          $('.redirect-url-block').show();
     }
     if ($('#block').prop("checked") === true ) {
          $('.redirect-auto-block').show();
     }
     $('input#close').on('change',function() {
          if ($(this).prop("checked") === false) {
               $('.closeMesg').slideUp(200);
               $('#closeMsg').val('');
          } else if($(this).prop("checked") === true) {
               $('.closeMesg').slideDown(200);
          }
     });
     $('#redirect').on('change',function() {
          if ($(this).prop("checked") === false) {
               $('.redirect-url-block').slideUp(200);
          } else if($(this).prop("checked") === true) {
               $('.redirect-url-block').slideDown(200);
          }
     });
     $('#block').on('change',function() {
          if ($(this).prop("checked") === false) {
               $('.redirect-auto-block').slideUp(200);
          } else if($(this).prop("checked") === true) {
               $('.redirect-auto-block').slideDown(200);
          }
     });
});