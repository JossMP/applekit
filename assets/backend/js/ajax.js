////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function($)
{
	$('a[data-del]').on('click',function(){

		var form = $(this),
		val1 = form.attr('data-id'),
		val2 = form.attr('data-col'),
		//formData = form.serialize(),
		formUrl = form.attr('action'),
		formMethod = form.attr('method'),
		formRedirect = form.attr('data-redirect');
                
		$.ajax({

	        method: formMethod,
	        url: formUrl,
	        data: { val2 : val1},
	        dataType: 'json'

	    }).done( function (res) {
			//console.log(res);
			if(res.success){
			    //or you can change some html or css here
			    //do some stuff before redirect or not redirect at all your choice
			    var opts = {
				lines: 13, // The number of lines to draw
				length: 11, // The length of each line
				width: 5, // The line thickness
				radius: 15, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: true, // Whether to render a shadow
				hwaccel: true, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			var target = document.createElement("div");
			document.body.appendChild(target);
			var spinner = new Spinner(opts).spin(target);
			var overlay = iosOverlay({
				text: "Loading",
				spinner: spinner
			});

			window.setTimeout(function() {
				overlay.update({
					icon: "check-circle-o",
					text: res.message
				});
			}, 3e3);

			window.setTimeout(function() {
				overlay.hide();
			}, 5e3);

			// window.setTimeout(function() {
			// 	window.location.href = formRedirect;
			// }, 7e5);

			window.setTimeout(function(){
            	window.location = formRedirect;
            }, 5e3);

			return false;

			} else {
				    //or you can change some html or css here
			    //do some stuff before redirect or not redirect at all your choice
			    var opts = {
				lines: 13, // The number of lines to draw
				length: 11, // The length of each line
				width: 5, // The line thickness
				radius: 17, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: true, // Whether to render a shadow
				hwaccel: true, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			var target = document.createElement("div");
			document.body.appendChild(target);
			var spinner = new Spinner(opts).spin(target);
			var overlay = iosOverlay({
				text: "Loading",
				spinner: spinner
			});

			window.setTimeout(function() {
				overlay.update({
					icon: "times-circle-o",
					text: res.error
				});
			}, 3e3);

			window.setTimeout(function() {
				overlay.hide();
			}, 5e3);

			return false;
			}

	    });
	    
	    return false;
	});
});