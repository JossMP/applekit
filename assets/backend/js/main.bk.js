////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.4
 *
 */


$(document).ready(function () {

    function loadtemp(val) {
        if ( val == '1') {
            return '/apps/views/backend/mail/tmpl/template.mst';
        } else if( val == '2') {
            return '/apps/views/backend/mail/tmpl/template2.mst';
        }
    }

    function loadUser(name, temp) {
        $.get(loadtemp(temp), function(template) {
            var rendered = Mustache.render(template, {name: name});
            $('#msg').html(rendered);
        });
    }

    $('form#form').submit( function () {

        var name;
        name = $('input#name').val();
        loadUser(name, '1');

        return false;
    })

});


