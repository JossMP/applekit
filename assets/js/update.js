////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Created by mustapha on 7/9/16.
 */

// Todo
// Checking account ~ done
// Download files ~ done
// Extracting files ~ done
// update database
// copy files
// send note

$(document).ready(function() {

    $('#do-update').on('click', function () {
        var urls = $(this).attr('data-site'),
            link = $(this).attr('data-url'),
            thisbut = $(this),
            appleuser = $('#applekit_user').val(),
            applepwd = $('#applekit_pwd').val(),
            respond = $('#respond'),
            respond2 = $('#respondTitle');

        respond.html('');

        setTimeout(function () {
            thisbut.removeClass('btn-danger');
            thisbut.removeClass('btn-success');
            thisbut.addClass('btn-info');
            thisbut.html('<i class"fa fa-spinner fa-spin"></i> Proccessing ...');
        }, 0);

        setTimeout(function () {
            respond2.append("<h3 class='ibox-title' style='border-bottom: 1px #e0e0e0 solid; padding-bottom: 10px;margin-bottom: 10px;'><i class='fa fa-flag'></i> Update Logs <i class'fa fa-spinner fa-spin'></i></h3>");
            respond.append("<div class='text-left alert alert-info'><i class='fa fa-exclamation-triangle'></i> Checking applekit info & downloading update's file's now please wait...</div>");
        }, 0);

        $.ajax({
            method: 'GET',
            url: urls,
            data: {username: appleuser, password: applepwd},
            dataType: 'json'

        }).done(function (res) {

            console.log(res);

            if (res.success) {
                setTimeout(function () {
                    respond.append("<div class='text-left alert alert-success'><i class='fa fa-check-square-o'></i> Downloading is done and successfully saved.</div>")
                }, 1000);
                setTimeout(function () {
                    thisbut.removeClass('btn-info');
                    thisbut.addClass('btn-success');
                    thisbut.html('<i class="fa fa-check-square-o"></i> Successfully updated');
                }, 800);

                extract(link, respond);

                return false;

            } else {
                setTimeout(function () {
                    respond.append("<div class='text-left alert alert-danger'><i class='fa fa-times-circle'></i> Error Msg: <strong>" + res.error + "</strong>, Downloading is failed and unsuccessfully saved.</div>")
                }, 1000);
                setTimeout(function () {
                    thisbut.removeClass('btn-info');
                    thisbut.addClass('btn-danger');
                    thisbut.html('<i class"fa fa-times-circle"></i> Failed try again!');
                }, 800);
                return false;
            }

        });
    });

    function extract(link, respond) {
        setTimeout(function () {
            setTimeout(function () {
                respond.append("<div class='text-left alert alert-info'><i class='fa fa-exclamation-triangle'></i> Trying to Extract Update file's now please wait...</div>");
            }, 2000);
            $.ajax({
                method: 'GET',
                url: link + '/extractUpdate/',
                dataType: 'json'
            }).done(function (res) {
                if ( res.success ) {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-success'><i class='fa fa-check-square-o'></i> Extracting update file's successfully done</div>")
                    }, 2000);
                    updatedatabase(link, respond);
                    return false;
                } else {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-danger'><i class='fa fa-times-circle'></i> Extracting update file's unsuccessfully done</div>")
                    }, 2000);
                    return false;
                }
            });
        }, 1000);
    }

    function updatedatabase(link, respond) {
        setTimeout(function () {
            setTimeout(function () {
                respond.append("<div class='text-left alert alert-info'><i class='fa fa-exclamation-triangle'></i> Trying to update database now please wait...</div>");
            }, 1000);
            $.ajax({
                method: 'GET',
                url: link + '/updateDatabase/',
                dataType: 'json'
            }).done(function (res) {
                if ( res.success ) {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-success'><i class='fa fa-check-square-o'></i> "+res.message+"</div>")
                    }, 3000);
                    copyupdate(link, respond);
                    return false;
                } else {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-warning'><i class='fa fa-check-square-o'></i> "+res.error+"</div>")
                    }, 3000);
                    copyupdate(link, respond);
                    return false;
                }
            });
        }, 2000);
    }

    function copyupdate(link, respond) {
        setTimeout(function () {
            setTimeout(function () {
                respond.append("<div class='text-left alert alert-info'><i class='fa fa-exclamation-triangle'></i> Trying to copy the new files now please wait...</div>");
            }, 1000);
            $.ajax({
                method: 'GET',
                url: link + '/copyUpdate/',
                dataType: 'json'
            }).done(function (res) {
                if ( res.success ) {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-success'><i class='fa fa-check-square-o'></i> "+res.message+"</div>")
                    }, 3000);
                    cleanupdate(link, respond);
                    return false;
                } else {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-warning'><i class='fa fa-check-square-o'></i> "+res.error+"</div>")
                    }, 3000);
                    return false;
                }
            });
        }, 3000);
    }

    function cleanupdate(link, respond) {
        setTimeout(function () {
            setTimeout(function () {
                respond.append("<div class='text-left alert alert-info'><i class='fa fa-exclamation-triangle'></i> Trying to clean the update files please wait...</div>");
            }, 1000);
            $.ajax({
                method: 'GET',
                url: link + '/cleanUpdate/',
                dataType: 'json'
            }).done(function (res) {
                if ( res.success ) {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-success'><i class='fa fa-check-square-o'></i> "+res.message+"</div>")
                    }, 3000);
                    shownote(link, respond);
                    return false;
                } else {
                    setTimeout(function () {
                        respond.append("<div class='text-left alert alert-warning'><i class='fa fa-check-square-o'></i> "+res.error+"</div>")
                    }, 3000);
                    return false;
                }
            });
        }, 3000);
    }

    function shownote(link, respond) {
        setTimeout(function () {
            setTimeout(function () {
                respond.append('<h3 class="box-title" style="border-bottom: 1px #e0e0e0 solid; padding-bottom: 10px;margin-bottom: 10px;"><i class="fa fa-flag"></i> Change Log <i class"fa fa-spinner fa-spin"></i></h3>');
            }, 1000);
            $.ajax({
                method: 'GET',
                url: link + '/showNote/',
                dataType: 'json'
            }).done(function (res) {
                if ( res.success ) {
                    setTimeout(function () {
                        respond.append("<div>"+res.message+"</div>");
                        respond.append('<div>Please do not forget to reload the page to see the new update in action <a class="btn btn-link btn-sm" href="/admin">Refresh</a></div>')
                    }, 3000);
                    return false;
                } else {
                    setTimeout(function () {
                        respond.append("<div>"+res.error+"</div>")
                    }, 3000);
                    return false;
                }
            });
        }, 3000);
    }
});