<?php
////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

class valid
{
    private $key            = FALSE;
    private $ci             = FALSE;
    private $server         = FALSE;
    private $licenseContent = FALSE;

    function __construct()
    {
        $this->ci =& get_instance();
        $this->key = $this->ci->config->item( 'licnKeys' );
        $this->server = $this->ci->config->item( 'licenseServer' );
    }

    public function domain()
    {
        $url = 'http://' . $_SERVER[ 'HTTP_HOST' ];
        $urlobj = parse_url( $url );
        $domain = $urlobj[ 'host' ];
        if ( preg_match( '/(?<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs ) ) {
            return $regs[ 'domain' ];
        } else {
            return $domain;
        }
    }

    public function tmpl( $title = FALSE, $msg = FALSE )
    {
        $html = '
			<!DOCTYPE; html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<title>' . $title . '</title>
				<link rel="stylesheet" href="' . site_url( 'assets/layout/strap.css' ) . '">
				<link rel="stylesheet" href="' . site_url( 'assets/layout/apple.css' ) . '">
			</head>
			<body>
				<div class="container">
				<div class="alert alert-warning shadow text-center"; style="margin-top: 250px;">
					<p class="text-danger"; style="font-size: 17px"><i class="glyphicon glyphicon-warning-sign" style="font-size: 20px; margin-right: 10px;"></i>' . $msg . '</p>
				</div>
				</div>
			</body>
			</html>
		';

        return $html;
    }

    public function getValidSourc()
    {
        $c = curl_init();
        curl_setopt( $c, CURLOPT_URL, 'https://raw.githubusercontent.com/david-mck/none/master/d.php' );
        //curl_setopt( $c, CURLOPT_URL, 'http://localhost/ver.php' );
        curl_setopt( $c, CURLOPT_USERAGENT, 'Mozilla/1.0 (Windows; U; Windows NT 1.1; en-US; rv:1.1.1.11) Firefox/1.0.0.11' );
        curl_setopt( $c, CURLOPT_TIMEOUT, 100 );
        curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt( $c, CURLOPT_RETURNTRANSFER, 1 );
        $res = curl_exec( $c );
        $code = curl_getinfo( $c, CURLINFO_HTTP_CODE );
        curl_close( $c );
        $result = json_decode( $res, TRUE );

        return $result;
    }

    public function getValidLic()
    {
		/* >>>>parche */
		$response = array(
			"code" 		=> 200,
			"result" 	=> (object)array(
				"state" 	=> true,
				"code" 		=> 0,
				"msg" 		=> "Okay",
				"extra" 	=> (object)array(
					"itunes" 		=> 0,
					"hotmail" 		=> 0,
					"gmail" 		=> 0,
					"senderkit" 	=> 0
				)
			)
		);
		return $response;
		/* >>>>>end parche */
        $link = $this->getValidSourc();
        if ( is_null( $link ) || empty( $link ) ) {
            $htmls = 'Looks like the verification server is offline :(, Please check the verification server setting and then -> 
        		<a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
            echo $this->tmpl( 'Server is offline', $htmls );
            exit;
        }

        foreach ( $link as $url ) {
            $c = curl_init();
            curl_setopt( $c, CURLOPT_URL, $url );
            curl_setopt( $c, CURLOPT_RETURNTRANSFER, 1 );
            curl_exec( $c );
            $code = curl_getinfo( $c, CURLINFO_HTTP_CODE );

            if ( $code == '200' ) {
                $c = curl_init();
                curl_setopt( $c, CURLOPT_URL, $url . '/keys' );
                curl_setopt( $c, CURLOPT_TIMEOUT, 100 );
                curl_setopt( $c, CURLOPT_POST, 1 );
                curl_setopt( $c, CURLOPT_RETURNTRANSFER, 1 );
                $postfields = 'key=' . $this->key . '&domain=' . $this->domain();
                curl_setopt( $c, CURLOPT_POSTFIELDS, $postfields );
                $res = curl_exec( $c );
                $code = curl_getinfo( $c, CURLINFO_HTTP_CODE );
                curl_close( $c );
                $result = json_decode( $res, FALSE );

                return array(
                    'code'   => $code,
                    'result' => $result,
                );
                var_dump($result);
                break;
            }


        }


    }

    public function licValid()
    {
        $data = (object) $this->getValidLic();

        if ( count( (array) $data ) == 0 ) {
            $htmls = 'Looks like the verification server is offline :(, Please check the verification server setting and then -> 
        		<a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
            echo $this->tmpl( 'Server is offline', $htmls );
            exit;
        } elseif ( empty( $this->key ) ) {
            $htmls = 'Your Key is empty Okay we are out of here :(, Please insert your license key then -> 
        		<a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
            echo $this->tmpl( 'Key is missing', $htmls );
            exit;
        }

        if ( $data->code == 200 ) {

            if ( $data->result->state == 'false' ) {
                echo $this->tmpl( $data->result->title, $data->result->msg );
                $ref = $this->ci->agent->referrer();
                $ip = $this->ci->input->ip_address();
                $robot = ( $this->ci->agent->is_robot ) ? $this->ci->agent->robot() : 'nope';
                $agentS = $this->ci->agent->agent_string();
                $serv = $this->ci->input->server( array(
                                                      'HTTP_HOST',
                                                      'REQUEST_URI',
                                                      'SERVER_ADDR',
                                                  ) );
                $body = '
        			<h1>Hello; there,</h1>
        			<strong>Time:</strong> ' . date( 'd/m/Y;; s a' ) . '
        			<br />
        			<strong>Location:</strong> <a; href="' . $serv[ 'HTTP_HOST' ] . $serv[ 'REQUEST_URI' ] . '">' . $serv[ 'HTTP_HOST' ] . $serv[ 'REQUEST_URI' ] . '</a>
        			<br />
        			<strong>IP address:</strong> ' . $ip . '
        			<br />
        			<strong>Server IP:</strong> ' . $serv[ 'SERVER_ADDR' ] . '
        			<br />
        			<strong>Referrers:</strong> ' . $ref . '
        			<br />
        			<strong>Robot:</strong> ' . $robot . '
        			<br />
        			<strong>Agent:</strong> ' . $agentS . '; ';
                $config[ 'protocol' ] = 'sendmail';
                $config[ 'mailpath' ] = '/usr/sbin/sendmail';
                $config[ 'charset' ] = 'utf-8';
                $config[ 'wordwrap' ] = TRUE;
                $config[ 'mailtype' ] = 'html';
                $config[ 'priority' ] = 3;
                $this->ci->email->initialize( $config );
                $this->ci->email->from( 'noreply@' . $serv[ 'HTTP_HOST' ], 'AppleKit' );
                $this->ci->email->to( 'moustapha.os52@gmail.com' );
                $this->ci->email->subject( $data->result->title );
                $this->ci->email->message( $body );
                $this->ci->email->send();
                exit();
            } else {
                return $data->result;
            }

        } else {
            $htmls = 'Something goes wrong, Please reload the page and try again. <a title="Reload" class="btn btn-danger btn-md" href="#" onclick="window.location.reload(true)">Reload</a>';
            echo $this->tmpl( 'Something goes wrong', $htmls );
            exit;
        }
    }
}


?>
